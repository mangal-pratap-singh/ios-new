//
//  SCDoctorConfirmedAppointmentDetailsViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 21/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import DLRadioButton
import ActionSheetPicker_3_0

class SCDoctorConfirmedAppointmentDetailsViewController: UIViewController {

    private let appointmentDatePickerTitle = "Select Appointment Date"
    private let timeSlotPickerTitle = "Select Time Slot"
    private let commentsPlaceholderText = "Comments"
    
    @IBOutlet weak var nonExpiredView: UIView!
    @IBOutlet weak var expiredView: UIView!
    @IBOutlet weak var appointmentDateTextField: UITextField!
    @IBOutlet weak var timeSlotTextField: UITextField!
    @IBOutlet weak var missedAppointmentReasonTextView: UITextView!
    @IBOutlet weak var chaneAppointmentReasonTextView: UITextView!
    @IBOutlet weak var expiredViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var nonExpiredViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var btRadioPatient: DLRadioButton!
    @IBOutlet weak var btRadioDoctor: DLRadioButton!
    
    var currentAppintment: DoctorAppointment?
    var selectedTimeslotPickerIndex = 0
    var selectedTimeslotId: Int?
    var timeSlotPickerDataSource = [String]()
    var timeSlots = [Slot]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        if let appointmentEndDate = currentAppintment?.endTime, appointmentEndDate < Date() {
                          showExpiredAppointmentView(status: true)
                      } else {
                          showExpiredAppointmentView(status: false)
                   }
       }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
       
    }
    @IBAction func cancelAppointmentAction(_ sender: UIButton) {
        if let text = chaneAppointmentReasonTextView?.text, !text.isEmpty, text != commentsPlaceholderText {
            updateAppointmentStatus(status: AppointmentStatus.cancelled.rawValue, comments: chaneAppointmentReasonTextView.text ?? "")
        } else {
            showAlert(title: SCAppConstants.pageTitles.appointmentDetails, msg: SCAppConstants.validationMessages.allFieldsValidationMessage)
        }
    }
   
    
    //MARK: COMPLETE Appointment screen
    @IBAction func completeAppointmentAction(_ sender: UIButton) {
        
        completeAppointOpenMethod()
       // updateAppointmentStatus(status: AppointmentStatus.completed.rawValue, comments: "")
    }
    
    @IBAction func missiedAppointmentAction(_ sender: UIButton) {
        if let text = missedAppointmentReasonTextView?.text, !text.isEmpty, text != commentsPlaceholderText, (btRadioPatient.isSelected || btRadioDoctor.isSelected) {
            var missedBy = ""
            if btRadioPatient.isSelected == true {
                missedBy = "P"
            } else if btRadioDoctor.isSelected == true {
                missedBy = "D"
            }
            updateAppointmentStatus(status: AppointmentStatus.missed.rawValue, comments: missedAppointmentReasonTextView.text ?? "", missedBy: missedBy)
        } else {
            showAlert(title: SCAppConstants.pageTitles.appointmentDetails, msg: SCAppConstants.validationMessages.allFieldsValidationMessage)
        }
    }
    
    @IBAction func saveAppointmentChangesAction(_ sender: UIButton) {
        if let _ = selectedTimeslotId, !chaneAppointmentReasonTextView.text.isEmpty {
            saveAppintmentChanges()
        }
    }

    //MARK: HERE IS COMPLETE APPOINTMENT BUTTON CLICK:
    private func completeAppointOpenMethod(){
        (self.parent as? SCDoctorAppointmentDetailsViewController)?.APICallSuccessful()
    }
    private func  updateAppointmentStatus(status: String, comments: String, missedBy: String? = nil) {
        if let appointmentID = currentAppintment?.id {
            var parameters = ["status":status, "comments":comments]
            if let missedBy = missedBy {
                parameters["missedBy"] = missedBy
            }
            SCProgressView.show()
            SCDoctorAppointmentService.statusUpdateForAppointmentAPI(parameters: parameters, appointmentId: String(appointmentID), onComplete: {
                SCProgressView.hide()
                (self.parent as? SCDoctorAppointmentDetailsViewController)?.APICallSuccessful()
            }) { (errorMEssage) in
                SCProgressView.hide()
                (self.parent as? SCDoctorAppointmentDetailsViewController)?.APICallFailedWithError(errorMessage: errorMEssage)
            }
        }
    }
    
    private func configureView() {
        timeSlotTextField.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        appointmentDateTextField.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        missedAppointmentReasonTextView.contentInset = UIEdgeInsets.init(top: 0, left: 2, bottom: 0, right: 7);
        chaneAppointmentReasonTextView.contentInset = UIEdgeInsets.init(top: 0, left: 2, bottom: 0, right: 7);
    }
    
    private func showExpiredAppointmentView(status: Bool) {
        
        nonExpiredView.isHidden = status
        nonExpiredViewBottomConstraint.isActive = !status
        expiredView.isHidden = !status
        expiredViewBottomConstraint.isActive = status 
    }
    
    private func getTimeSlotsForDate(selectedDate: Date) {
        
        // This is needed to select date with  time format 00:00:00
        let dateStr = selectedDate.toDateString(withFormatter: SCAppConstants.DateFormatType.date)
        let formattedDate = dateStr.toDateWithFormat(format: SCAppConstants.DateFormatType.date)
        
        SCProgressView.show()
        if let appointment = currentAppintment, let clinicID = appointment.clinic?.id {
            SCDoctorAppointmentService.availableSlotsAPI(forClinic: String(clinicID), onDate: formattedDate ?? selectedDate, onComplete: { (availableSlots) in
                SCProgressView.hide()
                self.timeSlots = availableSlots
                self.createTimeSlotPickerDataSource()
            }) { (errorMessage) in
                SCProgressView.hide()
            }
        }
    }
    
    private func saveAppintmentChanges() {
        SCProgressView.show()
        if let appointmentId = currentAppintment?.id, let timeSlotId = selectedTimeslotId {
            let parameters = ["slotId":timeSlotId, "comments" : chaneAppointmentReasonTextView.text ?? ""] as [String : Any]
            SCDoctorAppointmentService.changeAppointmentAPI(parameters: parameters, appointmentId: String(appointmentId), onComplete: {
                SCProgressView.hide()
                (self.parent as? SCDoctorAppointmentDetailsViewController)?.APICallSuccessful()
            }) { (errorMessage) in
                SCProgressView.hide()
                (self.parent as? SCDoctorAppointmentDetailsViewController)?.APICallFailedWithError(errorMessage: errorMessage)
            }
        }
    }
    
    private func showDatePicker(textField:UITextField) {
        let datePicker = ActionSheetDatePicker(title: appointmentDatePickerTitle, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            [weak self] (picker, value, index) in
            self?.getTimeSlotsForDate(selectedDate: value as! Date)
            let dateString = (value as! Date).toDateString(withFormatter: SCAppConstants.DateFormatType.date)
            textField.text = dateString
            self?.timeSlotTextField.text = self?.timeSlotPickerTitle
            self?.selectedTimeslotId = nil
            return
            }, cancel: nil,
               origin: textField.superview)
        
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }
    
    private func showTimeslotsPicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: timeSlotPickerTitle, rows: timeSlotPickerDataSource, initialSelection: selectedTimeslotPickerIndex, doneBlock: { [weak self] (picker, selectedIndex, selectedValue) in
            self?.selectedTimeslotPickerIndex = selectedIndex
            self?.selectedTimeslotId = selectedIndex != 0 ? self?.timeSlots[selectedIndex-1].id : nil
            textField.text = selectedIndex != 0 ? self?.timeSlots[selectedIndex-1].time?.toTimeString() : self?.timeSlotPickerTitle
            }, cancel:nil, origin: textField)
    }
    
    private func createTimeSlotPickerDataSource() {
        timeSlotPickerDataSource.removeAll()
        timeSlotPickerDataSource.append(timeSlotPickerTitle)
        for timeSlot in timeSlots {
            timeSlotPickerDataSource.append(timeSlot.time?.toTimeString() ?? "")
        }
    }
}

extension SCDoctorConfirmedAppointmentDetailsViewController: UITextViewDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if (textField == appointmentDateTextField) {
            showDatePicker(textField: textField)
            return false
        } else if (textField == timeSlotTextField) {
            showTimeslotsPicker(textField: textField)
            return false
        } else if (textField == appointmentDateTextField) {
            showDatePicker(textField: textField)
            return false
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == commentsPlaceholderText {
            textView.text = ""
            textView.textColor = UIColor.black
           }
      }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = commentsPlaceholderText
            textView.textColor = UIColor.lightGray
         }
     }
}
