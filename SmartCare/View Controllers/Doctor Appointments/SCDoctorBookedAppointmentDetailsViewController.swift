//
//  SCDoctorBookedAppointmentDetailsViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 21/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCDoctorBookedAppointmentDetailsViewController: UIViewController {

    private let commentsPlaceholderText = "Comments"
    
    @IBOutlet weak var cancelAppointmentView: UIView!
    @IBOutlet weak var cancelAppointmentViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var commentsTextView: UITextView!
    
    var currentAppintment: DoctorAppointment?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let appointmentEndDate = currentAppintment?.endTime, appointmentEndDate < Date() {
            cancelAppointmentView.isHidden = true
            cancelAppointmentViewBottomConstraint.isActive = false
        } else {
            cancelAppointmentView.isHidden = false
            cancelAppointmentViewBottomConstraint.isActive = true
        }
    }
    
    
    @IBAction func cancelAppointmentAction(_ sender: UIButton) {
        
        
        
        if let text = commentsTextView?.text, !text.isEmpty, text != commentsPlaceholderText {
            updateAppointmentStatus(status: AppointmentStatus.cancelled.rawValue, comments: text)
        } else {
            showAlert(title: SCAppConstants.pageTitles.appointmentDetails, msg: SCAppConstants.validationMessages.allFieldsValidationMessage)
        }
    }
    
    private func updateAppointmentStatus(status: String, comments: String) {
        
        if let appointmentID = currentAppintment?.id {
            let parameters = ["status":status, "comments":commentsTextView.text ?? ""]
            SCProgressView.show()
            SCDoctorAppointmentService.statusUpdateForAppointmentAPI(parameters: parameters, appointmentId: String(appointmentID), onComplete: {
                SCProgressView.hide()
                (self.parent as? SCDoctorAppointmentDetailsViewController)?.APICallSuccessful()
                
            }) { (errorMEssage) in
                SCProgressView.hide()
                (self.parent as? SCDoctorAppointmentDetailsViewController)?.APICallFailedWithError(errorMessage: errorMEssage)
            }
        }
    }
}

extension SCDoctorBookedAppointmentDetailsViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == commentsPlaceholderText {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = commentsPlaceholderText
            textView.textColor = UIColor.lightGray
        }
    }
}
