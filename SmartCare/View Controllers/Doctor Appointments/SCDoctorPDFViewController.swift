//
//  SCDoctorPDFViewController.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 24/04/20.
//  Copyright © 2020 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import PDFKit
class SCDoctorPDFViewController: UIViewController {
    
    @IBOutlet weak var view_pdfView: UIView!
    var pdfUrl: String? = ""
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
         customizeNavigationItem(title: "View Consultant Note", isDetail: true)
        var pdfView = PDFView()
        pdfView.frame = view_pdfView.frame
        view_pdfView.addSubview(pdfView)
        guard let path = pdfUrl else { return }

         guard let url = URL(string: "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf")  else { return }
        if let document = PDFDocument(url: url) {
            pdfView.document = document
        }

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
