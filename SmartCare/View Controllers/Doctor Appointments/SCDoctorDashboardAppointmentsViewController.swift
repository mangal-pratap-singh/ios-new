import UIKit

class SCDoctorDashboardAppointmentsViewController: UIViewController {

    @IBOutlet weak var appointmentsListTable: UITableView!
    var appointments = [DoctorAppointment]()
    var fullScreenMessageView:FullScreenMessageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        registerNibsForCells()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getAppointmentList()
    }
    
    private func configureView() {
        appointmentsListTable.tableFooterView = UIView()
    }
    
    private func registerNibsForCells() {
        let nibName = UINib(nibName: SCAppConstants.nibNames.doctorAppointmentTableViewCell, bundle:nil)
        appointmentsListTable.register(nibName, forCellReuseIdentifier: SCAppConstants.cellReuseIdentifiers.doctorAppointmentListItem)
    }
    
    private func getAppointmentList() {
        SCProgressView.show()
        SCAppointmentService.doctorUpcomingappointmentsListAPI(onComplete: { [weak self] (appointmentList) in
            SCProgressView.hide()
            self?.fullScreenMessageView?.removeFromSuperview()
            if appointmentList.count == 0 {
                self?.fullScreenMessageView = self?.showFullScreenMessageOnView(parentView: self!.appointmentsListTable, withMessage: "No appointments for selected period")
                self?.view.addSubview(self!.fullScreenMessageView!)
            }
            self?.appointments = appointmentList
            self?.appointmentsListTable.reloadData()
        }) {[weak self] (errorMessage) in
            SCProgressView.hide()
            self?.fullScreenMessageView = self?.showFullScreenMessageOnView(parentView: self!.appointmentsListTable, withMessage: "No appointments for selected period")
            self?.view.addSubview(self!.fullScreenMessageView!)
        }
    }
}

extension SCDoctorDashboardAppointmentsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appointments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SCAppConstants.cellReuseIdentifiers.doctorAppointmentListItem, for: indexPath as IndexPath) as! DoctorAppointmentTableViewCell
        cell.selectionStyle = .none
        let currentAppointment = appointments[indexPath.row]
        cell.configureCell(WithAppointment: currentAppointment)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentAppointment = appointments[indexPath.row]
        navigateToAppointmentDetail(for: currentAppointment)
    }
    
    private func navigateToAppointmentDetail(for appointment: DoctorAppointment) {
        let appointmentDetailsVC = SCDoctorAppointmentDetailsViewController.initialize(withDoctorAppointment: appointment)
        navigationController?.pushViewController(appointmentDetailsVC, animated: true)
    }
}
