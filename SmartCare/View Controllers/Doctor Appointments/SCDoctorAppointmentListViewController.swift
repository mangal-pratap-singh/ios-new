//
//  SCDoctorAppointmentsViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 18/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class SCDoctorAppointmentListViewController: UIViewController {

    private let noOfSecondForWeak = 604799
    private let weakDisplayDateFormat = "MMM dd, yyyy"
    private let statusPickerTitle = "Select Status"
    
    @IBOutlet weak var appointmemntsTable: UITableView!
    @IBOutlet weak var weakDateLabel: UILabel!
    @IBOutlet weak var statusTextField: UITextField!
    
    var doctorAppointmentList = [DoctorAppointment]()
    var fullScreenMessageView:FullScreenMessageView?
    var displayDate = Date()
    var selectedPickerIndex = 0
    var pickerDataSource = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.appointments, isDetail: false)
        configureView()
        registerNibsForCells()
        showWeakDateStringFromDate()
        createPickerDataSource()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         fullScreenMessageView?.center = self.view.center
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getAppointmentsList()
    }
    
    @IBAction func nextButtonAction(_ sender: UIButton) {
        let nextDate = Calendar.current.date(byAdding: .second, value: noOfSecondForWeak, to: displayDate)!
        displayDate = nextDate
        showWeakDateStringFromDate()
        getAppointmentsList()
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        let previousDate = Calendar.current.date(byAdding: .second, value: -noOfSecondForWeak, to: displayDate)!
        displayDate = previousDate
        showWeakDateStringFromDate()
        getAppointmentsList()
    }
    
    private func showWeakDateStringFromDate() {
        let startWeakDate = (displayDate.startOfWeek).toDateString(withFormatter: weakDisplayDateFormat)
        let endWeakDate = (displayDate.endOfWeek).toDateString(withFormatter: weakDisplayDateFormat)
        weakDateLabel.text = "\(startWeakDate) - \(endWeakDate) "
    }
    
    private func configureView() {
        appointmemntsTable.tableFooterView = UIView()
    }
    
    private func registerNibsForCells() {
        let nibName = UINib(nibName: SCAppConstants.nibNames.doctorAppointmentTableViewCell, bundle:nil)
        appointmemntsTable.register(nibName, forCellReuseIdentifier: SCAppConstants.cellReuseIdentifiers.doctorAppointmentListItem)
    }
    
    private func createPickerDataSource() {
        pickerDataSource.append(statusPickerTitle)
        for appointment in AppointmentStatus.toArray() {
            pickerDataSource.append(appointment.statusText())
        }
    }
    
    private func showStatusPicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: statusPickerTitle, rows: pickerDataSource, initialSelection: selectedPickerIndex, doneBlock: { picker, selectedIndex, selectedValue in
            self.selectedPickerIndex = selectedIndex
            textField.text = selectedIndex == 0 ? self.statusPickerTitle : AppointmentStatus.toArray()[selectedIndex-1].statusText()
            self.getAppointmentsList()
        }, cancel: nil, origin: textField)
    }
    
    private func getAppointmentsList() {
        SCProgressView.show()
        let selectedStatus = selectedPickerIndex == 0 ? nil : AppointmentStatus.toArray()[selectedPickerIndex-1]
        SCDoctorAppointmentService.doctorAppointmentsListAPI(withStatus: selectedStatus, apoointmentDate:displayDate.startOfWeek, onComplete: { [weak self] (appointments) in
            SCProgressView.hide()
            self?.fullScreenMessageView?.removeFromSuperview()
            if appointments.count == 0 {
                self?.fullScreenMessageView = self?.showFullScreenMessageOnView(parentView: self!.appointmemntsTable, withMessage: "No appointments for selected period")
                self?.view.addSubview(self!.fullScreenMessageView!)
                //self?.fullScreenMessageView?.center = (self?.view.center)!
            }
            self?.doctorAppointmentList = appointments
            self?.appointmemntsTable.reloadData()
        }) { (errorMessage) in
            SCProgressView.hide()
        }
    }
}

extension SCDoctorAppointmentListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return doctorAppointmentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SCAppConstants.cellReuseIdentifiers.doctorAppointmentListItem, for: indexPath as IndexPath) as! DoctorAppointmentTableViewCell
        cell.selectionStyle = .none
        let currentAppointment = doctorAppointmentList[indexPath.row]
        cell.configureCell(WithAppointment: currentAppointment)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentAppointment = doctorAppointmentList[indexPath.row]
        loadDetailScreenForDoctorAppointment(appointment: currentAppointment)
    }
    
    private func loadDetailScreenForDoctorAppointment(appointment: DoctorAppointment) {
        let appointmentDetailsVC = SCDoctorAppointmentDetailsViewController.initialize(withDoctorAppointment: appointment)
        self.navigationController?.pushViewController(appointmentDetailsVC, animated: true)
    }
}

extension SCDoctorAppointmentListViewController {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if (textField == statusTextField) {
            showStatusPicker(textField: textField)
            return false
        }
        return true
    }
}
