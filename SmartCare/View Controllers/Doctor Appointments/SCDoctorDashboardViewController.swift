import RxSwift
import RxCocoa
import Charts
import ActionSheetPicker_3_0

struct SCDoctorDashboardViewModel {
    
    private let _dailyStatus = PublishSubject<DailyStatus>()
    private let _dailyChart = PublishSubject<DailyChart>()
    private let _monthwiseStats = PublishSubject<MonthwiseStats>()
    private let _yearwiseStats = PublishSubject<YearwiseStats>()
    private let _selectedMonth = BehaviorRelay<Int>(value: Int(Date().toDateString(withFormatter: DateFormat.currentMonth)) ?? 0)
    private let _selectedYearForMonthWiseChart = BehaviorRelay<Int>(value: Int(Date().toDateString(withFormatter: DateFormat.currentYear)) ?? 2019)
    private let _selectedYearForYearWiseChart = BehaviorRelay<Int>(value: Int(Date().toDateString(withFormatter: DateFormat.currentYear)) ?? 2019)
    
    lazy var dailyChartCompletedColor: NSUIColor = {
        return NSUIColor(red: 4/255, green: 169/255, blue: 244/255, alpha: 1)
    }()
    lazy var dailyChartCancelledColor: NSUIColor = {
        return NSUIColor(red: 251/255, green: 192/255, blue: 46/255, alpha: 1)
    }()
    lazy var dailyChartBookedColor: NSUIColor = {
        return NSUIColor(red: 133/255, green: 72/255, blue: 54/255, alpha: 1)
    }()
    
    var dailyStatus: Driver<DailyStatus> {
        return _dailyStatus.asDriver(onErrorJustReturn: DailyStatus())
    }
    var dailyChart: Driver<DailyChart> {
        return _dailyChart.asDriver(onErrorJustReturn: DailyChart())
    }
    var monthwiseStats: Driver<MonthwiseStats> {
        return _monthwiseStats.asDriver(onErrorJustReturn: MonthwiseStats())
    }
    var yearwiseStats: Driver<YearwiseStats> {
        return _yearwiseStats.asDriver(onErrorJustReturn: YearwiseStats())
    }
    let yearPickerTitle = "Select Year"
    var yearList: ([String], Int) {
        let currentYear = Calendar.current.component(.year, from: Date())
        let years = (1979...currentYear).map { String($0) }
        let selectedYearIndex = years.firstIndex(of: String(_selectedYearForMonthWiseChart.value)) ?? 0
        return (years, selectedYearIndex)
    }
    let monthPickerTitle = "Select Month"
    var monthList: ([String], Int) {
        let months = Calendar.current.shortMonthSymbols
        return (months, _selectedMonth.value - 1)
    }
    
    func xValuesForMonthlyChart() -> [Double] {
        if let date = Calendar.current.date(from: DateComponents(year: _selectedYearForMonthWiseChart.value, month: _selectedMonth.value)) {
            let range = Calendar.current.range(of: .day, in: .month, for: date)!
            return (1...range.count).map({ Double($0) })
        }
        return [Double]()
    }
    
    private let bag = DisposeBag()
    
    func setCurrentMonth(with index: Int) {
        _selectedMonth.accept(index)
    }
    
    func setCurrentYearForMonthlyChart(with year: Int) {
        _selectedYearForMonthWiseChart.accept(year)
    }
    
    func setCurrentYearForYearlyChart(with year: Int) {
        _selectedYearForYearWiseChart.accept(year)
    }
    
    func getDashboardStatus() {
        getDailyStatus().bind(to: _dailyStatus).disposed(by: bag)
        getDailyChart().bind(to: _dailyChart).disposed(by: bag)
        _selectedMonth.subscribe(onNext: { _ in
            self.getMonthwiseStats().bind(to: self._monthwiseStats).disposed(by: self.bag)
        }).disposed(by: bag)
        _selectedYearForMonthWiseChart.subscribe(onNext: { _ in
            self.getMonthwiseStats().bind(to: self._monthwiseStats).disposed(by: self.bag)
        }).disposed(by: bag)
        
        _selectedYearForYearWiseChart.subscribe(onNext: { _ in
            self.getYearwiseStats().bind(to: self._yearwiseStats).disposed(by: self.bag)
        }).disposed(by: bag)
        
    }
    
    private func getDailyStatus() -> Observable<DailyStatus> {
        return Observable<DailyStatus>.create { observer -> Disposable in
            SCDashboardService.doctorDashboardStatus(onComplete: { dailyStatus in
                observer.onNext(dailyStatus)
            }) {(errorMessage) in
                observer.onError(SCError(error: .serverError, localizedDescription: errorMessage))
            }
            return Disposables.create()
         }
    }
    
    private func getDailyChart() -> Observable<DailyChart> {
        return Observable<DailyChart>.create { observer -> Disposable in
            SCDashboardService.doctorDashboardDailyChart(onComplete: {
                observer.onNext($0)
            }) {(errorMessage) in
                observer.onError(SCError(error: .serverError, localizedDescription: errorMessage))
            }
            return Disposables.create()
        }
    }
    
    private func getMonthwiseStats() -> Observable<MonthwiseStats> {
        return Observable<MonthwiseStats>.create { observer -> Disposable in
            let components = DateComponents(year: self._selectedYearForMonthWiseChart.value, month: self._selectedMonth.value, day: 2)
            SCDashboardService.doctorDashboardDayStats(for: Calendar.current.date(from: components) ?? Date(), onComplete: {
                observer.onNext($0)
            }) {(errorMessage) in
                observer.onError(SCError(error: .serverError, localizedDescription: errorMessage))
            }
            return Disposables.create()
        }
    }
    
    private func getYearwiseStats() -> Observable<YearwiseStats> {
        return Observable<YearwiseStats>.create { observer -> Disposable in
            let components = DateComponents(year: self._selectedYearForYearWiseChart.value)
            SCDashboardService.doctorDashboardMonthStats(for: Calendar.current.date(from: components) ?? Date(), onComplete: {
                observer.onNext($0)
            }) {(errorMessage) in
                observer.onError(SCError(error: .serverError, localizedDescription: errorMessage))
            }
            return Disposables.create()
        }
    }
}

class SCDoctorDashboardViewController: UIViewController {
 
    @IBOutlet weak var todayAppointmentsLabel: UILabel!
    @IBOutlet weak var upcomingAppointmentsLabel: UILabel!
    @IBOutlet weak var overallAppointmentsLabel: UILabel!
    @IBOutlet weak var satisfiedAppointmentsLabel: UILabel!
    @IBOutlet weak var dailyChartDateLabel: UILabel!
    @IBOutlet weak var dailyChart: PieChartView!
    @IBOutlet weak var dailyChartHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var monthlyStatsMonthPicker: UITextField!
    @IBOutlet weak var monthlyStatsYearPicker: UITextField!
    @IBOutlet weak var yearlyStatsYearPicker: UITextField!
    @IBOutlet weak var monthlyStatsChart: BarChartView!
    @IBOutlet weak var yearlyStatsChart: BarChartView!
    
    
    private var viewModel: SCDoctorDashboardViewModel!
    private let bag = DisposeBag()
    
    private enum ChartType {
        case Month
        case Year
    }
    
    static func initialize(with viewModel: SCDoctorDashboardViewModel) -> SCDoctorDashboardViewController {
        let storyboard = UIStoryboard(name: "DoctorHome", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.doctorDashboard) as! SCDoctorDashboardViewController
        vc.viewModel = viewModel
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureDailyStatusView()
        configureDailyChartView()
        configureMonthlyStatusView()
        configureYearlyStatusView()
    }
    
    private func configureDailyStatusView() {
        viewModel.dailyStatus.map({ "\($0.todayAppointment)" }).drive(todayAppointmentsLabel.rx.text).disposed(by: bag)
        viewModel.dailyStatus.map({ "\($0.upcomingAppointment)" }).drive(upcomingAppointmentsLabel.rx.text).disposed(by: bag)
        viewModel.dailyStatus.map({ "\($0.overallAppointment)" }).drive(overallAppointmentsLabel.rx.text).disposed(by: bag)
        viewModel.dailyStatus.map({ "\($0.satisfiedPatient)" }).drive(satisfiedAppointmentsLabel.rx.text).disposed(by: bag)
    }
    
    private func configureMonthlyStatusView() {
        monthlyStatsMonthPicker.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        monthlyStatsYearPicker.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        monthlyStatsMonthPicker.delegate = self
        monthlyStatsYearPicker.delegate = self
        monthlyStatsMonthPicker.text = viewModel.monthList.0[viewModel.monthList.1]
        monthlyStatsYearPicker.text = viewModel.yearList.0[viewModel.yearList.1]
        
        monthlyStatsChart.pinchZoomEnabled = false
        monthlyStatsChart.drawBarShadowEnabled = false
        monthlyStatsChart.drawValueAboveBarEnabled = false
        monthlyStatsChart.maxVisibleCount = 31
        monthlyStatsChart.fitBars = true
        monthlyStatsChart.drawGridBackgroundEnabled = false
        
        monthlyStatsChart.xAxis.labelPosition = .bottom
        monthlyStatsChart.xAxis.granularity = 1
        monthlyStatsChart.xAxis.labelCount = 25
        monthlyStatsChart.xAxis.forceLabelsEnabled = true
        
        monthlyStatsChart.leftAxis.granularity = 1
        monthlyStatsChart.leftAxis.axisMinimum = 0
        
        monthlyStatsChart.legend.horizontalAlignment = .right
        monthlyStatsChart.legend.verticalAlignment = .top
        monthlyStatsChart.legend.orientation = .horizontal
        monthlyStatsChart.legend.drawInside = false
        
        monthlyStatsChart.rightAxis.enabled = false
        
        viewModel.monthwiseStats.drive(onNext: { [weak self] stat in
            if let `self` = self {
                let xValues = self.viewModel.xValuesForMonthlyChart()

                self.monthlyStatsChart.xAxis.entries = xValues
                self.monthlyStatsChart.xAxis.axisMinimum = 1
                self.monthlyStatsChart.xAxis.axisMaximum = xValues.last ?? 31
                
                let entries = stat.days.map({ s in
                    BarChartDataEntry(x: Double(s.unit) ?? 1, yValues: [Double(s.totalPatient), Double(s.reviewedPatient), Double(s.satisfiedPatient)])
                })
                
                let set = BarChartDataSet(entries: entries, label: "")
                set.setColor(UIColor(red: 104/255, green: 241/255, blue: 175/255, alpha: 1))
                set.stackLabels = ["Total Patients", "Reviewed Patients", "Statisfied Patients"]
                set.colors = [ChartColorTemplates.material()[0], ChartColorTemplates.material()[1], ChartColorTemplates.material()[2]]
                set.valueFormatter = DefaultValueFormatter(block: { (value, entry, index, _) -> String in
                    if value == 0 {
                        return ""
                    } else {
                        return String(format: "%.0f", value)
                    }
                    
                })
                
                let data = BarChartData(dataSet: set)
                self.monthlyStatsChart.data = data
            }
        }).disposed(by: bag)
    }
    
    private func configureYearlyStatusView() {
        yearlyStatsYearPicker.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        yearlyStatsYearPicker.delegate = self
        yearlyStatsYearPicker.text = viewModel.yearList.0[viewModel.yearList.1]
        
        yearlyStatsChart.pinchZoomEnabled = false
        yearlyStatsChart.drawBarShadowEnabled = false
        yearlyStatsChart.drawValueAboveBarEnabled = false
        yearlyStatsChart.maxVisibleCount = 12
        yearlyStatsChart.fitBars = true
        yearlyStatsChart.drawGridBackgroundEnabled = false
        yearlyStatsChart.xAxis.labelPosition = .bottom
        yearlyStatsChart.xAxis.granularity = 1
        yearlyStatsChart.xAxis.labelCount = 12
        yearlyStatsChart.xAxis.forceLabelsEnabled = true
        yearlyStatsChart.leftAxis.granularity = 1
        yearlyStatsChart.leftAxis.axisMinimum = 0
        yearlyStatsChart.legend.horizontalAlignment = .right
        yearlyStatsChart.legend.verticalAlignment = .top
        yearlyStatsChart.legend.orientation = .horizontal
        yearlyStatsChart.legend.drawInside = false
        yearlyStatsChart.rightAxis.enabled = false
        
        viewModel.yearwiseStats.drive(onNext: { [weak self] stat in
            if let `self` = self {
                self.yearlyStatsChart.xAxis.valueFormatter = DefaultAxisValueFormatter(block: { (value, _) -> String in
                    let monthlist = self.viewModel.monthList.0
                    return monthlist[Int(value) % monthlist.count]
                })
                self.yearlyStatsChart.xAxis.axisMinimum = 0
                self.yearlyStatsChart.xAxis.axisMaximum = 11
                
                let entries: [BarChartDataEntry] = stat.months.map({ [unowned self] s in
                    return BarChartDataEntry(x: Double(self.viewModel.monthList.0.firstIndex(of: s.unit) ?? 0), yValues: [Double(s.totalPatient), Double(s.reviewedPatient), Double(s.satisfiedPatient)])
                })
                
                let set = BarChartDataSet(entries: entries, label: "")
                set.setColor(UIColor(red: 104/255, green: 241/255, blue: 175/255, alpha: 1))
                set.stackLabels = ["Total Patients", "Reviewed Patients", "Statisfied Patients"]
                set.colors = [ChartColorTemplates.material()[0], ChartColorTemplates.material()[1], ChartColorTemplates.material()[2]]
                set.valueFormatter = DefaultValueFormatter(block: { (value, entry, index, _) -> String in
                    if value == 0 {
                        return ""
                    } else {
                        return String(format: "%.0f", value)
                    }
                    
                })
                
                let data = BarChartData(dataSet: set)
                self.yearlyStatsChart.data = data
            }
        }).disposed(by: bag)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.getDashboardStatus()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == monthlyStatsYearPicker {
            showYearPicker(from: textField, for: .Month)
        }
        if textField == monthlyStatsMonthPicker {
            showMonthPicker(from: textField)
        }
        if textField == yearlyStatsYearPicker {
            showYearPicker(from: textField, for: .Year)
        }
        return false
    }
    
    private func showYearPicker(from textField: UITextField, for chartType: ChartType ) {
        ActionSheetStringPicker.show(
            withTitle: viewModel.yearPickerTitle,
            rows: viewModel.yearList.0,
            initialSelection: viewModel.yearList.1,
            doneBlock: { [weak self] picker, selectedIndex, selectedValue in
                if let year = selectedValue as? String {
                    textField.text = year
                    if chartType == .Month {
                        self?.viewModel.setCurrentYearForMonthlyChart(with: Int(year) ?? 2019)
                    } else {
                        self?.viewModel.setCurrentYearForYearlyChart(with: Int(year) ?? 2019)
                    }
                }
            },
            cancel: { _ in },
            origin: textField
        )
    }
    
    private func showMonthPicker(from textField: UITextField) {
        ActionSheetStringPicker.show(
            withTitle: viewModel.monthPickerTitle,
            rows: viewModel.monthList.0,
            initialSelection: viewModel.monthList.1,
            doneBlock: { [weak self] picker, selectedIndex, selectedValue in
                textField.text = selectedValue as? String
                self?.viewModel.setCurrentMonth(with: selectedIndex + 1)
            },
            cancel: { _ in },
            origin: textField
        )
    }
    
}

extension SCDoctorDashboardViewController {
    private func configureDailyChartView() {
        dailyChartDateLabel.text = Date().toDateString(withFormatter: SCAppConstants.DateFormatType.appointment)
        viewModel.dailyChart.drive(onNext: { [weak self] in
            if let `self` = self {
                if self.generateDailyChartValues(for: $0).count > 0 {
                    let dataSet = PieChartDataSet(
                        entries: self.generateDailyChartValues(for: $0),
                        label: nil
                    )
                    dataSet.colors = self.generateDailyChartColors(for: $0)
                    dataSet.sliceSpace = 2
                    let formatter = NumberFormatter()
                    formatter.numberStyle = .none
                    dataSet.valueFormatter = DefaultValueFormatter(formatter: formatter)
                    dataSet.valueFont = UIFont(name: SCAppConstants.applicationFontFamily, size: 11)!
                    self.dailyChart.data = PieChartData(dataSet: dataSet)
                    self.dailyChartHeightContraint.constant = 200
                } else {
                    self.dailyChart.data = nil
                    self.dailyChartHeightContraint.constant = 100
                }
            }
        }).disposed(by: bag)
        
        self.dailyChart.drawHoleEnabled = true
        self.dailyChart.drawEntryLabelsEnabled = false
        self.dailyChart.highlightPerTapEnabled = false
        self.dailyChart.rotationWithTwoFingers = false
        self.dailyChart.rotationEnabled = false
        self.dailyChart.noDataText = "No Appointments"
        
        let l = self.dailyChart.legend
        l.horizontalAlignment = .right
        l.verticalAlignment = .top
        l.orientation = .vertical
        l.drawInside = false
        l.xEntrySpace = 7
        l.yEntrySpace = 5
        l.yOffset = 0
        l.setCustom(entries: generateDailyChartLengendEntries())
    }
    
    private func generateDailyChartLengendEntries() -> [LegendEntry] {
        return [
            LegendEntry(label: "Completed", form: .circle, formSize: CGFloat.nan, formLineWidth: CGFloat.nan, formLineDashPhase: 0, formLineDashLengths: nil, formColor: viewModel.dailyChartCompletedColor),
            LegendEntry(label: "Cancelled", form: .circle, formSize: CGFloat.nan, formLineWidth: CGFloat.nan, formLineDashPhase: 0, formLineDashLengths: nil, formColor: viewModel.dailyChartCancelledColor),
            LegendEntry(label: "Booked", form: .circle, formSize: CGFloat.nan, formLineWidth: CGFloat.nan, formLineDashPhase: 0, formLineDashLengths: nil, formColor: viewModel.dailyChartBookedColor)
        ]
    }
    
    private func generateDailyChartValues(for object: DailyChart) -> [PieChartDataEntry] {
        var entries = [PieChartDataEntry]()
        if object.done > 0 {
            entries.append(PieChartDataEntry(value: Double(object.done)))
        }
        if object.cancelled > 0 {
            entries.append(PieChartDataEntry(value: Double(object.cancelled)))
        }
        if object.pending > 0 {
            entries.append(PieChartDataEntry(value: Double(object.pending)))
        }
        return entries
    }
    
    private func generateDailyChartColors(for object: DailyChart) -> [NSUIColor] {
        var entries = [NSUIColor]()
        if object.done > 0 {
            entries.append(viewModel.dailyChartCompletedColor)
        }
        if object.cancelled > 0 {
            entries.append(viewModel.dailyChartCancelledColor)
        }
        if object.pending > 0 || entries.isEmpty {
            entries.append(viewModel.dailyChartBookedColor)
        }
        return entries
    }
}
