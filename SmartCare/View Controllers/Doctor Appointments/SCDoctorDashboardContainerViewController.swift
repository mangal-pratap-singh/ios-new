//
//  SCDoctorDashboardViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 23/10/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCDoctorDashboardContainerViewController: UIViewController {

    var titleBarDataSource = ["Overview", "Appointments"]
    
    @IBOutlet weak var tabbedView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.dashboard, isDetail: false)
        setupMenuView()
    }
    
    func setupMenuView() {
        let swipeableView = SMSwipeableTabViewController()
        swipeableView.titleBarDataSource = titleBarDataSource
        swipeableView.delegate = self
        swipeableView.segmentBarAttributes = [SMBackgroundColorAttribute : UIColor.SCNavigationBarBackgroundColor()]
        swipeableView.selectionBarAttributes = [SMBackgroundColorAttribute : UIColor.SCTextFilePlaceHolderTextColor()]
        swipeableView.buttonAttributes = [
            SMBackgroundColorAttribute : UIColor.clear,
            SMFontAttribute : UIFont(name: SCAppConstants.applicationFontFamily, size: 16.0)!,
            SMForegroundColorAttribute: UIColor.white,
            SMUnselectedColorAttribute: UIColor.gray
        ]
        swipeableView.segementBarHeight = 50
        swipeableView.buttonWidth = tabbedView.frame.width/2
        swipeableView.viewFrame = CGRect(x: 0.0, y: 0, width:tabbedView.frame.width, height: tabbedView.frame.height)
        swipeableView.selectionBarHeight = 2.0

        self.addChild(swipeableView)
        self.tabbedView.addSubview(swipeableView.view)
        swipeableView.didMove(toParent: self)
    }
}


extension SCDoctorDashboardContainerViewController: SMSwipeableTabViewControllerDelegate {
    
    func didLoadViewControllerAtIndex(_ index: Int) -> UIViewController {
        if index == 0 {
            return SCDoctorDashboardViewController.initialize(with: SCDoctorDashboardViewModel())
        } else if index == 1 {
            let doctorDashboardAppointmentVC = self.storyboard?.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.doctorDashboardAppointments) as! SCDoctorDashboardAppointmentsViewController
            return doctorDashboardAppointmentVC
        }
        return UIViewController()
    }
}
