//
//  SCDoctorAppointmentDetailsViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 21/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import SafariServices
import PDFKit
class SCDoctorAppointmentDetailsViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var commentDetailsView: UIView!
    
    @IBOutlet weak var view_joinCall: UIView!
    
    @IBOutlet weak var paymentModeLabel: UILabel!
    @IBOutlet weak var paymentStatusLabel: UILabel!
    @IBOutlet weak var consultationFeeLabel: UILabel!
    
    @IBOutlet weak var patientNameLabel: UILabel!
    @IBOutlet weak var patientEmailButton: UIButton!
    @IBOutlet weak var patientPhoneButton: UIButton!
    @IBOutlet weak var patientAgeLabel: UILabel!
    @IBOutlet weak var patientIdLabel: UILabel!
    
    @IBOutlet weak var appointmentTypeLabel: UILabel!
    @IBOutlet weak var appointmentTimeLabel: UILabel!
    @IBOutlet weak var hospitalNameLabel: UILabel!
    @IBOutlet weak var hospitalPhoneButton: UIButton!
    
    @IBOutlet weak var btn_viewConsultantNote: UIButton!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var cancelledByLabel: UILabel!
    
    @IBOutlet weak var commentDetailsViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var dateTimeViewBottomConstraint: NSLayoutConstraint!
     var tokenGenerationObject : VideoToken? = nil
    private lazy var bookedAppointmentDetailsVC: SCDoctorBookedAppointmentDetailsViewController = {
        var viewController = self.storyboard?.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.bookedAppointmentDetails) as! SCDoctorBookedAppointmentDetailsViewController
        viewController.currentAppintment = currentAppointment
        return viewController
    }()
    
    private lazy var confirmedAppointmentDetailsVC: SCDoctorConfirmedAppointmentDetailsViewController = {
        var viewController = self.storyboard?.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.confirmedAppointmentDetails) as! SCDoctorConfirmedAppointmentDetailsViewController
        viewController.currentAppintment = currentAppointment
        return viewController
    }()
    
    var currentAppointment: DoctorAppointment?
    
    static func initialize(withDoctorAppointment doctorAppointment: DoctorAppointment) -> SCDoctorAppointmentDetailsViewController {
        let storyboard = UIStoryboard(name: SCAppConstants.storyBoardName.doctorAppointments, bundle: Bundle.main)
        let appointmentDetailsVC = storyboard.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.doctorAppointmentDetails) as! SCDoctorAppointmentDetailsViewController
        appointmentDetailsVC.currentAppointment = doctorAppointment
        return appointmentDetailsVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        GetDoctorsToken()
         btn_viewConsultantNote.isHidden = true
        customizeNavigationItem(title: SCAppConstants.pageTitles.appointmentDetails, isDetail: true)
      }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
       // VCConnectorPkg.vcInitialize()
        view_joinCall.isHidden = true
        showAppointmentDetails()
    }
    
    private func showAppointmentDetails() {
        
        if let appointment =  currentAppointment {
            // Show Payment Details
            paymentModeLabel.text = appointment.payment?.type?.statusText()
            paymentStatusLabel.text = appointment.payment?.status?.statusText()
            consultationFeeLabel.text = appointment.payment?.amount
            
            //Show Patient Details
            patientNameLabel.text = appointment.patient?.name
            patientAgeLabel.text = appointment.patient?.age
            patientIdLabel.text = appointment.patient?.id
            patientEmailButton.setTitle(appointment.patient?.email, for: .normal)
            let phoneNumberStr = "+ \(appointment.patient?.countryCode ?? "")- \(appointment.patient?.phoneNumber ?? "")"
            patientPhoneButton.setTitle(phoneNumberStr, for: .normal)
            
            //Show Appointment Details
            appointmentTypeLabel.text = appointment.type?.text()
            hospitalNameLabel.text = "\(appointment.doctorHospital?.name ?? "") / \(appointment.clinic?.name ?? "")"
            hospitalPhoneButton.setTitle(appointment.clinic?.clinicAddress?.phoneNumber, for: .normal)
            locationLabel.text = appointment.location
            
            if appointment.type?.text() == "Walk-in Consultation"{
                view_joinCall.isHidden = true
                 VideoCallHideShowMethod()
            }else if appointment.type?.text() == "Remote Consultation"{
                 VideoCallHideShowMethod()
            }else if appointment.type?.text() == "Procedure Consultation" {
                view_joinCall.isHidden = true
            }
            if let startDate = appointment.startTime, let endDate = appointment.endTime {
                appointmentTimeLabel.text = startDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointment) + " at " + startDate.toTimeString() + " - " + endDate.toTimeString()
            }
            commentsLabel.text = appointment.cancelComments
            cancelledByLabel.text = appointment.cancelBy
            
            commentDetailsView.sizeToFit()
            let height = commentDetailsView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            if let apoointmentStatus = currentAppointment?.status {
                commentDetailsView.isHidden = true
                view_joinCall.isHidden = true
                commentDetailsViewHeightConstraint.constant = 16
                switch apoointmentStatus {
                     
                    case .booked:
                        btn_viewConsultantNote.isHidden = true
                       view_joinCall.isHidden = true
                        showBookedAppointmentDetails()
                    case .confirmed:
                        btn_viewConsultantNote.isHidden = true
                        VideoCallHideShowMethod()
                        showConfirmedAppointmentDetails()
                       GetDoctorsToken()
                    case .cancelled:
                        btn_viewConsultantNote.isHidden = true
                        view_joinCall.isHidden = true
                        commentDetailsView.isHidden = false
                        commentDetailsViewHeightConstraint.constant = height + 32
                    
                    case .missed:
                        btn_viewConsultantNote.isHidden = true
                        view_joinCall.isHidden = true
                        commentDetailsView.isHidden = false
                        commentDetailsViewHeightConstraint.constant = height + 32
                    
                    case .completed:
                        btn_viewConsultantNote.isHidden = false
                        view_joinCall.isHidden = true
                        showOnlyCommonAppointmentDetails()
                   
                }
                
               }
         }
  }
    
    
  func VideoCallHideShowMethod(){
        
        guard let endDate = currentAppointment?.endTime else { return  }
                               let currentDate = Date()
                               let curruntDate  = currentDate.currentDateTime.toDate()
   

    
    //let cStr = curruntDate?.currentDateTime
        let date = endDate.addingTimeInterval(TimeInterval(5.0 * 60.0))
        
   // let PassDateStr = date.currentDateTime
    
    //print("currentDATE==\(cStr) AND =PASS=\(PassDateStr)")
           //                    let dateFormatter = DateFormatter()
           //                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:SSZ"
           //                    let curruntDateStr = dateFormatter.string(from: currentDate)
                               if curruntDate?.compare(date) == ComparisonResult.orderedDescending {
                                   print("appointment Date is smaller than currentDate")
                                 view_joinCall.isHidden = true
                               }else if curruntDate?.compare(date) == ComparisonResult.orderedAscending{
                                   view_joinCall.isHidden = false
                                  print("appointment Date is greater than currentDate")
                               }else{
                                // print("myDate is equal than" )
                                view_joinCall.isHidden = true
                                   
                               }
                              
       }
       
    private func showBookedAppointmentDetails() {
        containerView.isHidden = false
        for controller in self.children {
            removeAsChildViewController(viewController: controller)
          }
        addAsChildViewController(viewController: bookedAppointmentDetailsVC)
       }
    
    private func GetDoctorsToken(){
//        SCProgressView.show()
        if let appointmentId = currentAppointment?.id {
        SCProgressView.hide()
        SCAppointmentService.DoctorstokenGenerationAPI(UsersAppointmentID: appointmentId, completionHandler: { (model) in
            
            self.tokenGenerationObject = model
            print("classModel:" + "\(self.tokenGenerationObject as Any)")
            DispatchQueue.main.async {
                
            }
        }) { (err) in
            print(err)
        }
        }
    }
    
    
    private func showConfirmedAppointmentDetails() {
        containerView.isHidden = false
        for controller in self.children {
            removeAsChildViewController(viewController: controller)
        }
        self.addAsChildViewController(viewController: confirmedAppointmentDetailsVC)
    }
    
    private func showOnlyCommonAppointmentDetails() {
        containerView.isHidden = true
        for controller in self.children {
            removeAsChildViewController(viewController: controller)
        }
    }
    
    private func addAsChildViewController(viewController: UIViewController) {
        addChild(viewController)
        containerView.addSubview(viewController.view)
        viewController.view.translatesAutoresizingMaskIntoConstraints = false
        
        // this is to maintain the constraints for childview
        let views = Dictionary(dictionaryLiteral: ("childView", viewController.view),("container", containerView))
        //Horizontal constraints
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[childView]|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: views as [String : Any])
        containerView.addConstraints(horizontalConstraints)
        
        //Vertical constraints
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[childView]|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: views as [String : Any])
        containerView.addConstraints(verticalConstraints)
        viewController.didMove(toParent: self)
    }

    private func removeAsChildViewController(viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    func APICallSuccessful() {
        
          let vc = self.storyboard?.instantiateViewController(withIdentifier: "SCDoctorsConsultationNotesViewController")  as! SCDoctorsConsultationNotesViewController
        
        vc.doctorAppointment = currentAppointment
         
        
         self.navigationController?.pushViewController(vc, animated: true)
        
//        self.navigationController?.popViewController(animated: true)
        
    }
    
    func APICallFailedWithError(errorMessage: String) {
        showAlert(title: SCAppConstants.pageTitles.appointmentDetails, msg: errorMessage)
    }
    
    
    
    
    
    @IBAction func callTapBtnClick(_ sender: UIButton) {
        
         initiateCall(forNumber: currentAppointment?.patient?.phoneNumber ?? "")
    }
    
    @IBAction func viewConsultantNoteBtnClick(_ sender: UIButton) {
        
        
        
        let consultantNoteStr = currentAppointment?.consultationNoteUrl
        
        
        guard let urlString = consultantNoteStr else { return}
        let replaceStr = urlString.replacingOccurrences(of: "\"", with: "")
        
        print("print pdf url==\(urlString)")
        /*
         guard let url = URL(string: replaceStr)  else { return }
        let document = PSPDFDocument(URL: url)
        let pdfController = PSPDFViewController(document: document)

        // Present document.
        let navigationController = UINavigationController(rootViewController: pdfController)
        presentViewController(navigationController, animated: true, completion: nil)
        return
        */
        /*
        let storyboard = UIStoryboard(name: SCAppConstants.storyBoardName.doctorAppointments, bundle: Bundle.main)
        let viewPdfVC = storyboard.instantiateViewController(withIdentifier: "SCDoctorPDFViewController") as! SCDoctorPDFViewController
       // appointmentDetailsVC.currentAppointment = doctorAppointment
                         
        viewPdfVC.pdfUrl = replaceStr
        self.navigationController?.pushViewController(viewPdfVC, animated: true)
        */
        openInstagram(webUrl: replaceStr)
        
        
    }
    
    func openInstagram(webUrl: String) {
        
        
          //https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf
        //https://teststaging-api-private.s3.ap-south-1.amazonaws.com/smartcare-documents/receipt/UD-20010422A0003/561.pdf?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAJIBNHG5YOPTMFKZQ%2F20200424%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20200424T040959Z&X-Amz-SignedHeaders=host&X-Amz-Expires=1200&X-Amz-Signature=4854281f8687206cb5c0a24affb8bf9372d99cdcd1dc6eaca26e0481f17e4a17
        
        //https://teststaging-api-private.s3.ap-south-1.amazonaws.com/smartcare-documents/consulation-note/UD-20190611A0001/consultation-note-547.pdf?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAJIBNHG5YOPTMFKZQ%2F20200424%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20200424T045703Z&X-Amz-SignedHeaders=host&X-Amz-Expires=1200&X-Amz-Signature=8180aa192e218df7d24a95452a22d36c899f68c506c36e2922e01e942fc1a84e
        
             guard let url = URL(string: webUrl)  else { return }
        
             let safariVC = SFSafariViewController(url: url)
             present(safariVC,animated: true)
         }
    @IBAction func callJoinBtnClick(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
               
                             let vc = storyBoard.instantiateViewController(withIdentifier: "SCVideoCustomViewController")  as! SCVideoCustomViewController
                                    vc.fromVideo = "fromVideo"
               
                                     vc.displayName = tokenGenerationObject?.data?.patient?.name
        
                                     vc.senderCommonName = tokenGenerationObject?.data?.doctor?.name
                                     vc.resourceID = tokenGenerationObject?.data?.resourceId ?? ""
                                      vc.VIDYO_TOKEN = tokenGenerationObject?.data?.joinToken ?? ""
               
                                      vc.videoCallModel = tokenGenerationObject?.data
                                   self.navigationController?.pushViewController(vc, animated: true)
        
     }
    
   
}
