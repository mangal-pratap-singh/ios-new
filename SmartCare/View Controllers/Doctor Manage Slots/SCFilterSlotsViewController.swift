//
//  SCFilterSlotsViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 10/12/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class SCFilterSlotsViewController: UIViewController {
    
    private let datePickerTitle = "Select Date"
    
    @IBOutlet weak var startDateTextField: UITextField!
    @IBOutlet weak var endDateTextField: UITextField!
    @IBOutlet weak var slotItemsTable: UITableView!

    var startDate: Date?
    var endDate: Date?
    var fullScreenMessageView:FullScreenMessageView?
    
    var slotsDataSource = [(key: String, value: [SlotItem])]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.filterSlots, isDetail: false)
        configureView()
        registerNibsForCells()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         getFilteredSlotsForSelectedDates()
    }
    
    @IBAction func goButtonAction(_ sender: UIButton) {
        getFilteredSlotsForSelectedDates()
    }
    
    private func getFilteredSlotsForSelectedDates() {
        if let startDate = self.startDate, let endDate = self.endDate {
            let formattedStartDate = startDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentAPI)
            let formattedEndDate = endDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentAPI)
            getFilteredSlotsFor(startDate: formattedStartDate, endDate: formattedEndDate)
        } else {
            showAlert(title: SCAppConstants.pageTitles.filterSlots, msg: "Please enter Start date and End date to filter")
        }
    }
    
    private func configureView() {
        slotItemsTable.tableFooterView = UIView()
        fullScreenMessageView = showFullScreenMessageOnView(parentView: slotItemsTable, withMessage: "Currently there are no slots matching your filter")
        view.addSubview(fullScreenMessageView!)
    }
    
    private func registerNibsForCells() {
        let nibName = UINib(nibName: SCAppConstants.nibNames.slotItemTableViewCell, bundle:nil)
        slotItemsTable.register(nibName, forCellReuseIdentifier: SCAppConstants.cellReuseIdentifiers.slotListItem)
    }
    
    private func getFilteredSlotsFor(startDate: String, endDate: String) {
        SCProgressView.show()
        SCSlotService.filteredSlotListAPI(startDate: startDate, endDate: endDate, onComplete: { [weak self] (slotItemList) in
            SCProgressView.hide()
            self?.fullScreenMessageView?.removeFromSuperview()
            if slotItemList.count == 0 {
                self?.fullScreenMessageView = self?.showFullScreenMessageOnView(parentView: self!.slotItemsTable, withMessage: "Currently there are no slots matching your filter")
                self?.view.addSubview(self!.fullScreenMessageView!)
              }
            //Convert Data into Dictionary usinbg Group By with Date
            let unsortedDict = slotItemList.categorise {$0.startTime!.toDateString(withFormatter: SCAppConstants.DateFormatType.appointment)}
            self?.slotsDataSource = unsortedDict.sorted(by: { $0.key.toDateWithFormat(format: SCAppConstants.DateFormatType.appointment)! < $1.key.toDateWithFormat(format: SCAppConstants.DateFormatType.appointment)! })
            self?.slotItemsTable.reloadData()
            
        }) { (errorMessage) in
            SCProgressView.hide()
            self.showAlert(title: SCAppConstants.pageTitles.filterSlots, msg: errorMessage)
        }
    }
    
    private func showDatePicker(textField:UITextField) {
        let datePicker = ActionSheetDatePicker(title: datePickerTitle, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let dateString = (value as! Date).toDateString(withFormatter: SCAppConstants.DateFormatType.date)
            if textField == self.startDateTextField {
                self.startDate = value as? Date
            } else if textField == self.endDateTextField {
                self.endDate = value as? Date
            }
            textField.text = dateString
            return
        }, cancel: {
            ActionStringCancelBlock in return
        },
        origin: textField.superview)
        datePicker?.show()
    }
    
    private func editSlotWithId(slotId: String, parameters: [String: Any]? = nil) {
        SCProgressView.show()
        SCSlotService.editSlotAPI(slotId: slotId, parameters: parameters, onComplete: {
            SCProgressView.hide()
            self.getFilteredSlotsForSelectedDates()
        }) { (errorMessage) in
            SCProgressView.hide()
            print("ERROR -- \(errorMessage)")
        }
    }
    
    private func showEditScreenForSlot(slot: SlotItem) {
        if let changeSlotVC = storyboard?.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.changeSlot) as? SCChangeSlotViewController {
            changeSlotVC.currentSlot = slot
             self.navigationController?.pushViewController(changeSlotVC, animated: true)
        }
    }
}

extension SCFilterSlotsViewController {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.showDatePicker(textField: textField)
        return false
    }
}

extension SCFilterSlotsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.slotsDataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return Array(self.slotsDataSource)[section].key
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Array(self.slotsDataSource)[section].value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SCAppConstants.cellReuseIdentifiers.slotListItem, for: indexPath as IndexPath) as! SlotItemTableViewCell
        cell.selectionStyle = .none
        cell.slotActionDelegate = self
        let currentItem = Array(self.slotsDataSource)[indexPath.section].value[indexPath.row]
        cell.configureCell(withSlotItem: currentItem)
        return cell
    }
}

extension SCFilterSlotsViewController: SlotActionsDelegate {
    func enableButtonClickedForSlot(slot: SlotItem) {
        let parameters = ["status": true]
        self.editSlotWithId(slotId: String(slot.id!), parameters: parameters)
    }
    
    func disableButtonClickedForSlot(slot: SlotItem) {
        let confirmationAlert = UIAlertController(title: SCAppConstants.pageTitles.filterSlots, message: SCAppConstants.alertMessages.slotDeleteConfirmation, preferredStyle: .alert)
        let disAgreeAction = UIAlertAction(title: SCAppConstants.alertButtonTitles.disagree, style: .default, handler: nil)
        confirmationAlert.addAction(disAgreeAction)
        
        let agreeAction = UIAlertAction(title: SCAppConstants.alertButtonTitles.agree, style: .default) { (action) in
            let parameters = ["status": false]
            self.editSlotWithId(slotId: String(slot.id!), parameters: parameters)
        }
        confirmationAlert.addAction(agreeAction)
        present(confirmationAlert, animated: true, completion: nil)
    }
    
    func changeButtonClickedForSlot(slot: SlotItem) {
        showEditScreenForSlot(slot: slot)
    }
}

