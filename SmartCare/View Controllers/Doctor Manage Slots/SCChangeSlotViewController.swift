//
//  SCChangeSlotViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 15/12/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class SCChangeSlotViewController: UIViewController {

    private let consultationTypePickerTitle = "Select Consultation Type"
    
    @IBOutlet weak var slotDateTimeLabel: UILabel!
    @IBOutlet weak var consultationTypeTextfield: UITextField!
    
    var currentSlot: SlotItem?
    var consultationTypes = ConsultationType.toArray()
    var selectedslotTypeIndex = 0
    var selectedSlotType: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.editSlot, isDetail: true)
        configureView()
    }
    
    @IBAction func changeSlotTypeButtonAction(_ sender: UIButton) {
        if let slot = currentSlot, let slotType = selectedSlotType {
            let parameters = ["slotType": slotType]
            self.editSlotWithId(slotId: String(slot.id!), parameters: parameters)
        }
    }
    
    @IBAction func dismissButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    private func configureView() {
        consultationTypeTextfield.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        if let slot = currentSlot, let startTime = slot.startTime, let endTime = slot.endTime  {
            let timestr = startTime.toDateString(withFormatter: SCAppConstants.DateFormatType.slotDateTime) + " to " + endTime.toDateString(withFormatter: SCAppConstants.DateFormatType.slotTime)
            slotDateTimeLabel.text = timestr
        }
        
        if let consultationType = currentSlot?.consultationType {
            consultationTypeTextfield.text = consultationType.text()
        }
    }
    
    private func editSlotWithId(slotId: String, parameters: [String: Any]? = nil) {
        SCProgressView.show()
        SCSlotService.editSlotAPI(slotId: slotId, parameters: parameters, onComplete: {
            SCProgressView.hide()
            self.navigationController?.popViewController(animated: true)
        }) { (errorMessage) in
            SCProgressView.hide()
            self.showAlert(title: SCAppConstants.pageTitles.filterSlots, msg: errorMessage)
        }
    }
    
    private func showConsultationTypePicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: consultationTypePickerTitle, rows: consultationTypes.map { $0.text()}, initialSelection: 0, doneBlock: { picker, selectedIndex, selectedValue in
            self.selectedslotTypeIndex = selectedIndex
            if let aValue = selectedValue {
                textField.text = aValue as? String
                self.selectedSlotType = self.consultationTypes[self.selectedslotTypeIndex].rawValue
            }
        }, cancel: { picker in
        }, origin: textField)
    }
}

extension SCChangeSlotViewController{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        showConsultationTypePicker(textField: textField)
        return false
    }
}

