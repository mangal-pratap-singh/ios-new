//
//  SCCreateSlotViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 08/12/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class SCCreateSlotViewController: UIViewController {

    private let hospitalPickerTitle = "Select Hospital"
    private let clinicPickerTitle = "Select Clinic"
    private let shiftPickerTitle = "Select Shift"
    private let slotTypePickerTitle = "Select Slot Type"
    private let consultationTypePickerTitle = "Select Consultation Type"
    
    @IBOutlet weak var hospitalNameTextField: UITextField!
    @IBOutlet weak var clinicNameTextField: UITextField!
    @IBOutlet weak var shiftTextField: UITextField!
    @IBOutlet weak var slotTypeTextField: UITextField!
    @IBOutlet weak var consultationTypeTextField: UITextField!
    @IBOutlet weak var consultationTypeView: UIView!
    @IBOutlet weak var buttonTopConstraint: NSLayoutConstraint!
    
    var hospitals = [Hospital]()
    var selectedHospitalIndex = -1
    var selectedclinicIndex = -1
    
    var shifts = [Shift]()
    var selectedShiftIndex = -1
    
    var slotTypes = SlotType.toArray()
    var consultationTypes = ConsultationType.toArray()
    
    var selectedslotTypeIndex = -1
    var selectedConsultationTypeIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.createSlots, isDetail: false)
        configureView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getAsocialtedHospitalList()
       // getShiftsList()
    }
    
    @IBAction func selectDatesButtonAction(_ sender: UIButton) {
        if selectedHospitalIndex != -1 && selectedclinicIndex != -1 && selectedslotTypeIndex != -1 && selectedShiftIndex != -1 {
            showAppointmentDateSelectionView()
        } else {
            showAlert(title: SCAppConstants.pageTitles.createSlots, msg: SCAppConstants.validationMessages.allFieldsValidationMessage)
        }
    }
    
    private func configureView() {
        hospitalNameTextField.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        clinicNameTextField.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        shiftTextField.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        slotTypeTextField.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        consultationTypeTextField.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
    }
    
    private func showAppointmentDateSelectionView() {
        if let selectDateVC = self.storyboard?.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.selectAppointmentDateVC) as? SCSelectDatesViewController {
            selectDateVC.selectedShift = shifts[selectedShiftIndex]
            selectDateVC.selectedHospital = hospitals[selectedHospitalIndex]
            selectDateVC.selectedClinic = hospitals[selectedHospitalIndex].clinics?[selectedclinicIndex]
            selectDateVC.selectedSlotType = selectedConsultationTypeIndex != -1 ? consultationTypes[selectedConsultationTypeIndex].rawValue : slotTypes[selectedslotTypeIndex].rawValue
            self.navigationController?.pushViewController(selectDateVC, animated: true)
        }
    }
    
    private func getAsocialtedHospitalList(){
        SCCommanService.getAsociateHospitalListAPI(onComplete: { (hospitalList) in
            self.hospitals = hospitalList
            self.getShiftsList()
        }) { (errorMessage) in
            print("Error Getting Hospital List - \(errorMessage)")
        }
    }
    
    func getShiftsList(){
        SCShiftService.shiftsListAPI(onComplete: { (shiftsList) in
            self.shifts = shiftsList
        }) { (errorMessage) in
            print("Error Getting Shifts List - \(errorMessage)")
        }
    }
    
    func showHospitalPicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: hospitalPickerTitle, rows: hospitals.map { $0.name ?? "" }, initialSelection: 0, doneBlock: { picker, selectedIndex, selectedValue in
            self.selectedHospitalIndex = selectedIndex
            
            // Reset clinic selection when hospital is changed
            self.selectedclinicIndex = -1
            self.clinicNameTextField.text = self.clinicPickerTitle
            
            if let aValue = selectedValue {
                textField.text = aValue as? String
            }
        }, cancel: { picker in
        }, origin: textField)
    }
    
    func showClinicPicker(textField:UITextField) {
        ActionSheetStringPicker.show(
            withTitle: clinicPickerTitle,
            rows: hospitals[selectedHospitalIndex].clinics?.map { $0.name ?? "" },
            initialSelection: 0,
            doneBlock: { picker, selectedIndex, selectedValue in
                self.selectedclinicIndex = selectedIndex
                if let aValue = selectedValue {
                    textField.text = aValue as? String
                }
            },
            cancel: { _ in },
            origin: textField
        )
    }
    
    func showSlotTypePicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: slotTypePickerTitle, rows: slotTypes.map { $0.text()}, initialSelection: 0, doneBlock: { picker, selectedIndex, selectedValue in
            self.selectedslotTypeIndex = selectedIndex
            self.consultationTypeView.isHidden = selectedIndex == 0 ? true : false
            self.buttonTopConstraint.constant = selectedIndex == 0 ? 50 : 140
            
            if let aValue = selectedValue {
                textField.text = aValue as? String
            }
        }, cancel: { picker in
        }, origin: textField)
    }
    
    func showShiftsPicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: shiftPickerTitle, rows: shifts.map { $0.name ?? ""}, initialSelection: 0, doneBlock: { picker, selectedIndex, selectedValue in
            self.selectedShiftIndex = selectedIndex
            if let aValue = selectedValue {
                textField.text = aValue as? String
            }
        }, cancel: { picker in
        }, origin: textField)
    }
    
    func showConsultationTypePicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: consultationTypePickerTitle, rows: consultationTypes.map { $0.text()}, initialSelection: 0, doneBlock: { picker, selectedIndex, selectedValue in
            self.selectedConsultationTypeIndex = selectedIndex
            if let aValue = selectedValue {
                textField.text = aValue as? String
            }
        }, cancel: { picker in
        }, origin: textField)
    }
    
}

extension SCCreateSlotViewController {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if (textField == hospitalNameTextField) {
            showHospitalPicker(textField: textField)
            return false
        } else if (textField == clinicNameTextField) {
            showClinicPicker(textField: textField)
            return false
        } else if (textField == slotTypeTextField) {
            showSlotTypePicker(textField: textField)
            return false
        } else if (textField == shiftTextField) {
            showShiftsPicker(textField: textField)
            return false
        } else if (textField == consultationTypeTextField) {
            showConsultationTypePicker(textField: textField)
            return false
        }
        return true
    }
}
