//
//  SCSelectDatesViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 08/12/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON

class SCSelectDatesViewController: UIViewController {

    @IBOutlet weak var calenderView: CalendarView!
    @IBOutlet weak var hospitalNameLabel: UILabel!
    @IBOutlet weak var slotDurationLabel: UILabel!
    @IBOutlet weak var totalSlotsLabel: UILabel!
    
    var selectedShift: Shift?
    var selectedHospital: Hospital?
    var selectedClinic: Clinic?
    var selectedSlotType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.selectAppointmentDates, isDetail: true)
        configureView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showDetails()
    }
    
    @IBAction func saveSelectedDatesButtonAction(_ sender: UIButton) {
        saveSelectedDates()
    }
    
    private func configureView() {
       // calenderView.dataSource = self
        customiseCalenderView()
    }
    
    private func customiseCalenderView() {
        /*
        CalendarView.Style.cellColorDefault = UIColor.clear
        CalendarView.Style.cellColorToday = UIColor.SCBrownColor()
        CalendarView.Style.cellTextColorToday = UIColor.white
        CalendarView.Style.cellTextColorDefault = UIColor.darkGray
        CalendarView.Style.cellShape = .round
        CalendarView.Style.cellSelectedColor = UIColor.SCButtonBackground()
        CalendarView.Style.cellSelectedBorderColor = UIColor.SCButtonBackground()
        CalendarView.Style.headerTextColor = UIColor.black
        */
        //calenderView.marksWeekends = false
        //calenderView.delegate = self
        
        let style = CalendarView.Style()
        
        
        style.cellShape                = .round//.bevel(8.0)
        style.cellColorDefault         = UIColor.clear
        style.cellColorToday           = UIColor.SCBrownColor()//UIColor(red:1.00, green:0.84, blue:0.64, alpha:1.00)
        style.cellSelectedBorderColor  = UIColor(red:1.00, green:0.63, blue:0.24, alpha:1.00)
        style.cellEventColor           = UIColor(red:1.00, green:0.63, blue:0.24, alpha:1.00)
        style.headerTextColor          = UIColor.gray
        
        style.cellTextColorDefault     = UIColor.darkGray//UIColor(red: 249/255, green: 180/255, blue: 139/255, alpha: 1.0)
        style.cellTextColorToday       = UIColor.white//UIColor.orange
        style.cellTextColorWeekend     = UIColor(red: 237/255, green: 103/255, blue: 73/255, alpha: 1.0)
        style.cellColorOutOfRange      = UIColor(red: 249/255, green: 226/255, blue: 212/255, alpha: 1.0)
            
        style.headerBackgroundColor    = UIColor.white
        style.weekdaysBackgroundColor  = UIColor.white
        style.firstWeekday             = .sunday
        
        style.locale                   = Locale(identifier: "en_US")
        
        style.cellFont = UIFont(name: "Helvetica", size: 20.0) ?? UIFont.systemFont(ofSize: 20.0)
        style.headerFont = UIFont(name: "Helvetica", size: 20.0) ?? UIFont.systemFont(ofSize: 20.0)
        style.weekdaysFont = UIFont(name: "Helvetica", size: 14.0) ?? UIFont.systemFont(ofSize: 14.0)
        
        calenderView.style = style
        
        calenderView.dataSource = self
        calenderView.delegate = self
        
        calenderView.direction = .horizontal
        calenderView.multipleSelectionEnable = false
        calenderView.marksWeekends = true
        
        
        calenderView.backgroundColor = UIColor(red: 252/255, green: 252/255, blue: 252/255, alpha: 1.0)
    }
    
    private func showDetails() {
        if let hospitalName = selectedHospital?.name, let clinicName = selectedClinic?.name {
            hospitalNameLabel.text = hospitalName + " / " + clinicName
        }
        slotDurationLabel.text = "Slot Duration: \(UserProfile.getUserProfile()?.slotDuration ?? 0) min"
        totalSlotsLabel.text = "Time Slots: \(0)"
    }
    
    private func saveSelectedDates() {
        
        guard let shift = selectedShift, let hospitalId = selectedHospital?.id, let clinicId = selectedClinic?.id else {
            return
        }
        
        let slotParameters = NSMutableArray()
        for date in calenderView.selectedDates {
            
            let selectedDate = date.toDateString(withFormatter: SCAppConstants.DateFormatType.date)
            let startTime = selectedDate + "T\(selectedShift?.startTime?.toDateString(withFormatter: SCAppConstants.DateFormatType.time) ?? "")" + "Z"
            
            let endTime = selectedDate + "T\(selectedShift?.endTime?.toDateString(withFormatter: SCAppConstants.DateFormatType.time) ?? "")" + "Z"
            
            
            let groupSlots = getSlotGroupForDate(selectedDate: selectedDate, andShift: shift)
            
            let parameter: [String : Any] = ["date": date.toDateString(withFormatter: SCAppConstants.DateFormatType.slotAPI),
                                             "startTime": startTime,
                                             "endTime": endTime,
                                             "groups": groupSlots
            ]
            slotParameters.add(parameter)
        }
        
        let finalParameters: [String : Any] = ["hospitalId": hospitalId,
                                               "clinicId": clinicId,
                                               "masterShiftId": shift.id,
                                               "slotType":selectedSlotType,
                                               "slots": slotParameters]
        
        createSlotWithParameters(parameters: finalParameters)
        //totalSlotsLabel.text = "Time Slots: \(dailySlotsArray.count * calenderView.selectedDates.count)"
    }
    
    private func getSlotGroupForDate(selectedDate: String, andShift shift: Shift) -> [String] {
        let calendar = Calendar.current
        let slotDuration = UserProfile.getUserProfile()?.slotDuration ?? 10 // Default slot duration is 10 mins
        
        var dailySlotsArray = [String]()
        var currentDate = shift.startTime
        
        while currentDate! < shift.endTime! {
            if let date = calendar.date(byAdding: .minute, value: slotDuration, to: currentDate!) {
                let timeslot = selectedDate + "T\(date.toDateString(withFormatter: SCAppConstants.DateFormatType.time))" + "Z"
                dailySlotsArray.append(timeslot)
                currentDate = date
             }
         }
        return dailySlotsArray
     }
    
    private func createSlotWithParameters(parameters: [String : Any]) {
        SCProgressView.show()
        SCSlotService.createSlotAPI(withParameters: parameters, onComplete: {
            SCProgressView.hide()
             self.showAlert(title: SCAppConstants.pageTitles.addProcedureFees, msg: "Slots created successfully")
            self.navigationController?.popViewController(animated: true)
        }) { (errorMessage) in
            SCProgressView.hide()
            self.showAlert(title: SCAppConstants.pageTitles.addProcedureFees, msg: errorMessage)
           }
     }
    
    private func getSlotcountForShift() -> Int {
        guard let shift = selectedShift else {
            return 0
        }
        
        let calendar = Calendar.current
        let slotDuration = UserProfile.getUserProfile()?.slotDuration ?? 10 // Default slot duration is 10 mins
        
        var currentDate = shift.startTime
        var count = 0
        while currentDate! < shift.endTime! {
            if let date = calendar.date(byAdding: .minute, value: slotDuration, to: currentDate!) {
                count = count + 1
                currentDate = date
            }
        }
        return count
    }
}

extension SCSelectDatesViewController: CalendarViewDataSource, CalendarViewDelegate {
    func calendar(_ calendar: CalendarView, didScrollToMonth date: Date) {
        
    }
    
    func calendar(_ calendar: CalendarView, didSelectDate date: Date, withEvents events: [CalendarEvent]) {
        let slotCount = getSlotcountForShift() * calenderView.selectedDates.count
        totalSlotsLabel.text = "Time Slots: \(slotCount)"
    }
    
    func calendar(_ calendar: CalendarView, canSelectDate date: Date) -> Bool {
        return true
    }
    
    func calendar(_ calendar: CalendarView, didDeselectDate date: Date) {
        let slotCount = getSlotcountForShift() * (calenderView.selectedDates.count-1) // -1 is because this executes before the element is removed from selectedDatesArray
        totalSlotsLabel.text = "Time Slots: \(slotCount)"
    }
    
    func calendar(_ calendar: CalendarView, didLongPressDate date: Date) {
        
    }
    
    
    func startDate() -> Date {
        return Date()
    }
    
    func endDate() -> Date {
        var dateComponents = DateComponents()
        dateComponents.month = +12
        let today = Date()
        let threeMonthsAgo = calenderView.calendar.date(byAdding: dateComponents, to: today)
        return threeMonthsAgo ?? Date()
    }
}
