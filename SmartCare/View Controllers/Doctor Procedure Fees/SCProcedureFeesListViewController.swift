//
//  SCProcedureFeesListViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 15/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCProcedureFeesListViewController: UIViewController {

    @IBOutlet weak var procedureFeesItemsTable: UITableView!
    
    var procedureFeesList = [ProcedureFeeItem]()
    var fullScreenMessageView:FullScreenMessageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.procedureFees, isDetail: false)
        configureView()
        registerNibsForCells()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getProcedureFeesList()
    }
    
    private func configureView() {
        procedureFeesItemsTable.tableFooterView = UIView()
    }
    
    private func registerNibsForCells() {
        let nibName = UINib(nibName: SCAppConstants.nibNames.procedureFeesTableViewCell, bundle:nil)
        procedureFeesItemsTable.register(nibName, forCellReuseIdentifier: SCAppConstants.cellReuseIdentifiers.procedureFeesListItem)
    }
    
    private func getProcedureFeesList() {
        SCProgressView.show()
        SCProcedureFeesService.procedureFeesListAPI(onComplete: { [weak self] (procedureFeesList) in
            SCProgressView.hide()
            self?.fullScreenMessageView?.removeFromSuperview()
            if procedureFeesList.count == 0 {
                self?.fullScreenMessageView = self?.showFullScreenMessageOnView(parentView: self!.procedureFeesItemsTable, withMessage: "Currently there are no Hospital Requests available")
                self?.view.addSubview(self!.fullScreenMessageView!)
            }
            self?.procedureFeesList = procedureFeesList
            self?.procedureFeesItemsTable.reloadData()
        }) { (errorMessage) in
            SCProgressView.hide()
        }
    }
}

extension SCProcedureFeesListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return procedureFeesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SCAppConstants.cellReuseIdentifiers.procedureFeesListItem, for: indexPath as IndexPath) as! ProcedureFeesTableViewCell
        cell.selectionStyle = .none
        let currentItem = procedureFeesList[indexPath.row]
        cell.configureCell(WithProcedureFees: currentItem)
        return cell
    }
}
