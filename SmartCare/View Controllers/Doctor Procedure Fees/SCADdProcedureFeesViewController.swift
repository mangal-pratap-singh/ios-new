//
//  SCADdProcedureFeesViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 17/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class SCADdProcedureFeesViewController: UIViewController {

    private let hospitalPickerTitle = "Select Hospital"
    private let clinicPickerTitle = "Select Clinic"
    private let procedureTypePickerTitle = "Select Procedure Type"
    
    @IBOutlet weak var gstInclusiveButton: UIButton!
    @IBOutlet weak var hospitalNameTextField: UITextField!
    @IBOutlet weak var clinicNameTextField: UITextField!
    @IBOutlet weak var procedureTypeTextField: UITextField!
    @IBOutlet weak var procedureFeeTextField: UITextField!
    
    var hospitals = [Hospital]()
    var selectedHospitalIndex = -1
    var selectedclinicIndex = -1
    
    var procedureTypes = [ProcedureType]()
    var selectedProcedureTypeIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.addProcedureFees, isDetail: true)
        configureView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getAsocialtedHospitalList()
    }
    
    @IBAction func gstInclusiveButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func saveButtonAction(_ sender: UIButton) {
        if selectedHospitalIndex != -1 && selectedclinicIndex != -1 && selectedProcedureTypeIndex != -1 && !(procedureFeeTextField.text?.isEmpty)! && hospitals.count > 0, procedureTypes.count > 0 {
            saveProcedureFees()
        } else {
            showAlert(title: SCAppConstants.pageTitles.addProcedureFees, msg: SCAppConstants.validationMessages.allFieldsValidationMessage)
        }
    }
    
    private func configureView() {
        hospitalNameTextField.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        clinicNameTextField.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        procedureTypeTextField.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
    }
    
    private func getAsocialtedHospitalList(){
        SCCommanService.getAsociateHospitalListAPI(onComplete: { (hospitalList) in
            self.hospitals = hospitalList
        }) { (errorMessage) in
            print("Error Getting Hospital List - \(errorMessage)")
        }
    }
    
    private func getProcedureTypes() {
        let selectedHospitalId = hospitals[selectedHospitalIndex].id
        SCProcedureFeesService.procedureTypesListForHospitalAPI(hospitalId: "\(selectedHospitalId)", onComplete: { (procedureTypes) in
            self.procedureTypes = procedureTypes
        }) { (errorMessage) in
            print("Error Getting Procedure Types - \(errorMessage)")
        }   
    }
    
    private func saveProcedureFees() {
        
        let hospitalId = hospitals[selectedHospitalIndex].id
        let clinicId = hospitals[selectedHospitalIndex].clinics?[selectedclinicIndex].id ?? 0
        let procedureTypeId = procedureTypes[selectedProcedureTypeIndex].id ?? 0
        let inclusiveGST = gstInclusiveButton.isSelected

        let procedureFeeParameters:[String: Any] = [
            "hospitalId":hospitalId,
            "clinicId":clinicId,
            "procedureId":procedureTypeId,
            "charges":Int(procedureFeeTextField.text ?? "") ?? 0,
            "inclusiveGst":inclusiveGST
        ]
        
        SCProgressView.show()
        SCProcedureFeesService.createProcedureFeeAPI(parameters: procedureFeeParameters, onComplete: {
            SCProgressView.hide()
            self.navigationController?.popViewController(animated: true)
        }) { (errorMessage) in
            SCProgressView.hide()
            self.showAlert(title: SCAppConstants.pageTitles.addProcedureFees, msg: errorMessage)
        }
    }

    func showHospitalPicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: hospitalPickerTitle, rows: hospitals.map { $0.name ?? "" }, initialSelection: 0, doneBlock: { picker, selectedIndex, selectedValue in
            self.selectedHospitalIndex = selectedIndex
            
            // When you change hospital, you should also reset clinic and Procedure type
            self.selectedclinicIndex = -1
            self.clinicNameTextField.text = self.clinicPickerTitle
                
            self.selectedProcedureTypeIndex = -1
            self.procedureTypeTextField.text = self.procedureTypePickerTitle
            
            if let aValue = selectedValue {
                textField.text = aValue as? String
            }
            self.getProcedureTypes()
        }, cancel: { picker in
        }, origin: textField)
    }
    
    func showClinicPicker(textField:UITextField) {
        ActionSheetStringPicker.show(
            withTitle: clinicPickerTitle,
            rows: hospitals[selectedHospitalIndex].clinics?.map { $0.name ?? "" },
            initialSelection: 0,
            doneBlock: { picker, selectedIndex, selectedValue in
                self.selectedclinicIndex = selectedIndex
                if let aValue = selectedValue {
                    textField.text = aValue as? String
                }
            },
            cancel: { _ in },
            origin: textField)
    }
    
    func showProcedureTypePicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: procedureTypePickerTitle, rows: procedureTypes.map { $0.name }, initialSelection: 0, doneBlock: { picker, selectedIndex, selectedValue in
            self.selectedProcedureTypeIndex = selectedIndex
            if let aValue = selectedValue {
                textField.text = aValue as? String
            }
        }, cancel: { picker in
        }, origin: textField)
    }
}

extension SCADdProcedureFeesViewController{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if (textField == hospitalNameTextField) {
            showHospitalPicker(textField: textField)
            return false
        } else if (textField == clinicNameTextField) {
            showClinicPicker(textField: textField)
            return false
        } else if (textField == procedureTypeTextField) {
            showProcedureTypePicker(textField: textField)
            return false
        }
        return true
    }
}
