//
//  SCResetConfirmationViewController.swift
//  SmartCare
//
//  Created by synerzip on 02/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCResetConfirmationViewController: UIViewController {

    @IBOutlet weak var logoContainerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customizeNavigationItem(title: SCAppConstants.pageTitles.forgotPassword, isDetail: true)
        self.configureView()
    }
    
    private func configureView() {
        logoContainerView.layer.cornerRadius = logoContainerView.frame.width/2
        logoContainerView.layer.masksToBounds = true
        logoContainerView.layer.borderColor = UIColor.white.cgColor
        logoContainerView.layer.borderWidth = SCAppConstants.borderWidths.medium.rawValue
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
         self.navigationController?.popToRootViewController(animated: true)
    }
    
}
