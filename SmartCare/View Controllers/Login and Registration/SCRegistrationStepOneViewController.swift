//
//  SCRegistrationViewController.swift
//  SmartCare
//
//  Created by Prashant Pawar on 23/08/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import DLRadioButton
import ActionSheetPicker_3_0

class SCRegistrationStepOneViewController: UIViewController {
    
    private let datePickerTitle = "Select Date"
    
    @IBOutlet weak var profileImage: UIButton!
    @IBOutlet weak var btRadioMale: DLRadioButton!
    @IBOutlet weak var btRadioPatient: DLRadioButton!
    @IBOutlet weak var btRadioFemale: DLRadioButton!
    @IBOutlet weak var btRadioDoctor: DLRadioButton!
    @IBOutlet weak var tfDateOfBirth: UITextField!
    @IBOutlet weak var tfLastname: UITextField!
    @IBOutlet weak var tfUsername: UITextField!
    
    let picker = UIImagePickerController()
    var pickedImagePath: NSURL?
    var pickedImageData: NSData?
    var userInfo = UserInfo(userType: "", firstName: "", lastName: "", gender: "", dateOfBirth: "", emailId: "", countryCode: "", phoneNumber: "", password: "", confirmPasword: "", selectIdentityText: "" ,selectIdentityId: "" ,countryCodeId: "", identificationNumber: "" ,avtarUrl:"")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customizeNavigationItem(title: SCAppConstants.pageTitles.registration, isDetail: true)
        configureView()
    }

    func setRadioButton(name:String) {
        
        switch name {
        case SCAppConstants.radioButtonValues.patient.rawValue:
            if(self.btRadioPatient.isSelected == true) {
                self.userInfo.setUserType(userType:"P")
            }
        case SCAppConstants.radioButtonValues.doctor.rawValue:
            if(self.btRadioDoctor.isSelected == true) {
                self.userInfo.setUserType(userType:"D")
            }
        case SCAppConstants.radioButtonValues.male.rawValue:
            if(self.btRadioMale.isSelected == true) {
                self.userInfo.setGender(gender:"M")
            }
        case SCAppConstants.radioButtonValues.female.rawValue:
            if(self.btRadioFemale.isSelected == true) {
                self.userInfo.setGender(gender:"F")
            }
        default : break
        }
    }
    
    private func checkTextFiledValidations() -> Bool{
    
       if (self.btRadioDoctor.isSelected  == false && self.btRadioPatient.isSelected  == false) {
             self.showAlert(title:SCAppConstants.pageTitles.registration, msg:SCAppConstants.validationMessages.registrationUserTypeValidationMessage, style: .alert)
              return false
      }
       if (tfUsername.text?.isEmpty)! {
            self.showAlert(title:SCAppConstants.pageTitles.registration, msg:SCAppConstants.validationMessages.registrationFirstNameValidationMessage, style: .alert)
            return false
     }
       if (tfLastname.text?.isEmpty)! {
           self.showAlert(title:SCAppConstants.pageTitles.registration, msg:SCAppConstants.validationMessages.registrationLastNameValidationMessage, style: .alert)
           return false
         }
       if (tfDateOfBirth.text?.isEmpty)! {
           self.showAlert(title:SCAppConstants.pageTitles.registration, msg:SCAppConstants.validationMessages.registrationDateOfBirthValidationMessage, style: .alert)
            return false
         }
        return true
    }
    
    
    @IBAction func nextButtonAction(_ sender: Any) {
        
        if(self.checkTextFiledValidations()){
            self.setRadioButton(name: SCAppConstants.radioButtonValues.patient.rawValue)
            self.setRadioButton(name:SCAppConstants.radioButtonValues.doctor.rawValue)
            self.setRadioButton(name: SCAppConstants.radioButtonValues.male.rawValue)
            self.setRadioButton(name: SCAppConstants.radioButtonValues.female.rawValue)
            
            self.userInfo.setFirstName(fristName:tfUsername.text ?? "")
            self.userInfo.setLastName(lastName:tfLastname.text ?? "")
            self.userInfo.setDateOfBirth(dateOfBirth: tfDateOfBirth.text ?? "")
            
            if let registrationStepTwoVC = UIStoryboard(name: SCAppConstants.storyBoardName.login, bundle: nil).instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.registrationStepTwo) as? SCRegistrationStepTwoViewController {
                registrationStepTwoVC.userInfo = self.userInfo
                self.navigationController?.pushViewController(registrationStepTwoVC, animated: true)
            }
        }
    }
    
    @IBAction func uploadAvtarButtonAction(_ sender: Any) {
        present(picker, animated: true, completion: nil)
    }
    
    private func configureView() {
        picker.allowsEditing = false
        picker.delegate = self
        self.btRadioMale.isSelected  = true
        self.btRadioPatient.isSelected  = true
        profileImage.layer.cornerRadius = profileImage.frame.height / 2
        profileImage.clipsToBounds = true
        
        tfDateOfBirth.configureTextFieldLeftView(width: SCAppConstants.textFieldPadding.textFieldPaddingLeft.rawValue, borderColor: UIColor.white, borderWidth: SCAppConstants.borderWidths.textFiledBorderWidth.rawValue, cornerRadius: SCAppConstants.cornerRadiusValues.verySmall.rawValue)
        
        tfUsername.configureTextFieldLeftView(width:SCAppConstants.textFieldPadding.textFieldPaddingLeft.rawValue, borderColor: UIColor.white, borderWidth: SCAppConstants.borderWidths.textFiledBorderWidth.rawValue, cornerRadius: SCAppConstants.cornerRadiusValues.verySmall.rawValue)
        
         tfLastname.configureTextFieldLeftView(width: SCAppConstants.textFieldPadding.textFieldPaddingLeft.rawValue, borderColor: UIColor.white, borderWidth: SCAppConstants.borderWidths.textFiledBorderWidth.rawValue, cornerRadius: SCAppConstants.cornerRadiusValues.verySmall.rawValue)
    }
    
    func showDatePicker(textField:UITextField) {
        let datePicker = ActionSheetDatePicker(title: datePickerTitle, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let dateString = (value as! Date).toDateString(withFormatter: SCAppConstants.DateFormatType.date)
             textField.text = dateString
            return
        }, cancel: {
            ActionStringCancelBlock in return
        },
           origin: textField.superview)
        datePicker?.show()
    }
}

extension SCRegistrationStepOneViewController {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if (textField == tfDateOfBirth) {
            self.showDatePicker(textField: textField)
            return false
        }
        return true
    }
}

extension SCRegistrationStepOneViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)


        guard let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage else {
            return
        }
        
        SCProgressView.show()
        SCUserService.uploadAvatar(withImage: image, onComplete: { ( data ,success) in
            SCProgressView.hide()
            if success {
                self.userInfo.setAvtar(path: (data["tempPath"].string) ?? "")
            }
        }) { (errorMessage) in
            SCProgressView.hide()
            print("Failed -- \(errorMessage)")
        }
        profileImage.contentMode = .scaleAspectFit
        profileImage.setImage(image, for:[])
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
