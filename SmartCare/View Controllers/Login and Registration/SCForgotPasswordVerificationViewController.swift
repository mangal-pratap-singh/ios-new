//
//  SCForgotPasswordVerificationViewController.swift
//  SmartCare
//
//  Created by synerzip on 02/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCForgotPasswordVerificationViewController: UIViewController {
    @IBOutlet weak var tfVerificationCode: UITextField!
    @IBOutlet weak var logoContainerView: UIView!
    
    var email:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customizeNavigationItem(title: SCAppConstants.pageTitles.forgotPassword, isDetail: true)
        self.configureView()
    }
    
    private func configureView() {
        logoContainerView.layer.cornerRadius = logoContainerView.frame.width/2
        logoContainerView.layer.masksToBounds = true
        logoContainerView.layer.borderColor = UIColor.white.cgColor
        logoContainerView.layer.borderWidth = SCAppConstants.borderWidths.medium.rawValue
        tfVerificationCode.configureTextFieldLeftView(width: SCAppConstants.textFieldPadding.textFieldPaddingLeftSmall.rawValue, borderColor: UIColor.white, borderWidth: SCAppConstants.borderWidths.textFiledBorderWidth.rawValue, cornerRadius: SCAppConstants.cornerRadiusValues.verySmall.rawValue)
    }
    
    private func isValidInput() -> Bool {

        if (tfVerificationCode.text?.isEmpty)! {
            self.showAlert(title:SCAppConstants.pageTitles.forgotPassword, msg:SCAppConstants.validationMessages.loginVerificationCodeValidationMessage, style: .alert)
            return false
        }
        return true
    }
    
    @IBAction func resendButtonAction(_ sender: Any) {
        self.resendVerificationCode()
    }

    private func resendVerificationCode() {
        SCProgressView.show(withMessage: SCAppConstants.progressMessages.resendCodeVarification)
        var userCredential = ""
        if let userCredentialValue = UserProfile.getUserProfile()?.email{
            userCredential = userCredentialValue
        }
        let parameters = [ "userCredential":userCredential]
        SCPasswordService.verifyAccountAPI(withParameters: parameters, onComplete: { [] in
            SCProgressView.hide()
        }) { (errorMessage) in
            SCProgressView.hide()
            print("Resend Code verification Failed-- \(errorMessage)")
             self.showAlert(title:SCAppConstants.pageTitles.login, msg: errorMessage, style: .alert)
        }
    }
    
    private func verifyAccountWithVerificationCode() {
        SCProgressView.show(withMessage: SCAppConstants.progressMessages.sendingVerificationCode)
        let parameters = ["userCredential":self.email,
                          "verificationCode":tfVerificationCode.text ?? "",
                          "type":"forgot_password_code"
        ]
        SCPasswordService.codeVerificationAPI(withParameters: parameters as! [String : String], onComplete: { [weak self] in
            SCProgressView.hide()
            self?.gotoConfirmPasswordViewController()
        }) { (errorMessage) in
            SCProgressView.hide()
            print("Account Verification with Code Failed -- \(errorMessage)")
             self.showAlert(title:SCAppConstants.pageTitles.login, msg: errorMessage, style: .alert)
        }
    }
    
    func gotoConfirmPasswordViewController() {
        if let confirmPasswordVerificationVC = UIStoryboard(name: SCAppConstants.storyBoardName.login, bundle: nil).instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.confirmPasswordViewController) as? SCConfirmPasswordViewController {
            let  verificationCode = tfVerificationCode.text ?? ""
            confirmPasswordVerificationVC.email = self.email
            confirmPasswordVerificationVC.verificationCode = verificationCode
            self.navigationController?.pushViewController(confirmPasswordVerificationVC, animated: true)
        }
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
        if(self.isValidInput()){
             self.verifyAccountWithVerificationCode()
        }
    }

}
