//
//  SCPhonelVerificationViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 28/08/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCPhoneVerificationViewController: UIViewController {

    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var verificationCodeTextField: UITextField!
    @IBOutlet weak var logoContainerView: UIView!
    
    var currentPhoneNumber: String {
        if let userProfile = UserProfile.getUserProfile() {
            return userProfile.phone
        }
        return ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customizeNavigationItem(title: SCAppConstants.pageTitles.phoneVerification, isDetail: true )
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        phoneNumberTextField.text = currentPhoneNumber
        sendVerificationCodeToPhoneNumber()
    }
    
    @IBAction func resendVerificationCodeButtonAction(_ sender: UIButton) {
        sendVerificationCodeToPhoneNumber()
    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        if(self.checkTextFiledValidations()){
           doPhoneVerification()
        }
    }
    
    private func configureView() {
        logoContainerView.layer.cornerRadius = logoContainerView.frame.width/2
        logoContainerView.layer.masksToBounds = true
        logoContainerView.layer.borderColor = UIColor.white.cgColor
        logoContainerView.layer.borderWidth = SCAppConstants.borderWidths.medium.rawValue
        
        phoneNumberTextField.configureTextFieldLeftView(width: SCAppConstants.textFieldPadding.textFieldPaddingLeftSmall.rawValue, borderColor: UIColor.white, borderWidth: SCAppConstants.borderWidths.textFiledBorderWidth.rawValue, cornerRadius: SCAppConstants.cornerRadiusValues.verySmall.rawValue)
        
        verificationCodeTextField.configureTextFieldLeftView(width: SCAppConstants.textFieldPadding.textFieldPaddingLeftSmall.rawValue, borderColor: UIColor.white, borderWidth: SCAppConstants.borderWidths.textFiledBorderWidth.rawValue, cornerRadius: SCAppConstants.cornerRadiusValues.verySmall.rawValue)
    }
    
    private func checkTextFiledValidations() -> Bool{
    
        if ((phoneNumberTextField.text?.isValidPhone(phone:(phoneNumberTextField.text)!))! == false){
            self.showAlert(title:SCAppConstants.pageTitles.login, msg:SCAppConstants.validationMessages.registrationValidPhoneMessage, style: .alert)
            return false
        }
        if (verificationCodeTextField.text?.isEmpty)! {
            self.showAlert(title:SCAppConstants.pageTitles.login, msg:SCAppConstants.validationMessages.loginVerificationCodeValidationMessage, style: .alert)
            return false
        }
        return true
    }
    
    
    private func sendVerificationCodeToPhoneNumber() {
        SCProgressView.show(withMessage: SCAppConstants.progressMessages.sendingVerificationCode)
        let parameters = ["userId":UserProfile.getUserId(),
                          "phoneNo":currentPhoneNumber,
                          "countryCode":UserProfile.getUserProfile()?.countryCode ?? "",
                          ]
        
        SCUserService.sendVerificationCodeAPI(withParameters: parameters, onComplete: {
            SCProgressView.hide()
        }) { (errorMessage) in
            SCProgressView.hide()
            print("Verification Code sendiing Failed -- \(errorMessage)")
            
             self.showAlert(title:SCAppConstants.pageTitles.login, msg: errorMessage, style: .alert)
        }
    }
    
    private func doPhoneVerification() {
        SCProgressView.show(withMessage: SCAppConstants.progressMessages.verifyingPhone)
        let parameters = ["userId":UserProfile.getUserId(),
                          "verificationCode":verificationCodeTextField.text ?? "",
                          "type":"phone_verification_code"
        ]
        
        SCUserService.verifyContactAPI(withParameters: parameters, onComplete: {
            SCProgressView.hide()
            SCSessionManager.sharedInstance.loginComplete()
            SCRoutingManager.sharedInstance.showHomeDashboard()
        }) { (errorMessage) in
            SCProgressView.hide()
            print("Phone number not Verified -- \(errorMessage)")
             self.showAlert(title:SCAppConstants.pageTitles.login, msg: errorMessage, style: .alert)
        }
    }
}
