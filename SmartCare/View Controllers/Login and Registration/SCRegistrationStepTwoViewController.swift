//
//  RegistrationFinalViewController.swift
//  SmartCare
//
//  Created by Prashant Pawar on 23/08/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SwiftyJSON

class SCRegistrationStepTwoViewController: UIViewController {
    
    private let countryCodePickerTitle = "Select Country Code"
    private let identityPickerTitle = "Select Identity"
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfIdentificationNumber: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    @IBOutlet weak var tfSelectSequrity: UITextField!
    @IBOutlet weak var tfPhoneNumner: UITextField!
    @IBOutlet weak var tfPinCode: UITextField!
    
    var countryCodeArray = [String]()
    var countryCodeIDArray = [String]()
    var identityCodeArray = [String]()
    var identityIDCodeArray = [Int]()
    var selectedIdentityIndex = 0
    var selectedIdCountryIndex = 0
    var userInfo:UserInfo?
    var selectedCountryCodePickerIndex = 0
    var countryCodePickerDataSource = [String]()
    var idnetityPickerDataSource = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customizeNavigationItem(title: SCAppConstants.pageTitles.registration, isDetail: true)
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getCountryCode()
        getIdnetity()
    }
    
    private func configureView() {
        tfEmail.configureTextFieldLeftView(width: SCAppConstants.textFieldPadding.textFieldPaddingLeft.rawValue, borderColor: UIColor.white, borderWidth: SCAppConstants.borderWidths.textFiledBorderWidth.rawValue, cornerRadius: SCAppConstants.cornerRadiusValues.verySmall.rawValue)
        tfPassword.configureTextFieldLeftView(width: SCAppConstants.textFieldPadding.textFieldPaddingLeft.rawValue, borderColor: UIColor.white, borderWidth: SCAppConstants.borderWidths.textFiledBorderWidth.rawValue, cornerRadius: SCAppConstants.cornerRadiusValues.verySmall.rawValue)
        tfIdentificationNumber.configureTextFieldLeftView(width: SCAppConstants.textFieldPadding.textFieldPaddingLeft.rawValue, borderColor: UIColor.white, borderWidth: SCAppConstants.borderWidths.textFiledBorderWidth.rawValue, cornerRadius: SCAppConstants.cornerRadiusValues.verySmall.rawValue)
        tfConfirmPassword.configureTextFieldLeftView(width: SCAppConstants.textFieldPadding.textFieldPaddingLeft.rawValue, borderColor: UIColor.white, borderWidth: SCAppConstants.borderWidths.textFiledBorderWidth.rawValue, cornerRadius: SCAppConstants.cornerRadiusValues.verySmall.rawValue)
        tfSelectSequrity.configureTextFieldLeftView(width: SCAppConstants.textFieldPadding.textFieldPaddingLeft.rawValue, borderColor: UIColor.white, borderWidth: SCAppConstants.borderWidths.textFiledBorderWidth.rawValue, cornerRadius: SCAppConstants.cornerRadiusValues.verySmall.rawValue)
        tfPinCode.configureTextFieldLeftView(width: SCAppConstants.textFieldPadding.textFieldPaddingLeftSmall.rawValue, borderColor: UIColor.white, borderWidth: SCAppConstants.borderWidths.zeroBorderWidth.rawValue, cornerRadius:  SCAppConstants.cornerRadiusValues.zeroCornerRadus.rawValue)
        tfPhoneNumner.configureTextFieldLeftView(width: SCAppConstants.textFieldPadding.textFieldPaddingLeftSmall.rawValue, borderColor: UIColor.clear, borderWidth: SCAppConstants.borderWidths.zeroBorderWidth.rawValue, cornerRadius: SCAppConstants.cornerRadiusValues.zeroCornerRadus.rawValue)
        
        tfPinCode.SetRightSideImage(with: SCAppConstants.Image.downArrow.image(), tintColor: UIColor.SCTextFilePlaceHolderTextColor(), leftPadding: 13)
        tfSelectSequrity.SetRightSideImage(with: SCAppConstants.Image.downArrow.image(), tintColor: UIColor.SCTextFilePlaceHolderTextColor())
        
    }
    
    
    private func checkTextFiledValidations() -> Bool {
        
        if ((tfEmail.text?.isValidEmail(candidate: (tfEmail.text)!))! == false){
            self.showAlert(title:SCAppConstants.pageTitles.registration, msg:SCAppConstants.validationMessages.registrationValidEmailValidationMessage, style: .alert)
            return false
        }
        if (tfPinCode.text?.isEmpty)! {
            self.showAlert(title:SCAppConstants.pageTitles.registration, msg:SCAppConstants.validationMessages.registrationCountryCodeValidationMessage, style: .alert)
            return false
        }
        if ((tfPhoneNumner.text?.isValidPhone(phone:tfPhoneNumner.text!))! == false) {
            self.showAlert(title:SCAppConstants.pageTitles.registration, msg:SCAppConstants.validationMessages.registrationValidPhoneMessage, style: .alert)
            return false
        }
        if (tfPassword.text?.isEmpty)! {
            self.showAlert(title:SCAppConstants.pageTitles.registration, msg:SCAppConstants.validationMessages.registrationPasswordValidationMessage, style: .alert)
            return false
        }
        if (tfConfirmPassword.text?.isEmpty)! {
            self.showAlert(title:SCAppConstants.pageTitles.registration, msg:SCAppConstants.validationMessages.registrationConfirmPasswordValidationMessage, style: .alert)
            return false
        }
        if (tfSelectSequrity.text?.isEmpty)! {
            self.showAlert(title:SCAppConstants.pageTitles.registration, msg:SCAppConstants.validationMessages.registrationSelectIdentityValidationMessage, style: .alert)
            return false
        }
//        if (tfIdentificationNumber.text?.isEmpty)! {
//            self.showAlert(title:SCAppConstants.pageTitles.registration, msg:SCAppConstants.validationMessages.registrationIdentificationValidationMessage, style: .alert)
//            return false
//        }
        
        return true
        
    }
    
    
    @IBAction func registerButtonAction(_ sender: Any) {
        if(self.checkTextFiledValidations()){
        self.userInfo?.setEmailId(emailId: tfEmail.text ?? "")
        self.userInfo?.setCountryCode(countryCode:tfPinCode.text ?? "")
        self.userInfo?.setPhoneNumber(phoneNumber:tfPhoneNumner.text ?? "")
        self.userInfo?.setPassword(password: tfPassword.text?.base64EncodedString() ?? "")
        self.userInfo?.setConfirmPasword(confirmPasword: tfConfirmPassword.text?.base64EncodedString() ?? "")
        self.userInfo?.setIdentityText(selectIdentityText: tfSelectSequrity.text ?? "")
        let identityId = self.identityIDCodeArray[selectedIdentityIndex]
        self.userInfo?.setIdentityId(selectIdentityId: String(identityId))
         self.userInfo?.setCountryCodeId(countryCodeId: self.countryCodeIDArray[selectedIdCountryIndex])
        self.userInfo?.setIdentificationNumber(identificationNumber: tfIdentificationNumber.text ?? "")
          SCProgressView.show()
        SCUserService.registrationAPI(withUserInfo: self.userInfo!, onComplete: { (response,success) in
            if success {
                SCProgressView.hide()
                self.navigateToRootController(message: response["message"].string!)
            }
        }) { (errorMessage) in
              SCProgressView.hide()
             self.showAlert(title:SCAppConstants.pageTitles.registration, msg:errorMessage, style: .alert)
        }
      }
    }
    
    func navigateToRootController(message:String) {
        let alertController = UIAlertController(title:SCAppConstants.pageTitles.registration, message:message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    func  getCountryCode() {
        SCUserService.getCountryCodeAPI(onComplete: { (countryList ,success) in
            if success {
                self.processCountryCodeResponse(responseJSON: countryList["data"])
            }
        }) { (errorMessage) in
            print("Failed -- \(errorMessage)")
        }
    }
    
    func  getIdnetity() {
        SCUserService.getIdentityAPI(onComplete: { (identityList ,success) in
            if success {
                self.processIdentityResponse(responseJSON: identityList["data"])
            }
        }) { (errorMessage) in
            print("Failed -- \(errorMessage)")
        }
    }
    
    private func processCountryCodeResponse(responseJSON: JSON)  -> Void {
        for (_, countryCodeJSON): (String, JSON) in responseJSON {
            let countryCodeName = "+\(countryCodeJSON["label"].string!) \(countryCodeJSON["value"].string!)"
              self.countryCodeArray.append(countryCodeName)
              self.countryCodeIDArray.append(countryCodeJSON["label"].string!)
        }
        self.createCountryCodePickerDataSource()
        tfPinCode.text = self.countryCodeArray[0]
    }
    
    private func processIdentityResponse(responseJSON: JSON)  -> Void {
        for (_, identityCodeJSON): (String, JSON) in responseJSON {
            self.identityCodeArray.append(identityCodeJSON["name"].string!)
            self.identityIDCodeArray.append(identityCodeJSON["id"].int!)
        }
        self.createIdnetityPickerDataSource()
       // tfSelectSequrity.text = self.identityIDCodeArray[0] as String
    }
    
    private func createCountryCodePickerDataSource() {
        countryCodePickerDataSource.append(countryCodePickerTitle)
        for countryCode in self.countryCodeArray {
            countryCodePickerDataSource.append(countryCode)
        }
    }
    
    private func createIdnetityPickerDataSource() {
        idnetityPickerDataSource.append(identityPickerTitle)
        for identityCode in self.identityCodeArray {
            idnetityPickerDataSource.append(identityCode)
        }
    }
    
    func showSelectIdnetityPicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: identityPickerTitle, rows: idnetityPickerDataSource, initialSelection: self.selectedIdentityIndex, doneBlock: { picker, selectedIndex, selectedValue in
            self.selectedIdentityIndex = selectedIndex-1
            if let aValue = selectedValue {
                 textField.text = aValue as? String
            }
        }, cancel:nil, origin: textField)
    }
   
    func showCountryCodePicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: countryCodePickerTitle, rows: countryCodePickerDataSource, initialSelection: self.selectedIdCountryIndex, doneBlock: { picker, selectedIndex, selectedValue in
            self.selectedIdCountryIndex = selectedIndex-1
            if let aValue = selectedValue {
                textField.text = aValue as? String
            }
        }, cancel: nil, origin: textField)
    }
}

extension SCRegistrationStepTwoViewController {

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if (textField == tfSelectSequrity) {
            self.showSelectIdnetityPicker(textField: textField)
            return false
        } else if (textField == tfPinCode) {
            self.showCountryCodePicker(textField: textField)
            return false
        }
        return true
    }
}


