//
//  SCConfirmPasswordViewController.swift
//  SmartCare
//
//  Created by synerzip on 02/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCConfirmPasswordViewController: UIViewController {
    @IBOutlet weak var logoContentView: UIView!
    @IBOutlet weak var tfNewPassword: UITextField!
    
    @IBOutlet weak var tfConfirmPassword: UITextField!
    var verificationCode = ""
    var email:String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.customizeNavigationItem(title: SCAppConstants.pageTitles.forgotPassword, isDetail: true)
        self.configureView()
    }
    
    private func configureView() {
        logoContentView.layer.cornerRadius = logoContentView.frame.width/2
        logoContentView.layer.masksToBounds = true
        logoContentView.layer.borderColor = UIColor.white.cgColor
        logoContentView.layer.borderWidth = SCAppConstants.borderWidths.medium.rawValue
        tfNewPassword.isSecureTextEntry = true
        tfConfirmPassword.isSecureTextEntry = true
        tfConfirmPassword.configureTextFieldLeftView(width: SCAppConstants.textFieldPadding.textFieldPaddingLeftSmall.rawValue, borderColor: UIColor.white, borderWidth: SCAppConstants.borderWidths.textFiledBorderWidth.rawValue, cornerRadius: SCAppConstants.cornerRadiusValues.verySmall.rawValue)
         tfNewPassword.configureTextFieldLeftView(width: SCAppConstants.textFieldPadding.textFieldPaddingLeftSmall.rawValue, borderColor: UIColor.white, borderWidth: SCAppConstants.borderWidths.textFiledBorderWidth.rawValue, cornerRadius: SCAppConstants.cornerRadiusValues.verySmall.rawValue)
    }
    
    private func isValidInput() -> Bool {
        if (tfNewPassword.text?.isEmpty)! {
            self.showAlert(title:SCAppConstants.pageTitles.forgotPassword, msg:SCAppConstants.validationMessages.loginVerificationCodeValidationMessage, style: .alert)
            return false
        }
        if (tfConfirmPassword.text?.isEmpty)! {
            self.showAlert(title:SCAppConstants.pageTitles.forgotPassword, msg:SCAppConstants.validationMessages.forgotPasswordConfirmPasswordValidationMessage, style: .alert)
            return false
        }
        return true
    }
    
    private func doConfirmPassword() {
        SCProgressView.show(withMessage: SCAppConstants.progressMessages.changingPasswordCode)
        let parameters = ["userCredential":self.email,
        "verificationCode":verificationCode,
        "newPassword":tfNewPassword.text?.base64EncodedString() ?? "",
        ]
        SCPasswordService.passwordChangeAPI(withParameters: parameters as! [String : String], onComplete: { [weak self] in
            SCProgressView.hide()
            self?.goToPasswordChangeSuccessController()
        }) { (errorMessage) in
            SCProgressView.hide()
            print("Change Password Failed -- \(errorMessage)")
             self.showAlert(title:SCAppConstants.pageTitles.login, msg: errorMessage, style: .alert)
        }
    }
    
    func goToPasswordChangeSuccessController() {
        if let resetConfirmPasswordSuccessVC = UIStoryboard(name: SCAppConstants.storyBoardName.login, bundle: nil).instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.resetConfirmPasswordViewController) as? SCResetConfirmationViewController {
            self.navigationController?.pushViewController(resetConfirmPasswordSuccessVC, animated: true)
        }
    }
    
    @IBAction func resetPasswordBtnAction(_ sender: Any) {
        if(self.isValidInput()){
             self.doConfirmPassword()
        }
    }
}
