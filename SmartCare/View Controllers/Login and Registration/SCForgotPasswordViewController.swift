//
//  SCEmailForgotPasswordViewController.swift
//  SmartCare
//
//  Created by synerzip on 01/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCForgotPasswordViewController: UIViewController {

    @IBOutlet weak var tfemailForgotPassword: UITextField!
    @IBOutlet weak var imageView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customizeNavigationItem(title: SCAppConstants.pageTitles.forgotPassword, isDetail: true)
        self.configureView()
    }
    
    private func doEmailVerification() {
        SCProgressView.show(withMessage: SCAppConstants.progressMessages.verifyingAccountEmail)
        let parameters = [ "userCredential":tfemailForgotPassword.text ?? ""]
        
        SCPasswordService.verifyAccountAPI(withParameters: parameters, onComplete: { [weak self] in
            SCProgressView.hide()
            self?.loadForgotPasswordVerificationScreen()
        }) { (errorMessage) in
            SCProgressView.hide()
            print("Veraification Code send to Email Failed -- \(errorMessage)")
             self.showAlert(title:SCAppConstants.pageTitles.forgotPassword, msg:errorMessage, style: .alert)
        }
    }

    func loadForgotPasswordVerificationScreen() {
            if let forgotPasswordVerificationVC = UIStoryboard(name: SCAppConstants.storyBoardName.login, bundle: nil).instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.forgotPasswordVerificationScreen) as? SCForgotPasswordVerificationViewController {
                 forgotPasswordVerificationVC.email = tfemailForgotPassword.text ?? ""
                self.navigationController?.pushViewController(forgotPasswordVerificationVC, animated: true)
            }
    }

    @IBAction func nextButtonAction(_ sender: Any) {
        doEmailVerification()
    }
    
    private func configureView() {
        imageView.layer.cornerRadius = imageView.frame.width/2
        imageView.layer.masksToBounds = true
        imageView.layer.borderColor = UIColor.white.cgColor
        imageView.layer.borderWidth = SCAppConstants.borderWidths.medium.rawValue
        
        tfemailForgotPassword.configureTextFieldLeftView(width: SCAppConstants.textFieldPadding.textFieldPaddingLeftSmall.rawValue, borderColor: UIColor.white, borderWidth: SCAppConstants.borderWidths.textFiledBorderWidth.rawValue, cornerRadius: SCAppConstants.cornerRadiusValues.verySmall.rawValue)
    }
    
    private func isValidInput() -> Bool {
        if ((tfemailForgotPassword.text?.isValidEmail(candidate: (tfemailForgotPassword.text)!))! == false){
            self.showAlert(title:SCAppConstants.pageTitles.forgotPassword, msg:SCAppConstants.validationMessages.registrationValidEmailValidationMessage, style: .alert)
            return false
        }
        return true
    }

}
