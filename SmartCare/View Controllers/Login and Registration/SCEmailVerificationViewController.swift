//
//  SCEmailVerificationViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 28/08/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCEmailVerificationViewController: UIViewController {

    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var emailidTextField: UITextField!
    @IBOutlet weak var verificationCodeTextField: UITextField!
    @IBOutlet weak var logoContainerView: UIView!
    
    var currentEmailId: String {
        if let userProfile = UserProfile.getUserProfile() {
            return userProfile.email
        }
        return ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customizeNavigationItem(title: SCAppConstants.pageTitles.emailVerification, isDetail: true)
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        emailidTextField.text = currentEmailId
        sendVerificationCodeToEmailId()
    }
    
    @IBAction func resendVerificationCodeButtonAction(_ sender: UIButton) {
        sendVerificationCodeToEmailId()
    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        if(self.checkTextFiledValidations()){
             doEmailVerification()
        }
    }
    
    private func configureView() {
        logoContainerView.layer.cornerRadius = logoContainerView.frame.width/2
        logoContainerView.layer.masksToBounds = true
        logoContainerView.layer.borderColor = UIColor.white.cgColor
        logoContainerView.layer.borderWidth = SCAppConstants.borderWidths.medium.rawValue
        
        emailidTextField.configureTextFieldLeftView(width: SCAppConstants.textFieldPadding.textFieldPaddingLeftSmall.rawValue, borderColor: UIColor.white, borderWidth: SCAppConstants.borderWidths.textFiledBorderWidth.rawValue, cornerRadius: SCAppConstants.cornerRadiusValues.verySmall.rawValue)
    
        verificationCodeTextField.configureTextFieldLeftView(width: SCAppConstants.textFieldPadding.textFieldPaddingLeftSmall.rawValue, borderColor: UIColor.white, borderWidth: SCAppConstants.borderWidths.textFiledBorderWidth.rawValue, cornerRadius: SCAppConstants.cornerRadiusValues.verySmall.rawValue)
    }
    
    private func checkTextFiledValidations() -> Bool{
        
        if ((emailidTextField.text?.isValidEmail(candidate: (emailidTextField.text)!))! == false){
            self.showAlert(title:SCAppConstants.pageTitles.login, msg:SCAppConstants.validationMessages.registrationValidEmailValidationMessage, style: .alert)
            return false
        }
        if (verificationCodeTextField.text?.isEmpty)! {
            self.showAlert(title:SCAppConstants.pageTitles.login, msg:SCAppConstants.validationMessages.loginVerificationCodeValidationMessage, style: .alert)
            return false
        }
        return true
    }
    
    private func sendVerificationCodeToEmailId() {
        SCProgressView.show(withMessage: SCAppConstants.progressMessages.sendingVerificationCode)
        let parameters = ["userId":UserProfile.getUserId(),
                          "emailId":currentEmailId,
                         ]
        
        SCUserService.sendVerificationCodeAPI(withParameters: parameters, onComplete: {
            SCProgressView.hide()
        }) { (errorMessage) in
            SCProgressView.hide()
            print("Verification Code sendiing Failed -- \(errorMessage)")
        }
    }
    
    private func doEmailVerification() {
        SCProgressView.show(withMessage: SCAppConstants.progressMessages.verifyingEmail)
        let parameters = ["userId":UserProfile.getUserId(),
                          "verificationCode":verificationCodeTextField.text ?? "",
                          "type":"email_verification_code"
                          ]
        
        SCUserService.verifyContactAPI(withParameters: parameters, onComplete: { [weak self] in
            SCProgressView.hide()
            self?.loadPhoneVerificationScreen()
        }) { (errorMessage) in
            SCProgressView.hide()
            print("Email not Verified -- \(errorMessage)")
            self.showAlert(title:SCAppConstants.pageTitles.emailVerify, msg:errorMessage, style: .alert)
        }
    }
    
    private func loadPhoneVerificationScreen() {
        if let phoneVerificationVC = UIStoryboard(name: SCAppConstants.storyBoardName.login, bundle: nil).instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.phoneVerification) as? SCPhoneVerificationViewController {
        self.navigationController?.pushViewController(phoneVerificationVC, animated: true)
            
        }
    }
}
