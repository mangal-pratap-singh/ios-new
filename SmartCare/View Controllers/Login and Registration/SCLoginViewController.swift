//
//  ViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 20/08/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import RealmSwift

class SCLoginViewController: UIViewController {

    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var emailidTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var iconClick = true
    var deviceTokenGenerated : String!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.emailidTextField.delegate = self
        self.passwordTextField.delegate = self
        self.customizeNavigationItem(isDetail: false)
passwordView.layer.cornerRadius = 5
        passwordView.layer.borderColor = UIColor.white.cgColor
        passwordView.layer.borderWidth = 1
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        addKeyboardObserver()
    }
    @IBOutlet weak var eyeButton: UIButton!
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
        removeKeyboardObserver()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
        
    }
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//     self.view.endEditing(true)
//        super.touchesBegan(touches, with: event)
//
//    }
    @IBAction func passwordTapped(_ sender: Any) {

        eyeButton.isSelected = !eyeButton.isSelected
        if eyeButton.isSelected {
            passwordTextField.isSecureTextEntry = false
        }
        else{
           passwordTextField.isSecureTextEntry = true
        }
    }
    
    @IBAction func loginButtonAction(sender: UIButton) {
        
        if (emailidTextField.text?.isEmpty)! {
            showAlert(title:SCAppConstants.pageTitles.login, msg:SCAppConstants.validationMessages.loginUserNameValidationMessage, style: .alert)
            return
        }
        if (passwordTextField.text?.isEmpty)! {
            showAlert(title:SCAppConstants.pageTitles.login, msg:SCAppConstants.validationMessages.loginPassswordValidationMessage, style: .alert)
                 return
        }
        
        //Valid Credentials, go for API Call
        //                   let deviceToken = deviceTokenGenerated ,

        if let userName = emailidTextField.text,
            let password = passwordTextField.text,
            !userName.isEmpty && !password.isEmpty,
            let encodedPassword = password.base64EncodedString(){
            SCProgressView.show()
            SCSessionManager.sharedInstance.login(withUsername: userName, password: encodedPassword, onSuccess: {
                SCProgressView.hide()
                self.decideNextScreenToShow()
            }) { (errorMessage) in
                SCProgressView.hide()
                self.showAlert(title: SCAppConstants.pageTitles.login, msg: errorMessage)
            }
        }
        else {
            print("No Details Available")
        }
    }
    
    @IBAction func registerButtonAction(sender: UIButton) {
        if let registerStepOneVC = UIStoryboard(name: SCAppConstants.storyBoardName.login, bundle: nil).instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.registrationStepOne) as? SCRegistrationStepOneViewController {
            self.navigationController?.pushViewController(registerStepOneVC, animated: true)
        }
    }
    
    @IBAction func forgotPasswordButtonAction(sender: UIButton) {
        if let forgotPasswordVC = UIStoryboard(name: SCAppConstants.storyBoardName.login, bundle: nil).instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.forgotPasswordEmailScreen) as? SCForgotPasswordViewController {
            self.navigationController?.pushViewController(forgotPasswordVC, animated: true)
        }
    }
    
    private func configureView() {
        emailidTextField.setPaddingWithImage(image: SCAppConstants.Image.whiteEmail.image())
        passwordTextField.setPaddingWithImage(image: SCAppConstants.Image.whitePassword.image())
    }
    
    
    private func decideNextScreenToShow() {
        /*
        do {
            let realm = try Realm()
            if let userProfile = realm.objects(UserProfile.self).first, userProfile.isEmailVerified == false {
                loadEmailVerificationScreen()
            } else if let userProfile = realm.objects(UserProfile.self).first, userProfile.isPhoneVerified == false {
                loadPhoneVerificationScreen()
            } else {
                SCSessionManager.sharedInstance.loginComplete()
                SCRoutingManager.sharedInstance.showHomeDashboard()
            }
        } catch let error as NSError {
            print("Realm Error :: Retriving User Profile -- \(error.localizedDescription)")
             self.showAlert(title: SCAppConstants.pageTitles.login, msg: error.description)
        }
        */
        
        do {
            let realm = try Realm()
            if let userProfile = realm.objects(UserProfile.self).first, userProfile.isEmailVerified == false {
                loadEmailVerificationScreen()
            }else {
                SCSessionManager.sharedInstance.loginComplete()
                SCRoutingManager.sharedInstance.showHomeDashboard()
            }
        } catch let error as NSError {
            print("Realm Error :: Retriving User Profile -- \(error.localizedDescription)")
             self.showAlert(title: SCAppConstants.pageTitles.login, msg: error.description)
        }
    }
    
    private func loadEmailVerificationScreen() {
        if let emailVerificationVC = UIStoryboard(name: SCAppConstants.storyBoardName.login, bundle: nil).instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.emailVerification) as? SCEmailVerificationViewController {
            self.navigationController?.pushViewController(emailVerificationVC, animated: true)
        }
    }
    
    private func loadPhoneVerificationScreen() {
        if let phoneVerificationVC = UIStoryboard(name: SCAppConstants.storyBoardName.login, bundle: nil).instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.phoneVerification) as? SCPhoneVerificationViewController {
            self.navigationController?.pushViewController(phoneVerificationVC, animated: true)
        }
    }
    
    func addLeftImageTo(txtField: UITextField, andImage img: UIImage) {
        let RightImageView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: img.size.width, height: img.size.height))
        RightImageView.image = img
        passwordTextField.rightView = RightImageView
        passwordTextField.rightViewMode = .always
    }
}

