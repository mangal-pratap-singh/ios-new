//
//  SCLogoutViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 07/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCLogoutViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = SCAppConstants.pageTitles.logout
    }
    
    @IBAction func logoutButtonAction(_ sender: UIButton) {
        SCSessionManager.sharedInstance.logout()
      //  SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .login)
       
        self.dismiss(animated: true) {
            SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .login)
        }
    }
    
    @IBAction func backBtnClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
}
}
