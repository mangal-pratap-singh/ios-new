//
//  PMedicinesCell.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 25/09/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class PMedicinesCell: UITableViewCell {
    
    @IBOutlet weak var lbl_name: UILabel!
    
    @IBOutlet weak var lbl_morning: UILabel!
    
    @IBOutlet weak var lbl_after: UILabel!
    
    @IBOutlet weak var lbl_evening: UILabel!
    
    @IBOutlet weak var lbl_night: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
