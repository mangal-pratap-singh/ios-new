//
//  PatientConsultNoteCell.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 20/09/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
protocol cellViewetailDelegate {
    func cellDelegateViewDetailClick(index: Int)
}
class PatientConsultNoteCell: UITableViewCell {

    var delegateCell: cellViewetailDelegate?
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var lbl_doctorName: UILabel!
    
    @IBOutlet weak var lbl_patientName: UILabel!
    
    
    @IBOutlet weak var lbl_date: UILabel!
    
    @IBOutlet weak var btn_viewDetail: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
//cornerRadius
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func viewDetailBtnClick(_ sender: UIButton) {
        delegateCell?.cellDelegateViewDetailClick(index: sender.tag)
    }
    

}
