//
//  SCPatientConsultationNotesViewController.swift
//  SmartCare
//
//  Created by Rediflex PvtLtd on 01/08/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCPatientConsultationNotesViewController: UIViewController,cellViewetailDelegate {
    private let noOfSecondForWeak = 1
    private let weakDisplayDateFormat = "MMM dd, yyyy"
    var fullScreenMessageView:FullScreenMessageView?
    var patientConsultNote:PatientConsultNote?
    var displayDate = Date()

    @IBOutlet weak var tblView_patient: UITableView!
    @IBOutlet weak var noConsultationslText: UILabel!
    @IBOutlet weak var datesView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblView_patient.dataSource = self
        self.tblView_patient.delegate = self
//         customizeNavigationItem(title: SCAppConstants.pageTitles.appointmentDetails, isDetail: true)
        customizeNavigationItem(title: SCAppConstants.pageTitles.consultationNotes, isDetail: false)
        configureView()
//        CompletedAppointmentList.isHidden = true
//        self.fullScreenMessageView?.removeFromSuperview()
//        self.fullScreenMessageView = self.showFullScreenMessageOnView(parentView: self.CompletedAppointmentList, withMessage: "No appointments for selected period")
//        self.view.addSubview(self.fullScreenMessageView!)
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tblView_patient.isHidden = true
        self.fullScreenMessageView?.removeFromSuperview()
        self.fullScreenMessageView = self.showFullScreenMessageOnView(parentView: self.view, withMessage: "No appointments for selected period")
        fullScreenMessageView?.center = self.view.center
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Disable Drawer for Detailed Pages
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = false
        getAppointments()
    }
//
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = true
//        getAppointments()
//    }
    
    private func configureView() {
        showWeakDateStringFromDate()
       // CompletedAppointmentList.tableFooterView = UIView()
    }
    
    
    @IBAction func backDateTapped(_ sender: Any) {
        let previousDate = Calendar.current.date(byAdding: .day, value: -noOfSecondForWeak, to: displayDate)!
        displayDate = previousDate
        showWeakDateStringFromDate()
        getAppointments()
    }
    
    @IBAction func futureDatesTapped(_ sender: Any) {
        let nextDate = Calendar.current.date(byAdding: .day, value: noOfSecondForWeak, to: displayDate)!
        displayDate = nextDate
        showWeakDateStringFromDate()
        getAppointments()
    }
    
    private func getAppointments(){
           SCProgressView.show()
       print("get user id====\(UserProfile.getUserId())")
        SCConsultationNoteService.getConsultationNoteListAPI(withParameters: ["date": displayDate], onComplete: { (response, result) in
             SCProgressView.hide()
            self.fullScreenMessageView?.removeFromSuperview()
            print("patien consultation notes list==\(response)")
            self.patientConsultNote = PatientConsultNote(jsonData: response)
            DispatchQueue.main.async {
                self.tblView_patient.isHidden = false
                self.tblView_patient.reloadData()
                
            }
            
        }) { (errorMessage) in
             SCProgressView.hide()
             self.view.addSubview(self.fullScreenMessageView!)
            
        }
    }
    
    private func showWeakDateStringFromDate() {
//        let startWeakDate = (displayDate.startOfWeek).toDateString(withFormatter: weakDisplayDateFormat)
//        let endWeakDate = (displayDate.endOfWeek).toDateString(withFormatter: weakDisplayDateFormat)
//        let weakNumber = NSCalendar.current.component(.weekOfYear, from: displayDate)
        let currentDate = displayDate.toDateString(withFormatter: weakDisplayDateFormat)
        dateLabel.text = " \(currentDate) "
    }
}


extension  SCPatientConsultationNotesViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patientConsultNote?.patientConsultNoteList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 240
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell:PatientConsultNoteCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath as IndexPath) as! PatientConsultNoteCell
        cell.btn_viewDetail.tag = indexPath.row
        cell.delegateCell = self
        let patientConsultData = patientConsultNote?.patientConsultNoteList?[indexPath.row]
        cell.lbl_doctorName.text = patientConsultData?.doctorNote?.name
        cell.lbl_patientName.text = patientConsultData?.patientNote?.name
        let startDate = patientConsultData?.appointmentNote?.startTime?.toDate()
        cell.lbl_date.text = startDate?.toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentListItemStart)
        
       // cell.lbl_date.text = patientConsultData?.patientNote?.address.da
        if let imageBaseURL = AppInfo.getAppInfo()?.s3Url, let avtarImageURL = patientConsultData?.doctorNote?.picUrl {
            // Load Image From URLwith Caching
            let doctorAvtarImageURLString = imageBaseURL + avtarImageURL
            if let url = URL(string: doctorAvtarImageURLString) {
                cell.imgView.sd_setImage(with: url, placeholderImage: SCAppConstants.Image.userProfile.image())
            }
        }

        return cell
    }
    
    func cellDelegateViewDetailClick(index: Int) {
        let patientConsultData = patientConsultNote?.patientConsultNoteList?[index]
        let story = UIStoryboard(name:SCAppConstants.storyBoardName.home , bundle: nil)
        if #available(iOS 13.0, *) {
            let consultant = story.instantiateViewController(identifier: "ConsultantNoteDetailVC") as! ConsultantNoteDetailVC
            
            consultant.patientDict = patientConsultData
             self.navigationController?.pushViewController(consultant, animated: true)
        } else {
            // Fallback on earlier versions
        }
       
//        let consultantNoteDetail = story.instantiateViewController(identifier: SCAppConstants.viewControllerIdentifiers.consultationNotesPatient) as! ConsultantNoteDetailVC
//        self.navigationController?.pushViewController(consultantNoteDetail!, animated: true)
//        if #available(iOS 13.0, *) {
//            let consultantNoteDetail = self.storyboard?.instantiateViewController(identifier: SCAppConstants.viewControllerIdentifiers.consultationNotesPatient)
//             self.navigationController?.pushViewController(consultantNoteDetail!, animated: true)
//        } else {
//            // Fallback on earlier versions
//
//
//        }
           
       }

}
