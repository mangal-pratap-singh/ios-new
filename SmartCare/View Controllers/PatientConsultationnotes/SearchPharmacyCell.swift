//
//  SearchPharmacyCell.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 26/09/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
protocol AuthoriseCellDelegate {
    func authoriseCellBtnClick(index: Int)
}
class SearchPharmacyCell: UITableViewCell {
    
    var cellDelegate: AuthoriseCellDelegate?
    @IBOutlet weak var lbl_name: UILabel!
    
    @IBOutlet weak var lbl_address: UILabel!
    
    @IBOutlet weak var lbl_city: UILabel!
    
    @IBOutlet weak var lbl_country: UILabel!
    
    @IBOutlet weak var btn_authorise: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    @IBAction func authorisedBtnClick(_ sender: UIButton) {
        cellDelegate?.authoriseCellBtnClick(index: sender.tag)
        
    }
}
