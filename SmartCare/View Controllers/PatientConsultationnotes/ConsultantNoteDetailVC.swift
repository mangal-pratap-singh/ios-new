//
//  CosultantNoteDetailVC.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 23/09/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class ConsultantNoteDetailVC: UIViewController {
    
    @IBOutlet weak var lbl_hospitalName: UILabel!
    
    @IBOutlet weak var lbl_hospitalAddress: UILabel!
    
    @IBOutlet weak var lbl_hospitalCity: UILabel!
    
    @IBOutlet weak var lbl_hospitalState: UILabel!
    
    @IBOutlet weak var lbl_hospitalNo: UILabel!
    
    @IBOutlet weak var lbl_patientName: UILabel!
    
    @IBOutlet weak var lbl_patientAge: UILabel!
    
    @IBOutlet weak var lbl_patientGender: UILabel!
    
    @IBOutlet weak var lbl_patientAddress: UILabel!
    
    @IBOutlet weak var lbl_patientPhone: UILabel!
    
    
    @IBOutlet weak var lbl_doctorName: UILabel!
    
    @IBOutlet weak var lbl_doctorRegNo: UILabel!
    
    @IBOutlet weak var lbl_doctorPhone: UILabel!
    
    
    @IBOutlet weak var lbl_appType: UILabel!
    
    @IBOutlet weak var lbl_appStatus: UILabel!
    
    @IBOutlet weak var lbl_appStartTime: UILabel!
    
    @IBOutlet weak var lbl_appEndTime: UILabel!
    
    @IBOutlet weak var lbl_appPaymentStatus: UILabel!
    
    @IBOutlet weak var btn_backCollection: UIButton!
    
    @IBOutlet weak var btn_nextCollection: UIButton!
    
    var setectOptionStr:String? = nil
    var selectIndex:Int? = -1
    var indexIcrease:Int = 0
    var indexSubIncrease = -1
    var IndexCountArrow:Int = 1
    var boolTrue:Bool = false
    var selectSection:String? = "complete"
    
    let collectionDataArr = ["COMPLAINT","VITALS","ALLERGIES","LABTEST","ADVICE","DIAGNOSIS","MEDICINE","PROCEDURE"]
    var changeCollectionData = [String]()
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var tbleView: UITableView!
    
    var patientDict: PatientConsultNoteList?
    var consultNoteDetailOption: PatientConsultationNoteDetail?
    override func viewDidLoad() {
        super.viewDidLoad()
        tbleView.delegate = self
        tbleView.dataSource = self
        collectionView.delegate = self
        collectionView.dataSource = self
//customizeNavigationItem(title: SCAppConstants.pageTitles.consultationNotesDetail, isDetail: false)
         self.customizeNavigationItem(title: SCAppConstants.pageTitles.consultationNotesDetail, isDetail: true)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lbl_hospitalName.text = patientDict?.hospitalNote?.name
        lbl_hospitalAddress.text = patientDict?.hospitalNote?.address?.address1
        lbl_hospitalCity.text = patientDict?.hospitalNote?.address?.city
        lbl_hospitalState.text = patientDict?.hospitalNote?.address?.state
       // lbl_hospitalpin.text = patientDict?.hospitalNote?.address?.pincode
        lbl_hospitalNo.text = patientDict?.hospitalNote?.address?.phoneNumber
        
        lbl_patientName.text = patientDict?.patientNote?.name
        lbl_patientAge.text = patientDict?.patientNote?.age
         lbl_patientGender.text = patientDict?.patientNote?.gender
        lbl_patientAddress.text = "\(patientDict?.patientNote?.patientNoteAddress?.address1 ?? ""), \(patientDict?.patientNote?.patientNoteAddress?.address2 ?? "")"
        lbl_patientPhone.text = patientDict?.patientNote?.phoneNumber
        
        lbl_doctorName.text = patientDict?.doctorNote?.name
        lbl_doctorRegNo.text = patientDict?.doctorNote?.regNo
        lbl_doctorPhone.text = patientDict?.doctorNote?.phoneNumber
        
        lbl_appType.text = patientDict?.appointmentNote?.appointmentType?.statusTypeText()
        lbl_appStatus.text = patientDict?.appointmentNote?.appointmentStatus?.statusText()
       

        lbl_appStartTime.text = patientDict?.appointmentNote?.startTime
        lbl_appPaymentStatus.text = patientDict?.appointmentNote?.paymentStatus?.statusText()
       
        if let startDate = patientDict?.appointmentNote?.startTime?.toDate(), let endDate = patientDict?.appointmentNote?.endTime?.toDate() {
            lbl_appStartTime.text = startDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentListItemStart)
            lbl_appEndTime.text = endDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentListItemEnd)
          }
        
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        getAppointments()
    }
    
    private func getAppointments(){
              SCProgressView.show()
        let appointMentId = (patientDict?.appointmentNote?.id)!
        
          print("get user id====\(UserProfile.getUserId())")
        SCConsultationNoteService.getConsultationNoteDetailsAPI(withParameters: ["appointMentId": String(appointMentId)], onComplete: { (response, result) in
                SCProgressView.hide()
           
               DispatchQueue.main.async {
                print("Helow===\(response)")
                    self.consultNoteDetailOption = PatientConsultationNoteDetail(data: response)
                self.nextClickMethod()
               }
               
           }) { (errorMessage) in
                SCProgressView.hide()
               
            }
       }
    
    
    @IBAction func searchBtnOpenClick(_ sender: UIButton) {
        
        //let story = UIStoryboard(name: "Main", bundle: nil)
       // let appint = patientDict?.appointmentNote?.id!
        //let str = "\(String(describing: appint))"
         let story = UIStoryboard(name:SCAppConstants.storyBoardName.home , bundle: nil)
        if #available(iOS 13.0, *) {
            let viewController = story.instantiateViewController(identifier: "ConsultationNoteDetailSearchVC") as! ConsultationNoteDetailSearchVC
            viewController.appointmentDict = patientDict?.appointmentNote
             self.present(viewController, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
        }
        
    }
    @IBAction func AuthoriseHistoryBtnClick(_ sender: UIButton) {
        
         let story = UIStoryboard(name:SCAppConstants.storyBoardName.home , bundle: nil)
        
        let authoriseHisViewController = story.instantiateViewController(identifier: "SCAuthorisedHistoryViewController") as! SCAuthorisedHistoryViewController
        authoriseHisViewController.appointMentId =  String(patientDict?.appointmentNote?.id ?? 0)
        self.navigationController?.pushViewController(authoriseHisViewController, animated: true)
    
    }
    
    
    @IBAction func previousBtnClick(_ sender: UIButton) {
        
         changeCollectionData.removeAll()
         selectIndex = -1
       // indexIcrease = indexIcrease - 1
        if indexSubIncrease != 0 && indexSubIncrease != -1 {
            btn_nextCollection.isUserInteractionEnabled = true
             indexIcrease = indexIcrease - 1
            let count1 = indexIcrease * 2
            if selectSection == "started"{
//              indexSubIncrease = IndexCountArrow - 1
//                boolTrue = true
                indexSubIncrease = indexSubIncrease - 3
            }else if selectSection == "complete"{
                //indexSubIncrease = indexSubIncrease - 3
            }
             
        for j in 0..<3{
            
      changeCollectionData.append(collectionDataArr[indexSubIncrease])
            indexSubIncrease = indexSubIncrease - 1
            print("Circle===indexSub==\(indexSubIncrease)")
           
           }
            if indexSubIncrease == -1 || indexSubIncrease == 0{
//                btn_nextCollection.isUserInteractionEnabled = true
//                btn_backCollection.isUserInteractionEnabled = false
                selectSection = "complete"
                indexIcrease = 1
                indexSubIncrease = 2
            }else{
                selectSection = "NotComplete"
            }
            changeCollectionData.reverse()
            
        }else{
             print("===indexSub==\(indexSubIncrease)")
             selectSection = "complete"
            indexIcrease =  1
            indexSubIncrease = 2
            btn_nextCollection.isUserInteractionEnabled = true
            btn_backCollection.isUserInteractionEnabled = false
           }
        DispatchQueue.main.async {
            self.collectionView.reloadData()
            self.tbleView.reloadData()
            }
      }
    
    
    @IBAction func nextBtnClick(_ sender: UIButton) {
         
        nextClickMethod()
        
    }
    
    func nextClickMethod(){
                //btn_backCollection.isUserInteractionEnabled = true
                selectIndex = -1
               //indexSubIncrease = indexIcrease
               indexIcrease = indexIcrease + 1
               let increatCount = indexIcrease * 3
               print("increatCount == \(increatCount)")
               if collectionDataArr.count / increatCount != 0{
                  btn_nextCollection.isUserInteractionEnabled = true
                  btn_backCollection.isUserInteractionEnabled = true
               changeCollectionData.removeAll()
//                if selectSection == "firstSection"{
//                    indexSubIncrease = indexSubIncrease + 3
//                }
                IndexCountArrow = indexIcrease * 2
                 print("circle index count =\(IndexCountArrow)")
                if selectSection == "NotComplete"{
                    indexSubIncrease  = indexSubIncrease + 3
                 for i in 0..<3 {
                    print("circle index before =\(indexSubIncrease)")
          changeCollectionData.append(collectionDataArr[indexSubIncrease])
                    indexSubIncrease  = indexSubIncrease + 1
                    print("circle index after =\(indexSubIncrease)")

                    }}else if selectSection == "complete" || selectSection == "started"{
                    for i in 0..<3 {
                              indexSubIncrease  = indexSubIncrease + 1
                              print("circle index before =\(indexSubIncrease)")
                    changeCollectionData.append(collectionDataArr[indexSubIncrease])
                              print("circle index after =\(indexSubIncrease)")

                              }
                    
                }
                selectSection = "started"
               }else{
                // indexIcrease = indexIcrease - 1
                
                   print("hello hello index =\(indexSubIncrease)")
                   btn_nextCollection.isUserInteractionEnabled = false
                   btn_backCollection.isUserInteractionEnabled = true
                   changeCollectionData.removeAll()
                    print("===indexSub==\(indexSubIncrease)")
                if selectSection == "NotComplete"{
                      indexSubIncrease = indexSubIncrease + 3 + 1
                    for i in indexSubIncrease..<collectionDataArr.count {
                        changeCollectionData.append(collectionDataArr[i])
                    // IndexCountArrow = IndexCountArrow + 1
                    }
                    selectSection = "started"
                }else{
                   let index = indexSubIncrease + 1
                   for i in index..<collectionDataArr.count {
                       changeCollectionData.append(collectionDataArr[i])
                   // IndexCountArrow = IndexCountArrow + 1
                   }
                     selectSection = "complete"
                }
               
                }
               DispatchQueue.main.async {
                           self.collectionView.reloadData()
                           self.tbleView.reloadData()
                      }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
@available(iOS 13.0, *)
extension ConsultantNoteDetailVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        
        return changeCollectionData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PattientCollCell",
                                                      for: indexPath as IndexPath) as! PattientCollCell
        
        cell.lbl_title.text = changeCollectionData[indexPath.item]
       cell.lbl_title.textColor = .SCCollectionTextCellColor()
        if selectIndex == indexPath.row{
             cell.lbl_title.textColor = .SCCollectionSelectTextCellColor()
        }
       
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  10
        let collectionViewSize = collectionView.frame.size.width - padding

        return CGSize(width: collectionViewSize/3, height: 30)
    }
    
    // MARK: - UICollectionViewDelegate protocol

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
//        let cell = collectionView.cellForItem(at: indexPath) as! PattientCollCell
//        cell.lbl_title.textColor = .SCCollectionSelectTextCellColor()
        print("You selected cell #\(indexPath.item)!")
        selectIndex = indexPath.row
        setectOptionStr   =    changeCollectionData[indexPath.item]
        DispatchQueue.main.async {
             self.collectionView.reloadData()
             self.tbleView.reloadData()
        }
    }
    


//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        let noOfCellsInRow = 4
//
//        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
//
//        let totalSpace = flowLayout.sectionInset.left
//            + flowLayout.sectionInset.right
//            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
//
//        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
//
//        return CGSize(width: size, height: size)
//    }

    
}

@available(iOS 13.0, *)
extension ConsultantNoteDetailVC: UITableViewDataSource,UITableViewDelegate{

func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    if setectOptionStr == "COMPLAINT"{
        return 1
    }else if setectOptionStr == "VITALS"{
         return 1
        
    }else if setectOptionStr == "ALLERGIES"{
         return consultNoteDetailOption?.allergies?.count ?? 0
        
    }else if setectOptionStr == "LABTEST"{
         return consultNoteDetailOption?.labTest?.count ?? 0
        
    }else if setectOptionStr == "ADVICE"{
         return consultNoteDetailOption?.advice?.count ?? 0
        
    }else if setectOptionStr == "DIAGNOSIS"{
         return consultNoteDetailOption?.diagnosis?.count ?? 0
        
    }else if setectOptionStr == "MEDICINE"{
        return consultNoteDetailOption?.medicines?.count ?? 0
        
    }else if setectOptionStr == "PROCEDURE"{
         return consultNoteDetailOption?.procedure?.count ?? 0
        
    }
       return 1
   }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if setectOptionStr == "COMPLAINT"{
             return 260
         }else if setectOptionStr == "VITALS"{
              return 220
             
         }else if setectOptionStr == "ALLERGIES"{
            return UITableView.automaticDimension
             
         }else if setectOptionStr == "LABTEST"{
               return UITableView.automaticDimension
             
         }else if setectOptionStr == "ADVICE"{
              return UITableView.automaticDimension
             
         }else if setectOptionStr == "DIAGNOSIS"{
           return UITableView.automaticDimension
             
         }else if setectOptionStr == "MEDICINE"{
              return 160
             
         }else if setectOptionStr == "PROCEDURE"{
              return 60
             
         }
            return 1
        }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
    if setectOptionStr == "COMPLAINT"{
      let cell:PPrimaryComplainCell! = tbleView.dequeueReusableCell(withIdentifier: "PPrimaryComplainCell", for: indexPath) as? PPrimaryComplainCell
       // let primaryComplain = consultNoteDetailOption?.primaryComplain?.primarycomplaint
        cell.lbl_primaryComplain.text = consultNoteDetailOption?.primaryComplain?.primarycomplaint
        cell.lbl_followUp.text = consultNoteDetailOption?.primaryComplain?.followUp
        cell.lbl_referTo.text = consultNoteDetailOption?.primaryComplain?.referTo
        cell.lbl_presentHistory.text = consultNoteDetailOption?.primaryComplain?.presentHistory
        cell.lbl_presentHistory.text = consultNoteDetailOption?.primaryComplain?.pastHistory
        return cell
    }else if setectOptionStr == "VITALS"{
        
        var cell: PVitalsCell! = tbleView.dequeueReusableCell(withIdentifier: "PVitalsCell") as? PVitalsCell
               if cell == nil{
                   tbleView.register(UINib.init(nibName: "PVitalsCell", bundle: nil), forCellReuseIdentifier: "PVitalsCell")
                   cell = tbleView.dequeueReusableCell(withIdentifier: "PVitalsCell", for: indexPath) as? PVitalsCell
               }
        
        cell.lbl_wieght.text = consultNoteDetailOption?.vitals?.weight
        cell.lbl_height.text = consultNoteDetailOption?.vitals?.height
        cell.lbl_heartRate.text = consultNoteDetailOption?.vitals?.heartRate
        cell.lbl_bpSystolic.text = consultNoteDetailOption?.vitals?.bpSystolic
        cell.lbl_bpDiastolic.text = consultNoteDetailOption?.vitals?.bpDiastolic
        cell.lbl_respiratoryRate.text = String((consultNoteDetailOption?.vitals?.respiratoryRate)!)
        cell.lbl_temperature.text = consultNoteDetailOption?.vitals?.temperature
               return cell
        
    }else if setectOptionStr == "ALLERGIES"{
        var cell: PAllergiesCell! = tbleView.dequeueReusableCell(withIdentifier: "PAllergiesCell") as? PAllergiesCell
               if cell == nil{
                   tbleView.register(UINib.init(nibName: "PAllergiesCell", bundle: nil), forCellReuseIdentifier: "PAllergiesCell")
                   cell = tbleView.dequeueReusableCell(withIdentifier: "PAllergiesCell", for: indexPath) as? PAllergiesCell
               }
        
         let aliergiesDict = consultNoteDetailOption?.allergies?[indexPath.row]
         cell.lbl_allergiesCount.text = "Allergies \(indexPath.row)"
        cell.lbl_aliergiesDescription.text = aliergiesDict?.description
        return cell
        
    }else if setectOptionStr == "LABTEST"{
       var cell: PLabTestCell! = tbleView.dequeueReusableCell(withIdentifier: "PLabTestCell") as? PLabTestCell
                    if cell == nil{
                        tbleView.register(UINib.init(nibName: "PLabTestCell", bundle: nil), forCellReuseIdentifier: "PLabTestCell")
                        cell = tbleView.dequeueReusableCell(withIdentifier: "PLabTestCell", for: indexPath) as? PLabTestCell
                    }
             
              let aliergiesDict = consultNoteDetailOption?.labTest?[indexPath.row]
         cell.lbl_testName.text = aliergiesDict?.testName
         cell.lbl_testGroupName.text = aliergiesDict?.testGroupName
             cell.lbl_description.text = aliergiesDict?.description
             return cell
        
    }else if setectOptionStr == "ADVICE"{
       var cell: PAdviceCell! = tbleView.dequeueReusableCell(withIdentifier: "PAdviceCell") as? PAdviceCell
              if cell == nil{
                  tbleView.register(UINib.init(nibName: "PAdviceCell", bundle: nil), forCellReuseIdentifier: "PAdviceCell")
                  cell = tbleView.dequeueReusableCell(withIdentifier: "PAdviceCell", for: indexPath) as? PAdviceCell
              }
       
        let diagnosisDict = consultNoteDetailOption?.advice?[indexPath.row]
       cell.lbl_advice.text = diagnosisDict?.advice
       return cell
    }else if setectOptionStr == "DIAGNOSIS"{
         var cell: PDiagnosisCell! = tbleView.dequeueReusableCell(withIdentifier: "PDiagnosisCell") as? PDiagnosisCell
                if cell == nil{
                    tbleView.register(UINib.init(nibName: "PDiagnosisCell", bundle: nil), forCellReuseIdentifier: "PDiagnosisCell")
                    cell = tbleView.dequeueReusableCell(withIdentifier: "PDiagnosisCell", for: indexPath) as? PDiagnosisCell
                }
         
          let diagnosisDict = consultNoteDetailOption?.diagnosis?[indexPath.row]
         cell.lbl_diagosisCount.text = "Diagnosis \(indexPath.row)"
         cell.lbl_description.text = diagnosisDict?.description
         return cell
    }else if setectOptionStr == "MEDICINE"{
        var cell: PMedicinesCell! = tbleView.dequeueReusableCell(withIdentifier: "PMedicinesCell") as? PMedicinesCell
        if cell == nil{
            tbleView.register(UINib.init(nibName: "PMedicinesCell", bundle: nil), forCellReuseIdentifier: "PMedicinesCell")
            cell = tbleView.dequeueReusableCell(withIdentifier: "PMedicinesCell", for: indexPath) as? PMedicinesCell
              }
             let medicinesDict = consultNoteDetailOption?.medicines?[indexPath.row]
              cell.lbl_name.text = medicinesDict?.name
              cell.lbl_morning.text = medicinesDict?.morning
              cell.lbl_after.text = medicinesDict?.afternoon
              cell.lbl_evening.text = medicinesDict?.evening
              cell.lbl_night.text = medicinesDict?.night
             
            return cell
        
    }else if setectOptionStr == "PROCEDURE"{
       var cell: PProcedureCell! = tbleView.dequeueReusableCell(withIdentifier: "PProcedureCell") as? PProcedureCell
              if cell == nil{
                  tbleView.register(UINib.init(nibName: "PProcedureCell", bundle: nil), forCellReuseIdentifier: "PProcedureCell")
                  cell = tbleView.dequeueReusableCell(withIdentifier: "PProcedureCell", for: indexPath) as? PProcedureCell
              }
       
        let procedureDict = consultNoteDetailOption?.procedure?[indexPath.row]
        cell.lbl_procedureCount.text = "Procedure \(indexPath.row)"
       cell.lbl_name.text = procedureDict?.name
       return cell
       }
   
    return UITableViewCell()
   }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    
}
