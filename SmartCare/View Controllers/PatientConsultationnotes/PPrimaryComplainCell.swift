//
//  PPrimaryComplainCell.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 25/09/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class PPrimaryComplainCell: UITableViewCell {
    
    @IBOutlet weak var lbl_primaryComplain: UILabel!
    
    @IBOutlet weak var lbl_presentHistory: UILabel!
    
    @IBOutlet weak var lbl_pastHistory: UILabel!
    
    @IBOutlet weak var lbl_followUp: UILabel!
    
    @IBOutlet weak var lbl_referTo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
