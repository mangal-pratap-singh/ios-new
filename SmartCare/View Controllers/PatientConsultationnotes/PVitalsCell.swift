//
//  PVitalsCell.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 25/09/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class PVitalsCell: UITableViewCell {
    

    @IBOutlet weak var lbl_wieght: UILabel!
    
    @IBOutlet weak var lbl_height: UILabel!
    
    @IBOutlet weak var lbl_bpSystolic: UILabel!
    
    @IBOutlet weak var lbl_bpDiastolic: UILabel!
    @IBOutlet weak var lbl_heartRate: UILabel!
    
    @IBOutlet weak var lbl_respiratoryRate: UILabel!
    
    @IBOutlet weak var lbl_temperature: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
