//
//  ConsultationNoteDetailSearchVC.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 25/09/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class ConsultationNoteDetailSearchVC: UIViewController ,UISearchBarDelegate,AuthoriseCellDelegate{
    
    var fullScreenMessageView:FullScreenMessageView?
    @IBOutlet weak var tbleView: UITableView!
    var patientConsultSearchPharmacy: PatientConsultSearchPharmacy?
    var appointmentDict: AppointmentNote?
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var searchActive = false
    var searchData = [PatientSearchPharmacy]()
    var searchedData = [PatientSearchPharmacy]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchBar.delegate = self
     //coor code: 181F36
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.fullScreenMessageView = self.showFullScreenMessageOnView(parentView: self.view, withMessage: "No Pharmacy List")
        fullScreenMessageView?.center = self.view.center
        getAppointments()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    private func getAppointments(){
                 SCProgressView.show()
        let appointMentId = appointmentDict?.id!
           //getGraphAPI
      //  getConsultationNoteSearchPharmacyAPI
            print("get user id====\(UserProfile.getUserId())")
        SCConsultationNoteService.getConsultationNoteSearchPharmacyAPI( withParameters: ["appointMentId": String(appointMentId!)], onComplete: { (response, result) in
                   SCProgressView.hide()
             self.fullScreenMessageView?.removeFromSuperview()
                //  self.patientConsultNote = PatientConsultNote(jsonData: response)
            self.patientConsultSearchPharmacy = PatientConsultSearchPharmacy(data: response)
                  
           DispatchQueue.main.async {
            print("Helow===\(response)")
            self.tbleView.reloadData()
           }
              }) { (errorMessage) in
                   SCProgressView.hide()
                self.view.addSubview(self.fullScreenMessageView!)
                  
              }
          }
    @IBAction func dissmissBtnClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension ConsultationNoteDetailSearchVC{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = false
       // self.searchBar.endEditing(false)
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
        //self.searchBar.endEditing(true)
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
           searchActive = false
        //self.searchBar.endEditing(true)
        DispatchQueue.main.async {
         
         self.tbleView.reloadData()
            
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
          searchActive = false
        self.searchBar.endEditing(true)
        self.searchBar.searchTextField.resignFirstResponder()
    }


    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchActive = false
        searchData.removeAll()
        if searchText == ""{
            DispatchQueue.main.async {
                    
              self.tbleView.reloadData()

            }
            return
          }
        if let listSearchData = patientConsultSearchPharmacy?.searchPharmacy {
        searchedData  = SCSearchModel().searchMethod(search: searchText,listData: listSearchData)//patientConsultSearchPharmacy?.searchPharmacy
             searchActive = true
                }
             DispatchQueue.main.async {
        
              self.tbleView.reloadData()
           
             }
    }
    
         
       
    }
    
extension ConsultationNoteDetailSearchVC: UITableViewDataSource,UITableViewDelegate

{

func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if searchActive == true{
        return searchedData.count
       }
    return patientConsultSearchPharmacy?.searchPharmacy?.count ?? 0
     }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
    let cell:SearchPharmacyCell! = tbleView.dequeueReusableCell(withIdentifier: "SearchPharmacyCell", for: indexPath) as? SearchPharmacyCell
    cell.cellDelegate = self
    cell.btn_authorise.tag = indexPath.row
    if searchActive == true{
       // return searchData.count
        cell.lbl_name.text = searchedData[indexPath.row].name
        cell.lbl_address.text = searchedData[indexPath.row].addressSearchPharmacy?.address1
        cell.lbl_city.text = searchedData[indexPath.row].addressSearchPharmacy?.city
        cell.lbl_country.text = searchedData[indexPath.row].addressSearchPharmacy?.country
    }else{
    let dict = patientConsultSearchPharmacy?.searchPharmacy?[indexPath.row] 
        cell.lbl_name.text = dict?.name
        cell.lbl_address.text = dict?.addressSearchPharmacy?.address1
        cell.lbl_city.text = dict?.addressSearchPharmacy?.city!
        cell.lbl_country.text = dict?.addressSearchPharmacy?.country!
    }
    return cell
   }
    
    func authoriseCellBtnClick(index: Int) {
        
        var pharmacyId: Int?
        if searchActive == true{
           // return searchData.count
            pharmacyId = searchedData[index].id ?? 0
        }else{
        let dict = patientConsultSearchPharmacy?.searchPharmacy?[index]
            pharmacyId = dict?.id ?? 0
        }
        SCProgressView.show()
        let appointMentId = appointmentDict?.id!
        guard let preId = appointmentDict?.prescriptionId else { return  }
          // SCAppConstants.apiRequestPath.consultationNotesSearchPharmacy,app
         let useriD = UserProfile.getUserId()
        //prescriptionId
        //appointmentId
        //pharmacyId
        let urlStr = String(format: SCAppConstants.apiRequestPath.consultationNotesSearchPharmacyAuthorise,useriD)
        SCConsultationNoteService.postHitAPI(urlStr: urlStr, withParameters: ["appointmentId": String(appointMentId!),"prescriptionId": preId,"pharmacyId": String(pharmacyId!)], onComplete: { (response, result) in
                   SCProgressView.hide()
               //  self.patientConsultNote = PatientConsultNote(jsonData: response)
            print( response.dictionaryObject as Any)
       //self.patientConsultSearchPharmacy = PatientConsultSearchPharmacy(data: response)
            let message = response.dictionaryObject!["message"] as! String
            
           DispatchQueue.main.async {
            self.patientConsultSearchPharmacy?.searchPharmacy?.remove(at: index)
            self.showAlert(title: "Alert", msg: message, okButtonTitle: "Ok", style: .alert)
            print("Helow===\(response)")
            self.tbleView.reloadData()
               
           }
              }) { (errorMessage) in
                   SCProgressView.hide()
                self.showAlert(title: "Alert", msg: errorMessage, okButtonTitle: "Ok", style: .alert)
                  
              }
        
        
        
    }
}

