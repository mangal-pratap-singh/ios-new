import UIKit
import RxSwift
import RxCocoa

struct ShiftListError: Error {
    enum ErrorType {
        case inCompleteForm
        case serverError
    }
    let error: ErrorType
    let localizedDescription: String
}


struct SCShiftsLiftViewModel {
    
    let shifts = BehaviorRelay<[Shift]>(value: [Shift]())
    private let disposeBag = DisposeBag()
    
    init() {
        refreshList()
    }
    
    func refreshList() {
        fetchShiftList()
            .bind(to: shifts)
            .disposed(by: disposeBag)
    }
    
    private func fetchShiftList() -> Observable<[Shift]> {
        return Observable.create({ observer -> Disposable in
            SCShiftService.shiftsListAPI(onComplete: { (shiftsList) in
                observer.onNext(shiftsList)
            }) { (errorMessage) in
                observer.onError(ShiftListError(error: .serverError, localizedDescription: errorMessage))
            }
            return Disposables.create()
        })
    }
    
    func delete(shift: Shift, completion: @escaping (ShiftListError?) -> ()) {
        SCShiftService.deleteShiftAPI(forShiftId: String(shift.id), onComplete: {
            completion(nil)
        }) { (errorMessage) in
            completion(ShiftListError(error: .serverError, localizedDescription: errorMessage))
        }
    }
}

class SCShiftsListViewController: UIViewController {

    @IBOutlet weak var shiftItemsTable: UITableView!
    @IBOutlet weak var createShiftButton: UIBarButtonItem!
    
    var shifts = [Shift]()
    private var viewModel: SCShiftsLiftViewModel!
    
    private let disposeBag = DisposeBag()
    
    static func initialize(with viewModel: SCShiftsLiftViewModel) -> UINavigationController {
        let storyboard = UIStoryboard(name: SCAppConstants.storyBoardName.manageShift, bundle: nil)
        let shiftListNavigationVC = storyboard.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.shiftListNavigation) as! UINavigationController
        let shiftListVC = shiftListNavigationVC.topViewController as! SCShiftsListViewController
        shiftListVC.viewModel = viewModel
        return shiftListNavigationVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        registerNibsForCells()
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.refreshList()
    }
    
    private func setup() {
        createShiftButton.rx.tap
            .bind { [unowned self] in self.pushCreateShiftViewController() }
            .disposed(by: disposeBag)
        
        viewModel.shifts
            .bind(to: shiftItemsTable.rx.items(cellIdentifier: SCAppConstants.cellReuseIdentifiers.shiftListItem, cellType: ShiftItemTableViewCell.self)) { [unowned self] (row, shift, cell) in
                cell.configureCell(withShift: shift)
                cell.edit = { shift in self.pushCreateShiftViewController(with: shift) }
                cell.delete = { shift in self.askForDeletetion(of: shift) }
            }
            .disposed(by: disposeBag)
    }
    
    private func configureView() {
        shiftItemsTable.tableFooterView = UIView()
        customizeNavigationItem(title: SCAppConstants.pageTitles.shiftList, isDetail: false)
    }
    
    private func registerNibsForCells() {
        let nibName = UINib(nibName: SCAppConstants.nibNames.shiftItemTableViewCell, bundle:nil)
        shiftItemsTable.register(nibName, forCellReuseIdentifier: SCAppConstants.cellReuseIdentifiers.shiftListItem)
    }
    
    private func pushCreateShiftViewController(with shift: Shift? = nil) {
        let viewModel = SCCreateShiftViewModel(with: shift)
        if let editShiftVC = SCCreateShiftViewController.initialze(with: viewModel) {
            self.navigationController?.pushViewController(editShiftVC, animated: true)
        }
    }
    
    private func confirmedDelete(shift: Shift) {
        SCProgressView.show()
        viewModel.delete(shift: shift) { [weak self] error in
            SCProgressView.hide()
            if let error = error {
                self?.showAlert(title: SCAppConstants.pageTitles.shiftList, msg: error.localizedDescription)
            } else {
                self?.viewModel.refreshList()
            }
        }
    }
    
    private func askForDeletetion(of shift: Shift) {
        let confirmationAlert = UIAlertController(title: SCAppConstants.pageTitles.shiftList , message: SCAppConstants.alertMessages.shiftDeleteConfirmation, preferredStyle: .alert)
        let disAgreeAction = UIAlertAction(title: SCAppConstants.alertButtonTitles.disagree, style: .default, handler: nil)
        confirmationAlert.addAction(disAgreeAction)
        
        let agreeAction = UIAlertAction(title: SCAppConstants.alertButtonTitles.agree, style: .default) { [weak self] (action) in
            self?.confirmedDelete(shift: shift)
        }
        confirmationAlert.addAction(agreeAction)
        present(confirmationAlert, animated: true, completion: nil)
    }
}
