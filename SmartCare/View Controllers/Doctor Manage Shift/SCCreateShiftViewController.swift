import UIKit
import ActionSheetPicker_3_0
import RxCocoa
import RxSwift

class SCCreateShiftViewController: UIViewController {

    private var viewModel: SCCreateShiftViewModel!
    
    @IBOutlet weak var hospitalNameTextField: UITextField!
    @IBOutlet weak var clinicNameTextField: UITextField!
    @IBOutlet weak var fromTimeTextField: UITextField!
    @IBOutlet weak var toTimeTextField: UITextField!
    @IBOutlet weak var shiftNameTextField: UITextField!
    @IBOutlet weak var createShiftButton: UIButton!
    
    private var disposeBag = DisposeBag()
    
    static func initialze(with viewModel: SCCreateShiftViewModel) -> SCCreateShiftViewController? {
        let storyboard = UIStoryboard(name: "ManageShift", bundle: Bundle.main)
        if let vc = storyboard.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.createAndEditShift) as? SCCreateShiftViewController {
            vc.viewModel = viewModel
            return vc
        }
        return nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureObservers()
        viewModel.loadShiftDetailsIfAvailable()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //self.clinicNameTextField.isEnabled = false
    }
    
    private func configureObservers() {
        viewModel.selectedHospital
            .asObservable()
            .map({ $0?.name })
            .bind(to: hospitalNameTextField.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.selectedHospital
            .asObservable()
            .map({$0 != nil})
            .bind(to: clinicNameTextField.rx.isEnabled)
            .disposed(by: disposeBag)
        
        viewModel.selectedClinic
            .asObservable()
            .map({ $0?.name })
            .bind(to: clinicNameTextField.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.selectedStartTime
            .asObservable()
            .map({ $0?.toDateString(withFormatter: SCAppConstants.DateFormatType.slotTime) })
            .bind(to: fromTimeTextField.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.selectedEndTime
            .asObservable()
            .map({ $0?.toDateString(withFormatter: SCAppConstants.DateFormatType.slotTime) })
            .bind(to: toTimeTextField.rx.text)
            .disposed(by: disposeBag)
        
        (shiftNameTextField.rx.text <-> viewModel.selectedShiftName).disposed(by: disposeBag)
        
        let requriedObservers: Observable<(Hospital?, Clinic?, Date?, Date?, String?)> =
            Observable.combineLatest(viewModel.selectedHospital.asObservable(), viewModel.selectedClinic.asObservable(), viewModel.selectedStartTime.asObservable(), viewModel.selectedEndTime.asObservable(), viewModel.selectedShiftName.asObservable())
        requriedObservers
            .map({ $0.0 != nil && $0.1 != nil && $0.2 != nil && $0.3 != nil && !($0.4 ?? "").isEmpty })
            .map({ [unowned self] in
                self.createShiftButton.backgroundColor = $0 ? #colorLiteral(red: 0.169464767, green: 0.8075588346, blue: 0.8998508453, alpha: 1) : #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
                return $0
            })
            .bind(to: createShiftButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        createShiftButton.rx.tap.bind {
            SCProgressView.show()
            self.viewModel.submitForm(completion: { [weak self] error in
                SCProgressView.hide()
                if let error = error {
                    self?.showAlert(title: SCAppConstants.pageTitles.createShift, msg: error.localizedDescription)
                } else {
                    self?.navigationController?.popViewController(animated: true)
                }
            })
            }.disposed(by: disposeBag)
    }
    
    private func configureView() {
        
        customizeNavigationItem(title: viewModel.screenTitle, isDetail: true)
        hospitalNameTextField.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        clinicNameTextField.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        fromTimeTextField.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        toTimeTextField.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        createShiftButton.setTitle(viewModel.submitButtonTitle, for: .normal)
    }
    
    private func showHospitalPicker(textField:UITextField) {
        ActionSheetStringPicker.show(
            withTitle: viewModel.hospitalPickerTitle,
            rows: viewModel.hospitals.value.map { $0.name ?? "" },
            initialSelection: 0,
            doneBlock: { [weak self] picker, selectedIndex, selectedValue in
                self?.viewModel.selectedHospital.value = self?.viewModel.hospitals.value.filter({ $0.name == selectedValue as? String }).first
            },
            cancel: { _ in },
            origin: textField
        )
    }
    
    private func showClinicPicker(textField:UITextField) {
        ActionSheetStringPicker.show(
            withTitle: viewModel.clinicPickerTitle,
            rows: self.viewModel.selectedHospital.value?.clinics?.map { $0.name ?? "" },
            initialSelection: 0,
            doneBlock: { [weak self] picker, selectedIndex, selectedValue in
                self?.viewModel.selectedClinic.value = self?.viewModel.selectedHospital.value?.clinics?.filter({ $0.name == selectedValue as? String}).first
            },
            cancel: { _ in },
            origin: textField
        )
    }
    
    private func showTimePicker(textField:UITextField) {
        let datePicker = ActionSheetDatePicker(
            title: viewModel.timePickerTitle,
            datePickerMode: UIDatePicker.Mode.time,
            selectedDate: Date(),
            doneBlock: {
                [weak self] (picker, value, index) in
                if textField == self?.fromTimeTextField {
                    self?.viewModel.selectedStartTime.value = value as? Date
                } else if textField == self?.toTimeTextField {
                    self?.viewModel.selectedEndTime.value = value as? Date
                }
            },
            cancel: nil,
            origin: textField.superview
        )
        if textField == self.toTimeTextField {
            datePicker?.minimumDate = self.viewModel.selectedStartTime.value
        }
        datePicker?.show()
    }
}

extension SCCreateShiftViewController{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if (textField == hospitalNameTextField) {
            showHospitalPicker(textField: textField)
            return false
        } else if (textField == clinicNameTextField) {
            showClinicPicker(textField: textField)
            return false
        } else if (textField == fromTimeTextField || textField == toTimeTextField) {
            showTimePicker(textField: textField)
            return false
        }
        return true
    }
}
