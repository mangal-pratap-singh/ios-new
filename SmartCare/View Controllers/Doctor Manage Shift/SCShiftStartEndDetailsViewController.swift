import RxCocoa
import RxSwift
import ActionSheetPicker_3_0

class SCShiftStartEndDetailsViewModel {
    
    let isShiftStarted = BehaviorRelay<Bool>(value: false)
    let currentShift = BehaviorRelay<Shift?>(value: nil)
    let selectedShiftTime = BehaviorRelay<Date?>(value: nil)
    
    let timePickerTitle = "Select Shift time"
    
    let pageTitle = BehaviorRelay<String>(value: "")
    let submitButtonTitle = BehaviorRelay<String>(value: "")
    let endShiftNotesTextObservable = BehaviorRelay<String>(value: "")
    let actualSelectedTimeTitle = BehaviorRelay<String>(value: "")
    let actualShiftFromLabelTitle = BehaviorRelay<String>(value: "")
    
    private let endShiftNotesText = "Note : Make sure that you must save the consultation note for all the completed appointments, otherwise those appointments will become missed and paid fees will be refunded."
    
    private let disposeBag = DisposeBag()
    
    init(with shift: Shift, isShiftStarted: Bool) {
        getShiftDetails(for: shift.id) { shift in
            self.currentShift.accept(shift)
        }
        
        self.isShiftStarted.accept(isShiftStarted)
        
        self.isShiftStarted
            .map({ !$0 ? SCAppConstants.pageTitles.shiftStart : SCAppConstants.pageTitles.shiftEnd })
            .bind(to: pageTitle)
            .disposed(by: disposeBag)
        
        self.isShiftStarted
            .map({ !$0 ? SCAppConstants.pageTitles.shiftStart : SCAppConstants.pageTitles.shiftEnd })
            .bind(to: submitButtonTitle)
            .disposed(by: disposeBag)
        
        self.isShiftStarted
            .map({ [weak self] in !$0 ? "" : (self?.endShiftNotesText ?? "") })
            .bind(to: endShiftNotesTextObservable)
            .disposed(by: disposeBag)
        
        self.isShiftStarted
            .map({ $0 ? "Actual Shift To" : "Actual Shift From" })
            .bind(to: actualShiftFromLabelTitle)
            .disposed(by: disposeBag)
        
        self.selectedShiftTime
            .filterNil()
            .map({ $0.toDateString(withFormatter: SCAppConstants.DateFormatType.slotTime) })
            .bind(to: actualSelectedTimeTitle)
            .disposed(by: disposeBag)
    }
    
    private func getShiftDetails(for id: Int, completion: @escaping (Shift?)->()) {
        SCShiftService.shiftsDetailsAPI(shiftId: String(id), onComplete: { (shift) in
            completion(shift)
        }) { (errorMessage) in
            print("Error getting shift details -- \(errorMessage)")
            completion(nil)
        }
    }
    
    func submitForm(completion: @escaping (SCError?) -> Void) {
        guard let shiftTime = selectedShiftTime.value, let shift = currentShift.value else {
            let errorMessage = isShiftStarted.value ? SCAppConstants.validationMessages.actualEndTimeRequired : SCAppConstants.validationMessages.actualStartTimeRequired
            completion(SCError(error: .inCompleteForm, localizedDescription: errorMessage))
            return
        }
            
        let key = !isShiftStarted.value ? "actualStartTime" : "actualEndTime"
        let parameters = [key: shiftTime.toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentAPI)]
        SCShiftService.updateShiftWithActualTimeAPI(forShiftId: String(shift.id), isShiftStarted: isShiftStarted.value, parameters: parameters, onComplete: {
            completion(nil)
        }) { (errorMessage) in
            completion(SCError(error: .serverError, localizedDescription: errorMessage))
        }
        
    }
}

class SCShiftStartEndDetailsViewController: UIViewController {

    private let endShiftTopConstraint: CGFloat = 72.0
    private let startShiftTopConstraint: CGFloat = 16.0
    
    private let disposeBag = DisposeBag()

    var viewModel: SCShiftStartEndDetailsViewModel!
    
    @IBOutlet weak var shiftActionButton: UIButton!
    @IBOutlet weak var shiftActualStartTimeView: UIView!
    @IBOutlet weak var shiftNotesLabel: UILabel!
    @IBOutlet weak var actualShiftToTextfieldTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var shiftNameLabel: UILabel!
    @IBOutlet weak var shiftFromLabel: UILabel!
    @IBOutlet weak var shiftToLabel: UILabel!
    @IBOutlet weak var shiftHospitalLabel: UILabel!
    
    @IBOutlet weak var totalSlotsLabel: UILabel!
    @IBOutlet weak var totalAppointmentsLabel: UILabel!
    @IBOutlet weak var confirmedAppointmentsLabel: UILabel!
    @IBOutlet weak var missedAppointmentsLabel: UILabel!
    @IBOutlet weak var cancelledAppointmentsLabel: UILabel!
    @IBOutlet weak var completedAppointmentsLabel: UILabel!
    @IBOutlet weak var bookedAppointmentsLabel: UILabel!
    
    @IBOutlet weak var actualShiftFromValueLabel: UILabel!
    @IBOutlet weak var actualShiftFromLabel: UILabel!
    @IBOutlet weak var actualShiftTimeTextfield: UITextField!
    
    static func initialize(with viewModel: SCShiftStartEndDetailsViewModel) -> SCShiftStartEndDetailsViewController {
        let storyboard = UIStoryboard(name: SCAppConstants.storyBoardName.manageShift, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.shiftStartEndDetails) as! SCShiftStartEndDetailsViewController
        vc.viewModel = viewModel
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    private func configureView() {
        viewModel.pageTitle.asDriver().drive(rx.detailNavigationTitle).disposed(by: disposeBag)
        viewModel.submitButtonTitle.asDriver()
            .map({ $0.uppercased()})
            .drive(shiftActionButton.rx.title(for: .normal))
            .disposed(by: disposeBag)
        
        viewModel.isShiftStarted.asDriver()
            .map({ !$0 })
            .drive(shiftActualStartTimeView.rx.isHidden)
            .disposed(by: disposeBag)
        
        viewModel.isShiftStarted.asDriver()
            .map({ !$0 })
            .drive(shiftNotesLabel.rx.isHidden)
            .disposed(by: disposeBag)
        
        viewModel.isShiftStarted.asDriver()
            .map({[weak self] in (!$0 ? self?.startShiftTopConstraint : self?.endShiftTopConstraint) ?? 0 })
            .drive(actualShiftToTextfieldTopConstraint.rx.constant)
            .disposed(by: disposeBag)
        
        viewModel.endShiftNotesTextObservable.asDriver()
            .drive(shiftNotesLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.actualSelectedTimeTitle.asDriver()
            .drive(actualShiftTimeTextfield.rx.text)
            .disposed(by: disposeBag)
        
        shiftActionButton.rx.tap
            .bind { [weak self] in self?.submitForm() }
            .disposed(by: disposeBag)
        
        viewModel.currentShift.asDriver()
            .map({ $0?.name})
            .drive(shiftNameLabel.rx.text).disposed(by: disposeBag)
        viewModel.currentShift.asDriver()
            .map({ "\(($0?.hospital?.name ?? "")) / \(($0?.clinic?.name ?? ""))" })
            .drive(shiftHospitalLabel.rx.text).disposed(by: disposeBag)
        viewModel.currentShift.asDriver()
            .map({ $0?.startTime?.toDateString(withFormatter: SCAppConstants.DateFormatType.slotTime) })
            .drive(shiftFromLabel.rx.text).disposed(by: disposeBag)
        viewModel.currentShift.asDriver()
            .map({ $0?.endTime?.toDateString(withFormatter: SCAppConstants.DateFormatType.slotTime) })
            .drive(shiftToLabel.rx.text).disposed(by: disposeBag)
        viewModel.currentShift.asDriver()
            .map({ "\($0?.shiftSummary?.totalSlots ?? 0)"})
            .drive(totalSlotsLabel.rx.text).disposed(by: disposeBag)
        viewModel.currentShift.asDriver()
            .map({ "\($0?.shiftSummary?.totalAppointments ?? 0)"})
            .drive(totalAppointmentsLabel.rx.text).disposed(by: disposeBag)
        viewModel.currentShift.asDriver()
            .map({ "\($0?.shiftSummary?.confirmedAppointments ?? 0)"})
            .drive(confirmedAppointmentsLabel.rx.text).disposed(by: disposeBag)
        viewModel.currentShift.asDriver()
            .map({ "\($0?.shiftSummary?.missedAppointments ?? 0)"})
            .drive(missedAppointmentsLabel.rx.text).disposed(by: disposeBag)
        viewModel.currentShift.asDriver()
            .map({ "\($0?.shiftSummary?.cancelledAppointments ?? 0)"})
            .drive(cancelledAppointmentsLabel.rx.text).disposed(by: disposeBag)
        viewModel.currentShift.asDriver()
            .map({ "\($0?.shiftSummary?.completedAppointments ?? 0)"})
            .drive(completedAppointmentsLabel.rx.text).disposed(by: disposeBag)
        viewModel.currentShift.asDriver()
            .map({ "\($0?.shiftSummary?.bookedAppointments ?? 0)"})
            .drive(bookedAppointmentsLabel.rx.text).disposed(by: disposeBag)
        viewModel.currentShift.asDriver()
            .map({ $0?.actualStartTime?.toDateString(withFormatter: SCAppConstants.DateFormatType.slotTime) })
            .drive(actualShiftFromValueLabel.rx.text).disposed(by: disposeBag)
        viewModel.actualShiftFromLabelTitle.asDriver()
            .drive(actualShiftFromLabel.rx.text).disposed(by: disposeBag)
    }
    
    private func submitForm() {
        SCProgressView.show()
        viewModel.submitForm { [weak self] error in
            SCProgressView.hide()
            if let error = error {
                let title = (self?.viewModel.isShiftStarted.value ?? false) ? SCAppConstants.pageTitles.shiftEnd : SCAppConstants.pageTitles.shiftStart
                self?.showAlert(title: title, msg: error.localizedDescription)
            } else {
                self?.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    private func showTimePicker(textField:UITextField) {
        ActionSheetDatePicker.show(
            withTitle: viewModel.timePickerTitle,
            datePickerMode: UIDatePicker.Mode.time,
            selectedDate: Date(),
            doneBlock: {
                [weak self] (_, value, _) in
                self?.viewModel.selectedShiftTime.accept(value as? Date)
            },
            cancel: nil,
            origin: textField.superview)
    }
}

extension SCShiftStartEndDetailsViewController {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if (textField == actualShiftTimeTextfield) {
            showTimePicker(textField: textField)
            return false
        }
        return true
    }
}
