import RxCocoa
import RxSwift
import RxDataSources

class SCShiftSummaryDetailViewModel {
    let pageTitle = BehaviorRelay<String>(value: SCAppConstants.pageTitles.shiftSummary)
    let currentShift = PublishRelay<Shift?>()
    let appointments = BehaviorRelay<[DoctorAppointment]?>(value: nil)
    
    let date: Date
    let shiftId: Int
    
    init(with id: Int, date: Date) {
        self.date = date
        self.shiftId = id
    }
    
    private func getShiftDetails(for id: Int, completion: @escaping (Shift?)->()) {
        SCShiftService.shiftsDetailsAPI(shiftId: String(id), onComplete: { (shift) in
            completion(shift)
        }) { (errorMessage) in
            print("Error getting shift details -- \(errorMessage)")
            completion(nil)
        }
    }
    
    private func getAppointmentDetails(forShiftId id: Int, completion: @escaping ([DoctorAppointment]?) -> ()) {
        SCShiftService.shiftSummaryDetailsAPI(forShiftId: String(id), onComplete: { appointments in
            completion(appointments)
        }) { (errorMessage) in
            print("Error getting shift appointment details -- \(errorMessage)")
            completion(nil)
        }
    }
    
    func refreshSummary() {
        getShiftDetails(for: shiftId) { [weak self] in self?.currentShift.accept($0) }
        getAppointmentDetails(forShiftId: shiftId) { [weak self] in self?.appointments.accept($0) }
    }
}

class SCShiftSummaryDetailViewController: UIViewController, UITableViewDelegate {

    @IBOutlet weak var summaryDetailTableView: UITableView!
    
    private let disposeBag = DisposeBag()
    private var viewModel: SCShiftSummaryDetailViewModel!
    private var dataSource: RxTableViewSectionedReloadDataSource<SectionModel<String, CellModel>>!
    
    enum CellModel {
        case shiftDetails(Shift?)
        case summaryDetails(ShiftSummary?)
        case appointmentListHeader(String)
        case appointmentList(DoctorAppointment)
    }
    
    static func initialize(with viewModel: SCShiftSummaryDetailViewModel) -> SCShiftSummaryDetailViewController {
        let storyboard = UIStoryboard(name: SCAppConstants.storyBoardName.manageShift, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.shiftSummaryDetail) as! SCShiftSummaryDetailViewController
        vc.viewModel = viewModel
        return vc
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.refreshSummary()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNibsForCells()
        
        Observable.combineLatest(viewModel.currentShift.asObservable().filterNil(), viewModel.appointments).bind { [weak self] (shift, appointments) in
                self?.loadShiftSummary(for: shift, appointments: appointments)
        }.disposed(by: disposeBag)
        
        summaryDetailTableView.rowHeight = UITableView.automaticDimension
        summaryDetailTableView.estimatedRowHeight = 300
        summaryDetailTableView.sectionHeaderHeight = CGFloat.leastNormalMagnitude
        summaryDetailTableView.sectionFooterHeight = CGFloat.leastNormalMagnitude
        
        configureView()
    }
    
    private func registerNibsForCells() {
        let nibName = UINib(nibName: SCAppConstants.nibNames.doctorAppointmentTableViewCell, bundle:nil)
        summaryDetailTableView.register(nibName, forCellReuseIdentifier: SCAppConstants.cellReuseIdentifiers.doctorAppointmentListItem)
    }
    
    private func loadShiftSummary(for shift: Shift, appointments: [DoctorAppointment]?) {
        self.summaryDetailTableView.delegate = nil
        self.summaryDetailTableView.dataSource = nil
        
        func buildSections() -> [SectionModel<String, CellModel>] {
            let sections = [
                SectionModel(model: "Shift Details", items: [CellModel.shiftDetails(shift)]),
                SectionModel(model: "Shift Summary", items: [CellModel.summaryDetails(shift.shiftSummary)])
            ]
            if let appointments = appointments {
                let header = [CellModel.appointmentListHeader("Appointment List")]
                let items = header + appointments
                    .sorted(by: { $0.patient?.name ?? "" < $1.patient?.name ?? "" })
                    .map({ CellModel.appointmentList($0) })
                return sections + [SectionModel(model: "Appointment List", items: items)]
            }
            
            return sections
        }
        
        let sections = Observable.just(buildSections())
        
        dataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, CellModel>>(configureCell: { dataSource, table, indexPath, item in
            switch item {
            case .shiftDetails(let shift):
                return self.buildShiftDetailsCell(with: shift, from: table)
            case .summaryDetails(let summary):
                return self.buildSummaryDetailsCell(with: summary, from: table)
            case .appointmentList(let appointment):
                return self.buildAppointmentListDetailsCell(with: appointment, from: table)
            case .appointmentListHeader(let title):
                return self.buildAppointmentListHeaderCell(with: title, from: table)
            }
        })
        
        sections
            .bind(to: summaryDetailTableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        summaryDetailTableView.rx.setDelegate(self).disposed(by: disposeBag)
    }
    
    func configureView() {
        viewModel.pageTitle.asDriver().drive(rx.detailNavigationTitle).disposed(by: disposeBag)
    }
    
    private func buildShiftDetailsCell(with shift: Shift?, from table: UITableView) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: SCAppConstants.cellReuseIdentifiers.shiftDetailTableCell) as! SCShiftDetailTableCell
        cell.configureCell(with: shift, andDate: self.viewModel.date)
        return cell
    }

    private func buildSummaryDetailsCell(with appointmentDetails: ShiftSummary?, from table: UITableView) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: SCAppConstants.cellReuseIdentifiers.shiftAppointmentDetailTableViewCell) as! SCShiftAppointmentDetailTableViewCell
        cell.configureCell(with: appointmentDetails)
        return cell
    }
    
    private func buildAppointmentListDetailsCell(with appointment: DoctorAppointment, from table: UITableView) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: SCAppConstants.cellReuseIdentifiers.doctorAppointmentListItem) as! DoctorAppointmentTableViewCell
        cell.configureCell(WithAppointment: appointment)
        return cell
    }
    
    private func buildAppointmentListHeaderCell(with title: String, from table: UITableView) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: SCAppConstants.cellReuseIdentifiers.shiftAppointmentListItemHeader) as! SCShiftAppointmentDetailHeaderCell
        cell.configureCell(with: title)
        return cell
    }
    
    private func navigateToAppointmentDetail(for appointment: DoctorAppointment) {
        let appointmentDetailsVC = SCDoctorAppointmentDetailsViewController.initialize(withDoctorAppointment: appointment)
        navigationController?.pushViewController(appointmentDetailsVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2, let appointments = viewModel.appointments.value {
            self.navigateToAppointmentDetail(for: appointments[indexPath.row - 1])
        }
    }
}

class SCShiftAppointmentDetailHeaderCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    func configureCell(with title: String) {
        titleLabel.text = title
    }
}

class SCShiftAppointmentDetailTableViewCell: UITableViewCell {
    
    let shiftSummary = BehaviorRelay<ShiftSummary?>(value: nil)
    private let disposeBag = DisposeBag()
    
    @IBOutlet weak var totalSlotsLabel: UILabel!
    @IBOutlet weak var totalAppointmentsLabel: UILabel!
    @IBOutlet weak var confirmedAppointmentsLabel: UILabel!
    @IBOutlet weak var missedAppointmentsLabel: UILabel!
    @IBOutlet weak var cancelledAppointmentsLabel: UILabel!
    @IBOutlet weak var completedAppointmentsLabel: UILabel!
    @IBOutlet weak var bookedAppointmentsLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        shiftSummary.asDriver()
            .map({ "\($0?.totalSlots ?? 0)"})
            .drive(totalSlotsLabel.rx.text).disposed(by: disposeBag)
        shiftSummary.asDriver()
            .map({ "\($0?.totalAppointments ?? 0)"})
            .drive(totalAppointmentsLabel.rx.text).disposed(by: disposeBag)
        shiftSummary.asDriver()
            .map({ "\($0?.confirmedAppointments ?? 0)"})
            .drive(confirmedAppointmentsLabel.rx.text).disposed(by: disposeBag)
        shiftSummary.asDriver()
            .map({ "\($0?.missedAppointments ?? 0)"})
            .drive(missedAppointmentsLabel.rx.text).disposed(by: disposeBag)
        shiftSummary.asDriver()
            .map({ "\($0?.cancelledAppointments ?? 0)"})
            .drive(cancelledAppointmentsLabel.rx.text).disposed(by: disposeBag)
        shiftSummary.asDriver()
            .map({ "\($0?.completedAppointments ?? 0)"})
            .drive(completedAppointmentsLabel.rx.text).disposed(by: disposeBag)
        shiftSummary.asDriver()
            .map({ "\($0?.bookedAppointments ?? 0)"})
            .drive(bookedAppointmentsLabel.rx.text).disposed(by: disposeBag)
    }
    
    func configureCell(with shiftSummary: ShiftSummary?) {
        self.selectionStyle = .none
        self.shiftSummary.accept(shiftSummary)
    }
}
