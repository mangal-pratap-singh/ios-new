import ActionSheetPicker_3_0
import RxCocoa
import RxSwift
import RxOptional

enum ShiftListState {
    case startEnd
    case summary
}

struct SCShiftsByDateListViewModel {

    private let listState: ShiftListState
    
    let shifts: PublishSubject<[Shift]> = PublishSubject()
    var selectedDate: Variable<Date?> = Variable<Date?>(nil)
    let pageTitle = BehaviorRelay<String>(value: "")
    
    var cellType: ShiftListState {
        return listState
    }
    
    private let disposeBag = DisposeBag()
    
    init(with state: ShiftListState) {
        listState = state
        refreshList()
        
        switch state {
        case .startEnd:
            pageTitle.accept(SCAppConstants.pageTitles.shiftStartEnd)
        case .summary:
            pageTitle.accept(SCAppConstants.pageTitles.shiftSummary)
        }
    }
    
    func refreshList() {
        selectedDate.asObservable()
            .filterNil()
            .bind(onNext: {
                self.fetchShifts(by: $0).bind(to: self.shifts).disposed(by: self.disposeBag)
            })
            
            .disposed(by: disposeBag)
    }
    
    private func fetchShifts(by date: Date) -> Observable<[Shift]> {
        return Observable.create({ observer -> Disposable in
            SCShiftService.shiftsByDateAPI(date: date, onComplete: { (shiftList) in
                observer.onNext(shiftList)
            }) { (errorMessage) in
                observer.onError(SCError(error: .serverError, localizedDescription: errorMessage))
            }
            return Disposables.create()
        })
    }
    
    func detailType() -> ShiftListState {
        return listState
    }
    
    func isShiftStarted(for shift: Shift) -> Bool {
        return shift.actualStartTime != nil
    }
}

class SCShiftsByDateListViewController: UIViewController {
    
    private let datePickerTitle = "Select Shift Date"
    
    @IBOutlet weak var selectShiftDateButton: UIButton!
    @IBOutlet weak var shiftItemsTable: UITableView!
    
    private let disposeBag = DisposeBag()
    var viewModel: SCShiftsByDateListViewModel!
    
    static func initialize(with viewModel: SCShiftsByDateListViewModel) -> UINavigationController {
        let storyboard = UIStoryboard(name: SCAppConstants.storyBoardName.manageShift, bundle: nil)
        let navigationVC = storyboard.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.shiftsByDateListNavigationn) as! UINavigationController
        let vC = navigationVC.topViewController as! SCShiftsByDateListViewController
        vC.viewModel = viewModel
        return navigationVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        registerNibsForCells()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.refreshList()
    }
    
    private func configureView() {
        viewModel.pageTitle.asDriver().drive(rx.navigationTitle).disposed(by: disposeBag)
        shiftItemsTable.tableFooterView = UIView()
        
        selectShiftDateButton.rx.tap
            .bind { [weak self] in self?.showDatePicker() }
            .disposed(by: disposeBag)
        
        viewModel.shifts
            .bind(to: shiftItemsTable.rx.items(cellIdentifier: SCAppConstants.cellReuseIdentifiers.ShiftsByDateItem, cellType: SCShiftsByDateTableViewCell.self)) { [unowned self] (row, shift, cell) in
                cell.configureCell(with: shift, for: self.viewModel.cellType)
                cell.start = { shift in self.loadDetailsPage(for: shift) }
                cell.end = { shift in self.loadDetailsPage(for: shift) }
                cell.summary = { shift in self.loadDetailsPage(for: shift) }
            }
            .disposed(by: disposeBag)
        
        viewModel.selectedDate.asObservable()
            .filterNil()
            .map({ $0.toDateString(withFormatter: SCAppConstants.DateFormatType.slotDate) })
            .bind(to: selectShiftDateButton.rx.title(for: .normal))
            .disposed(by: disposeBag)
        
    }
    
    private func registerNibsForCells() {
        let nibName = UINib(nibName: SCAppConstants.nibNames.shiftStartEndItemTableViewCell, bundle:nil)
        shiftItemsTable.register(nibName, forCellReuseIdentifier: SCAppConstants.cellReuseIdentifiers.ShiftsByDateItem)
    }
    
    private func showDatePicker() {
        ActionSheetDatePicker.show(
            withTitle: datePickerTitle,
            datePickerMode: UIDatePicker.Mode.date,
            selectedDate: Date(),
            doneBlock: { [weak self] (_, value, _) in
                self?.viewModel.selectedDate.value = value as? Date
            },
            cancel: nil,
            origin: selectShiftDateButton
        )
    }
    
    private func loadDetailsPage(for shift: Shift) {
        switch viewModel.detailType() {
        case .startEnd:
            let viewModel = SCShiftStartEndDetailsViewModel(with: shift, isShiftStarted: self.viewModel.isShiftStarted(for: shift))
            let vc = SCShiftStartEndDetailsViewController.initialize(with: viewModel)
            navigationController?.pushViewController(vc, animated: true)
        case .summary:
            let viewModel = SCShiftSummaryDetailViewModel(with: shift.id, date: self.viewModel.selectedDate.value!)
            let vc = SCShiftSummaryDetailViewController.initialize(with: viewModel)
            navigationController?.pushViewController(vc, animated: true)
        }
        
    }
}

