import RxSwift
import RxCocoa

class SCCreateShiftViewModel {
    let currentShift: Shift?
    
    let hospitalPickerTitle = "Select Hospital"
    let clinicPickerTitle = "Select Clinic"
    let timePickerTitle = "Select time"
    let createShiftButtonTitle = "CREATE SHIFT"
    let editShiftButtonTitle = "SAVE CHANGES"
    
    let hospitals: Variable<[Hospital]> = Variable<[Hospital]>([Hospital]())
    let selectedHospital: Variable<Hospital?> = Variable<Hospital?>(nil)
    let selectedClinic: Variable<Clinic?> = Variable<Clinic?>(nil)
    let selectedStartTime: Variable<Date?> = Variable<Date?>(nil)
    let selectedEndTime: Variable<Date?> = Variable<Date?>(nil)
    let selectedShiftName: Variable<String?> = Variable<String?>(nil)
    
    private var disposeBag = DisposeBag()
    
    init(with currentShift: Shift? = nil) {
        self.currentShift = currentShift
        
        getAsocialtedHospitalList()
            .subscribe(onNext: { [weak self] hospitalList in
                self?.hospitals.value = hospitalList
                self?.loadShiftDetailsIfAvailable()
            })
            .disposed(by: disposeBag)
    }
    
    var screenTitle: String {
        return currentShift?.id == nil ? SCAppConstants.pageTitles.createShift : SCAppConstants.pageTitles.editShift
    }
    var submitButtonTitle: String {
        return currentShift?.id == nil ? createShiftButtonTitle : editShiftButtonTitle
    }
    
    private func getAsocialtedHospitalList() -> Observable<[Hospital]> {
        return Observable<[Hospital]>.create({ observer -> Disposable in
            SCCommanService.getAsociateHospitalListAPI(onComplete: { (hospitalList) in
                observer.onNext(hospitalList)
            }) { (errorMessage) in
                print("Error Getting Hospital List - \(errorMessage)")
                observer.onError(NSError(domain: "", code: -1, userInfo: ["description": errorMessage]))
            }
            return Disposables.create()
        })
    }
    
    func submitForm(completion: @escaping (SCError?) -> ()) {
        
        guard let hospital = selectedHospital.value,
            let clinic = selectedClinic.value,
            let fromTime = selectedStartTime.value,
            let toTime = selectedEndTime.value,
            let shiftName = selectedShiftName.value else {
                completion(SCError(error: .inCompleteForm, localizedDescription: "All fields are compulsary."))
                return
        }
        let shiftRequestParameters: [String : Any] = [
            "hospitalId": hospital.id,
            "clinicId": clinic.id,
            "name": shiftName,
            "startTime": fromTime.toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentAPI),
            "endTime": toTime.toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentAPI)
        ]
        var shiftId: String? = nil
        if let id = currentShift?.id {
            shiftId = String(id)
        }
        
        SCShiftService.createOrEditShiftAPI(forShiftId: shiftId, andParameters: shiftRequestParameters, onComplete: {
            completion(nil)
        }) { (errorMessage) in
            completion(SCError(error: .serverError, localizedDescription: errorMessage))
        }
    }
    
    func loadShiftDetailsIfAvailable() {
        selectedHospital.value = hospitals.value.filter({ $0 == currentShift?.hospital}).first
        selectedClinic.value = currentShift?.clinic
        selectedStartTime.value = currentShift?.startTime
        selectedEndTime.value = currentShift?.endTime
        selectedShiftName.value = currentShift?.name
    }
}
