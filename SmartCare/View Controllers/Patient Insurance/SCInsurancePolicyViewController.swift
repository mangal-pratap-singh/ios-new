//
//  SCInsurancePolicyViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 05/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCInsurancePolicyViewController: UIViewController {

    @IBOutlet weak var insuranceTable: UITableView!
    var insurancePolicyListArray = [Insurance]()
    var fullScreenMesageView: FullScreenMessageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.insurancePolicy, isDetail: false)
        configureView()
        addRightNavBarItem()
        registerNibsForCells()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getInsurancePolicyList()
    }
    
    private func configureView() {
        insuranceTable.tableFooterView = UIView()
    }
    
    private func registerNibsForCells() {
        let nibName = UINib(nibName: SCAppConstants.nibNames.insuranceTableViewCell, bundle:nil)
        insuranceTable.register(nibName, forCellReuseIdentifier: SCAppConstants.cellReuseIdentifiers.insuranceTableListItem)
    }
    
    private func addRightNavBarItem() {
        let plusButton:UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        plusButton.setImage(SCAppConstants.Image.plusButton.image(), for: UIControl.State.normal)
        plusButton.addTarget(self, action: #selector(plusButtonTapped), for: UIControl.Event.touchUpInside)
        let plusBarButton : UIBarButtonItem = UIBarButtonItem(customView: plusButton)
        self.navigationItem.rightBarButtonItem  = plusBarButton
    }
    
    @objc private func plusButtonTapped() {
        goToInsurancePolicyEditViewControllerWithSelectedPolicy(policy: nil)
    }
    
    private func getInsurancePolicyList() {
        SCProgressView.show()
        SCInsuranceServices.insurancePolicyListAPI(onComplete: {[weak self](insuranceList) in
            SCProgressView.hide()
            self?.fullScreenMesageView?.removeFromSuperview()
            if insuranceList.count == 0 {
                self?.fullScreenMesageView = self?.showFullScreenMessageOnView(parentView: self!.insuranceTable, withMessage: "No Insurance Policies Available")
                self?.view.addSubview(self!.fullScreenMesageView!)
              }
            self?.insurancePolicyListArray = insuranceList
            self?.insuranceTable.reloadData()
        }) { (errorMessage) in
             SCProgressView.hide()
        }
    }
}

extension SCInsurancePolicyViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.insurancePolicyListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SCAppConstants.cellReuseIdentifiers.insuranceTableListItem, for: indexPath as IndexPath) as! InsuranceTableViewCell
        cell.selectionStyle = .none
        let currentInsurance = self.insurancePolicyListArray[indexPath.row]
        cell.configureCell(WithInsurance: currentInsurance)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         goToInsurancePolicyEditViewControllerWithSelectedPolicy(policy: self.insurancePolicyListArray[indexPath.row])
    }
    
    private func goToInsurancePolicyEditViewControllerWithSelectedPolicy(policy:Insurance?) {
        if let insuranceEditVC = UIStoryboard(name: SCAppConstants.storyBoardName.insurancePolicy, bundle: nil).instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.insurancePolicyEditViewController) as? SCInsurancePolicyEditViewController {
            insuranceEditVC.selectedInsuranceObject = policy
            self.navigationController?.pushViewController(insuranceEditVC, animated: true)
        }
    }
}

