//
//  FullScreenImageViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 13/10/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCFullScreenImageViewController: UIViewController {

    @IBOutlet weak var fullScreenImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var imagePath = ""
    var imageTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Disable Drawer for Detailed Pages
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = false
        showImage()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = true
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    private func showImage() {
        titleLabel.text = imageTitle
        if let url = URL(string: imagePath) {
            fullScreenImageView.sd_setImage(with: url, placeholderImage: SCAppConstants.Image.image_placeholder.image())
        }
    }
    
}
