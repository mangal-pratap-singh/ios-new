//
//  SCInsurancePolicyEditViewController.swift
//  SmartCare
//
//  Created by synerzip on 15/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class SCInsurancePolicyEditViewController: UIViewController {
    
    private let datePickerTitle = "Select Date"
    private let insuranceCompanyPickerTitle = "Select Insurance Company"
    
    private let uploadPolicyButtonTitle = "UPLOAD POLICY ID"
    private let viewPolicyButtonTitle = "VIEW POLICY ID"
    private let submitButtonTitle = "SUBMIT"
    private let deleteButtonTitle = "DELETE"
    
    private let commentsPlaceholderText = "Comments"
    
    @IBOutlet weak var policyCompanyTextField: TKAutoCompleteTextField!
    @IBOutlet weak var policyNumberTextField: UITextField!
    @IBOutlet weak var startDateTextField: UITextField!
    @IBOutlet weak var endDateTextField: UITextField!
    @IBOutlet weak var commentTextView: UITextView!
    
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var policyDocumentButton: UIButton!
    
    var selectedInsuranceObject:Insurance?
    var policyAttachmentLink = ""
    let picker = UIImagePickerController()
    var insuranceCompanies = [String]()
    var isEditModeEnabled = false
    var isEditModeEnabledStr:String? = nil
    var selectedStartDate: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.insurancePolicy, isDetail: true)
        configureView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Disable Drawer for Detailed Pages
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = false
        getInsuranceCompanies()
        if let insurancePolicy = self.selectedInsuranceObject {
            isEditModeEnabled = false
            isEditModeEnabledStr = "NotEditDelete"
            addNavigationBarEditButton(enabled: true)
            shouldPolicyEditable(value: false)
            showInsuracePolicyDetails(policy: insurancePolicy)
            submitButton.setTitle(deleteButtonTitle, for: .normal)
            let policyButtonTitle = insurancePolicy.attachmentLink!.isEmpty ? uploadPolicyButtonTitle : viewPolicyButtonTitle
            let policyButtonImage = insurancePolicy.attachmentLink!.isEmpty ? SCAppConstants.Image.uploadButton.imageName() : ""
            policyDocumentButton.setTitle(policyButtonTitle, for: .normal)
            policyDocumentButton.setImage(UIImage(named: policyButtonImage), for: .normal)
        } else {
            isEditModeEnabledStr = "NotEdit"
            isEditModeEnabled = true
            shouldPolicyEditable(value: true)
            submitButton.setTitle(submitButtonTitle, for: .normal)
            policyDocumentButton.setTitle(uploadPolicyButtonTitle, for: .normal)
            policyDocumentButton.setImage(SCAppConstants.Image.uploadButton.image(), for: .normal)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = true
    }
    
    @IBAction func viewPolicyButtonAction(_ sender: Any) {
        if let _ = self.selectedInsuranceObject, isEditModeEnabled == false {
            showPolicyDocument()
        } else if policyAttachmentLink.isEmpty ||  isEditModeEnabled == true {
            selectAndUploadPolicyDocument()
        } else {
            showPolicyDocument()
        }
    }
    
    @IBAction func submitButtonAction(_ sender: Any) {
        
        if isEditModeEnabledStr == "NotEdit"{
            // Call Create Policy API
                       isInputValid() ? createInsurancePolicy() : showAlert(title: SCAppConstants.pageTitles.insurancePolicy, msg: SCAppConstants.validationMessages.allFieldsValidationMessage)
            
        }else if isEditModeEnabledStr == "EditActive"{
              isInputValid() ? editeInsurancePolicy() : showAlert(title: SCAppConstants.pageTitles.insurancePolicy, msg: SCAppConstants.validationMessages.allFieldsValidationMessage)
        }else{
             deleteInsurancePolicy()
        }
        /*
        if isEditModeEnabled == false {
            deleteInsurancePolicy()
        } else {
            // Call Create Policy API
            isInputValid() ? createInsurancePolicy() : showAlert(title: SCAppConstants.pageTitles.insurancePolicy, msg: SCAppConstants.validationMessages.allFieldsValidationMessage)
        }
        */
    }
    
    private func configureView() {
        startDateTextField.SetRightSideImage(with: SCAppConstants.Image.calenderBlack.image())
        endDateTextField.SetRightSideImage(with: SCAppConstants.Image.calenderBlack.image())
        picker.allowsEditing = false
        picker.delegate = self
        commentTextView.delegate = self
    }
    
    private func addNavigationBarEditButton(enabled: Bool) {
        let editBarButtonItem = UIBarButtonItem(title: "EDIT", style: .plain, target: self, action: #selector(editButtonTapped))
        let foregroundColor = enabled ? UIColor.white : UIColor.gray
        editBarButtonItem.setTitleTextAttributes([
            NSAttributedString.Key.font : UIFont(name: SCAppConstants.applicationFontFamily, size: 16)!,
            NSAttributedString.Key.foregroundColor : foregroundColor
            ], for: .normal)
        self.navigationItem.rightBarButtonItem  = editBarButtonItem
    }
    
    @objc private func editButtonTapped() {
        submitButton.setTitle(submitButtonTitle, for: .normal)
        policyDocumentButton.setTitle(uploadPolicyButtonTitle, for: .normal)
        policyDocumentButton.setImage(SCAppConstants.Image.uploadButton.image(), for: .normal)
        isEditModeEnabled = true
        isEditModeEnabledStr = "EditActive"
        addNavigationBarEditButton(enabled: false)
        shouldPolicyEditable(value: true)
    }
    
    private func isInputValid() -> Bool {
        guard let policyNumbner = policyNumberTextField.text, let policyCompany = policyCompanyTextField.text, let startDate = startDateTextField.text, let endDate = endDateTextField.text else {
            return false
        }
        return !policyNumbner.isEmpty && !policyCompany.isEmpty && !startDate.isEmpty && !endDate.isEmpty && !(commentTextView.text == commentsPlaceholderText) && !policyAttachmentLink.isEmpty
    }
    
    private func showInsuracePolicyDetails(policy: Insurance) {
        policyNumberTextField.text = policy.policyNum
        policyCompanyTextField.text = policy.company
        startDateTextField.text = policy.startDate
        endDateTextField.text = policy.endDate
        commentTextView.text = policy.comment
        policyAttachmentLink = policy.attachmentLink ?? ""
      }
    
    private func shouldPolicyEditable(value: Bool) {
        policyNumberTextField.isEnabled = value
        policyCompanyTextField.isEnabled = value
        startDateTextField.isEnabled = value
        endDateTextField.isEnabled = value
        commentTextView.isEditable = value
     }
    
    func showDatePicker(textField:UITextField) {
        let datePicker = ActionSheetDatePicker(title: datePickerTitle, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            if textField == self.startDateTextField {
                self.selectedStartDate = value as? Date
            }
            let dateString = (value as! Date).toDateString(withFormatter: SCAppConstants.DateFormatType.date)
            textField.text = dateString
            return
        }, cancel: {
            ActionStringCancelBlock in return
        },
        origin: textField.superview)
        if textField == self.endDateTextField {
            datePicker?.minimumDate = ((self.selectedStartDate) != nil) ? self.selectedStartDate! : Date()
        }
        datePicker?.show()
    }
    
    private func createInsurancePolicy() {
        let parameters = ["policyNum":policyNumberTextField.text ?? "",
                          "companyName":policyCompanyTextField.text ?? "",
                          "startDate":startDateTextField.text ?? "",
                          "endDate":endDateTextField.text ?? "",
                          "comment":commentTextView.text ?? "",
                          "attachmentLink":policyAttachmentLink]
        
        SCProgressView.show()
        SCInsuranceServices.createInsurancePolicyAPI(parameters: parameters, onComplete: {
            SCProgressView.hide()
            self.navigationController?.popViewController(animated: true)
        }) { (errorMessage) in
            SCProgressView.hide()
            self.showAlert(title: SCAppConstants.pageTitles.insurancePolicy, msg: errorMessage)
        }
    }
    
    
    private func editeInsurancePolicy() {
           let parameters = ["policyNum":policyNumberTextField.text ?? "",
                             "companyName":policyCompanyTextField.text ?? "",
                             "startDate":startDateTextField.text ?? "",
                             "endDate":endDateTextField.text ?? "",
                             "comment":commentTextView.text ?? "",
                             "attachmentLink":policyAttachmentLink]
           
           SCProgressView.show()
        SCInsuranceServices.editeInsurancePolicyAPI(parameters: parameters,policyId: String(selectedInsuranceObject?.id ?? 0), onComplete: {
               SCProgressView.hide()
               self.navigationController?.popViewController(animated: true)
           }) { (errorMessage) in
               SCProgressView.hide()
               self.showAlert(title: SCAppConstants.pageTitles.insurancePolicy, msg: errorMessage)
           }
       }
    
    private func deleteInsurancePolicy() {
        if let insurancePolicy = self.selectedInsuranceObject, let policyId = insurancePolicy.id {
            SCProgressView.show()
            SCInsuranceServices.deleteInsurancePolicyAPI(policyId: "\(policyId)", onComplete: {
                SCProgressView.hide()
                self.navigationController?.popViewController(animated: true)
            }) { (errorMessage) in
                SCProgressView.hide()
                self.showAlert(title: SCAppConstants.pageTitles.insurancePolicy, msg: errorMessage)
            }
        }
    }
    
    private func showPolicyDocument() {
        if let fullScreenVC = self.storyboard?.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.fullScreenImageView) as? SCFullScreenImageViewController {
            fullScreenVC.imagePath = policyAttachmentLink
            fullScreenVC.imageTitle = selectedInsuranceObject?.policyNum ?? ""
            present(fullScreenVC, animated: true, completion: nil)
        }
    }
    
    private func selectAndUploadPolicyDocument() {
        present(picker, animated: true, completion: nil)
    }
    
    private func  getInsuranceCompanies() {
        SCProgressView.show()
        SCCommanService.getInsuranceCompanyListAPI(onComplete: { [weak self] (insuranceCompanies, success) in
            SCProgressView.hide()
            if success {
                for company in insuranceCompanies {
                    self?.insuranceCompanies.append(company.name ?? "")
                }
                self?.policyCompanyTextField.suggestions = self?.insuranceCompanies
            }
        }) { (errorMessage) in
            SCProgressView.hide()
            print("Error Locations API - \(errorMessage)")
        }
    }
}

extension SCInsurancePolicyEditViewController: UITextViewDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if textField == startDateTextField || textField == endDateTextField {
            self.showDatePicker(textField: textField)
            return false
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == commentsPlaceholderText {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = commentsPlaceholderText
            textView.textColor = UIColor.lightGray
        }
    }
}

extension SCInsurancePolicyEditViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        guard let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage else {
            return
        }
           
        if let data = image.jpegData(compressionQuality: 0.3) {
            let compressedImage = UIImage(data: data)!
            dismiss(animated: true) {
                // Upload the Compressed Image
                SCProgressView.show()
                SCUserService.uploadAvatar(withImage: compressedImage, onComplete: { [weak self] ( data ,success) in
                    SCProgressView.hide()
                    if success {
                        self?.policyAttachmentLink = data["tempPath"].string ?? ""
                    }
                }) { (errorMessage) in
                    SCProgressView.hide()
                    self.showAlert(title: SCAppConstants.pageTitles.insurancePolicy, msg: errorMessage)
                }
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
