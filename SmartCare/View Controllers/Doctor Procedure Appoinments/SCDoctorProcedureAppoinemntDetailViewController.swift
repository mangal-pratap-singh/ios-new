//
//  SCDoctorProcedureAppoinemntDetailViewController.swift
//  SmartCare
//
//  Created by synerzip on 14/12/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCDoctorProcedureAppoinemntDetailViewController: UIViewController {
    
    var currentAppointment: DoctorAppointment?
    
    @IBOutlet weak var procedureDetailScrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var procedureDenyButton: UIButton!
    @IBOutlet weak var changeProcedureTypeButton: UIButton!
    @IBOutlet weak var denyButton: UIButton!
    @IBOutlet weak var approveButton: UIButton!
    @IBOutlet weak var changeProcedureButtonTopContraint: NSLayoutConstraint!
    @IBOutlet weak var procedureLabel: UILabel!
    @IBOutlet weak var procedureView: UIView!
    @IBOutlet weak var appointmentView: UIView!
    @IBOutlet weak var procedureDetailLabel: UILabel!
    @IBOutlet weak var procedureFeeLabel: UILabel!
    @IBOutlet weak var procedureTypeLabel: UILabel!
    @IBOutlet weak var patientPhoneLabel: UILabel!
    @IBOutlet weak var patientEmailLabel: UILabel!
    @IBOutlet weak var patientNameLabel: UILabel!
    @IBOutlet weak var patientAgeLabel: UILabel!
    @IBOutlet weak var patientIdLabel: UILabel!
    @IBOutlet weak var procedureAppoinmentStatusLabel: UILabel!
    @IBOutlet weak var appointmentTimeLabel: UILabel!
    @IBOutlet weak var hospitalNameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var hospitalPhoneLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.procedureDetails, isDetail: true)
        showAppointmentDetails()
    }
    
    private func showAppointmentDetails() {
        
        if let appointment =  currentAppointment {
            //Show Patient Details
            patientNameLabel.text = appointment.patient?.name
            patientAgeLabel.text = appointment.patient?.age
            patientIdLabel.text = appointment.patient?.id
            patientEmailLabel.text = appointment.patient?.email
            patientPhoneLabel.text = "+ \(appointment.patient?.countryCode ?? "")- \(appointment.patient?.phoneNumber ?? "")"
            //Show Appointment Details
            hospitalNameLabel.text = "\(appointment.hospital?.name ?? "") / \(appointment.clinic?.name ?? "")"
            hospitalPhoneLabel.text = appointment.clinic?.clinicAddress?.phoneNumber
            locationLabel.text = appointment.location
            if let startDate = appointment.startTime, let endDate = appointment.endTime {
                appointmentTimeLabel.text = startDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointment) + " at " + startDate.toTimeString() + " - " + endDate.toTimeString()
            }
            commentsLabel.text = appointment.cancelComments
            commentsLabel.sizeToFit()
            
            if(appointment.procedure?.name == ""){
                procedureView.isHidden = true
                procedureLabel.isHidden = true
                procedureDenyButton.isHidden = true
                changeProcedureTypeButton.isHidden = true
                denyButton.isHidden = false
                approveButton.isHidden = false
                contentView.frame.size.height = 500
                procedureDetailScrollView.frame.size.height = 500
                //changeProcedureButtonTopContraint.constant = -240
            }else{
                procedureView.isHidden = false
                procedureLabel.isHidden = false
                procedureDenyButton.isHidden = false
                changeProcedureTypeButton.isHidden = false
                denyButton.isHidden = true
                approveButton.isHidden = true
                contentView.frame.size.height = 800
                procedureDetailScrollView.frame.size.height = 800
                procedureTypeLabel.text = appointment.procedure?.name ?? ""
                procedureDetailLabel.text = appointment.procedure?.detail ?? ""
                commentsLabel.text = appointment.procedure?.comment ?? ""
                // changeProcedureButtonTopContraint.constant = 8
            }
        }
    }
    
    private func showApproveProcedureAppoinemntVC() {
        if let procedureApproveAppoinmentDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.procedureApproveAppoinmentVC) as? SCDoctorApproveProcedureViewController {
            procedureApproveAppoinmentDetailsVC.currentAppointment = currentAppointment
            procedureApproveAppoinmentDetailsVC.changeProcedureType = false; self.navigationController?.pushViewController(procedureApproveAppoinmentDetailsVC, animated: true)
        }
    }
    
    @IBAction func changeProcedureTypeButtonAction(_ sender: Any) {
        
        if let procedureApproveAppoinmentDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.procedureApproveAppoinmentVC) as? SCDoctorApproveProcedureViewController {
            procedureApproveAppoinmentDetailsVC.changeProcedureType = true
            procedureApproveAppoinmentDetailsVC.currentAppointment = currentAppointment
            self.navigationController?.pushViewController(procedureApproveAppoinmentDetailsVC, animated: true)
        }
    }
    
    @IBAction func denyButtonAction(_ sender: Any) {
        if let appointment =  currentAppointment  {
            denyProcedureAppointment(status: AppointmentStatus.cancelled.rawValue, comments: appointment.cancelComments ?? "", cancelBy : appointment.cancelBy)
        }
    }
    
    private func denyProcedureAppointment(status: String, comments: String , cancelBy:String) {
        if let appointmentID = currentAppointment?.id {
            let parameters = ["status":status, "comments":comments ,"cancelBy":cancelBy]
            SCProgressView.show()
            SCDoctorProcedureAppointmentService.denyProcedureAppointmentAPI(parameters: parameters, appointmentId: String(appointmentID), onComplete: {
                SCProgressView.hide()
                self.navigationController?.popViewController(animated: true)
                
            }) { (errorMessage) in
                SCProgressView.hide()
                self.showAlert(title: SCAppConstants.pageTitles.procedureAppoinemts, msg: errorMessage)
            }
        }
    }
    
    @IBAction func approveButtonAction(_ sender: Any) {
        showApproveProcedureAppoinemntVC()
    }
}
