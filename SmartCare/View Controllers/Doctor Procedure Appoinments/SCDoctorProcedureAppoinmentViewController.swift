//
//  SCDoctorProcedureAppoinmentViewController.swift
//  SmartCare
//
//  Created by synerzip on 13/12/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCDoctorProcedureAppoinmentViewController: UIViewController {
    
    @IBOutlet weak var procedureAppoinmentTbl: UITableView!
    
    var displayDate = Date()
    private let noOfSecondForWeak = 604799
    var doctorProcedureAppointmentList = [DoctorAppointment]()
    var fullScreenMessageView:FullScreenMessageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.procedureAppoinemts, isDetail: false)
        configureView()
        registerNibsForCells()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.getProcedureAppointmentsList()
    }
    
    private func configureView() {
        procedureAppoinmentTbl.tableFooterView = UIView()
        let nextDate = Calendar.current.date(byAdding: .second, value: noOfSecondForWeak, to: displayDate)!
        displayDate = nextDate;
    }
    
    private func registerNibsForCells() {
        let nibName = UINib(nibName: SCAppConstants.nibNames.procedureAppoinmentTableViewCell, bundle:nil)
        procedureAppoinmentTbl.register(nibName, forCellReuseIdentifier: SCAppConstants.cellReuseIdentifiers.procedureAppointmentListItem)
    }
    
    private func showProcedureAppoinemntDetailVC(appointment: DoctorAppointment) {
        if let procedureAppoinmentDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.procedureAppoinmentDetailVC) as? SCDoctorProcedureAppoinemntDetailViewController {
            
            procedureAppoinmentDetailsVC.currentAppointment = appointment
            self.navigationController?.pushViewController(procedureAppoinmentDetailsVC, animated: true)
        }
    }
    
    private func getProcedureAppointmentsList() {
            SCProgressView.show()
        SCDoctorProcedureAppointmentService.doctorProcedureAppointmentsListAPI(withDate: displayDate.startOfWeek, onComplete: {  [weak self] (procedureAppointments) in
            SCProgressView.hide()
            self?.fullScreenMessageView?.removeFromSuperview()
            if procedureAppointments.count == 0 {
                self?.procedureAppoinmentTbl.isHidden = true
                self?.fullScreenMessageView = self?.showFullScreenMessageOnView(parentView: self!.procedureAppoinmentTbl, withMessage: "No Procedure appointments")
                self?.view.addSubview(self!.fullScreenMessageView!)
            }else{
                self?.procedureAppoinmentTbl.isHidden = false
                self?.doctorProcedureAppointmentList = procedureAppointments
                self?.procedureAppoinmentTbl.reloadData()
            }
        }) { (errorMessage) in
            SCProgressView.hide()
            self.fullScreenMessageView?.removeFromSuperview()
            self.procedureAppoinmentTbl.isHidden = true
            self.fullScreenMessageView = self.showFullScreenMessageOnView(parentView: self.procedureAppoinmentTbl, withMessage: errorMessage)
            self.view.addSubview(self.fullScreenMessageView!)
            //self.showAlert(title: SCAppConstants.pageTitles.procedureAppoinemts, msg: errorMessage)
        }
    }
}

extension SCDoctorProcedureAppoinmentViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.doctorProcedureAppointmentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SCAppConstants.cellReuseIdentifiers.procedureAppointmentListItem, for: indexPath as IndexPath) as! ProcedureAppoinmentTableViewCell
        cell.selectionStyle = .none
        let currentAppointment = doctorProcedureAppointmentList[indexPath.row]
        cell.configureCell(WithAppointment: currentAppointment)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let currentAppointment = doctorProcedureAppointmentList[indexPath.row]
        showProcedureAppoinemntDetailVC(appointment: currentAppointment)
    }
}
