//
//  SCDoctorApproveProcedureViewController.swift
//  SmartCare
//
//  Created by synerzip on 16/12/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class SCDoctorApproveProcedureViewController: UIViewController {
    
    var changeProcedureType:Bool?
    var currentAppointment: DoctorAppointment?
    var currentProcedureAppointment: ProcedureFeeItem?
    var procedureFeesList = [ProcedureFeeItem]()
    var selectedProcedureNameIndex = 0
    var procedureTypePickerDataSource = [String]()
    private let procedureTypePickerTitle = "Select Procedure Type"
    
    @IBOutlet weak var procedureTypeTextField: UITextField!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var chargesTextField: UITextField!
    @IBOutlet weak var procedureDetailLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.approveProcedure, isDetail: true)
        getProcedureTypeData()
    }
    
    func getProcedureTypeData() {
        SCProgressView.show()
        SCDoctorProcedureAppointmentService.changeProcedureTypeAPI(withAppoinment:currentAppointment!, onComplete: { [weak self] (procedureFeesList) in
            SCProgressView.hide()
            self?.procedureFeesList = procedureFeesList
            self?.createProcedureTypePickerDataSource()
            self?.configureView()
        }) { (errorMessage) in
            SCProgressView.hide()
        }
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        if(!(self.procedureTypeTextField.text?.isEmpty)!){
            saveProcedureAppointmentDetails()
        }else {
            self.showAlert(title: SCAppConstants.pageTitles.procedureAppoinemts, msg:SCAppConstants.validationMessages.procedureTypeRequired)
        }
        
    }
    
    func configureView() {
        procedureTypeTextField.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        self.currentProcedureAppointment = self.procedureFeesList[0]
        if(changeProcedureType == true){
            commentTextView.isEditable = false
            commentTextView.text = currentAppointment?.procedure?.comment ?? ""
        }else{
            commentTextView.isEditable = true
        }
    }
    
    private func saveProcedureAppointmentDetails() {
        if let appointmentID = currentAppointment?.id {
            let parameters = ["procedureTypeId": self.currentProcedureAppointment?.procedureType?.id ?? 0,
                              "procedureName":self.currentProcedureAppointment?.procedureType?.name ?? "",
                              "procedureDetail":self.currentProcedureAppointment?.procedureType?.details ?? "",
                              "procedureComment": self.commentTextView.text ?? "",
                              "amount":self.currentProcedureAppointment?.procedureFee?.charges ?? "",
                              "inclusiveGst":self.currentProcedureAppointment?.procedureFee?.gstInclusive ?? false,
                              
                              ] as [String : Any]
            SCProgressView.show()
            SCDoctorProcedureAppointmentService.saveProcedureAppointmentAPI(parameters:parameters , appointmentId: String(appointmentID), onComplete: {
                SCProgressView.hide()
                self.navigationController?.popToRootViewController(animated: true)
                
            }) { (errorMessage) in
                SCProgressView.hide()
                self.showAlert(title: SCAppConstants.pageTitles.procedureAppoinemts, msg: errorMessage)
            }
        }
    }
    
    private func createProcedureTypePickerDataSource() {
        
        for procedureFeeItem in self.procedureFeesList {
            procedureTypePickerDataSource.append(procedureFeeItem.procedureType?.name ?? "")
        }
        
    }
    
    private func showProcedureTypePicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: procedureTypePickerTitle, rows: procedureTypePickerDataSource, initialSelection: self.selectedProcedureNameIndex, doneBlock: { picker, selectedIndex, selectedValue in
            self.selectedProcedureNameIndex = selectedIndex
            self.currentProcedureAppointment = self.procedureFeesList[self.selectedProcedureNameIndex]
            if let aValue = selectedValue {
                textField.text = aValue as? String
            }
            self.procedureTypeTextField.text = self.currentProcedureAppointment?.procedureType?.name
            self.chargesTextField.text = self.currentProcedureAppointment?.procedureFee?.charges
            self.procedureDetailLabel.text = self.currentProcedureAppointment?.procedureType?.details
        }, cancel: nil, origin: textField)
    }
}

extension SCDoctorApproveProcedureViewController{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if (textField == self.procedureTypeTextField) {
            self.showProcedureTypePicker(textField: textField)
            return false
        }
        return true
    }
}
