//
//  SCDashboardViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 01/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class SCDashboardViewController: UIViewController {

    @IBOutlet weak var completedAppointmentsView: UIView!
    @IBOutlet weak var upcomingAppointmentsView: UIView!
    @IBOutlet weak var cancelledAppointmentsView: UIView!
    
    @IBOutlet weak var completedAppointmentsLabel: UILabel!
    @IBOutlet weak var upcomingAppointmentsLabel: UILabel!
    @IBOutlet weak var cancelledAppointmentsLabel: UILabel!
    
    @IBOutlet weak var appointmentsScheduleButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var pickAppointmentDateText: UITextField!
    @IBOutlet weak var appointmentTypeText: UITextField!
    
    @IBOutlet weak var locationText: UITextField!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var specialityText: UITextField!
    
    
    var locations = [Location]()
    var appointmentType = [AppointmentType]()
    var specialities = [Speciality]()
    var selectedLocation: Location?
    var selectedSpeciality: Speciality?
    var selectedappointmentType: AppointmentType?
    var selectedDate: Date?
     var selectedTypeForAPI : String?
    var selectedLocationPickerIndex = 0
    var locationPickerDataSource = [String]()
    
    var selectedSpecialityPickerIndex = 0
    var specialityPickerDataSource = [String]()
    
    var appointeTypePickerIndex = 0
    var appointmentTypePickerDataSource = [String]()
    
    
    var appointmentTypeArray = ["Walk-in Consultation","Remote Consultation","Procedure Consultation"]
    private let locationPickerTitle = "Select Location"
    private let specialityPickerTitle = "Select Speciality"
    private let appointmentDatePickerTitle = "Select Appointment Date"
    private let AppointmentTypeTitle = "Select Appointment Type"
    
    var selectedIdentityIndex = 0
    
    private lazy var appointmentListVC: SCAppointmentListViewController = {
        var viewController = self.storyboard?.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.appointmentList) as! SCAppointmentListViewController
        return viewController
    }()
    
    private lazy var bookAppointmentVC: SCBookAppointmentViewController = {
        var viewController = self.storyboard?.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.bookAppointment) as! SCBookAppointmentViewController
        return viewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      // setStatusBarStyCustom()
    
        customizeNavigationItem(title: SCAppConstants.pageTitles.dashboard, isDetail: false)
        configureView()
        getLocations()
        getSpecialities()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getAppointmentsCount()
    }
    
    @IBAction func scheduleAppointmentsAction(_ sender: UIButton) {
        let appointmentScheduleVC = self.storyboard?.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.appointmentSchedule) as! SCAppointmentScheduleViewController
        self.navigationController?.pushViewController(appointmentScheduleVC, animated: true)
    }
    
    private func showLocationPicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: locationPickerTitle, rows: locationPickerDataSource, initialSelection: selectedLocationPickerIndex, doneBlock: { [weak self] (picker, selectedIndex, selectedValue) in
            self?.selectedLocationPickerIndex = selectedIndex
            self?.selectedLocation = selectedIndex != 0 ? self?.locations[selectedIndex-1] : nil
            textField.text = selectedIndex != 0 ? self?.selectedLocation?.city : self?.locationPickerTitle
            }, cancel: nil, origin: textField)
    }
    
    private func showSpecialityPicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: specialityPickerTitle, rows: specialityPickerDataSource, initialSelection: selectedSpecialityPickerIndex, doneBlock: { [weak self] (picker, selectedIndex, selectedValue) in
            self?.selectedSpecialityPickerIndex = selectedIndex
            self?.selectedSpeciality = selectedIndex != 0 ? self?.specialities[selectedIndex-1] : nil
            textField.text = selectedIndex != 0 ? self?.selectedSpeciality?.name : self?.specialityPickerTitle
            }, cancel:nil, origin: textField)
    }
    
    private func showAppointmentPicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: AppointmentTypeTitle, rows: appointmentTypePickerDataSource, initialSelection: appointeTypePickerIndex, doneBlock: { [weak self] (picker, selectedIndex, selectedValue) in
            self?.appointeTypePickerIndex = selectedIndex
            self?.selectedappointmentType = selectedIndex != 0 ? self?.appointmentType[selectedIndex-1] : nil
            textField.text = selectedIndex != 0 ? self?.selectedappointmentType?.text() : self?.AppointmentTypeTitle
            }, cancel:nil, origin: textField)
    }
    
    
    private func showDatePicker(textField:UITextField) {
        let datePicker = ActionSheetDatePicker(title: appointmentDatePickerTitle, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            [weak self] (picker, value, index) in
            self?.selectedDate = value as? Date
            let dateString = (value as! Date).toDateString(withFormatter: SCAppConstants.DateFormatType.date)
            textField.text = dateString
            return
            }, cancel: nil,
               origin: textField.superview)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }
    
    private func searchAppointmentsForDate(appointmentDate: Date) {
        SCProgressView.show()
        SCAppointmentService.searchAppointmentAPI(locationId: selectedLocation?.id, specialityId: selectedSpeciality?.id, apoointmentDate:appointmentDate, appointmentType: selectedTypeForAPI , onComplete: { [weak self] (appointmentSlots) in
            SCProgressView.hide()
            self?.showAvailableDoctorsList(appointmentSlots: appointmentSlots)
        }) { (errorMessage) in
            SCProgressView.hide()
            print("Error Search Appointments - \(errorMessage)")
        }
    }
    
    private func showAvailableDoctorsList(appointmentSlots: [AppointmentSlot]) {
        if let availableDoctorsListVC = UIStoryboard(name: SCAppConstants.storyBoardName.home, bundle: nil).instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.availableDoctorsList) as? SCAvailableDoctorsListViewController {
            availableDoctorsListVC.appointmentSlots = appointmentSlots
            availableDoctorsListVC.appointmentDate = selectedDate
            self.navigationController?.pushViewController(availableDoctorsListVC, animated: true)
        }
    }

    
    private func configureView() {
        completedAppointmentsView.layer.cornerRadius = SCAppConstants.cornerRadiusValues.large.rawValue
        upcomingAppointmentsView.layer.cornerRadius = SCAppConstants.cornerRadiusValues.large.rawValue
        cancelledAppointmentsView.layer.cornerRadius = SCAppConstants.cornerRadiusValues.large.rawValue
        appointmentsScheduleButton.layer.cornerRadius = SCAppConstants.cornerRadiusValues.large.rawValue
       
//            pickAppointmentDateText.SetRightSideImage(with: SCAppConstants.Image.calenderBlack.image())
//            appointmentTypeText.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
//            locationText.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
//            specialityText.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
       
        
    }
    
    private func getAppointmentsCount() {
        SCProgressView.show()
        SCAppointmentService.appointmentCountAPI(onComplete: { [weak self] (outputJOSN) in
            SCProgressView.hide()
            self?.completedAppointmentsLabel.text = "\(outputJOSN["completedAppointments"].int ?? 0)"
            let upcomingAppointments = outputJOSN["upcomingAppointments"].int ?? 0
            print(upcomingAppointments)
           
            self?.upcomingAppointmentsLabel.text = "\(outputJOSN["upcomingAppointments"].int ?? 0)"
            
            self?.cancelledAppointmentsLabel.text = "\(outputJOSN["cancelledAppointments"].int ?? 0)"
            self?.showAppointmentList()
//
            
        }) {[weak self] (errorMessage) in
            print("Error getting appointment count- \(errorMessage)")
            SCProgressView.hide()
            self?.showAppointmentList()
        }
    }
    private func createLocationPickerDataSource() {
        locationPickerDataSource.append(locationPickerTitle)
        for location in locations {
            locationPickerDataSource.append(location.city ?? "")
        }
    }
    
    private func createSpecialityPickerDataSource() {
        specialityPickerDataSource.append(specialityPickerTitle)
        for speciality in specialities {
            specialityPickerDataSource.append(speciality.name ?? "")
        }
    }
    
    private func showAppointmentList() {
       removeAsChildViewController(viewController: bookAppointmentVC)
        
        self.addAsChildViewController(viewController: appointmentListVC)
    }
    
    func showBookAppointmentsScreen() {
        removeAsChildViewController(viewController: appointmentListVC)
        self.addAsChildViewController(viewController: bookAppointmentVC)
    }
    
    private func addAsChildViewController(viewController: UIViewController) {
        addChild(viewController)
        containerView.addSubview(viewController.view)
        viewController.view.frame = containerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
    }
    private func  getLocations() {
        SCCommanService.getLocationsListAPI(onComplete: { [weak self] (locations, success) in
            if success {
                self?.locations = locations
                self?.createLocationPickerDataSource()
            }
        }) { (errorMessage) in
            print("Error Locations API - \(errorMessage)")
        }
    }
    
    func  getSpecialities() {
        SCCommanService.getSpecialityListAPI(onComplete: { [weak self] (specialities, success) in
            if success {
                self?.specialities = specialities
                self?.createSpecialityPickerDataSource()
            }
        }) { (errorMessage) in
            print("Error Speciality API - \(errorMessage)")
        }
    }
    private func removeAsChildViewController(viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    func showAppointmentType(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: AppointmentTypeTitle, rows: self.appointmentTypeArray, initialSelection: 0, doneBlock: { picker, selectedIndex, selectedValue in
            self.selectedIdentityIndex = selectedIndex
            if let aValue = selectedValue {
                let ourValue = aValue as? String
                textField.text = ourValue
                self.selectedTypeForAPI = ourValue
                
//                textField.text = aValue as? String
            }
        }, cancel: { picker in
        }, origin: textField)
    }
    
    
    
    
    @IBAction func searchAppointmentTapped(_ sender: Any) {
        if let appointmentDate = selectedDate {
            searchAppointmentsForDate(appointmentDate: appointmentDate)
        } else {
            showAlert(title: SCAppConstants.pageTitles.bookAppointment, msg: SCAppConstants.validationMessages.searchAppointmentValidationMessage)
        }
    }
}

extension SCDashboardViewController {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if (textField == locationText) {
            showLocationPicker(textField: textField)
            return false
        } else if (textField == specialityText) {
            showSpecialityPicker(textField: textField)
            return false
        } else if (textField == pickAppointmentDateText) {
            showDatePicker(textField: textField)
            return false
        }else if (textField == appointmentTypeText) {
            showAppointmentType(textField: textField)
            return false
        }

        return true
    }
}
