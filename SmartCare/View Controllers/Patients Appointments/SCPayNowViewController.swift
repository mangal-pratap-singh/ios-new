//
//  SCPayNowViewController.swift
//  SmartCare
//
//  Created by Rediflex PvtLtd on 19/06/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire
import WebKit
import SystemConfiguration

class SCPayNowViewController: UIViewController,WKNavigationDelegate  {
    
    var accessCode :String!
    var merchantId :String!
    var orderId :String!
    var amount :String!
    var currency :String!
   var redirectUrl :String!
    var cancelUrl :String?
    var UsersAppointmentID : Int!
//    var rsaKeyUrl :String?
    var rsaKeyDataStr  = String()
    var rsaKey :String!
    static var statusCode = 0//zero means success or else error in encrption with rsa
    var encVal = String()
    var rsaKeyUrl = String()
    
    let patientId = UserProfile.getUserId()
    @IBOutlet weak var wmWebView: WKWebView!
       override func viewDidLoad() {
        super.viewDidLoad()
         customizeNavigationRootBackItem(title: SCAppConstants.pageTitles.paymentDetails, isDetail: true)
        view.backgroundColor = .white
        wmWebView.navigationDelegate = self
//         rsaKeyUrl = SCAppConstants.apiBaseURL() + "patients/" + patientId + "/appointments/" + "\(UsersAppointmentID)" + "/get-rsa-key?"
//        accessCode=" + "\(accessCode)" + "&orderId=" + "\(orderId)
//       PayNowCCAvenue()
       
        print(accessCode ?? "")
        print(merchantId ?? "")
        print(orderId ?? "")
        print(amount ?? "")
        print(currency ?? "")
//        print(redirectUrl)
        print(cancelUrl ?? "")
//        print(rsaKeyUrl)
        print(rsaKeyDataStr)
        print(rsaKey ?? "")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        SCProgressView.show()
////        PayNowCCAvenue()
//        /**
//         * In viewWillAppear we will call gettingRsaKey method to generate RSA Key for the transaction and use the same to encrypt data
//         */
//
//        self.gettingRsaKey(){
//            (success, object) -> () in
//            DispatchQueue.main.sync {
//                if success {
//                  self.encyptCardDetails(data: object as! Data)
//                }
//                else{
//                    self.displayAlert(msg: object as! String)
//                }
//            }
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //        LoadingOverlay.shared.showOverlay(view: self.view)
         SCProgressView.show()
        //        PayNowCCAvenue()
                /**
                 * In viewWillAppear we will call gettingRsaKey method to generate RSA Key for the transaction and use the same to encrypt data
                 */
                
                self.gettingRsaKey(){
                    (success, object) -> () in
                    DispatchQueue.main.sync {
                        if success {
                            
                          self.encyptCardDetails(data: object as! Data)
                        }
                        else{
                             SCProgressView.hide()
                            self.displayAlert(msg: object as! String)
                        }
                    }
                }
        
         }
    
   
    //MARK:
    //MARK: Get RsaKey & encrypt card details
    
    /**
     * In this method we will generate RSA Key from the URL for this we will pass order id and the access code as the request parameter
     * after the successful key generation we'll pass the data to the request handler using complition block
     */
    
    private func gettingRsaKey(completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()){
        
        
//        print(rsaKey)

        let serialQueue = DispatchQueue(label: "serialQueue", qos: .userInitiated)

        serialQueue.sync {
            self.rsaKeyDataStr = "access_code=\(self.accessCode ?? "")&order_id=\(self.orderId ?? "")"
             print(rsaKeyDataStr)
            //            let requestData = self.rsaKeyDataStr.data(using: String.Encoding.utf8, allowLossyConversion: true)

            let requestData = self.rsaKeyDataStr.data(using: String.Encoding.utf8)
//            REPLACE CANCEL WITH RSAURL
            guard let urlFromString = URL(string: self.cancelUrl ?? "") else{
                 SCProgressView.hide()
                return
            }
            
            var urlRequest = URLRequest(url: urlFromString)
            urlRequest.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "content-type")
            urlRequest.httpMethod = "POST"
            urlRequest.httpBody = requestData

            let session = URLSession(configuration: URLSessionConfiguration.default)
            print("session",session)


            session.dataTask(with: urlRequest as URLRequest) {
                (data, response, error) -> Void in

                if let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode{

                    guard let data = data else{
                        print("No value for data")
                        completion(false, "Not proper data for RSA Key" as AnyObject?)
                        return
                    }
                    print("data :: ",data)
                    completion(true, data as AnyObject?)
                }
                else{
                    completion(false, "Unable to generate RSA Key please check" as AnyObject?)
                }
                }.resume()
        }
    }
    
    var request: NSMutableURLRequest?
    
    /**
     * encyptCardDetails method we will use the rsa key to encrypt amount and currency and onece the encryption is done we will pass this encrypted data to the initTrans to initiate payment
     */
    
    private func encyptCardDetails(data: Data){
        guard let rsaKeytemp = String(bytes: data, encoding: String.Encoding.ascii) else{
             print("No value for rsaKeyTemp")
             SCProgressView.hide()
            return
          }
//        rsaKey = rsaKeytemp
//        rsaKey = self.rsaKey.trimmingCharacters(in: CharacterSet.newlines)
        rsaKey =  "-----BEGIN PUBLIC KEY-----\n\(self.rsaKey ?? "")\n-----END PUBLIC KEY-----\n"
        print("rsaKey :: ",rsaKey)


       let myRequestString = "amount=\(amount ?? "")&currency=\(currency ?? "")"
        print(myRequestString)
//
        let ccTool = CCTool()
        var encVal = ccTool.encryptRSA(myRequestString, key: rsaKey)
       // print(encVal)
        
//        let originalString = "test/test"
//        let escapedString = originalString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
//        print(escapedString!)

        encVal = CFURLCreateStringByAddingPercentEscapes(
            nil,
            encVal! as CFString,
            nil,
            "!*'();:@&=+$,/?%#[]" as CFString,
            CFStringBuiltInEncodings.UTF8.rawValue) as String?
        SCPayNowViewController.statusCode = 0

        //Preparing for webview call
        if SCPayNowViewController.statusCode == 0{
            let urlAsString = "https://secure.ccavenue.com/transaction/initTrans"

//            i have changed the redirectUrl below to canceluRL
            let encryptedStr = "merchant_id=\(merchantId ?? "")&order_id=\(orderId ?? "")&redirect_url=\(redirectUrl ?? "")&cancel_url=\(cancelUrl ?? "")&enc_val=\(encVal!)&access_code=\(accessCode ?? "")"

            print("encValue :: \(encVal ?? "No val for encVal")")

            print("encryptedStr :: ",encryptedStr)
            let myRequestData = encryptedStr.data(using: String.Encoding.utf8)
            // request = NSMutableURLRequest(url: URL(string: urlAsString)!)

            request  = NSMutableURLRequest(url: URL(string: urlAsString)! as URL, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: 30)
            request?.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "content-type")
            request?.setValue(urlAsString, forHTTPHeaderField: "Referer")
            request?.httpMethod = "POST"
            request?.httpBody = myRequestData
           // print("\n\n\nwebview :: ",request?.url as Any)
            //print("\n\n\nwebview :: ",request?.description as Any)
           // print("\n\n\nwebview :: ",request?.httpBody?.description as Any)
           // print("\n\n\nwebview :: ",request?.allHTTPHeaderFields! as Any)

            let session = URLSession(configuration: URLSessionConfiguration.default)
           // print("session",session)

            session.dataTask(with: request! as URLRequest) {
                (data, response, error) -> Void in

                if let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode{

                    guard let data = data else{
                        print("No value for data")
                         SCProgressView.hide()
                        return
                    }
                    DispatchQueue.main.async {
                        self.wmWebView.load(self.request! as URLRequest)
                    }
                    print("data :: ",data)
                }
                else{
                    print("into else")
                    self.displayAlert(msg: "Unable to load webpage currently, Please try again later.")
                }
                }.resume()

            //print(viewWeb.isLoading)
        }
        else{
            print("Unable to create requestURL")
            displayAlert(msg: "Unable to create requestURL")
        }
    }
    
    func displayAlert(msg: String){
        let alert: UIAlertController = UIAlertController(title: "ERROR", message: msg, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
          //  LoadingOverlay.shared.hideOverlayView()
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func encryptRSA(raw: String, key pubKey: String) {
        
    }
    //MARK:
    //MARK: WebviewDelegate Methods
    
    func PayNowCCAvenue(){
        let myRequestString = "amount=\(amount)&currency=\(currency)"
        //
        let ccTool = CCTool()
        var encVal = ccTool.encryptRSA(myRequestString, key: rsaKey)
        
        encVal = CFURLCreateStringByAddingPercentEscapes(
            nil,
            encVal! as CFString,
            nil,
            "!*'();:@&=+$,/?%#[]" as CFString,
            CFStringBuiltInEncodings.UTF8.rawValue) as String?
        SCPayNowViewController.statusCode = 0
        
        //Preparing for webview call
        if SCPayNowViewController.statusCode == 0{
            let urlAsString = "https://secure.ccavenue.com/transaction/initTrans"
            
            //            i have changed the redirectUrl below to canceluRL
            let encryptedStr = "merchant_id=\(merchantId)&order_id=\(orderId)&redirect_url=\(redirectUrl)&cancel_url=\(cancelUrl)&enc_val=\(encVal!)&access_code=\(accessCode)"
            
            print("encValue :: \(encVal ?? "No val for encVal")")
            
            print("encryptedStr :: ",encryptedStr)
            let myRequestData = encryptedStr.data(using: String.Encoding.utf8)
            // request = NSMutableURLRequest(url: URL(string: urlAsString)!)
            
            request  = NSMutableURLRequest(url: URL(string: urlAsString)! as URL, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: 30)
            request?.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "content-type")
            request?.setValue(urlAsString, forHTTPHeaderField: "Referer")
            request?.httpMethod = "POST"
            request?.httpBody = myRequestData
           // print("\n\n\nwebview :: ",request?.url as Any)
           // print("\n\n\nwebview :: ",request?.description as Any)
            //print("\n\n\nwebview :: ",request?.httpBody?.description as Any)
           // print("\n\n\nwebview :: ",request?.allHTTPHeaderFields! as Any)
            
            let session = URLSession(configuration: URLSessionConfiguration.default)
            print("session",session)
            
            session.dataTask(with: request! as URLRequest) {
                (data, response, error) -> Void in
                
                if let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode{
                    
                    guard let data = data else{
                        print("No value for data")
                         SCProgressView.hide()
                        return
                    }
                    DispatchQueue.main.async {
                        self.wmWebView.load(self.request! as URLRequest)
                    }
                    print("data :: ",data)
                }
                else{
                    print("into else")
                    self.displayAlert(msg: "Unable to load webpage currently, Please try again later.")
                }
                }.resume()
            
           // print(viewWeb.isLoading)
        }
        else{
            print("Unable to create requestURL")
            displayAlert(msg: "Unable to create requestURL")
        }
    }
   
    
    //MARK: WebviewDelegate Methods
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
         SCProgressView.hide()
        
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
         SCProgressView.hide()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
   
    
  

