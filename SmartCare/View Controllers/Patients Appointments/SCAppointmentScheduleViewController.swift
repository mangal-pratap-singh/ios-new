//
//  SCAppointmentScheduleViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 16/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCAppointmentScheduleViewController: UIViewController {

    private let noOfSecondForWeak = 604799
    private let weakDisplayDateFormat = "MMM dd, yyyy"
    
    @IBOutlet weak var weakDateLabel: UILabel!
    @IBOutlet weak var appointmentsListTable: UITableView!
    
    var displayDate = Date()
    var appointments = [Appointment]()
    var fullScreenMessageView:FullScreenMessageView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.appointmentSchedule, isDetail: true)
        configureView()
        registerNibsForCells()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Disable Drawer for Detailed Pages
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = false
        getAppointmentSchedule()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = true
    }
    
    @IBAction func nextButtonAction(_ sender: UIButton) {
        let nextDate = Calendar.current.date(byAdding: .second, value: noOfSecondForWeak, to: displayDate)!
        displayDate = nextDate
        showWeakDateStringFromDate()
        getAppointmentSchedule()
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        let previousDate = Calendar.current.date(byAdding: .second, value: -noOfSecondForWeak, to: displayDate)!
        displayDate = previousDate
        showWeakDateStringFromDate()
        getAppointmentSchedule()
    }
    
    private func configureView() {
        showWeakDateStringFromDate()
        appointmentsListTable.tableFooterView = UIView()
    }
    
    private func registerNibsForCells() {
        let nibName = UINib(nibName: SCAppConstants.nibNames.appointmentListItemTableCell, bundle:nil)
        appointmentsListTable.register(nibName, forCellReuseIdentifier: SCAppConstants.cellReuseIdentifiers.appointmentListItem)
    }
    
    private func showWeakDateStringFromDate() {
        let startWeakDate = (displayDate.startOfWeek).toDateString(withFormatter: weakDisplayDateFormat)
        let endWeakDate = (displayDate.endOfWeek).toDateString(withFormatter: weakDisplayDateFormat)
        let weakNumber = NSCalendar.current.component(.weekOfYear, from: displayDate)
        weakDateLabel.text = " Weak \(weakNumber): \(startWeakDate) - \(endWeakDate) "
    }

    private func getAppointmentSchedule() {
        SCProgressView.show()
        SCAppointmentService.appointmentScheduleForWeakAPI(weakStartDate: displayDate.startOfWeek, onComplete: { [weak self] (appointments) in
            SCProgressView.hide()
            self?.fullScreenMessageView?.removeFromSuperview()
            if appointments.count == 0 {
                self?.fullScreenMessageView = self?.showFullScreenMessageOnView(parentView: self!.appointmentsListTable, withMessage: "No appointments for selected period")
                self?.view.addSubview(self!.fullScreenMessageView!)
            }
            self?.appointments = appointments
            self?.appointmentsListTable.reloadData()
            
        }) { (errorMessage) in
            SCProgressView.hide()
            print("Error Fetching Appointmnet Schedule - \(errorMessage)")
        }
    }
}

extension SCAppointmentScheduleViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appointments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SCAppConstants.cellReuseIdentifiers.appointmentListItem, for: indexPath as IndexPath) as! AppointmentListItemTableViewCell
        cell.selectionStyle = .none
        let currentAppointment = appointments[indexPath.row]
        cell.configureCell(WithAppointment: currentAppointment)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentAppointment = appointments[indexPath.row]
        print(currentAppointment.clinic)
        print(currentAppointment.location)
        loadDetailScreenForAppointment(currentAppointment)
    }
    
    private func loadDetailScreenForAppointment(_ appointment: Appointment) {
        if let appointmentDetailsVC = UIStoryboard(name: SCAppConstants.storyBoardName.home, bundle: nil).instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.appointmentDetails) as? SCAppointmentDetailsViewController {
            appointmentDetailsVC.currentAppointment = appointment
            self.navigationController?.pushViewController(appointmentDetailsVC, animated: true)
        }
    }
}


