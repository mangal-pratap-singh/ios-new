//
//  SCAvailableDoctorsViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 13/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCAvailableDoctorsListViewController: UIViewController {

    @IBOutlet weak var doctorsListTable: UITableView!
    var appointmentSlots = [AppointmentSlot]()
    var appointmentDate: Date?
    var fullScreenMesageView: FullScreenMessageView?
    var appointmentType : AppointmentType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.availableDoctors, isDetail: true)
        configureView()
        registerNibsForCells()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Disable Drawer for Detailed Pages
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = false
        fullScreenMesageView?.removeFromSuperview()
        if appointmentSlots.count == 0 {
            fullScreenMesageView = showFullScreenMessageOnView(parentView: doctorsListTable, withMessage: "No Doctors Available For This Slot")
            self.view.addSubview(fullScreenMesageView!)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = true
    }
    
    private func configureView() {
        doctorsListTable.tableFooterView = UIView()
    }
    
    private func registerNibsForCells() {
        let nibName = UINib(nibName: SCAppConstants.nibNames.availableDoctorTableCell, bundle:nil)
        doctorsListTable.register(nibName, forCellReuseIdentifier: SCAppConstants.cellReuseIdentifiers.availableDoctorListItem)
    }
}

extension SCAvailableDoctorsListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appointmentSlots.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SCAppConstants.cellReuseIdentifiers.availableDoctorListItem, for: indexPath as IndexPath) as! AvailableDoctorTableViewCell
        cell.selectionStyle = .none
        let appointmentSlot = appointmentSlots[indexPath.row]
        cell.configureCell(WithAppointmentSlot: appointmentSlot)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentAppointmentSlot = appointmentSlots[indexPath.row]
        showRequestAppointmentScreen(currentAppointmentSlot)
    }
    
    private func showRequestAppointmentScreen(_ appointmentSlot: AppointmentSlot) {
        if let requestAppintmentVC = UIStoryboard(name: SCAppConstants.storyBoardName.home, bundle: nil).instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.requestAppointment) as? SCRequestAppointmentViewController {
            requestAppintmentVC.appointmentSlot = appointmentSlot
            requestAppintmentVC.appointmentDate = appointmentDate
            requestAppintmentVC.appointmentType = appointmentType
            self.navigationController?.pushViewController(requestAppintmentVC, animated: true)
        }
    }
}
