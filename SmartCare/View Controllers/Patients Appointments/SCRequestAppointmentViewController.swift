//
//  SCRequestAppointmentViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 13/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0


class SCRequestAppointmentViewController: UIViewController {

    private let timeSlotPickerTitle = "Select Time Slot"
    private let patientPickerTitle = "Select Patient"
     private let paymentPickerTitle = "Select Payment Mode"
    
    enum paymentTypeOption : String {
        case clinic = "Pay at Clinic"
        case online = "Pay Online"
        
        static let arr = [clinic.rawValue, online.rawValue]
        
        var code:String {
            switch self {
            case .clinic:
                return "C"
            case .online:
                return "O"
            }
        }
    }
    
    var selectedIdentityIndex = 0
    var currentAppointment: Appointment?
    var appointmentSlot: AppointmentSlot?
    var appointmentDate: Date?
    var selectedTimeslotId: Int?
    var patientsIdArray = [String]()
    var patientsNameArray = [String]() 
    var selectedDependentId:String?
    var appointmentType : AppointmentType?
    var selectedPayment : paymentTypeOption?
    
    var PayNowmodelClassObject : PayNowModel? = nil
    var tokenGenerationObject : VideoToken? = nil
    var RSAmodelClassObject : RSA? = nil
//    var paymentTypeString : String?
    
    
    var selectedPatientPickerIndex = 0
    var patientPickerDataSource = [String]()
    
    var selectedTimeslotPickerIndex = 0
    var timeSlotPickerDataSource = [String]()
    
    @IBOutlet weak var doctorNameTextfield: UITextField!
    @IBOutlet weak var doctorDegreeTextfield: UITextField!
    @IBOutlet weak var doctorSpecialityTextfield: UITextField!
    @IBOutlet weak var hospitalNameTextfield: UITextField!
    @IBOutlet weak var appointmentDateTextfield: UITextField!
    @IBOutlet weak var timeSlotTextfield: UITextField!
    @IBOutlet weak var patientTextfield: UITextField!
    @IBOutlet weak var doctorAvtarImageView: UIImageView!
    @IBOutlet weak var appointmentTypeText: UITextField!
    
    @IBOutlet weak var paymentMethod: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.requestAppointment, isDetail: true)
        configureView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Disable Drawer for Detailed Pages
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = false
        showAppointmentSlotDetails()
        getManageDependentList()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = true
    }
    
    @IBAction func confirmAppointmentButtonAction(_ sender: UIButton) {
        bookAppointmentRequest()
    }
    
    private func configureView() {
        doctorAvtarImageView.layer.cornerRadius = doctorAvtarImageView.frame.width/2
        doctorAvtarImageView.layer.masksToBounds = true
        paymentMethod.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        timeSlotTextfield.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        patientTextfield.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
    }
    
    private func createPatientPickerDataSource() {
        patientPickerDataSource.removeAll()
        patientPickerDataSource.append(patientPickerTitle)
        for patient in patientsNameArray {
            patientPickerDataSource.append(patient)
        }
       
    }
    
    private func createTimeSlotPickerDataSource() {
        if let availableTimeSlots = appointmentSlot?.availableSlots {
            timeSlotPickerDataSource.append(timeSlotPickerTitle)
            for timeSlot in availableTimeSlots {
                timeSlotPickerDataSource.append(timeSlot.time?.toTimeString() ?? "")
            }
        }
    }
    
    private func showAppointmentSlotDetails() {
        if let currentSlot = self.appointmentSlot {
            doctorNameTextfield.text = currentSlot.doctorName
            doctorDegreeTextfield.text = currentSlot.doctorDegree
            doctorSpecialityTextfield.text = currentSlot.specialities
            hospitalNameTextfield.text = "\(currentSlot.hospitalName ?? "") - \(currentSlot.clinicName ?? "")"
            appointmentTypeText.text = appointmentType?.text()
//            appointmentTypeText.text = currentSlot
            
            if let date = appointmentDate {
                let dateString = date.toDateString(withFormatter: SCAppConstants.DateFormatType.date)
                appointmentDateTextfield.text = dateString
            }

            if let imageBaseURL = AppInfo.getAppInfo()?.s3Url, let avtarImageURL = currentSlot.doctorAvatarURL {
                // Load Image From URLwith Caching
                let doctorAvtarImageURLString = imageBaseURL + avtarImageURL
                if let url = URL(string: doctorAvtarImageURLString) {
                    doctorAvtarImageView.sd_setImage(with: url, placeholderImage: SCAppConstants.Image.userProfile.image())
                }
            }
            createTimeSlotPickerDataSource()
        }
    }
    
    private func getManageDependentList(){
        SCManageDependentServices.manageDependentListAPI(onComplete: {[weak self](dependnetList) in
            self?.patientsIdArray.append(UserProfile.getUserId())
            self?.patientsNameArray.append("Self") // This is Default value for logged in patient
            for dependent in dependnetList {
                let dependentName = "\(dependent.firstName ?? "") \(dependent.lastNmae ?? "")"
                self?.patientsIdArray.append(dependent.dependentID ?? "")
                self?.patientsNameArray.append(dependentName)
                self?.createPatientPickerDataSource()
            }
           
        }) { (errorMessage) in
        }
    }
    
    func payNowTappedFunct(appointId: Int){
        
            if appointId != 0 {
            SCProgressView.show()
        SCAppointmentService.payNowAPI(UsersAppointmentID: appointId, completionHandler: { (model) in
                    SCProgressView.hide()
                    self.PayNowmodelClassObject = model
                                    print("PayNowModel:" + "\(self.PayNowmodelClassObject as Any)")
                  
            self.GetRSA(appointMentId: appointId)
            
                }) { (err) in
                    SCProgressView.hide()
                  print("payNowTappedFunct_Error=\(err)")
                 }
            }else{
                SCProgressView.hide()
          }
            
        }
    
    private func GetRSA(appointMentId: Int){
          SCProgressView.show()
          let appointmentId = appointMentId
          let UsersaccessCode = self.PayNowmodelClassObject?.accessCode as Any
          let UsersOrderID = self.PayNowmodelClassObject?.orderId
     
        SCAppointmentService.RSAstringAPI(UsersAppointmentID: appointmentId, UsersaccessCode: UsersaccessCode as! String, UsersOrderID: UsersOrderID!, completionHandler: { (model) in
               SCProgressView.hide()
                  self.RSAmodelClassObject = model
                  print(self.RSAmodelClassObject as Any)
//                  DispatchQueue.main.async {
//                      print(self.RSAmodelClassObject?.rsaKey! as Any)
//                  }
             self.paymentScreenOpenMeth()
              }){ (err) in
                   SCProgressView.hide()
                  print(err)
          }

        }
      
    
    
    func paymentScreenOpenMeth(){
        if self.RSAmodelClassObject?.rsaKey == nil || self.RSAmodelClassObject?.rsaKey == ""{
            return
          }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SCPayNowViewController")  as! SCPayNowViewController
                       
                       
                       //        vc.paymentData = self.PayNowmodelClassObject as [String : AnyObject]
                               vc.accessCode = self.PayNowmodelClassObject?.accessCode
                               vc.rsaKey = self.RSAmodelClassObject?.rsaKey
                               vc.amount = self.PayNowmodelClassObject?.amount
                               vc.merchantId = self.PayNowmodelClassObject?.merchantId
                               vc.currency = self.PayNowmodelClassObject?.currency
                               vc.orderId = self.PayNowmodelClassObject?.orderId
                               vc.cancelUrl = self.PayNowmodelClassObject?.cancelUrl
                              vc.redirectUrl = self.PayNowmodelClassObject?.redirectUrl
                       vc.UsersAppointmentID = self.currentAppointment?.id
            self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func showTimeslotsPicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: timeSlotPickerTitle, rows: timeSlotPickerDataSource, initialSelection: selectedTimeslotPickerIndex, doneBlock: { [weak self] (picker, selectedIndex, selectedValue) in
            self?.selectedTimeslotPickerIndex = selectedIndex
            self?.selectedTimeslotId = selectedIndex != 0 ? self?.appointmentSlot?.availableSlots[selectedIndex-1].id : nil
            self?.timeSlotTextfield.text = selectedIndex != 0 ? self?.appointmentSlot?.availableSlots[selectedIndex-1].time?.toTimeString() : self?.timeSlotPickerTitle
        }, cancel:nil, origin: textField)
    }
    private func showPatientsPicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: patientPickerTitle, rows: patientPickerDataSource, initialSelection: selectedPatientPickerIndex, doneBlock: { [weak self] (picker, selectedIndex, selectedValue) in
            self?.selectedPatientPickerIndex = selectedIndex
            self?.selectedDependentId = selectedIndex != 0 ? self?.patientsIdArray[selectedIndex-1] : nil
            self?.patientTextfield.text = selectedIndex != 0 ? self?.patientsNameArray[selectedIndex-1] : self?.patientPickerTitle
        }, cancel: nil, origin: textField)
    }
    
    func showPaymentPicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: paymentPickerTitle, rows: paymentTypeOption.arr, initialSelection: 0, doneBlock: { picker, selectedIndex, selectedValue in
            self.selectedPayment = paymentTypeOption(rawValue: paymentTypeOption.arr[selectedIndex])
            self.paymentMethod.text = self.selectedPayment?.rawValue
            
//            self.selectedIdentityIndex = selectedIndex
//            if let aValue = selectedValue {
//                let mode = aValue as? String
//                self.paymentMethod.text = mode
////                self.selectedDependentId = mode
//                self.paymentTypeString = mode
//
//            }
        }, cancel: { picker in
        }, origin: textField)
    }
    private func bookAppointmentRequest() {
        SCProgressView.show()
        if let timeslotId = self.selectedTimeslotId, let patientId = self.selectedDependentId , let paymentType = self.selectedPayment?.code , let appointmentType = appointmentType

        {
            SCAppointmentService.bookAppointmentRequestAPI(slotId: timeslotId, patientId: patientId, paymentType :paymentType, appointmentType : appointmentType.Desc() ,  onComplete: { (jsonData) in
                SCProgressView.hide()
               // SCRoutingManager.sharedInstance.showHomeDashboard()
                if let jsosDict = jsonData.dictionaryObject{
                    
                    let appointMentId = jsosDict["appointmentId"] as? Int ?? 0
                    self.payNowTappedFunct(appointId: appointMentId)
                }
               
                 
                 
                
            }) { [weak self] (errorMessage) in
                SCProgressView.hide()
                self?.showAlert(title: SCAppConstants.pageTitles.requestAppointment, msg: errorMessage)
            }
        }else{
            
             SCProgressView.hide()
            self.showAlert(title: SCAppConstants.pageTitles.requestAppointment, msg: "Some field is not selected so please check once")
        }
    }
}

extension SCRequestAppointmentViewController{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if (textField == timeSlotTextfield) {
            showTimeslotsPicker(textField: textField)
            return false
        } else if (textField == patientTextfield) {
            showPatientsPicker(textField: textField)
            return false
        }else if (textField == paymentMethod) {
            showPaymentPicker(textField: textField)
            return false
        }
        return true
    }
}

