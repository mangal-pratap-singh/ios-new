//
//  SCAppointmentListViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 03/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCAppointmentListViewController: UIViewController {

    @IBOutlet weak var appointmentsListTable: UITableView!
    var appointments = [Appointment]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        registerNibsForCells()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getAppointmentList()
    }
    
    private func configureView() {
        appointmentsListTable.tableFooterView = UIView()
    }
    
    private func registerNibsForCells() {
        let nibName = UINib(nibName: SCAppConstants.nibNames.appointmentListItemTableCell, bundle:nil)
        appointmentsListTable.register(nibName, forCellReuseIdentifier: SCAppConstants.cellReuseIdentifiers.appointmentListItem)
    }
    
    private func getAppointmentList() {
        SCProgressView.show()
        SCAppointmentService.appointmentsListAPI(onComplete: { [weak self] (appointmentList) in
            SCProgressView.hide()
           
            if appointmentList.count == 0 {
                (self?.parent as! SCDashboardViewController).showBookAppointmentsScreen()
            } else {
                self?.appointments = appointmentList
                self?.appointmentsListTable.reloadData()
            }
        }) {(errorMessage) in
            SCProgressView.hide()
        }
    }
}




extension SCAppointmentListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appointments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SCAppConstants.cellReuseIdentifiers.appointmentListItem, for: indexPath as IndexPath) as! AppointmentListItemTableViewCell
        cell.selectionStyle = .none
        let currentAppointment = appointments[indexPath.row]
        cell.configureCell(WithAppointment: currentAppointment)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentAppointment = appointments[indexPath.row]
        print(appointments[indexPath.row])
        loadDetailScreenForAppointment(currentAppointment)
    }
    
    private func loadDetailScreenForAppointment(_ appointment: Appointment) {
        if let appointmentDetailsVC = UIStoryboard(name: SCAppConstants.storyBoardName.home, bundle: nil).instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.appointmentDetails) as? SCAppointmentDetailsViewController {
            appointmentDetailsVC.currentAppointment = appointment
            self.navigationController?.pushViewController(appointmentDetailsVC, animated: true)
        }
    }
}

