//
//  SCBookAppointmentViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 05/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class SCBookAppointmentViewController: UIViewController {
    
    private let locationPickerTitle = "Select Location"
    private let specialityPickerTitle = "Select Speciality"
    private let appointmentDatePickerTitle = "Select Appointment Date"
    private let AppointmentTypeTitle = "Select Appointment Type"
    
    @IBOutlet weak var appointmentDateTextField: UITextField!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var specialityTextField: UITextField!
    @IBOutlet weak var appointmentTypeTestFeild: UITextField!
    
    var locations = [Location]()
    var specialities = [Speciality]()
    var selectedLocation: Location?
    var selectedSpeciality: Speciality?
    var selectedappointmentType: AppointmentType?
    var selectedDate: Date?
    var selectedIdentityIndex = 0
    var selectedLocationPickerIndex = 0
    var locationPickerDataSource = [String]()
    
    var selectedSpecialityPickerIndex = 0
    var specialityPickerDataSource = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.bookAppointment, isDetail: false)
        configureView()
        getLocations()
        getSpecialities()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func searchAppoinmentButtonAction(_ sender: Any) {
        if selectedDate == nil {
            showAlert(title: SCAppConstants.pageTitles.bookAppointment, msg: SCAppConstants.validationMessages.searchAppointmentValidationMessage)
            return
        }
        if self.selectedappointmentType == nil {
            showAlert(title: SCAppConstants.pageTitles.bookAppointment, msg: SCAppConstants.validationMessages.searchAppointmentTypeMessage)
            return
        }
        searchAppointmentsForDate(appointmentDate: selectedDate!)
    }
    
    private func configureView() {
        appointmentDateTextField.SetRightSideImage(with: SCAppConstants.Image.calenderBlack.image())
        locationTextField.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        specialityTextField.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
    }
    
    private func createLocationPickerDataSource() {
        locationPickerDataSource.append(locationPickerTitle)
        for location in locations {
            locationPickerDataSource.append(location.city ?? "")
        }
    }
    
    private func createSpecialityPickerDataSource() {
        specialityPickerDataSource.append(specialityPickerTitle)
        for speciality in specialities {
            specialityPickerDataSource.append(speciality.name ?? "")
        }
    }
    
    private func  getLocations() {
        SCCommanService.getLocationsListAPI(onComplete: { [weak self] (locations, success) in
            if success {
                self?.locations = locations
                self?.createLocationPickerDataSource()
            }
        }) { (errorMessage) in
            print("Error Locations API - \(errorMessage)")
        }
    }
    
    func  getSpecialities() {
        SCCommanService.getSpecialityListAPI(onComplete: { [weak self] (specialities, success) in
            if success {
                self?.specialities = specialities
                self?.createSpecialityPickerDataSource()
            }
        }) { (errorMessage) in
            print("Error Speciality API - \(errorMessage)")
        }
    }
    
    private func showLocationPicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: locationPickerTitle, rows: locationPickerDataSource, initialSelection: selectedLocationPickerIndex, doneBlock: { [weak self] (picker, selectedIndex, selectedValue) in
            self?.selectedLocationPickerIndex = selectedIndex
            self?.selectedLocation = selectedIndex != 0 ? self?.locations[selectedIndex-1] : nil
            textField.text = selectedIndex != 0 ? self?.selectedLocation?.city : self?.locationPickerTitle
        }, cancel: nil, origin: textField)
    }
    
    private func showAppointmentPicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: AppointmentTypeTitle, rows: AppointmentType.arr.map({$0.text()}), initialSelection: 0, doneBlock: { [weak self] (picker, selectedIndex, selectedValue) in
            self?.selectedappointmentType = AppointmentType(rawValue: AppointmentType.arr[selectedIndex].Desc())
            textField.text = self?.selectedappointmentType?.text()
            }, cancel:nil, origin: textField)
    }
    
    private func showSpecialityPicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: specialityPickerTitle, rows: specialityPickerDataSource, initialSelection: selectedSpecialityPickerIndex, doneBlock: { [weak self] (picker, selectedIndex, selectedValue) in
            self?.selectedSpecialityPickerIndex = selectedIndex
            self?.selectedSpeciality = selectedIndex != 0 ? self?.specialities[selectedIndex-1] : nil
            textField.text = selectedIndex != 0 ? self?.selectedSpeciality?.name : self?.specialityPickerTitle
        }, cancel:nil, origin: textField)
    }
    
    private func showDatePicker(textField:UITextField) {
        let datePicker = ActionSheetDatePicker(title: appointmentDatePickerTitle, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            [weak self] (picker, value, index) in
            self?.selectedDate = value as? Date
           let dateString = (value as! Date).toDateString(withFormatter: SCAppConstants.DateFormatType.date)
            textField.text = dateString
            return
        }, cancel: nil,
        origin: textField.superview)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }
    
    private func searchAppointmentsForDate(appointmentDate: Date) {
        SCProgressView.show()
        SCAppointmentService.searchAppointmentAPI(locationId: selectedLocation?.id, specialityId: selectedSpeciality?.id, apoointmentDate:appointmentDate, appointmentType: self.selectedappointmentType?.Desc() , onComplete: { [weak self] (appointmentSlots) in
            SCProgressView.hide()
            self?.showAvailableDoctorsList(appointmentSlots: appointmentSlots)
        }) { (errorMessage) in
            SCProgressView.hide()
            print("Error Search Appointments - \(errorMessage)")
            self.showAlert(title: SCAppConstants.pageTitles.bookAppointment, msg: errorMessage)
        }
    }
    
    
    private func showAvailableDoctorsList(appointmentSlots: [AppointmentSlot]) {
        if let availableDoctorsListVC = UIStoryboard(name: SCAppConstants.storyBoardName.home, bundle: nil).instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.availableDoctorsList) as? SCAvailableDoctorsListViewController {
            availableDoctorsListVC.appointmentSlots = appointmentSlots
            availableDoctorsListVC.appointmentDate = selectedDate
            availableDoctorsListVC.appointmentType = self.selectedappointmentType
            self.navigationController?.pushViewController(availableDoctorsListVC, animated: true)
        }
    }
    
}

extension SCBookAppointmentViewController {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if (textField == locationTextField) {
            showLocationPicker(textField: textField)
            return false
        } else if (textField == specialityTextField) {
            showSpecialityPicker(textField: textField)
            return false
        } else if (textField == appointmentDateTextField) {
            showDatePicker(textField: textField)
            return false
        }else if (textField == appointmentTypeTestFeild) {
            showAppointmentPicker(textField: appointmentTypeTestFeild)
            return false
        }
        return true
    }
}
