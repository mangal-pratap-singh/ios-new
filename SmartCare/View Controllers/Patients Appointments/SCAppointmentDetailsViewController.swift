//
//  SCAppointmentDetailsViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 06/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import SDWebImage

class SCAppointmentDetailsViewController: UIViewController {

    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var doctorDegreeLabel: UILabel!
    @IBOutlet weak var doctorSpecialityLabel: UILabel!
    @IBOutlet weak var gstText: UILabel!
    
    @IBOutlet weak var netChargesText: UILabel!
    @IBOutlet weak var consultationFeesText: UILabel!
    @IBOutlet weak var appointmentTimeTextFeild: UILabel!
   
    @IBOutlet weak var paymentStatusText: UILabel!
    @IBOutlet weak var appointmentType: UILabel!
    
    @IBOutlet weak var patientType: UILabel!
    @IBOutlet weak var paymentModeText: UILabel!
    
    @IBOutlet weak var commentText: UITextField!
    @IBOutlet weak var hospitalNameTextFeild: UILabel!
   
    @IBOutlet weak var bakView: UILabel!
    @IBOutlet weak var hospitalAddressLabel: UILabel!
    
    @IBOutlet weak var doctorAvtarImageView: UIImageView!
//    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var payNow: UIButton!
    
    @IBOutlet weak var view_video: UIView!
    
    @IBOutlet weak var btn_videoCall: UIButton!
    var accessCode : String?
//    var token = [VideoTokenModel]()
    let single = Singleton.sharedInstance.userDefaultsObj()

    var currentAppointment: Appointment?
    var PayNowmodelClassObject : PayNowModel? = nil
    var tokenGenerationObject : VideoToken? = nil
    var RSAmodelClassObject : RSA? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bakView.isHidden = true
        payNow.isHidden = true
        btn_videoCall.isHidden = true
        view_video.isHidden = true
        
       // callButton.isHidden = true
        //cancelButton.isHidden = false
        //cancelButton.isEnabled = true
        // VCConnectorPkg.vcInitialize()
       
        print(self.PayNowmodelClassObject?.accessCode as Any)
        customizeNavigationItem(title: SCAppConstants.pageTitles.appointmentDetails, isDetail: true)
        configureView()
       // payNowTappedFunct()
       
    }
    
   
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
      // VCConnectorPkg.vcInitialize()
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = false
        payNow.isHidden = true
        btn_videoCall.isHidden = true
        view_video.isHidden = true
       // callButton.isHidden = true
        showAppointmentDetails()
        
       // payNowTappedFunct()

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = true
    }
    
    @IBAction func payNowTapped(_ sender: Any) {
        
//        PayNowTapped()
        
        if self.RSAmodelClassObject?.rsaKey == nil || self.RSAmodelClassObject?.rsaKey == ""{
                   return
           }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SCPayNowViewController")  as! SCPayNowViewController

        vc.accessCode = self.PayNowmodelClassObject?.accessCode
        vc.rsaKey = self.RSAmodelClassObject?.rsaKey
        vc.amount = self.PayNowmodelClassObject?.amount
        vc.merchantId = self.PayNowmodelClassObject?.merchantId
        vc.currency = self.PayNowmodelClassObject?.currency
        vc.orderId = self.PayNowmodelClassObject?.orderId
        vc.cancelUrl = self.PayNowmodelClassObject?.cancelUrl
        vc.redirectUrl = self.PayNowmodelClassObject?.redirectUrl
        vc.UsersAppointmentID = currentAppointment?.id
        
      self.navigationController?.pushViewController(vc, animated: true)
//        PayNowTapped()
       }
    @IBAction func mapButtonAction(sender: UIButton) {
        if let appointment = currentAppointment, let clinicAddress = appointment.clinic?.clinicAddress?.fullAddress() {
            let addressURLString = clinicAddress.replacingOccurrences(of: " ", with: "+")
            if let mapURL = URL(string:"comgooglemaps://?q=\(addressURLString)&zoom=15") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(mapURL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    UIApplication.shared.openURL(mapURL)
                }
            } else {
                showAlert(title: SCAppConstants.pageTitles.appointmentDetails, msg: "Can not open Maps with this address")
            }
        }
    }
    @IBAction func callTapped(_ sender: UIButton) {
      //  SCVideoCompositViewController
       // SCVideoCustomViewController
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        
                      let vc = storyBoard.instantiateViewController(withIdentifier: "SCVideoCustomViewController")  as! SCVideoCustomViewController
                             vc.fromVideo = "fromVideo"
        
                              vc.displayName = tokenGenerationObject?.data?.doctor?.name
                              vc.resourceID = tokenGenerationObject?.data?.resourceId ?? ""
                               vc.VIDYO_TOKEN = tokenGenerationObject?.data?.joinToken ?? ""
                    vc.senderCommonName = tokenGenerationObject?.data?.patient?.name
                               vc.videoCallModel = tokenGenerationObject?.data
                            self.navigationController?.pushViewController(vc, animated: true)
        /*
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        
                      let vc = storyBoard.instantiateViewController(withIdentifier: "SCVideoCompositViewController")  as! SCVideoCompositViewController
                             vc.fromVideo = "fromVideo"
        
                              vc.displayName = tokenGenerationObject?.data?.patient?.name
                              vc.resourceID = tokenGenerationObject?.data?.resourceId ?? ""
                               vc.VIDYO_TOKEN = tokenGenerationObject?.data?.joinToken ?? ""
                               vc.videoCallModel = tokenGenerationObject?.data
                            self.navigationController?.pushViewController(vc, animated: true)
        */
        /*
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
               let vc = storyBoard.instantiateViewController(withIdentifier: "SCChatViewController")  as! SCChatViewController
                       vc.displayName = tokenGenerationObject?.data?.patient?.name
                       vc.resourceID = tokenGenerationObject?.data?.resourceId ?? ""
                        vc.VIDYO_TOKEN = tokenGenerationObject?.data?.joinToken ?? ""
                        vc.videoCallModel = tokenGenerationObject?.data
                     self.navigationController?.pushViewController(vc, animated: true)
        */
        /*
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SCVideoCallingHomeViewController")  as! SCVideoCallingHomeViewController
        vc.videoCallModel = tokenGenerationObject?.data
        self.navigationController?.pushViewController(vc, animated: true)
        */
    }
    
    
    
    
    @IBAction func chatBtnClick(_ sender: UIButton) {
        /*
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
                      let vc = storyBoard.instantiateViewController(withIdentifier: "SCChatViewController")  as! SCChatViewController
                            vc.chatAndVideoStr = "fromChat"
                              vc.displayName = tokenGenerationObject?.data?.patient?.name
                              vc.resourceID = tokenGenerationObject?.data?.resourceId ?? ""
                               vc.VIDYO_TOKEN = tokenGenerationObject?.data?.joinToken ?? ""
                               vc.videoCallModel = tokenGenerationObject?.data
                            self.navigationController?.pushViewController(vc, animated: true)
        */
        
    }
    
    
    @IBAction func callButtonAction(sender: UIButton) {
        
        initiateCall(forNumber: currentAppointment?.clinic1?.address?.phoneNumber ?? "")
    }
    
    @IBAction func cancelButtonAction(sender: UIButton) {
        
        print("cancell appointment btn click")
        
        cancelCurrentAppointment()
    }
    
    private func configureView() {
        doctorAvtarImageView.layer.cornerRadius = doctorAvtarImageView.frame.width/2
        doctorAvtarImageView.layer.masksToBounds = true
        callButton.layer.cornerRadius = callButton.frame.width/2
    }
    
    private func showAppointmentDetails() {
        if let appointment = currentAppointment {
//            print(currentAppointment?.clinic?.name)
            
//            print(appointment.clinic?.name ?? "")
//            print(currentAppointment?.id ?? "")
            doctorNameLabel.text = appointment.doctor?.name
            doctorDegreeLabel.text = appointment.doctor?.degree
            doctorSpecialityLabel.text = appointment.doctor?.speciality
            
            if let hospitalName = appointment.hospital1?.name, let clicnicName = appointment.clinic1?.name {
                hospitalNameTextFeild.text = hospitalName + ", " + clicnicName
              }
            let hospitalAdd = appointment.hospital1?.name
           // hospitalNameTextFeild.text = hospitalAdd
            let address = appointment.clinic1?.address?.fullAddress()
           // let add = appointment.clinic?.clinicAddress?.address1
           
//        print("full address==\(address) and \( appointment.clinic1?.address?.phoneNumber)")
            hospitalAddressLabel.text = address
           // hospitalNameTextFeild.text = appointment.clinic1?.name
        // callButton.setTitle(appointment.clinic?.clinicAddress?.phoneNumber, for: .normal)
        callButton.setTitle(appointment.clinic1?.address?.phoneNumber, for: .normal)
            patientType.text = appointment.patient?.name
            paymentModeText.text = appointment.payment?.type?.statusText()
            paymentStatusText.text = appointment.payment?.status?.statusText()
            if paymentStatusText.text == "Pending"
             {
                payNow.isHidden = false
            }else if paymentStatusText.text == "Done"{
                btn_videoCall.isHidden = true
                view_video.isHidden = true
                payNow.isHidden = true
                  //callButton.isHidden = false
                if appointment.appointmentTypeStatus?.statusTypeText() == "Remote Consultation"{
                    btn_videoCall.isHidden = true
                    view_video.isHidden = true
                    
                    guard let endDate = appointment.endTime else { return  }
                    let currentDate = Date()
                    let curruntDate  = currentDate.currentDateTime.toDate()
//                    let dateFormatter = DateFormatter()
//                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:SSZ"
//                    let curruntDateStr = dateFormatter.string(from: currentDate)
                    if curruntDate?.compare(endDate) == ComparisonResult.orderedDescending {
                        print("appointment Date is smaller than currentDate")
                        btn_videoCall.isHidden = true
                        view_video.isHidden = true
                    }else if curruntDate?.compare(endDate) == ComparisonResult.orderedAscending{
                        btn_videoCall.isHidden = false
                        view_video.isHidden = false
                         print("appointment Date is greater than currentDate")
                        
                    }else{
                        btn_videoCall.isHidden = true
                        view_video.isHidden = true
                        
                     }
                    
                   }
              }
            consultationFeesText.text = appointment.payment?.amount
            gstText.text = appointment.payment?.gstNumeric
            netChargesText.text = appointment.payment?.netAmount
            appointmentType.text = appointment.appointmentTypeStatus?.statusTypeText()//appointment.status?.statusText()
           // hospitalNameTextFeild.text = appointment.hospital1?.name
          // callButton.titleLabel?.text = appointment.clinic?.clinicAddress?.phoneNumber
            
            
            if let startDate = appointment.startTime, let endDate = appointment.endTime {
                appointmentTimeTextFeild.text = startDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointment) + " at " + startDate.toTimeString() + " - " + endDate.toTimeString()
            }
            
            if let imageBaseURL = AppInfo.getAppInfo()?.s3Url, let avtarImageURL = appointment.doctor?.avtarImageURL {
                // Load Image From URLwith Caching
                let doctorAvtarImageURLString = imageBaseURL + avtarImageURL
                if let url = URL(string: doctorAvtarImageURLString) {
                    doctorAvtarImageView.sd_setImage(with: url, placeholderImage: SCAppConstants.Image.userProfile.image())
                  }
             }
            
            cancelButton.isEnabled = appointment.status == AppointmentStatus.cancelled  ? false : true
            cancelButton.isHidden = appointment.status == AppointmentStatus.cancelled ? true : false
            if appointment.status?.statusText() == "Cancelled"{
                self.commentText.isEnabled = false
                self.commentText.text = appointment.cancel?.comments
                //payNow.isHidden = false
            }else{
                 commentText.isEnabled = true
               }
           }
         GetPatientToken()
    }
    
    private func payNowTappedFunct(){
    
        SCProgressView.show()
        if let appointmentId = currentAppointment?.id {
            
  SCAppointmentService.payNowAPI(UsersAppointmentID: appointmentId, completionHandler: { (model) in
                SCProgressView.hide()
                self.PayNowmodelClassObject = model
                                print("PayNowModel:" + "\(self.PayNowmodelClassObject as Any)")
                                DispatchQueue.main.async {
//                                    let email = model["email"] as! String
                                    print(self.PayNowmodelClassObject?.accessCode as Any)
                                    let requiredAccessCode = self.PayNowmodelClassObject?.accessCode
                                    self.accessCode = requiredAccessCode
                                    print(self.PayNowmodelClassObject?.orderId as Any)
                                    self.GetRSA()
                }
            }) { (err) in
                SCProgressView.hide()
                self.payNow.isHidden = false
                print("payNowTappedFunct_Error=\(err)")
            }
        }
        
    }
    
    private func GetPatientToken(){
          SCProgressView.show()
          if let appointmentId = currentAppointment?.id  {
    
           SCAppointmentService.PatienttokenGenerationAPI(UsersAppointmentID: appointmentId, completionHandler: { (model) in
                SCProgressView.hide()
                self.tokenGenerationObject = model
                let joinTokenPatient = model.data?.joinToken ?? ""
            if joinTokenPatient == ""{
                 self.payNow.isHidden = false
            }else{
                 self.payNow.isHidden = true
            }
                UserDefaults.standard.set(joinTokenPatient, forKey: "Vidyo_tocken")
                print("classModel:" + "\(self.tokenGenerationObject as Any)")
                DispatchQueue.main.async {
                    self.payNowTappedFunct()
                }
            }) { (err) in
                SCProgressView.hide()
                self.payNowTappedFunct()
                print("patient Generate==\(err)")
             }
       }
    }
    
  private func GetRSA(){
        SCProgressView.show()
        let appointmentId = currentAppointment?.id
        let UsersaccessCode = self.PayNowmodelClassObject?.accessCode as Any
        let UsersOrderID = self.PayNowmodelClassObject?.orderId
   
        SCAppointmentService.RSAstringAPI(UsersAppointmentID: appointmentId!, UsersaccessCode: UsersaccessCode as! String, UsersOrderID: UsersOrderID!, completionHandler: { (model) in
             SCProgressView.hide()
                self.RSAmodelClassObject = model
                print(self.RSAmodelClassObject as Any)
                DispatchQueue.main.async {
                    print(self.RSAmodelClassObject?.rsaKey! as Any)
                }
            }){ (err) in
                 SCProgressView.hide()
                print(err)
        }

      }
    

    private func cancelCurrentAppointment() {
        if let appointmentId = currentAppointment?.id {
            SCAppointmentService.cancelAppointmentAPI(appointmentId: appointmentId, onComplete: { [weak self] () in
                self?.navigationController?.popViewController(animated: true)
            }) { [weak self] (errorMessage) in
                self?.showAlert(title: SCAppConstants.pageTitles.appointmentDetails, msg: errorMessage)
            }
        }
    }
}







// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
