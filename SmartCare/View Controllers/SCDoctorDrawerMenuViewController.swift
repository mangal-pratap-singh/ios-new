//
//  SCDoctorDrawerMenuViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 28/10/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCDoctorDrawerMenuViewController: UIViewController {

    var navigationOptions = ["0":[(SCAppConstants.pageTitles.dashboard,SCAppConstants.Image.dashboard),
                                             (SCAppConstants.pageTitles.appointments, SCAppConstants.Image.bookAppointment),
                                        (SCAppConstants.pageTitles.procedureAppointments, SCAppConstants.Image.procedureAppointment),
                                         (SCAppConstants.pageTitles.hospitalRequest, SCAppConstants.Image.hospitalRequest),
                                         (SCAppConstants.pageTitles.procedureFees, SCAppConstants.Image.procedureFees)],
                            "1":[(SCAppConstants.pageTitles.createSlots,SCAppConstants.Image.createSlot),
                                           (SCAppConstants.pageTitles.filterSlots, SCAppConstants.Image.filterSlot)],
                            "2":[(SCAppConstants.pageTitles.createShift,SCAppConstants.Image.createShift),
                                 (SCAppConstants.pageTitles.shiftStartEnd, SCAppConstants.Image.shiftStartEnd),(SCAppConstants.pageTitles.shiftSummary, SCAppConstants.Image.shiftSummary)],
                            "3":[(SCAppConstants.pageTitles.editProfile,SCAppConstants.Image.editProfile),
                                       (SCAppConstants.pageTitles.logout,SCAppConstants.Image.logout)]
                            ]

    @IBOutlet weak var navigationOptionTable: UITableView!
    @IBOutlet weak var userFullNameLabel: UILabel!
    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var userProfileImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        registerNibsForCells()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateProfileInformation()
    }
    
    private func configureView() {
        navigationOptionTable.tableFooterView = UIView()
        userProfileImageView.layer.cornerRadius = userProfileImageView.frame.width/2
        userProfileImageView.layer.masksToBounds = true
    }
    
    private func updateProfileInformation() {
        self.userFullNameLabel.text =  "\((UserProfile.getUserProfile()?.firstName) ?? "") \((UserProfile.getUserProfile()?.lastName) ?? "")"
        self.userEmailLabel.text = UserProfile.getUserProfile()?.email
        let imageBaseUrl = AppInfo.getAppInfo()?.s3Url ?? ""
        let imageUrl = imageBaseUrl + (UserProfile.getUserProfile()?.profilePhotoURL ?? "")
        if let url = URL(string: imageUrl) {
            userProfileImageView.sd_setImage(with: url, placeholderImage: SCAppConstants.Image.userProfile.image())
        }
    }
    
    private func registerNibsForCells() {
        let nibName = UINib(nibName: SCAppConstants.nibNames.drawerMenuItemTableCell, bundle:nil)
        navigationOptionTable.register(nibName, forCellReuseIdentifier: SCAppConstants.cellReuseIdentifiers.drawerMenuItem)
    }
}

extension SCDoctorDrawerMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return navigationOptions.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 1,2: return 50
        default: return 1
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        let seperatorView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 0.5))
        seperatorView.backgroundColor = UIColor.lightGray
        headerView.backgroundColor = UIColor.white
        headerView.addSubview(seperatorView)

        switch section {
        case 1:
            let headerLabel = UILabel(frame: CGRect(x: 10, y: 5, width: tableView.frame.height, height: 40))
            headerLabel.text = "Manage Slots"
            headerLabel.textColor = UIColor.darkGray
            headerView.addSubview(headerLabel)
        case 2:
            let headerLabel = UILabel(frame: CGRect(x: 10, y: 5, width: tableView.frame.height, height: 40))
            headerLabel.text = "Manage Shifts"
            headerLabel.textColor = UIColor.darkGray
            headerView.addSubview(headerLabel)
        default: break
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return navigationOptions["\(section)"]?.count ?? 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SCAppConstants.cellReuseIdentifiers.drawerMenuItem, for: indexPath as IndexPath) as! DrawerMenuItemCellTableViewCell
        
        let options = navigationOptions["\(indexPath.section)"]!
        cell.configureCell(WithMenu: options[indexPath.row].0, imageName: options[indexPath.row].1.imageName())
        cell.backgroundColor = SCRoutingManager.sharedInstance.selectedDrawerItemIndex == indexPath ? UIColor.SCTextFilePlaceHolderTextColor() : UIColor.clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
                case 0: SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .doctorDashboard)
                case 1: SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .doctorAppointments)
                case 2: SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .doctorProcedureAppointments)
                case 3: SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .hospitalRequests)
                case 4: SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .procedureFees)
                default: break
            }
        case 1:
            switch indexPath.row {
                case 0: SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .createSlot)
                case 1: SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .filterSlot)
                default: break
            }
        case 2:
            switch indexPath.row {
                case 0: SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .createShift)
                case 1: SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .startEndShift)
                case 2: SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .shiftSummary)
                default: break
            }
        case 3:
            switch indexPath.row {
                case 0: SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .profile)
                case 1:
                let storyboard = UIStoryboard(name: SCAppConstants.storyBoardName.login, bundle: nil)
                let menuOptionVC = storyboard.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.logoutNavigation) as! UINavigationController
                self.present(menuOptionVC, animated: true, completion: nil)
                   //SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .logout)
                default: break
            }
        default: break
        }
        
        SCRoutingManager.sharedInstance.selectedDrawerItemIndex = indexPath
        tableView.reloadData()
    }
}
