//
//  SCGeneralDetailViewController.swift
//  SmartCare
//
//  Created by synerzip on 28/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class SCGeneralDetailViewController: UIViewController,genericPopUpDelegate {
    
    
    var customView:GenericDetailCustomView?
    var picker = UIImagePickerController()
    var imagePath : String?
    
    @IBOutlet weak var registrationIDTextFiled: UITextField!
    @IBOutlet weak var consulationSlotDurationlbl: UILabel!
    @IBOutlet weak var procedurSlotDurationLbl: UILabel!
    @IBOutlet weak var dateTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.GeneralDetails, isDetail: true)
        configureView()
        updateInformation()
    }
    
    @IBAction func changeDurationProcedureButtonAction(_ sender: Any) {
        self.customView?.alpha = 0.0
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            self.view.addSubview(self.customView!)
            self.customView?.alpha = 1.0
            self.customView?.getConsultationDurationDetails(type:SCAppConstants.GeneralType.procedureType.rawValue)
            
        }, completion: nil)
    }
    
    func updateInformation() {
        if let consulationSlotDuration = UserProfile.getUserProfile()?.slotDurationGeneralDetails?.consultation{
            self.consulationSlotDurationlbl.text = "\(String(describing: consulationSlotDuration)) minutes"
        }
        if let procedureSlotDuration = UserProfile.getUserProfile()?.slotDurationGeneralDetails?.procedure{
            self.procedurSlotDurationLbl.text = "\(String(describing: procedureSlotDuration)) minutes"
        }
    }
    
    @IBAction func changeDurationConsolutionButtonAction(_ sender: Any) {
        self.customView?.alpha = 0.0
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            self.view.addSubview(self.customView!)
            self.customView?.alpha = 1.0
            self.customView?.getConsultationDurationDetails(type: SCAppConstants.GeneralType.consultationType.rawValue)
        }, completion: nil)
    }
    
    @IBAction func uploadButtonAction(_ sender: Any) {
        present(picker, animated: true, completion: nil)
    }
    
    private func uploadGenralDetails(withImage image:UIImage){
        SCProgressView.show()
        SCUserService.uploadAvatar(withImage: image, onComplete: { ( data ,success) in
            SCProgressView.hide()
            self.imagePath = data["tempPath"].string
        }) { (errorMessage) in
            SCProgressView.hide()
            self.showAlert(title:SCAppConstants.pageTitles.GeneralDetails, msg:errorMessage, style: .alert)
        }
    }
    
    private func configureView() {
        picker.allowsEditing = false
        picker.delegate = self
        self.dateTextField.text = UserProfile.getUserProfile()?.practiceStartedAt
        self.customView = GenericDetailCustomView.instanceFromNib(frame: self.view.frame)
        self.customView?.delegate = self
        self.registrationIDTextFiled.text = UserProfile.getUserProfile()?.governmentRegistrationId ?? ""
        addNavigationBarSaveButton(enabled: true)
    }
    
    private func addNavigationBarSaveButton(enabled: Bool) {
        let editBarButtonItem = UIBarButtonItem(title: "SAVE", style: .plain, target: self, action: #selector(saveButtonTapped))
        let foregroundColor = enabled ? UIColor.white : UIColor.gray
        editBarButtonItem.setTitleTextAttributes([
            NSAttributedString.Key.font : UIFont(name: SCAppConstants.applicationFontFamily, size: 16)!,
            NSAttributedString.Key.foregroundColor : foregroundColor
            ], for: .normal)
        self.navigationItem.rightBarButtonItem  = editBarButtonItem
    }
    
    @objc private func saveButtonTapped() {
        self.saveGeneralDetails()
    }
    
    func showDatePicker(textField:UITextField) {
        
        let datePicker = ActionSheetDatePicker(title: "Select Date", datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let dateString = (value as! Date).toDateString(withFormatter: SCAppConstants.DateFormatType.date)
            textField.text = dateString
            return
        }, cancel: {
            ActionStringCancelBlock in return
        },
           origin: textField.superview)
        datePicker?.show()
    }
    
    private func saveGeneralDetails() {
        
        let messageDictionary : [String: Any] =
            ["governmentRegistrationId": registrationIDTextFiled.text ?? "",
             "doctorDigitalSignature":self.imagePath ?? "",
             "practiceStartedAt":dateTextField.text ?? ""]
        
        SCProgressView.show()
        SCDoctorProfileService.uploadGeneralDetailAPI(withParameters: messageDictionary, onComplete: { (jsonString, success) in
            SCProgressView.hide()
            self.navigationController?.popToRootViewController(animated: true)
        }, onError: { (errorMessage) in
            SCProgressView.hide()
            self.showAlert(title:SCAppConstants.pageTitles.GeneralDetails, msg:errorMessage, style: .alert)
        })
    }
    
    //MARK: - genericPopUpDelegate Method
    func cancelButtonActionDelegate(sender:UIButton) {
        self.customView?.alpha = 1.0
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            self.view.addSubview(self.customView!)
            self.customView?.alpha = 0.0
        }, completion: nil)
    }
    
    func proceedButtonActionDelegate(slotDurationObject: [String : Any]) {
        SCProgressView.show()
        SCDoctorProfileService.changeDurationAPI(withParameters: slotDurationObject, onComplete: { (jsonString, success) in
            SCProgressView.hide()
            self.navigationController?.popToRootViewController(animated: true)
        }, onError: { (errorMessage) in
            SCProgressView.hide()
            self.showAlert(title:SCAppConstants.pageTitles.GeneralDetails, msg:errorMessage, style: .alert)
        })
    }
}

extension SCGeneralDetailViewController{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if (textField == dateTextField) {
            self.showDatePicker(textField: textField)
            return false
        }
        return true
    }
}

extension SCGeneralDetailViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        
        guard let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage else {
            return
        }
        let compressImage = image.compressImage()
        self.uploadGenralDetails(withImage: compressImage)
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
