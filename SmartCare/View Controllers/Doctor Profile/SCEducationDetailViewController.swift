//
//  EducationDetailViewController.swift
//  SmartCare
//
//  Created by synerzip on 06/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class SCEducationDetailViewController: UIViewController {
    private let degreeTitlePickerTitle = "Select Degree"
    //Successfullly Uploaded.
    var degreeItem:Degree?
    var degreesList:[Degree]?
    var picker = UIImagePickerController()
    var imagePath : String?
    var degreePickerDataSource = [String]()
    var selectedDegreeNameIndex = 0
    var isAddControllerShow:Bool?
    
    @IBOutlet weak var degreeNametextField: UITextField!
    @IBOutlet weak var uploadButtonTap: UIButton!
    @IBOutlet weak var sucessView: UIView!
    @IBOutlet weak var fileNamelbl: UILabel!
    @IBOutlet weak var uploadStatusLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.EducationDetails, isDetail: true)
        configureView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateDegreeInformation()
        getDegreeList()
    }
    
    private func configureView() {
        picker.allowsEditing = false
        picker.delegate = self
        addNavigationBarEditButton(enabled: true)
        self.sucessView.isHidden = true
    }
    
    @IBAction func upoladButtonAction(_ sender: Any) {
        present(picker, animated: true, completion: nil)
    }
    
    private func updateDegreeInformation() {
        if isAddControllerShow == false {
            degreeNametextField.text = degreeItem?.name
            degreeNametextField.isUserInteractionEnabled = false
        } else {
            degreeNametextField.text = ""
            degreeNametextField.isUserInteractionEnabled = true
        }
    }
    
    private func addNavigationBarEditButton(enabled: Bool) {
        let editBarButtonItem = UIBarButtonItem(title: "SAVE", style: .plain, target: self, action: #selector(saveButtonTapped))
        let foregroundColor = enabled ? UIColor.white : UIColor.gray
        editBarButtonItem.setTitleTextAttributes([
            NSAttributedString.Key.font : UIFont(name: SCAppConstants.applicationFontFamily, size: 16)!,
            NSAttributedString.Key.foregroundColor : foregroundColor
            ], for: .normal)
        self.navigationItem.rightBarButtonItem  = editBarButtonItem
    }
    
    @objc private func saveButtonTapped() {
        self.uploadDegree()
    }
    
    private func  getDegreeList() {
        SCProgressView.show()
        SCCommanService.geDegreeListAPI(onComplete: { [weak self] (degreesList, success) in
            SCProgressView.hide()
            if success {
                self?.degreesList = degreesList
                self?.createDegreePickerDataSource()
            }
        }) { (errorMessage) in
            SCProgressView.hide()
            print("Error Degree API - \(errorMessage)")
        }
    }
    
    private func uploadDegree() {
        if isAddControllerShow == true {
            if let item = self.degreesList?[selectedDegreeNameIndex]{
                self.degreeItem = item
            }
        }
        let degreeIdValue = self.degreeItem?.id
        let degreename = self.degreeItem?.name
        let filename = self.degreeItem?.name ?? "" + ".png"
        let messageDictionary : [String: Any] =
            [ "degree":[[
                "certificateLink": self.imagePath ?? "",
                "id":degreeIdValue ?? 0,
                "name":degreename ?? "",
                "fileName":filename
                ]]
        ]
        SCProgressView.show()
        SCDoctorProfileService.uploadDegreeAPI(withParameters: messageDictionary, onComplete: { (jsonString, success) in
            SCProgressView.hide()
            self.navigationController?.popToRootViewController(animated: true)
        }, onError: { (errorMessage) in
            SCProgressView.hide()
            self.showAlert(title:SCAppConstants.pageTitles.EducationDetails, msg:errorMessage, style: .alert)
        })
    }
    
    private func createDegreePickerDataSource() {
        var data = [Degree]()
         degreePickerDataSource.append(degreeTitlePickerTitle)
        if let newData = self.degreesList {
            data = newData
            for degreeItem in data {
                degreePickerDataSource.append(degreeItem.name ?? "")
            }
        }
    }
    
    private func showDegreePicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: degreeTitlePickerTitle, rows: degreePickerDataSource, initialSelection: self.selectedDegreeNameIndex, doneBlock: { picker, selectedIndex, selectedValue in
            self.selectedDegreeNameIndex = selectedIndex-1
            if let aValue = selectedValue {
                textField.text = aValue as? String
                self.fileNamelbl.text = aValue as? String
            }
        }, cancel: nil, origin: textField)
    }
    
    private func uploadDegree(withImage image:UIImage){
        SCProgressView.show()
        SCUserService.uploadAvatar(withImage: image, onComplete: { ( data ,success) in
            SCProgressView.hide()
            self.imagePath = data["tempPath"].string
            self.sucessView.isHidden = false
            if self.isAddControllerShow == false {
                self.fileNamelbl.text = self.degreeNametextField.text
                self.uploadStatusLabel.text = SCAppConstants.validationMessages.successfulUploadImage
            }
        }) { (errorMessage) in
            SCProgressView.hide()
            self.uploadStatusLabel.text = SCAppConstants.validationMessages.failedUploadImage
            self.showAlert(title:SCAppConstants.pageTitles.EducationDetails, msg:errorMessage, style: .alert)
        }
    }
}

extension SCEducationDetailViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        
        guard let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage else {
            return
        } 
        let compressImage = image.compressImage()
        self.uploadDegree(withImage: compressImage)
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension SCEducationDetailViewController{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if (textField == self.degreeNametextField) {
            self.showDegreePicker(textField: textField)
            return false
        }
        return true
    }
}



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
