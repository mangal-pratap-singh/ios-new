//
//  SCSpecialityListControllerViewController.swift
//  SmartCare
//
//  Created by synerzip on 25/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCSpecialityListViewController: UIViewController {
    
    @IBOutlet weak var specialityDetailTbl: UITableView!
    
    var isAddControllerShow = false
    var fullScreenMessageView:FullScreenMessageView?
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.speaciltiyDetails, isDetail: true)
        registerNibsForCells()
        configureView()
    }
    
    private func configureView() {
        specialityDetailTbl.tableFooterView = UIView()
        self.specialityDetailTbl.isHidden = true
        self.fullScreenMessageView?.removeFromSuperview()
        if(UserProfile.getUserProfile()?.specialities.count ?? 0 > 0){
            self.specialityDetailTbl.isHidden = false
            self.specialityDetailTbl.reloadData()
        }else{
             self.fullScreenMessageView = self.showFullScreenMessageOnView(parentView: self.specialityDetailTbl, withMessage: "No Speciality added yet.")
            self.view.addSubview(self.fullScreenMessageView!)
        }
    }
    
    @IBAction func addSpecialityButtonAction(_ sender: UIBarButtonItem) {
        self.isAddControllerShow = true
        showSpecialityDetailsVCForSpeciality()
    }
    
    private func registerNibsForCells() {
        let nibName = UINib(nibName: SCAppConstants.nibNames.specialityListItemTableCell, bundle:nil)
        specialityDetailTbl.register(nibName, forCellReuseIdentifier: SCAppConstants.cellReuseIdentifiers.specialityListItem)
    }
    
    private func showSpecialityDetailsVCForSpeciality(speciality: Speciality? = nil){
        if let specialityDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.speaclityDetailsViewController) as? SCSpecialityDetailViewController {
            specialityDetailsVC.specialityItem = speciality
            specialityDetailsVC.isAddControllerShow = self.isAddControllerShow
            self.navigationController?.pushViewController(specialityDetailsVC, animated: true)
        }
    }
}

extension SCSpecialityListViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UserProfile.getUserProfile()?.specialities.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SCAppConstants.cellReuseIdentifiers.specialityListItem, for: indexPath as IndexPath) as! SpecialityTableViewCell
        if let specialityItem = UserProfile.getUserProfile()?.specialities[indexPath.row] {
            cell.configureCell(WithSpecialityItem: specialityItem)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let specialityItem = UserProfile.getUserProfile()?.specialities[indexPath.row]
        self.isAddControllerShow = false
        showSpecialityDetailsVCForSpeciality(speciality: specialityItem)
    }
}
