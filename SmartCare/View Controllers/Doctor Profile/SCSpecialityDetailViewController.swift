//
//  SCSpecialityDetailViewController.swift
//  SmartCare
//
//  Created by synerzip on 25/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class SCSpecialityDetailViewController: UIViewController {
    
    private let specialityTitlePickerTitle = "Select Speciality"
    
    var specialityItem:Speciality?
    var specialityList:[Speciality]?
    let picker = UIImagePickerController()
    var imagePath : String?
    var specilityPickerDataSource = [String]()
    var selectedDegreeNameIndex = 0
    var isAddControllerShow:Bool?
    
    @IBOutlet weak var specialityNameTextFiled: UITextField!
    @IBOutlet weak var uploadButtonTap: UIButton!
    @IBOutlet weak var sucessView: UIView!
    @IBOutlet weak var fileNamelbl: UILabel!
     @IBOutlet weak var uploadStatusLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.speaciltiyDetails, isDetail: true)
        configureView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateSpecilityInformation()
        getSpecialityList()
    }
    
    private func configureView() {
        picker.allowsEditing = false
        picker.delegate = self
        addNavigationBarEditButton(enabled: true)
        picker.allowsEditing = false
        picker.delegate = self
        self.sucessView.isHidden = true
    }
    
    @IBAction func upoladButtonAction(_ sender: Any) {
        present(picker, animated: true, completion: nil)
    }
    
    private func updateSpecilityInformation() {
        if isAddControllerShow == false {
            specialityNameTextFiled.text = specialityItem?.name
            specialityNameTextFiled.isUserInteractionEnabled = false
        } else {
            specialityNameTextFiled.text = ""
            specialityNameTextFiled.isUserInteractionEnabled = true
        }
    }
    
    private func  getSpecialityList() {
        SCProgressView.show()
        SCCommanService.geSpecialityListAPI(onComplete: { [weak self] (specialitylist, success) in
            SCProgressView.hide()
            if success {
                self?.specialityList = specialitylist
                self?.createSpecialityPickerDataSource()
            }
        }) { (errorMessage) in
            SCProgressView.hide()
            print("Error Degree API - \(errorMessage)")
        }
    }
    
    private func addNavigationBarEditButton(enabled: Bool) {
        let editBarButtonItem = UIBarButtonItem(title: "SAVE", style: .plain, target: self, action: #selector(saveButtonTapped))
        let foregroundColor = enabled ? UIColor.white : UIColor.gray
        editBarButtonItem.setTitleTextAttributes([
            NSAttributedString.Key.font : UIFont(name: SCAppConstants.applicationFontFamily, size: 16)!,
            NSAttributedString.Key.foregroundColor : foregroundColor
            ], for: .normal)
        self.navigationItem.rightBarButtonItem  = editBarButtonItem
    }
    
    @objc private func saveButtonTapped() {
        self.uploadSpeciality()
    }
    
    private func uploadSpeciality() {
        if isAddControllerShow == true {
            if let item = self.specialityList?[selectedDegreeNameIndex]{
                self.specialityItem = item
            }
        }
        let specialityIdValue =  self.specialityItem?.id
        let specialityname =  self.specialityItem?.name ?? ""
        let filename = ( self.specialityItem?.name ?? "") + ".png"
        let messageDictionary : [String: Any] =
            [ "speciality":[[
                "certificateLink": self.imagePath ?? "",
                "id":specialityIdValue ?? 0,
                "name":specialityname,
                "fileName":filename
                ]]
        ]
        SCProgressView.show()
        SCDoctorProfileService.uploadSpecialityAPI(withParameters: messageDictionary, onComplete: { (jsonString, success) in
            SCProgressView.hide()
            self.navigationController?.popToRootViewController(animated: true)
        }, onError: { (errorMessage) in
            SCProgressView.hide()
            self.showAlert(title:SCAppConstants.pageTitles.speaciltiyDetails, msg:errorMessage, style: .alert)
        })
    }
    
    private func createSpecialityPickerDataSource() {
        var data = [Speciality]()
        specilityPickerDataSource.append(specialityTitlePickerTitle)
        if let newData = self.specialityList {
            data = newData
            for speacilityItem in data {
                specilityPickerDataSource.append(speacilityItem.name ?? "")
            }
        }
    }
    
    private func showDegreePicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: specialityTitlePickerTitle, rows: specilityPickerDataSource, initialSelection: self.selectedDegreeNameIndex, doneBlock: { picker, selectedIndex, selectedValue in
            self.selectedDegreeNameIndex = selectedIndex-1
            if let aValue = selectedValue {
                textField.text = aValue as? String
                self.fileNamelbl.text = aValue as? String
            }
        }, cancel: nil, origin: textField)
    }
    
    private func uploadSpeciality(withImage image:UIImage){
        SCProgressView.show()
        SCUserService.uploadAvatar(withImage: image, onComplete: { ( data ,success) in
            self.imagePath = data["tempPath"].string
            self.sucessView.isHidden = false
            if self.isAddControllerShow == false {
                self.fileNamelbl.text = self.specialityNameTextFiled.text
                 self.uploadStatusLabel.text = SCAppConstants.validationMessages.successfulUploadImage
            }
            SCProgressView.hide()
        }) { (errorMessage) in
            SCProgressView.hide()
            self.uploadStatusLabel.text = SCAppConstants.validationMessages.failedUploadImage
            self.showAlert(title:SCAppConstants.pageTitles.speaciltiyDetails, msg:errorMessage, style: .alert)
            print("Failed -- \(errorMessage)")
        }
    }
}

extension SCSpecialityDetailViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        
        guard let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage else {
            return
        }
        let compressImage = image.compressImage()
        self.uploadSpeciality(withImage: compressImage)
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension SCSpecialityDetailViewController {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if (textField == self.specialityNameTextFiled) {
            self.showDegreePicker(textField: textField)
            return false
        }
        return true
    }
}



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
