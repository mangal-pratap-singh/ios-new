//
//  EducationDetailListViewController.swift
//  SmartCare
//
//  Created by synerzip on 06/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCEducationDetailListViewController: UIViewController {

    @IBOutlet weak var educationDetailTbl: UITableView!
    var isAddControllerShow = false
    var fullScreenMessageView:FullScreenMessageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.EducationDetails, isDetail: true)
        registerNibsForCells()
        configureView()
    }
    
    private func configureView() {
        educationDetailTbl.tableFooterView = UIView()
        self.educationDetailTbl.isHidden = false
        self.fullScreenMessageView?.removeFromSuperview()
        if(UserProfile.getUserProfile()?.degrees.count ?? 0 > 0){
            self.educationDetailTbl.isHidden = false
            self.educationDetailTbl.reloadData()
        }else{
            self.fullScreenMessageView = self.showFullScreenMessageOnView(parentView: self.educationDetailTbl, withMessage: "No Degree added yet.")
            self.view.addSubview(self.fullScreenMessageView!)
        }
    }
    
    @IBAction func addDegreeButtonAction(_sender: UIBarButtonItem){
        self.isAddControllerShow = true
        showEducationDetailsForDegree()
    }
   
    private func registerNibsForCells() {
        let nibName = UINib(nibName: SCAppConstants.nibNames.educationDetailslListItemTableCell, bundle:nil)
        educationDetailTbl.register(nibName, forCellReuseIdentifier: SCAppConstants.cellReuseIdentifiers.educationDetailListItem)
    }
    
    private func showEducationDetailsForDegree(degree:Degree? = nil) {
        if let educationDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.educationDetailsViewController) as? SCEducationDetailViewController {
            educationDetailsVC.degreeItem = degree
            educationDetailsVC.isAddControllerShow = self.isAddControllerShow
            self.navigationController?.pushViewController(educationDetailsVC, animated: true)
        }
    }
}

extension SCEducationDetailListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UserProfile.getUserProfile()?.degrees.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SCAppConstants.cellReuseIdentifiers.educationDetailListItem, for: indexPath as IndexPath) as! EducationDetailTableViewCell
        if let degreeItem = UserProfile.getUserProfile()?.degrees[indexPath.row] {
            cell.configureCell(WithDegreeItem: degreeItem)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let degreeItem = UserProfile.getUserProfile()?.degrees[indexPath.row]
        self.isAddControllerShow = false
        showEducationDetailsForDegree(degree: degreeItem)
        
    }
}
