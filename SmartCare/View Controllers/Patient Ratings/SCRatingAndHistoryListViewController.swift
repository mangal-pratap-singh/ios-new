//
//  SCRatingAndHistoryViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 05/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class SCRatingAndHistoryListViewController: UIViewController {

    private let statusPickerTitle = "Select Status"
    
    @IBOutlet weak var monthNameLabel: UILabel!
    @IBOutlet weak var noDataAvailableLabel: UILabel!
    @IBOutlet weak var statusTextField: UITextField!
    @IBOutlet weak var ratingListTable: UITableView!
    
    var displayDate = Date()
    var appointments = [Appointment]()
    var selectedPickerIndex = 0
    var pickerDataSource = [String]()
    var fullScreenMesageView: FullScreenMessageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.ratingHistory, isDetail: false)
        configureView()
        registerNibsForCells()
        createPickerDataSource()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getRatingsList()
    }
    
    @IBAction func nextButtonAction(_ sender: UIButton) {
        let nextDate = Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: displayDate)!
        displayDate = nextDate
        monthNameLabel.text = displayDate.currentMonthAndYear
        getRatingsList()
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        let previousDate = Calendar.current.date(byAdding: DateComponents(month: -1, day: +1), to: displayDate)!
        displayDate = previousDate
        monthNameLabel.text = displayDate.currentMonthAndYear
        getRatingsList()
    }
    
    private func configureView() {
        ratingListTable.tableFooterView = UIView()
        statusTextField.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        monthNameLabel.text = displayDate.currentMonthAndYear
    }
    
    private func registerNibsForCells() {
        let nibName = UINib(nibName: SCAppConstants.nibNames.ratingTableViewCell, bundle:nil)
        ratingListTable.register(nibName, forCellReuseIdentifier: SCAppConstants.cellReuseIdentifiers.ratingListItem)
    }
    
    private func createPickerDataSource() {
        pickerDataSource.append(statusPickerTitle)
        for appointment in AppointmentStatus.toArray() {
            pickerDataSource.append(appointment.statusText())
        }
    }
    
    private func showStatusPicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: statusPickerTitle, rows: pickerDataSource, initialSelection: selectedPickerIndex, doneBlock: { picker, selectedIndex, selectedValue in
            self.selectedPickerIndex = selectedIndex
            textField.text = selectedIndex == 0 ? self.statusPickerTitle : AppointmentStatus.toArray()[selectedIndex-1].statusText()
            self.getRatingsList()
        }, cancel: nil, origin: textField)
    }
    
    private func getRatingsList() {
        let startDate = displayDate.startOfMonth
        let endDate = displayDate.endOfMonth
        let selectedStatus = selectedPickerIndex == 0 ? nil : AppointmentStatus.toArray()[selectedPickerIndex-1]
        
        SCProgressView.show()
        SCRatingService.ratingsListAPI(startDate: startDate, endDate: endDate, status: selectedStatus, onComplete: { [weak self] (appointmentList) in
            SCProgressView.hide()
            self?.fullScreenMesageView?.removeFromSuperview()
            if appointmentList.count == 0 {
                self?.fullScreenMesageView = self?.showFullScreenMessageOnView(parentView: self!.ratingListTable, withMessage: "No appointments for selected period")
                self?.view.addSubview(self!.fullScreenMesageView!)
            }
            self?.appointments = appointmentList
            self?.ratingListTable.reloadData()
        }) { [weak self] (errorMessage) in
            SCProgressView.hide()
            if !errorMessage.isEmpty {
                self?.showAlert(title: SCAppConstants.pageTitles.ratingHistory, msg: errorMessage)
            }
        }
    }
}

extension SCRatingAndHistoryListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appointments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SCAppConstants.cellReuseIdentifiers.ratingListItem, for: indexPath as IndexPath) as! RatingListItemTableViewCell
        cell.selectionStyle = .none
        let currentAppointment = appointments[indexPath.row]
        cell.configureCell(WithAppointment: currentAppointment)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentAppointment = appointments[indexPath.row]
        
        if currentAppointment.status == .completed {
            loadDetailScreenForAppointmentRatings(currentAppointment)
        }
    }
    
    private func loadDetailScreenForAppointmentRatings(_ appointment: Appointment) {
        if let appointmentDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.ratingDetails) as? SCRatingAndHistoryDetailsViewController {
            appointmentDetailsVC.selectedAppointment = appointment
            self.navigationController?.pushViewController(appointmentDetailsVC, animated: true)
        }
    }
}

extension SCRatingAndHistoryListViewController {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if (textField == statusTextField) {
            showStatusPicker(textField: textField)
            return false
        }
        return true
    }
}
