//
//  SCRatingAndHistoryDetailsViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 19/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCRatingAndHistoryDetailsViewController: UIViewController {

    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var hospitalNameLabel: UILabel!
    @IBOutlet weak var appointmentTimeLabel: UILabel!
    @IBOutlet weak var specialityLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var patientNameLabel: UILabel!
    @IBOutlet weak var reviewTextView: UITextView!
    @IBOutlet weak var doctorAvtarImageView: UIImageView!
    @IBOutlet weak var reviewTextViewHeight: NSLayoutConstraint!
    @IBOutlet weak var cosmosRatingsView: CosmosView!
    
    var selectedAppointment: Appointment?
    var selectedRatings = 0
    var newReviewText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.appointmentDetails, isDetail: true)
        configureView()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SCRatingAndHistoryDetailsViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = false
        showAppointmentDetails()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = true
        if selectedAppointment?.ratings?.count == nil && selectedAppointment?.ratings?.review == nil {
            saveRatingsAndReview()
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func configureView() {
        doctorAvtarImageView.layer.cornerRadius = doctorAvtarImageView.frame.width/2
        doctorAvtarImageView.layer.masksToBounds = true
        reviewTextView.contentInset = UIEdgeInsets.init(top: 0, left: 2, bottom: 0, right: 7);
        cosmosRatingsView.settings.fillMode = .full
    }
    
    private func showAppointmentDetails() {
        if let appointment = selectedAppointment {
            doctorNameLabel.text = appointment.doctor?.name
            specialityLabel.text = appointment.doctor?.speciality
            statusLabel.text = appointment.status?.statusText()
            
            var patientInfoText = appointment.patient?.name ?? ""
            if let age = appointment.patient?.age, !age.isEmpty {
                patientInfoText = "\(patientInfoText), \(age) Yrs"
            }
            patientNameLabel.text = patientInfoText
            
            if let hospitalName = appointment.hospital?.name, let clicnicName = appointment.clinic?.name {
                hospitalNameLabel.text = hospitalName + ", " + clicnicName
            }

            if let startDate = appointment.startTime, let endDate = appointment.endTime {
                appointmentTimeLabel.text = startDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointment) + " at " + startDate.toTimeString() + " - " + endDate.toTimeString()
            }
            
            if let imageBaseURL = AppInfo.getAppInfo()?.s3Url, let avtarImageURL = appointment.doctor?.avtarImageURL {
                // Load Image From URLwith Caching
                let doctorAvtarImageURLString = imageBaseURL + avtarImageURL
                if let url = URL(string: doctorAvtarImageURLString) {
                    doctorAvtarImageView.sd_setImage(with: url, placeholderImage: SCAppConstants.Image.userProfile.image())
                }
            }
            // Show Review Text
            if let reviewText = appointment.ratings?.review {
                reviewTextView.text = reviewText
                reviewTextView.sizeToFit()
                
                let fixedWidth = reviewTextView.frame.size.width
                let newSize = reviewTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                reviewTextViewHeight.constant = newSize.height
            }
            reviewTextView.isEditable = appointment.ratings?.review == nil ? true : false
            
            //Show Star Ratings
            cosmosRatingsView.rating = 0.0
            if let ratings = appointment.ratings?.count {
                cosmosRatingsView.rating = Double(ratings)
            }
            cosmosRatingsView.settings.updateOnTouch = appointment.ratings?.count == nil ? true : false
        }
    }
    
    private func saveRatingsAndReview() {
        if let appointmentId = selectedAppointment?.id, cosmosRatingsView.rating != 0.0 && !reviewTextView.text.isEmpty {
            SCRatingService.saveRatingsForAppointmentAPI(appointmentId: "\(appointmentId)", count: Int(cosmosRatingsView.rating), review: reviewTextView.text, onComplete: {
            }) { (errorMessage) in
                print("Error saving ratings - \(errorMessage)")
            }
        }
    }
}
