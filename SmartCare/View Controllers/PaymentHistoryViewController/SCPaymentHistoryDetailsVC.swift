//
//  SCPaymentHistoryDetailsVC.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 22/11/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCPaymentHistoryDetailsVC: UIViewController {
    
    @IBOutlet weak var lbl_paymentMode: UILabel!
    
    @IBOutlet weak var lbl_transactionId: UILabel!
    
    @IBOutlet weak var lbl_consultFees: UILabel!
    
    @IBOutlet weak var lbl_netCharges: UILabel!
    
    
    @IBOutlet weak var lbl_patientName: UILabel!
    
    @IBOutlet weak var lbl_patientAge: UILabel!
    
    @IBOutlet weak var lbl_patientPhone: UILabel!
    
    
    @IBOutlet weak var lbl_patientEmail: UILabel!
    
    @IBOutlet weak var lbl_patientId: UILabel!
    
    @IBOutlet weak var lbl_doctorName: UILabel!
    
    @IBOutlet weak var lbl_emailId: UILabel!
    
    @IBOutlet weak var lbl_degree: UILabel!
    
    @IBOutlet weak var img_doctorProfile: UIImageView!
    
    @IBOutlet weak var lbl_appointType: UILabel!
    
    @IBOutlet weak var lbl_dateTime: UILabel!
    
    
    @IBOutlet weak var lbl_hospitalClinic: UILabel!
    
    
    @IBOutlet weak var lbl_location: UILabel!
    
    @IBOutlet weak var lbl_doctorSpecialist: UILabel!
    var paymentDetailsDict: PaymentHistory?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customizeNavigationItem(title: SCAppConstants.pageTitles.paymentHistoryDetails, isDetail: true)
         

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
     // paymentModeText.text = appointment.payment?.type?.statusText()
       // paymentStatusText.text = appointment.payment?.status?.statusText()
       // lbl_paymentMode.text = paymentDetailsDict?.paymentDetailsStatus?.transactionMode
        lbl_paymentMode.text = paymentDetailsDict?.paymentDetailsStatus?.paymentType?.statusText()
       // lbl_paymentMode.text = paymentDetailsDict?.paymentDetailsStatus?.transactionMode
         lbl_transactionId.text = paymentDetailsDict?.paymentDetailsStatus?.transactionId
        lbl_consultFees.text = paymentDetailsDict?.paymentDetailsStatus?.amount
        lbl_netCharges.text = paymentDetailsDict?.paymentDetailsStatus?.netAmount
        lbl_patientName.text = paymentDetailsDict?.paymentPatient?.name
        lbl_patientAge.text = paymentDetailsDict?.paymentPatient?.age
        lbl_patientEmail.text = paymentDetailsDict?.paymentPatient?.emailId
        
        lbl_patientPhone.text = paymentDetailsDict?.paymentPatient?.phoneNo
        lbl_patientId.text = paymentDetailsDict?.paymentPatient?.id
        
        
        lbl_doctorName.text = paymentDetailsDict?.paymentDoctor?.name
        lbl_emailId.text = paymentDetailsDict?.paymentDoctor?.emailId
        lbl_degree.text = paymentDetailsDict?.paymentDoctor?.degree
        lbl_doctorSpecialist.text = paymentDetailsDict?.paymentDoctor?.speciality
        
        lbl_appointType.text = paymentDetailsDict?.paymentAppointment?.appointmentType?.statusTypeText()
        
        //paymentDetailsDict?.startDate1.toDateString(withFormatter: )
        lbl_dateTime.text = (paymentDetailsDict?.startDate1?.currentMontAndTime ?? "") + " to " + (paymentDetailsDict?.endDate1?.currentMontAndTime ?? "")
        lbl_hospitalClinic.text = (paymentDetailsDict?.paymentHospital?.name ?? "") + " / " + (paymentDetailsDict?.paymentClinic?.name ?? "")
        lbl_location.text = paymentDetailsDict?.location
        
        if let imageBaseURL = AppInfo.getAppInfo()?.s3Url, let avtarImageURL = paymentDetailsDict?.paymentDoctor?.avatar {
                   // Load Image From URLwith Caching
                   let doctorAvtarImageURLString = imageBaseURL + avtarImageURL
                   if let url = URL(string: doctorAvtarImageURLString) {
                       img_doctorProfile.sd_setImage(with: url, placeholderImage: SCAppConstants.Image.userProfile.image())
                   }
               }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
