//
//  PaymentHistoryCell.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 19/11/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
@objc protocol PaymentDetailsDelegate{
    
    @objc optional func showDetailsClick(index: Int)
    @objc optional func downLoadRecieptClick(index: Int)
    
}
class PaymentHistoryCell: UITableViewCell {
    

    var paymentDetailsDelegate:PaymentDetailsDelegate?
    @IBOutlet weak var lbl_patientName: UILabel!
    
    @IBOutlet weak var lbl_consultingFees: UILabel!
    
    @IBOutlet weak var lbl_doctorDetails: UILabel!
    
    @IBOutlet weak var lbl_clinicName: UILabel!
    
    @IBOutlet weak var btn_reciept: UIButton!
    
    @IBOutlet weak var btn_viewDetails: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func downLoadRecieptBtnClick(_ sender: UIButton) {
        self.paymentDetailsDelegate?.downLoadRecieptClick?(index: btn_reciept.tag)
    }
    
    @IBAction func viewDetailsBtnClick(_ sender: UIButton) {
        self.paymentDetailsDelegate?.showDetailsClick?(index: btn_viewDetails.tag)
    }
    
}
