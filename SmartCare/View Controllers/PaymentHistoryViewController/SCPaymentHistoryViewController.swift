//
//  SCPaymentHistoryViewController.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 19/11/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SafariServices
class SCPaymentHistoryViewController: UIViewController {
     private let datePickerTitle = "Select Date"

    private let noOfSecondForWeak = 1
    private let weakDisplayDateFormat = "MMM dd, yyyy"
    var fullScreenMessageView:FullScreenMessageView?
    var displayHistoryDate = Date()
    
    @IBOutlet weak var lbl_dateTitle: UILabel!
    
    
    @IBOutlet weak var tbleView_paymentHistory: UITableView!
    var paymentHistoryData:PaymentHistoryModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.paymentHistoryStatus, isDetail: false)
       // configureView()
        
        self.tbleView_paymentHistory.dataSource = self
        self.tbleView_paymentHistory.delegate = self
        
//        DispatchQueue.main.async {
//            self.tbleView_paymentHistory.reloadData()
//        }

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tbleView_paymentHistory.isHidden = true
        self.fullScreenMessageView?.removeFromSuperview()
        self.fullScreenMessageView = self.showFullScreenMessageOnView(parentView: self.tbleView_paymentHistory, withMessage: "No Payment History for selected period")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        displayHistoryDate = Date()

       // getPaymentHistory()
         showWeakDateStringFromDate()
      }
    
    private func configureView() {
           showWeakDateStringFromDate()
          // CompletedAppointmentList.tableFooterView = UIView()
         }
    private func showWeakDateStringFromDate() {
//        let startWeakDate = (displayHistoryDate.startOfWeek).toDateString(withFormatter: weakDisplayDateFormat)
//        let endWeakDate = (displayHistoryDate.endOfWeek).toDateString(withFormatter: weakDisplayDateFormat)
        let currentDate = displayHistoryDate.toDateString(withFormatter: weakDisplayDateFormat)
              lbl_dateTitle.text = " \(currentDate) "
       // displayHistoryDate = displayHistoryDate.toStringDate(dateString: currentDate, formatter: weakDisplayDateFormat, outPutFormatter: weakDisplayDateFormat)
          getPaymentHistory()
      }
    func showDatePicker(textField:UILabel) {
        
            let datePicker = ActionSheetDatePicker(title: datePickerTitle, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
                picker, value, index in
                let dateString = (value as! Date).toDateString(withFormatter: self.weakDisplayDateFormat)
                self.lbl_dateTitle.text = dateString
            self.displayHistoryDate = value as! Date
                self.getPaymentHistory()
                return
            }, cancel: {
                ActionStringCancelBlock in return
            },
               origin: textField.superview)
            datePicker?.show()
        }


    private func getPaymentHistory(){
              SCProgressView.show()
          print("get user id====\(UserProfile.getUserId())")
         SCPaymentHistoryService.getPaymentHistoryAPI(withParameters: ["date": displayHistoryDate], onComplete: { (response, result) in
                SCProgressView.hide()
               print("patien consultation notes list==\(response)")
               self.paymentHistoryData = PaymentHistoryModel(jsonData: response)
               DispatchQueue.main.async {
                  self.fullScreenMessageView?.removeFromSuperview()
                     self.tbleView_paymentHistory.isHidden = false
                     self.tbleView_paymentHistory.reloadData()
                   }
               
             }) { (errorMessage) in
                SCProgressView.hide()
            self.paymentHistoryData?.paymentHistory?.removeAll()
             DispatchQueue.main.async {
                 self.tbleView_paymentHistory.reloadData()
            }
                self.view.addSubview(self.fullScreenMessageView!)
               
             }
        }

    
    @IBAction func selectDateBtnClick(_ sender: UIButton) {
        if showDateMethod(dateLabel: lbl_dateTitle) == false{
            
          }
        
    }
    
    @IBAction func nextDateBtnClick(_ sender: UIButton) {
        
        let nextDate = Calendar.current.date(byAdding: .day, value: noOfSecondForWeak, to: displayHistoryDate)!
        displayHistoryDate = nextDate
        showWeakDateStringFromDate()
    }
    
    @IBAction func backDateBtnClick(_ sender: UIButton) {
        
        let previousDate = Calendar.current.date(byAdding: .day, value: -noOfSecondForWeak, to: displayHistoryDate)!
               displayHistoryDate = previousDate
               showWeakDateStringFromDate()
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SCPaymentHistoryViewController {
    func showDateMethod(dateLabel: UILabel) -> Bool {

        if (dateLabel == lbl_dateTitle) {
            self.showDatePicker(textField: dateLabel)
            return false
        }
        return true
    }
}
extension SCPaymentHistoryViewController: UITableViewDelegate,UITableViewDataSource,PaymentDetailsDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentHistoryData?.paymentHistory?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tbleView_paymentHistory.dequeueReusableCell(withIdentifier: "PaymentHistoryCell", for: indexPath) as! PaymentHistoryCell
        cell.paymentDetailsDelegate = self
        cell.btn_reciept.tag = indexPath.row
        cell.btn_viewDetails.tag = indexPath.row
        let paymentDict = paymentHistoryData?.paymentHistory?[indexPath.row]
        cell.lbl_patientName.text = paymentDict?.paymentPatient?.name
        cell.lbl_consultingFees.text = paymentDict?.paymentDetailsStatus?.amount
        cell.lbl_doctorDetails.text = paymentDict?.paymentDoctor?.degree
        cell.lbl_clinicName.text = (paymentDict?.paymentHospital?.name ?? "") + " / " + (paymentDict?.paymentClinic?.name ?? "")
        
        
        
        return cell
    }
    
    
    func downLoadRecieptClick(index: Int) {
        let paymentDict = paymentHistoryData?.paymentHistory?[index]
        let recieptStr = paymentDict?.paymentDetailsStatus?.receiptUrl
        /*
         // THIS BELLOW  CODE IS WORKING FOR RECIEPT DOWNLOAD
        CSFileDownload.loadFileAsync2(urlString: recieptStr!) { (pathStr, error) in
            if error == nil{
                
                print("print url and shaved")
            }else{
                
                
            }
            
        }
        
    }
    */
    
        
        
        guard let urlString = recieptStr else { return}
        let replaceStr = urlString.replacingOccurrences(of: "\"", with: "")
        
        print("print pdf url==\(urlString)")
      
        openInstagram(webUrl: replaceStr)
        
        
    }
    
    func openInstagram(webUrl: String) {
        
             guard let url = URL(string: webUrl)  else { return }
        
             let safariVC = SFSafariViewController(url: url)
             present(safariVC,animated: true)
         }
    func showDetailsClick(index: Int) {
        let paymentDict = paymentHistoryData?.paymentHistory?[index]
        let story = UIStoryboard(name:SCAppConstants.storyBoardName.paymentHistory , bundle: nil)
        if #available(iOS 13.0, *) {
            let paymentDetailsDict = story.instantiateViewController(identifier: "SCPaymentHistoryDetailsVC") as! SCPaymentHistoryDetailsVC
            
            paymentDetailsDict.paymentDetailsDict = paymentDict
             self.navigationController?.pushViewController(paymentDetailsDict, animated: true)
        } else {
            // Fallback on earlier versions
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          
    }
    
    
    
    
}
