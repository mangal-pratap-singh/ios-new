//
//  HospitalRequestDetailsViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 04/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCHospitalRequestDetailsViewController: UIViewController {

    private let sendRequestButtonTitle = "SEND REQUEST"
    private let resendRequestButtonTitle = "RESEND REQUEST"
    private let cancelRequestButtonTitle = "CANCEL REQUEST"
    
    @IBOutlet weak var hospitalNameTextfield: UITextField!
    @IBOutlet weak var addressTextView: UITextView!
    @IBOutlet weak var phoneNumberTextfield: UITextField!
    @IBOutlet weak var emailAddressTextfield: UITextField!
    @IBOutlet weak var requestByTextfield: UITextField!
    @IBOutlet weak var statusTextfield: UITextField!
    @IBOutlet weak var requestActionButton: UIButton!
    @IBOutlet weak var reviewTextViewHeight: NSLayoutConstraint!
    
    var currentHospitalRequest: HospitalRequest?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.hospitalDetails, isDetail: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Disable Drawer for Detailed Pages
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = false
        showCurrentHospitalDetail()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = true
    }
    
    @IBAction func sendRequestButtonAction(_ sender: UIButton) {
        if let request = currentHospitalRequest, let requestStatus = request.status {
            switch requestStatus  {
            case .requested:
                if request.requestedBy == "HSOPITAL" {
                    resendHospitalRequest()
                } else if request.requestedBy == "YOU" {
                    sendHospitalRequest(withStatus: "C")
                }
            case .new: sendHospitalRequest(withStatus: "S")
            case .confirmed: break
            case .denied: resendHospitalRequest()
            }
        }
    }
    
    private func showCurrentHospitalDetail() {
        if let request = currentHospitalRequest {
            hospitalNameTextfield.text = request.hospitalName
            phoneNumberTextfield.text = request.hospitalAdmin!.countryCode + "-" + request.hospitalAdmin!.phoneNo
            emailAddressTextfield.text = request.hospitalAdmin?.emailId
            requestByTextfield.text = request.requestedBy
            statusTextfield.text = request.status?.statusText().uppercased()
            statusTextfield.backgroundColor = request.status?.statusColor()
           // let address = appointment.clinic1?.address?.fullAddress()
            
            addressTextView.text = request.address1?.fullAddress()
            addressTextView.sizeToFit()
            let fixedWidth = addressTextView.frame.size.width
            let newSize = addressTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            reviewTextViewHeight.constant = newSize.height
            
            if let requestStatus = request.status {
                requestActionButton.isHidden = false
                switch requestStatus  {
                case .requested:
                    if request.requestedBy == "HSOPITAL" {
                        requestActionButton.setTitle(resendRequestButtonTitle, for: .normal)
                    } else if request.requestedBy == "YOU" {
                        requestActionButton.setTitle(cancelRequestButtonTitle, for: .normal)
                    } else {
                        requestActionButton.isHidden = true
                    }
                case .new: requestActionButton.setTitle(sendRequestButtonTitle, for: .normal)
                case .confirmed: requestActionButton.isHidden = true
                case .denied: requestActionButton.setTitle(resendRequestButtonTitle, for: .normal)
                }
            }
        }
    }
    
    private func sendHospitalRequest(withStatus status: String) {
        if let hospitalId = currentHospitalRequest?.orgNum {
            SCProgressView.show()
            SCHospitalServices.sendRequestForHospital(hospitalId: String(hospitalId), withStatus: status, onComplete: {
                SCProgressView.hide()
                self.navigationController?.popViewController(animated: true)
            }) { (errorMessage) in
                SCProgressView.hide()
                self.showAlert(title:SCAppConstants.pageTitles.manageDependant, msg:errorMessage, style: .alert)
            }
        }
    }
    
    private func resendHospitalRequest(withStatus status: String = "S") {
        if let hospitalId = currentHospitalRequest?.orgNum {
            SCProgressView.show()
            SCHospitalServices.resendRequestForHospital(hospitalId: String(hospitalId), withStatus: status, onComplete: {
                SCProgressView.hide()
                self.navigationController?.popViewController(animated: true)
            }) { (errorMessage) in
                SCProgressView.hide()
                self.showAlert(title:SCAppConstants.pageTitles.manageDependant, msg:errorMessage, style: .alert)
            }
        }
    }
}
