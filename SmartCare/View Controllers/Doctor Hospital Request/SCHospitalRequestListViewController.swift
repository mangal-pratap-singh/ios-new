//
//  HospitalRequestListViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 04/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCHospitalRequestListViewController: UIViewController {

    @IBOutlet weak var hospitalRequestsTable: UITableView!
    var hospitalRequests = [HospitalRequest]()
    var fullScreenMessageView:FullScreenMessageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.hospitalRequest, isDetail: false)
        configureView()
        registerNibsForCells()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getHospitalRequestListForDoctor()
     }
    
    private func configureView() {
        hospitalRequestsTable.tableFooterView = UIView()
       }
    
    private func registerNibsForCells() {
        let nibName = UINib(nibName: SCAppConstants.nibNames.hospitalRequestTableViewCell, bundle:nil)
        hospitalRequestsTable.register(nibName, forCellReuseIdentifier: SCAppConstants.cellReuseIdentifiers.hospitalRequestListItem)
         }
  private func getHospitalRequestListForDoctor() {
        SCProgressView.show()
        SCHospitalServices.hospitalRequestListAPI(onComplete: { [weak self] (hospitalRequestList) in
            SCProgressView.hide()
            self?.fullScreenMessageView?.removeFromSuperview()
            if hospitalRequestList.count == 0 {
                self?.fullScreenMessageView = self?.showFullScreenMessageOnView(parentView: self!.hospitalRequestsTable, withMessage: "Currently there are no Hospital Requests available")
                self?.view.addSubview(self!.fullScreenMessageView!)
              }
            self?.hospitalRequests = hospitalRequestList
            self?.hospitalRequestsTable.reloadData()
        }) { (errorMessage) in
            SCProgressView.hide()
          }
      }
  }

extension SCHospitalRequestListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hospitalRequests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SCAppConstants.cellReuseIdentifiers.hospitalRequestListItem, for: indexPath as IndexPath) as! HospitalRequestTableViewCell
        cell.selectionStyle = .none
        let currentrequest = hospitalRequests[indexPath.row]
        cell.configureCell(WithHospitalRequest: currentrequest)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentrequest = hospitalRequests[indexPath.row]
        loadDetailScreenForHospitalRequest(request: currentrequest)
    }
    
    private func loadDetailScreenForHospitalRequest(request: HospitalRequest) {
        if let hospitalRequestDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.hospitalRequestDetails) as? SCHospitalRequestDetailsViewController {
            hospitalRequestDetailsVC.currentHospitalRequest = request
            self.navigationController?.pushViewController(hospitalRequestDetailsVC, animated: true)
        }
    }
}
