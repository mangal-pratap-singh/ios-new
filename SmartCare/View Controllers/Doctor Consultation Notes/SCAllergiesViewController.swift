//
//  SCAllergiesViewController.swift
//  SmartCare
//
//  Created by Rediflex PvtLtd on 01/07/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import RealmSwift

class SCAllergiesViewController: UIViewController{
    @IBOutlet weak var noAllergiesYet: UILabel!
    
    @IBOutlet weak var lbl_typeTitle: UILabel!
    
    @IBOutlet weak var allergyListView: UIView!
    
    @IBOutlet weak var lbl_typeList: UILabel!
    @IBOutlet weak var currentAllergiesView: UIView!
    @IBOutlet weak var allergyDetailsText: UITextField!
    @IBOutlet weak var allergyTable: UITableView!
    
    var trickYes: String? = nil
    
    var allergiesType: String? = nil
//   realm here
    var currentItem:LabTestReaml?
    var currentAllergiesItem:AllergiesReaml?
    var currentDiagnosisItem:DiagnosisReaml?
    var currentProcedureItem:ProcedureReaml?
    var currentAdviceItem:AdviceReaml?
    //var pickUpLines : Results<LabTestReaml>!
    var notificationToken: NotificationToken?
    var appointMentId: String = "0"
    let realm = try! Realm()
    var placeHolderTitle: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        allergyDetailsText.delegate = self
        allergyTable.delegate = self
        allergyTable.dataSource = self
        lbl_typeTitle.text =   PatientTypeSwitchDetails(rawValue: allergiesType!).map { $0.rawValue }
//        currentAllergiesView.isHidden = false
      // allergyListView.isHidden = false
         self.customizeNavigationItem(title: allergiesType!, isDetail: true)
            
        if trickYes == "checked"{
            currentAllergiesView.isHidden = true
            allergyListView.isHidden = false
        }
        if allergiesType == "Allergies"{
            placeHolderTitle = "Skin allergy"
        }else if allergiesType == "Diagnosis"{
             placeHolderTitle = "Autism Spectrum Disorder"
        }else if allergiesType == "Procedures"{
             placeHolderTitle = "Arthoplasty knee"
        }else if allergiesType == "Advice"{
             placeHolderTitle = "Write here your advice"
        }
            let color = UIColor.lightGray
        let placeholder = placeHolderTitle //There should be a placeholder set in storyboard or elsewhere string or pass empty
            allergyDetailsText.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor : color])
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
        
       let realm = RealmService.shared.realm
//       // pickUpLines = realm.objects(labTest.self)
        
        notificationToken = realm.observe  { (notification, realm) in
            self.allergyTable.reloadData()
        }
        RealmService.shared.observeRealmError(in: self) { (error) in
            //handle error
            print(error ?? "no error detected")
        }
        
//        guard let Count1 = self.modelClassObject?.data![2].list!.count else {
//            return
//        }
//        self.brandsTableHeight.constant = CGFloat(100) * CGFloat(Count1);
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
         allergyTable.reloadData()
         notificationToken?.invalidate()
         RealmService.shared.stopObservingError(in: self)
    }
    
    
    //MARK:- HERE TEXTFIELD EDITING CHANGE
    
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
           textField.resignFirstResponder()
           return true
       }
    
    @IBAction func txtFieldEditingChanged(_ sender: UITextField) {
        if allergiesType == "Lab Test"{
            
        }
    }
   
    @objc func addTapped(){
        currentAllergiesView.isHidden = false
        allergyListView.isHidden = true
         self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveTapped))
    }

    @objc func saveTapped(){
       currentAllergiesView.isHidden = true
         allergyListView.isHidden = false
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
        if allergiesType == "Allergies"{
            lbl_typeList.text = "Allersies list"
            let item = AllergiesReaml()
                    
                    if(currentAllergiesItem == nil) {
                    // item.id = (DBManager.sharedInstance.getDatAllergiesFromDB()).count
                        item.id = 1
                    }
                    
              item.appointmentId = Int(appointMentId)!
                item.TestDecription = allergyDetailsText.text!
               DBManager.sharedInstance.addAlergiesData(object: item)
        }else if allergiesType == "Lab Test"{
             lbl_typeList.text = "Lab Test list"
            let item = LabTestReaml()
                               
            if(currentItem == nil) {
                       //            item.ID = lab.sharedInstance.getDataFromDB().count
                           // item.id = (DBManager.sharedInstance.getDataLabTestFromDB()).count
                item.id = 1
                               }
                               
                               item.TestDescription = allergyDetailsText.text!
                DBManager.sharedInstance.addLabTestData(object: item)
        }else if allergiesType == "Diagnosis"{
             lbl_typeList.text = "Diagnosis list"
            let item = DiagnosisReaml()
                                          
           if(currentDiagnosisItem == nil) {
                                  //            item.ID = lab.sharedInstance.getDataFromDB().count
              // item.id = (DBManager.sharedInstance.getDiagnosisFromDB()).count
            item.id = 1
                              }
             item.appointmentId = Int(appointMentId)!
                                          
               item.TestDecription = allergyDetailsText.text!
           DBManager.sharedInstance.addDiagnosisData(object: item)
            
        }else if allergiesType == "Procedures"{
            lbl_typeList.text = "Procedures list"
             let item = ProcedureReaml()
            if(currentProcedureItem == nil) {
                                             //            item.ID = lab.sharedInstance.getDataFromDB().count
                         // item.id = (DBManager.sharedInstance.getProcedureFromDB()).count
                item.id = 1
                                         }
                 item.appointmentId = Int(appointMentId)!
                                                     
                          item.TestDecription = allergyDetailsText.text!
                      DBManager.sharedInstance.addProcedureData(object: item)
            
        }else if allergiesType == "Advice"{
             lbl_typeList.text = "Advice list"
            let item = AdviceReaml()
            if(currentAdviceItem == nil) {
                                                        //            item.ID = lab.sharedInstance.getDataFromDB().count
                                     //item.id = (DBManager.sharedInstance.getAdviceFromDB()).count
                     item.id = 1
                              }
                            item.appointmentId = Int(appointMentId)!
                                                                
                                     item.TestDecription = allergyDetailsText.text!
                                 DBManager.sharedInstance.addAdviceData(object: item)
            
        }
        
        self.dismiss(animated: true) { }
       
//        let dataRecieved = labTest.init(name: allergyDetailsText.text!, scores: 1 , email: addText.text!)
//        RealmService.shared.create(dataRecieved)

    }
    @IBAction func barIteTapped(_ sender: Any) {
        
    }
}

extension SCAllergiesViewController : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return pickUpLines.count
        if allergiesType == "Allergies"{
           
            return DBManager.sharedInstance.getDatAllergiesFromDB().count
        }else if allergiesType == "Lab Test"{
            return DBManager.sharedInstance.getDataLabTestFromDB().count
        }else if allergiesType == "Diagnosis"{
            return DBManager.sharedInstance.getDiagnosisFromDB().count
        }else if allergiesType == "Procedures"{
            return DBManager.sharedInstance.getProcedureFromDB().count
        }else if allergiesType == "Advice"{
             return DBManager.sharedInstance.getAdviceFromDB().count
        }else if allergiesType == ""{
             return DBManager.sharedInstance.getDataLabTestFromDB().count
        }
         return DBManager.sharedInstance.getDataLabTestFromDB().count
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "AllergyCell", for: indexPath) as! allergyListTableViewCell
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllergyCell", for: indexPath) as! allergyListTableViewCell
//            else {
//                return UITableViewCell()}
        let index = Int(indexPath.row)
        if allergiesType == "Allergies"{
                   let item = DBManager.sharedInstance.getDatAllergiesFromDB()[index] as AllergiesReaml
            // let item = realm.objects(AllergiesReaml.self)[index] as AllergiesReaml
            cell.allergyType.text = "Allergies \(index + 1)"
                          cell.allergyDescription.text = item.TestDecription
               }else if allergiesType == "Lab Test"{
                  let item = DBManager.sharedInstance.getDataLabTestFromDB()[index] as LabTestReaml
               cell.allergyType.text = "Lab Test \(index + 1)"
                         cell.allergyDescription.text = item.TestDescription
               }else if allergiesType == "Diagnosis"{
                  let item = DBManager.sharedInstance.getDiagnosisFromDB()[index] as DiagnosisReaml
               cell.allergyType.text = "Diagnosis \(index + 1)"
                         cell.allergyDescription.text = item.TestDecription
               }else if allergiesType == "Procedures"{
                  let item = DBManager.sharedInstance.getProcedureFromDB()[index] as ProcedureReaml
               cell.allergyType.text = "Procedures \(index + 1)"
                         cell.allergyDescription.text = item.TestDecription
               }else if allergiesType == "Advice"{
                  let item = DBManager.sharedInstance.getAdviceFromDB()[index] as AdviceReaml
               cell.allergyType.text = "Advice \(index + 1)"
                         cell.allergyDescription.text = item.TestDecription
               }
    
        
//        let requiredData = pickUpLines[indexPath.row]
//        cell.allergyDescription?.text = requiredData.Testdescription
        
//        / RealmService.shared.create(requiredData)
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete
            else
        {return}
/*
        if( indexPath.row > -1) {
            

            let index = Int(indexPath.row)
           let items = DBManager.sharedInstance.getDataFromDB()[index] as labTest
            if let item = currentItem {
                DBManager.sharedInstance.deleteFromDb(object: item)
               
            }
           */
//            vc.currentItem = item
//            self.present(vc, animated: true, completion: nil)
        //}

    }
    
}


// delete is not working as of now
