//
//  SCDoctorsConsultationNotesViewController.swift
//  SmartCare
//
//  Created by Rediflex PvtLtd on 26/06/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
class SCDoctorsConsultationNotesViewController: UIViewController,DoctorToReferSearchDelegate {
    
    
    @IBOutlet weak var view_contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var imageAvtar: UIImageView!
    @IBOutlet weak var patientName: UILabel!
    
    @IBOutlet weak var hospitalName: UILabel!
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var patientsAppId: UILabel!
    @IBOutlet weak var patientsAge: UILabel!
    @IBOutlet weak var patientGender: UILabel!
    
    @IBOutlet weak var primaryComplaintsText: UITextField!
   
    @IBOutlet weak var pastHistoryText: UITextField!
    @IBOutlet weak var presentHistoryText: UITextField!
    
    @IBOutlet weak var contactNumberText: UITextField!
    @IBOutlet weak var resgistrationText: UITextField!
    @IBOutlet weak var docotorNameText: UITextField!
    @IBOutlet weak var follow_upOnText: UITextField!
    @IBOutlet weak var referToDocText: UITextField!
    
    @IBOutlet weak var txtField_specialisties: UITextField!
    @IBOutlet weak var tableList: UITableView!
    
    var doctorAppointment: DoctorAppointment?
    
   var listTitles = ["Allergies" , "Vitals" , "Lab Test" , "Diagnosis" , "Medicine" , "Procedures" , "Advice"  ]
    var checkArr :[String]?
    var sendDicParameter = [String: AnyObject]()
    
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
customizeNavigationItem(title: SCAppConstants.pageTitles.consultationNotes, isDetail: true)
         checkArr = [String]()
        
        patientName.text = doctorAppointment?.patient?.name
        dateTime.text = doctorAppointment?.endTime?.toTimeString()
        patientsAge.text = doctorAppointment?.patient?.age
        patientsAppId.text = String(doctorAppointment?.id ?? 0)
        patientGender.text = doctorAppointment?.patient?.gender
        hospitalName.text = doctorAppointment?.doctorHospital?.name
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkArr?.removeAll()
        for j in 0..<listTitles.count{
            checkArr?.append("unCheck")
          }
        
        for i in 0..<listTitles.count{
            let title = listTitles[i]
            if title == "Allergies"{
                 var allergiesArr = [AnyObject]()
                if DBManager.sharedInstance.getDatAllergiesFromDB().count == 0{
                    checkArr?[i] = "unCheck"
                }else{
                   checkArr?[i] = "unCheck"
                    for item in DBManager.sharedInstance.getDatAllergiesFromDB(){
                        let appId = item.appointmentId
                        if (doctorAppointment?.id) == 0 && appId == 0{
                        }else if (doctorAppointment?.id) == appId{
                            checkArr?[i] = "checked"
                            var allergiesDict = [String: AnyObject]()
                            
                            //"allergies":[{"description":"skin","id":"1"}],
                            let descriptionStr = item.TestDecription as String
                            allergiesDict["description"] = descriptionStr as AnyObject
                            allergiesDict["id"] = String(item.id) as AnyObject
                            allergiesArr.append(allergiesDict as AnyObject)
                            //allergiesDict["id"] = String(item)
                            
                        }
                
                     }
                    
                    
               }
                sendDicParameter["allergies"] = allergiesArr as AnyObject
            
            }else if title == "Vitals"{
                 var vitalsDict = [String: AnyObject]()
                checkArr?[i] = "unCheck"
                if DBManager.sharedInstance.getDataVitalsFromDB().count == 0{
                                
                            
                            }else{
                                
                                for item in DBManager.sharedInstance.getDataVitalsFromDB(){
                                    let appId = item.appointmentId
                                    if (doctorAppointment?.id) == 0 && appId == 0{
                                       
                                        
                                    }else if (doctorAppointment?.id) == appId{
                                         checkArr?[i] = "checked"
                                        // checkArr?.append("checked")
                                                                   
                                    vitalsDict["bpDiastolic"] = item.bloodPresureDiastolic as AnyObject
                                     vitalsDict["bpSystolic"] = item.bloodPresureSystolic as AnyObject
                                     vitalsDict["heartRate"] = item.heartRate as AnyObject
                                     vitalsDict["height"] = item.hieght as AnyObject
                                     vitalsDict["respiratoryRate"] = item.respiratory as AnyObject
                                    vitalsDict["temperature"] = item.temperature as AnyObject
                                    vitalsDict["weight"] = item.weight as AnyObject
                                        
                                      }
                                
                                 }
                            }
                sendDicParameter["vitals"] = vitalsDict as AnyObject
                
            }else if title == "Lab Test"{
               
                var labTestArr = [AnyObject]()
                  checkArr?[i] = "unCheck"
             if DBManager.sharedInstance.getDataLabTestFromDB().count == 0{
                                
                              
                            }else{
                                
                                for item in DBManager.sharedInstance.getDataLabTestFromDB(){
                                    let appId = item.appointmentId
                                    if (doctorAppointment?.id) == 0 && appId == 0{
                                       
                                        
                                    }else if (doctorAppointment?.id) == appId{
                                         //checkArr?.append("checked")
                                          checkArr?[i] = "checked"
                                        
                                      //  "labTest":[{"id":"1","name":"B2 GLYCOPRO AB IgG","type":"2"}],
                                        var labTestDict = [String: AnyObject]()
                                                                   
                                                                   //"allergies":[{"description":"skin","id":"1"}],
                                                                   
                                                                   labTestDict["description"] = item.TestDescription as AnyObject
                                        labTestDict["type"] = item.type as AnyObject
                                        labTestDict["id"] = String(item.id) as AnyObject
                                        //labTestDict["id"] =
                                                                   labTestArr.append(labTestDict as AnyObject)
                                    }
                                
                            }
                            }
                
                sendDicParameter["labTest"] = labTestArr as AnyObject
                
            }else if title == "Diagnosis"{
               
               var diagonosisArr = [AnyObject]()
                 checkArr?[i] = "unCheck"
                if DBManager.sharedInstance.getDiagnosisFromDB().count == 0{
                    
                    //checkArr?.append("unCheck")
                }else{
                    
                     
                    for item in DBManager.sharedInstance.getDiagnosisFromDB(){
                        let appId = item.appointmentId
                        if (doctorAppointment?.id) == 0 && appId == 0{
                            // checkArr?.append("unCheck")
                            
                        }else if (doctorAppointment?.id) == appId{
                            // checkArr?.append("checked")
                             checkArr?[i] = "checked"
                            
                            //"diagnosis":[{"description":"austine","id":"1"}],
                            var diagonosisDict = [String: AnyObject]()
                            
                            //"allergies":[{"description":"skin","id":"1"}],
                            
                            diagonosisDict["description"] = item.TestDecription as AnyObject
                           diagonosisDict["id"] = String(item.id) as AnyObject
                            diagonosisArr.append(diagonosisDict as AnyObject)
                        }
                    }
                    
                 }
                sendDicParameter["diagnosis"] = diagonosisArr as AnyObject
                
            }else if title == "Medicine"{
                 var medicineArr = [AnyObject]()
                checkArr?[i] = "unCheck"
                if DBManager.sharedInstance.getDataMedicineListFromDB().count == 0{
                                   
                                  // checkArr?.append("unCheck")
                    
                               }else{
                                   
                                   for item in DBManager.sharedInstance.getDataMedicineListFromDB(){
                                       let appId = item.appointmentId
                                       if (doctorAppointment?.id) == 0 && appId == 0{
                                           // checkArr?.append("unCheck")
                                           
                                       }else if (doctorAppointment?.id) == appId{
                                           // checkArr?.append("checked")
                                         checkArr?[i] = "checked"
                                        var medicineDict = [String: AnyObject]()
                                        
                                        //"allergies":[{"description":"skin","id":"1"}],
                                        
                                        medicineDict["name"] = item.medicineName as AnyObject
                                         medicineDict["morning"] = item.morning as AnyObject
                                         medicineDict["afternoon"] = item.afternoon as AnyObject
                                         medicineDict["evening"] = item.evening as AnyObject
                                         medicineDict["night"] = item.night as AnyObject
                                 
                                medicineDict["day"] = item.days as AnyObject
                                medicineDict["id"] = String(item.id) as AnyObject
                                        
                            medicineDict["comments"] = item.comments as AnyObject
                         medicineDict["uomId"] = item.uomId as AnyObject
                                        
               medicineArr.append(medicineDict as AnyObject)
                                }
                            }
                               }
                sendDicParameter["medicines"] = medicineArr as AnyObject
                
            }else if title == "Procedures"{
                var procedureArr = [AnyObject]()
                 checkArr?[i] = "unCheck"
            if DBManager.sharedInstance.getProcedureFromDB().count == 0{
                                            
                                           // checkArr?.append("unCheck")
                                        }else{
                                            
                                            for item in DBManager.sharedInstance.getProcedureFromDB(){
                                                let appId = item.appointmentId
                                                if (doctorAppointment?.id) == 0 && appId == 0{
                                                     //checkArr?.append("unCheck")
                                                    
                                                }else if (doctorAppointment?.id) == appId{
                                                     //checkArr?.append("checked")
                                                     checkArr?[i] = "checked"
                                                    var procedureDict = [String: AnyObject]()
                                                                               procedureDict["name"] = item.TestDecription as AnyObject
                                        
                                 procedureDict["id"] = String(item.id) as AnyObject
                                                                               procedureArr.append(procedureDict as AnyObject)
                                                    
                                        }
                                            
                       }
                }
                sendDicParameter["procedures"] = procedureArr as AnyObject
                
            }else if title == "Advice"{
                var adviceArr = [AnyObject]()
                checkArr?[i] = "unCheck"
        if DBManager.sharedInstance.getAdviceFromDB().count == 0{
                                                   
                                                   //checkArr?.append("unCheck")
                                               }else{
                                                   
                                                   for item in DBManager.sharedInstance.getAdviceFromDB(){
                                                       let appId = item.appointmentId
                                                       if (doctorAppointment?.id) == 0 && appId == 0{
                                                            //checkArr?.append("unCheck")
                                                           
                                                       }else if (doctorAppointment?.id) == appId{
                                                            //checkArr?.append("checked")
                                             checkArr?[i] = "checked"
                                                        let adviceCommentsStr = item.TestDecription
                                                                                                           
                                                                                        adviceArr.append(adviceCommentsStr as AnyObject)
                                    }
                                                    
                         }
                 }
                
                sendDicParameter["advice"]  = adviceArr as AnyObject
               }
         }
        
//      DispatchQueue.main.async {
//                self.tableList.reloadData()
//             }
        
      }
  
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //view_contentView.frame.size.height = 1350
        // scrollView.contentSize = CGSize(width: view_contentView.frame.size.width, height: 1350)
        DispatchQueue.main.async {
                  self.tableList.reloadData()
               }
    
     }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == follow_upOnText{
            view.endEditing(true)
//            DispatchQueue.main.async {
//                self.follow_upOnText.resignFirstResponder()
//                
//            }
            //showDatePicker(textField: textField)
         }
    }
    
    
    
    
    @IBAction func clickOnReferToDoctorTextField(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SCDoctorToReferViewController")  as! SCDoctorToReferViewController
                                        // vc.appointMentIdStr = appointMentId
                                   vc.doctorToReferSearchDelegate = self
                          // vc.appointMentIdStr = appointMentId
                                   self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    
    @IBAction func selectDateClick(_ sender: UIButton) {
        
        self.follow_upOnText.resignFirstResponder()
         showDatePicker(textField: follow_upOnText)
        
    }
    
    
    func sendBackValue(txt: ReferToDoctorSearch){
       // referToDocText.text = txt
        docotorNameText.text = txt.name
        contactNumberText.text = txt.phoneNumber
        resgistrationText.text = txt.id
        txtField_specialisties.text = txt.specialities
    }
    
    
    func showDatePicker(textField:UITextField) {
           
           let datePicker = ActionSheetDatePicker(title: "Select Date", datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
               picker, value, index in
               let dateString = (value as! Date).toDateString(withFormatter: SCAppConstants.DateFormatType.date)
               textField.text = dateString
               return
           }, cancel: {
               ActionStringCancelBlock in return
           },
              origin: textField.superview)
           datePicker?.show()
       }
    @IBAction func saveConsultationTapped(_ sender: UIButton) {
        
        
        sendDicParameter["presentHistory"]  = presentHistoryText?.text as AnyObject? ?? "" as AnyObject
        sendDicParameter["pastHistory"]  = pastHistoryText?.text as AnyObject? ?? "" as AnyObject
        sendDicParameter["primaryComplaint"]  = primaryComplaintsText?.text as AnyObject? ?? "" as AnyObject
        sendDicParameter["referTo"]  = docotorNameText?.text as AnyObject? ?? "" as AnyObject
        sendDicParameter["followUp"]  = follow_upOnText?.text as AnyObject? ?? "" as AnyObject
        
        print("sendParameterDict=\(sendDicParameter)")
        
        saveConsultationNoteMethod(sendParameter: sendDicParameter)
        
        
        
        
    }
    
    

    func saveConsultationNoteMethod(sendParameter: [String: AnyObject]){
               SCProgressView.show()
              
         let userId = UserProfile.getUserId()
        let  appointMentId = String(doctorAppointment?.id ?? 0)
        let urlString = String(format: SCAppConstants.apiRequestPath.doctorSaveConsultationNote,userId,appointMentId)
        

        SCConsultationNoteService.postHitAPI2(urlStr: urlString, withParameters: sendParameter, onComplete: { (json, bool) in
            
            if bool != true{
             SCProgressView.hide()
            self.showAlert(title: "Save Consultation Note", msg: "Not")
             }
             self.showAlert(title: "Save Consultation Note", msg: "Successfully")
           // DBManager.sharedInstance.deleteAllDatabase()
            
            for item in DBManager.sharedInstance.getDatAllergiesFromDB(){
                               let appId = item.appointmentId
                            if (self.doctorAppointment?.id) == appId{
                                    //checkArr?.append("checked")
                                DBManager.sharedInstance.deleteaNYFromDb(object: item)
                            }
                    
            }
            for item in DBManager.sharedInstance.getDataVitalsFromDB(){
                               let appId = item.appointmentId
                            if (self.doctorAppointment?.id) == appId{
                                    //checkArr?.append("checked")
                                DBManager.sharedInstance.deleteaNYFromDb(object: item)
                            }
                    
            }
            for item in DBManager.sharedInstance.getDataLabTestFromDB(){
                               let appId = item.appointmentId
                            if (self.doctorAppointment?.id) == appId{
                                    //checkArr?.append("checked")
                                DBManager.sharedInstance.deleteaNYFromDb(object: item)
                            }
                    
            }
            
            for item in DBManager.sharedInstance.getDiagnosisFromDB(){
                               let appId = item.appointmentId
                            if (self.doctorAppointment?.id) == appId{
                                    //checkArr?.append("checked")
                                DBManager.sharedInstance.deleteaNYFromDb(object: item)
                            }
                    
            }
            
            for item in DBManager.sharedInstance.getProcedureFromDB(){
                               let appId = item.appointmentId
                            if (self.doctorAppointment?.id) == appId{
                                    //checkArr?.append("checked")
                                DBManager.sharedInstance.deleteaNYFromDb(object: item)
                            }
                    
            }
            
            for item in DBManager.sharedInstance.getAdviceFromDB(){
                               let appId = item.appointmentId
                            if (self.doctorAppointment?.id) == appId{
                                    //checkArr?.append("checked")
                                DBManager.sharedInstance.deleteaNYFromDb(object: item)
                            }
                    
            }
            
            for item in DBManager.sharedInstance.getDataMedicineListFromDB(){
                               let appId = item.appointmentId
                            if (self.doctorAppointment?.id) == appId{
                                    //checkArr?.append("checked")
                                DBManager.sharedInstance.deleteaNYFromDb(object: item)
                            }
                    
            }
            
        SCProgressView.hide()
    
            self.navigationController?.popToRootViewController(animated: true)
            
            }) { (error) in
                SCProgressView.hide()
             self.showAlert(title: "Save Consultation Note", msg: error)
          }
        /*
           SCConsultationNoteService.getHitAPI(withParameters: urlString, onComplete: { (json, bool) in
                if bool != true{
                                          
                    return
                    }
                                          
                }) { (error) in
                                     
                }
        */
    
    }
}

extension SCDoctorsConsultationNotesViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ConsultationTableCell
        cell.typeText.text = self.listTitles[indexPath.row]
        let checkStr = checkArr?[indexPath.row]
        cell.img_checked.isHidden = true
        if checkStr == "checked"{
            cell.img_checked.isHidden = false
          }
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "SCAllergiesViewController")  as! SCAllergiesViewController
//            vc.hidesBottomBarWhenPushed = true
            vc.allergiesType = "Allergies"
            vc.trickYes = checkArr?[indexPath.row]
            vc.appointMentId = String(doctorAppointment?.id ?? 0)
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 1{
            let vc = storyboard?.instantiateViewController(withIdentifier: "SCVitalsViewController")  as! SCVitalsViewController

                        //            vc.hidesBottomBarWhenPushed = true
                      // vc.lbl_typeTitle.text = "Procedures"
             vc.appointMentId = String(doctorAppointment?.id ?? 0)
                      self.navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 2{
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "SCLabTestViewController")  as! SCLabTestViewController
                       //            vc.hidesBottomBarWhenPushed = true
              //vc.allergiesType = "Lab Test"
            vc.doctorAppointment = doctorAppointment
             vc.appointMentIdStr    =  String(doctorAppointment?.id ?? 0)
             self.navigationController?.pushViewController(vc, animated: true)
            
            
        }else if indexPath.row == 3{
            let vc = storyboard?.instantiateViewController(withIdentifier: "SCAllergiesViewController")  as! SCAllergiesViewController
            
                                  //            vc.hidesBottomBarWhenPushed = true
             vc.allergiesType = "Diagnosis"
             vc.trickYes = checkArr?[indexPath.row]
             vc.appointMentId = String(doctorAppointment?.id ?? 0)
           self.navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 4{
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "SCMedicineViewController")  as! SCMedicineViewController
            vc.appointMentId    =  String(doctorAppointment?.id ?? 0)

             //            vc.hidesBottomBarWhenPushed = true
           vc.medicineType = "Medicine"
         //vc.trickYes = self.listTitles[indexPath.row]
           self.navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 5{
            let vc = storyboard?.instantiateViewController(withIdentifier: "SCAllergiesViewController")  as! SCAllergiesViewController
             
           //            vc.hidesBottomBarWhenPushed = true
             vc.allergiesType = "Procedures"
             vc.trickYes = checkArr?[indexPath.row]
            vc.appointMentId = String(doctorAppointment?.id ?? 0)
            self.navigationController?.pushViewController(vc, animated: true)
            
         }else if indexPath.row == 6{
            let vc = storyboard?.instantiateViewController(withIdentifier: "SCAllergiesViewController")  as! SCAllergiesViewController
             
            //            vc.hidesBottomBarWhenPushed = true
             vc.allergiesType = "Advice"
             vc.trickYes = checkArr?[indexPath.row]
             vc.appointMentId = String(doctorAppointment?.id ?? 0)
          self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
}

extension UIScrollView {
       func updateContentView() {
           contentSize.height = subviews.sorted(by: { $0.frame.maxY < $1.frame.maxY }).last?.frame.maxY ?? contentSize.height
       }
   }
