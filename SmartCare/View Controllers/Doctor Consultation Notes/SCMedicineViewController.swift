//
//  SCMedicineViewController.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 28/03/20.
//  Copyright © 2020 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCMedicineViewController: UIViewController,MedicineSearchOnlyDelegate,MedicineFindBrandDelegate {
    
    @IBOutlet weak var txtField_search: UITextField!
    
    @IBOutlet weak var txtField_morning: UITextField!
    
    @IBOutlet weak var txtField_afterNoon: UITextField!
    
    @IBOutlet weak var txtField_evening: UITextField!
    
    @IBOutlet weak var txtField_night: UITextField!
    
    
    @IBOutlet weak var txtField_days: UITextField!
    
    @IBOutlet weak var txtView: UITextView!
    
    @IBOutlet weak var tbleView: UITableView!
    //   realm here
       var currentMedicine:MedicineReaml?
    var medicineType: String? = ""
    var appointMentId: String = "0"
    var delegateCall: String = ""
    var drugsUnitId: String = "1"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tbleView.delegate = self
        tbleView.dataSource = self
        tbleView.estimatedRowHeight  = 150
        
        // Do any additional setup after loading the view.
        customizeNavigationItem(title: medicineType ?? "", isDetail: true)
        txtView.text = "Write comments here"
        txtView.textColor = UIColor.lightGray
        DispatchQueue.main.async {
                   self.tbleView.reloadData()
                }
         }
    
    
    
    
    @IBAction func clickBtnOnTextField(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SCMedicineSearchOnlyViewController")  as! SCMedicineSearchOnlyViewController
                                 // vc.appointMentIdStr = appointMentId
                            vc.medicineSearchOnlyDelegate = self
                    vc.appointMentIdStr = appointMentId
                            self.navigationController?.pushViewController(vc, animated: true)
    }
    
//    @IBAction func txtFieldTouchUpInside(_ sender: UITextField) {
//        let vc = storyboard?.instantiateViewController(withIdentifier: "SCMedicineSearchOnlyViewController")  as! SCMedicineSearchOnlyViewController
//                          // vc.appointMentIdStr = appointMentId
//                     vc.medicineSearchOnlyDelegate = self
//                     self.navigationController?.pushViewController(vc, animated: true)
//        
//    }
    /*
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if delegateCall != ""{
            return
        }
        if textField == txtField_search{
            delegateCall = ""
            let vc = storyboard?.instantiateViewController(withIdentifier: "SCMedicineSearchOnlyViewController")  as! SCMedicineSearchOnlyViewController
                   // vc.appointMentIdStr = appointMentId
              vc.medicineSearchOnlyDelegate = self
              self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    */
    
    
    //MARK:- DELEGATE METHOD OF SCFindMedicineViewController
    
    func sendBackMedicineBrandNameMethod(txt: String, unitId: String) {
        delegateCall = "delegateCall"
        txtField_search.text = txt
        drugsUnitId = unitId
    }
    //MARK:- DELEGATE METHOD OF SCMedicineSearchOnlyViewController
    func sendBackMessageValue(txt: String,unitId: String) {
        delegateCall = "delegateCall"
        txtField_search.text = txt
        drugsUnitId = unitId
    }
    
  func textViewDidBeginEditing(_ textView: UITextView) {
      if textView.textColor == UIColor.lightGray {
          textView.text = nil
          textView.textColor = UIColor.black
      }
  }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write comments here"
            textView.textColor = UIColor.lightGray
        }
    }
    
    
    @IBAction func addBtnClick(_ sender: UIButton) {
    }
    
    
    @IBAction func saveBtnClick(_ sender: UIButton) {
        
        
        let item = MedicineReaml()
                               
                     if(currentMedicine == nil) {
                       // item.id = (DBManager.sharedInstance.getDataMedicineListFromDB()).count
                        item.id = 1
                        }
                               
                          // item.testDecription = allergyDetailsText.text!
        item.appointmentId =  Int(appointMentId)!
        item.medicineName = txtField_search.text ?? ""
        item.morning = txtField_morning.text ?? ""
        item.afternoon = txtField_afterNoon.text ?? ""
        item.evening = txtField_evening.text ?? ""
        item.night = txtField_night.text ?? ""
        item.days = txtField_days.text ?? ""
        item.comments = txtView.text ?? ""
        item.uomId = Int(drugsUnitId) ?? 1
         DBManager.sharedInstance.addMedicineData(object: item)
        DispatchQueue.main.async {
            self.tbleView.reloadData()
        }
                   self.dismiss(animated: true) { }
        
    }
    
    
    
    
    @IBAction func findMedicineBtnClick(_ sender: UIButton) {
      
        let vc = storyboard?.instantiateViewController(withIdentifier: "SCFindMedicineViewController")  as! SCFindMedicineViewController
          vc.appointMentIdStr = appointMentId
          vc.medicineFindBrandDelegate = self
      
    self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SCMedicineViewController : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return pickUpLines.count
        return (DBManager.sharedInstance.getDataMedicineListFromDB()).count
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 170
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "AllergyCell", for: indexPath) as! allergyListTableViewCell
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SCMedicineListCell", for: indexPath) as! SCMedicineListCell
//            else {
//                return UITableViewCell()}
        let index = Int(indexPath.row)
       // let drugsDict = self.doctorFindDrugs?.findDrugs?[index]
        //cell.lbl_medicineTitle.text = drugsDict?.drugName
       let medicineItem = DBManager.sharedInstance.getDataMedicineListFromDB()[index]
        cell.lbl_medicineName.text = medicineItem.medicineName
         cell.lbl_morning.text = medicineItem.morning
         cell.lbl_afternoon.text = medicineItem.afternoon
         cell.lbl_evening.text = medicineItem.evening
         cell.lbl_night.text = medicineItem.night
         cell.lbl_comments.text = medicineItem.comments
        
        return cell
}
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*
        let drugsDict = self.doctorFindDrugs?.findDrugs?[indexPath.row]
        txtField_select.text = drugsDict?.drugName
        if txtField_brandName.text == nil || txtField_brandName.text == ""{
            
            return
        }
        let userId = UserProfile.getUserId()
        
        let urlString = String(format: SCAppConstants.apiRequestPath.doctorFindMedicineSearch,userId,txtField_brandName.text!,appointMentIdStr!,drugsDict!.id)
               SCConsultationNoteService.getHitAPI(withParameters: urlString, onComplete: { (json, bool) in
                   if bool != true{
                       return
                   }
                  self.doctorFindMedicine = DoctorFindMedicineClass(data: json)
                   DispatchQueue.main.async {
                       self.tbleView.reloadData()
                   }
                   
               }) { (error) in
                   
               }
 */
              }
    
    
}
