//
//  SCVitalsViewController.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 28/03/20.
//  Copyright © 2020 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import RealmSwift
class SCVitalsViewController: UIViewController {
    
    
    @IBOutlet weak var txtField_height: UITextField!
    
    @IBOutlet weak var txtField_weight: UITextField!
    
    @IBOutlet weak var bloodPresureSystolic: UITextField!
    
    @IBOutlet weak var bloodPresureDiastolic: UITextField!
    
    @IBOutlet weak var heartRate: UITextField!
    
    @IBOutlet weak var txtField_tmp: UITextField!
    
    @IBOutlet weak var txtField_respiratoryRate: UITextField!
    //   realm here
    var currentVitals:VitalsReaml?
    var appointMentId: String  = "0"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customizeNavigationItem(title: "Vitals", isDetail: true)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))

        if DBManager.sharedInstance.getDataVitalsFromDB().count != 0{
         let item = DBManager.sharedInstance.getDataVitalsFromDB()[0] as VitalsReaml
        txtField_height.text = item.hieght
        txtField_weight.text = item.weight
        bloodPresureSystolic.text = item.bloodPresureSystolic
        bloodPresureDiastolic.text = item.bloodPresureDiastolic
        heartRate.text = item.heartRate
        txtField_tmp.text = item.temperature
        txtField_respiratoryRate.text = item.respiratory
        }
        // Do any additional setup after loading the view.
    }
    
    @objc func addTapped(){
           //currentAllergiesView.isHidden = false
           //allergyListView.isHidden = true
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveTapped))
       }
    
    
        @objc func saveTapped(){
          // currentAllergiesView.isHidden = true
            // allergyListView.isHidden = false
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
            
            
            guard let heightStr = txtField_height.text else {return}
             guard let weightStr = txtField_weight.text else {return}
             guard let bloodPresureSysStr = bloodPresureSystolic.text else {return}
             guard let bloodPresureDiasStr = bloodPresureDiastolic.text else {return}
             guard let heartRateStr = heartRate.text else {return}
             guard let tmpStr = txtField_tmp.text else {return}
             guard let respiratoryStr = txtField_respiratoryRate.text else {return}
          
                let item = VitalsReaml()
                        
              if(currentVitals == nil) {
                 item.id = (DBManager.sharedInstance.getDataVitalsFromDB()).count
                }
                        
                   // item.testDecription = allergyDetailsText.text!
            item.appointmentId =  Int(appointMentId)!
            item.hieght = heightStr
            item.weight = weightStr
            item.bloodPresureSystolic = bloodPresureSysStr
            item.bloodPresureDiastolic = bloodPresureDiasStr
            item.heartRate = heartRateStr
            item.temperature = tmpStr
            item.respiratory = respiratoryStr
             DBManager.sharedInstance.addVitalsData(object: item)
            self.dismiss(animated: true) { }
           
    //        let dataRecieved = labTest.init(name: allergyDetailsText.text!, scores: 1 , email: addText.text!)
    //        RealmService.shared.create(dataRecieved)

        }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
