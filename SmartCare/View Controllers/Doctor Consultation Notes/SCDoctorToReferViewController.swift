//
//  SCDoctorToReferViewController.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 16/04/20.
//  Copyright © 2020 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit


//MARK: step 1 Add Protocol here.
protocol DoctorToReferSearchDelegate: class {
    func sendBackValue(txt: ReferToDoctorSearch)
}

class SCDoctorToReferViewController: UIViewController {
    
    weak var doctorToReferSearchDelegate:DoctorToReferSearchDelegate?
    @IBOutlet weak var txtField_doctorRefer: UITextField!
    
    @IBOutlet weak var tbleView: UITableView!
    var doctorReferToSearchClass: DoctorReferToSearchClass?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tbleView.dataSource = self
        tbleView.delegate = self
        tbleView.isHidden = true
         self.customizeNavigationItem(title: "Doctor Search", isDetail: true)
        
        // Do any additional setup after loading the view.
     }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    @IBAction func txtFieldEditingChanged(_ sender: UITextField) {
        if sender.text != ""{
            getLabTestSearchMethod(searchStr: sender.text!)
        }
    }
    
    
    func getLabTestSearchMethod(searchStr: String){
      //let userId = UserProfile.getUserId()
        
        let urlString = String(format: SCAppConstants.apiRequestPath.doctorToReferSearch,searchStr)
                                   SCConsultationNoteService.getHitAPI(withParameters: urlString, onComplete: { (json, bool) in
                                       if bool != true{
                                       // self.labTestSearchSatatus = ""
                                        self.tbleView.isHidden = true
                                           return
                                       }
                                    self.tbleView.isHidden = false
                                  //  self.labTestSearchSatatus = "Yes"
                                    print("lab tes response=\(json)")
                                    // self.selectDrugsBtn = ""
                                      self.doctorReferToSearchClass = DoctorReferToSearchClass(data: json)
                                       DispatchQueue.main.async {
                                                             self.tbleView.reloadData()
                                                         }
                                       
                                   }) { (error) in
                                    self.tbleView.isHidden = true
                                   // self.labTestSearchSatatus = ""
                                     }
         }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SCDoctorToReferViewController : UITableViewDataSource , UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return pickUpLines.count
        return doctorReferToSearchClass?.referToDoctorSearch?.count ?? 0
        //return doctorLabTestSearch?.labtestSearch?.count ?? 0
        }
    
 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
   
                let cell = tableView.dequeueReusableCell(withIdentifier: "SCDoctorToReferSearchCell", for: indexPath) as! SCDoctorToReferSearchCell
                       let index = Int(indexPath.row)
                       
                 let referDict =   doctorReferToSearchClass?.referToDoctorSearch?[index]
                      // let item = DBManager.sharedInstance.getDataLabTestFromDB()[index] as LabTestReaml
                       
             cell.lbl_doctorName.text = referDict?.name
    
          return cell
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      
            return 44
       
      }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete
            else
        {return}
/*
        if( indexPath.row > -1) {
            

            let index = Int(indexPath.row)
           let items = DBManager.sharedInstance.getDataFromDB()[index] as labTest
            if let item = currentItem {
                DBManager.sharedInstance.deleteFromDb(object: item)
               
            }
           */
//            vc.currentItem = item
//            self.present(vc, animated: true, completion: nil)
        //}

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let referDict =   doctorReferToSearchClass?.referToDoctorSearch?[indexPath.row] else { return }
           doctorToReferSearchDelegate?.sendBackValue(txt: referDict)
        self.navigationController?.popViewController(animated: false)
            
           // txtField_labtest.text = doctorLabTestSearch?.labtestSearch?[indexPath.row].name
       
    }
    
}
