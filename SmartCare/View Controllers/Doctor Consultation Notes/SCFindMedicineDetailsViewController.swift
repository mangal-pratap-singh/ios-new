//
//  SCFindMedicineDetailsViewController.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 14/04/20.
//  Copyright © 2020 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCFindMedicineDetailsViewController: UIViewController {
    
    @IBOutlet weak var lbl_brandName: UILabel!
    
    @IBOutlet weak var lbl_companyName: UILabel!
    
    @IBOutlet weak var lbl_mrp: UILabel!
    
    @IBOutlet weak var lbl_use: UILabel!
    @IBOutlet weak var lbl_type: UILabel!
    
    @IBOutlet weak var tbleView: UITableView!
    
    @IBOutlet weak var collectioView_medicineDetails: UICollectionView!
    
    @IBOutlet weak var txtView_details: UITextView!
    
    var getFindMedicine: FindMedicine?
    var selectIndex: Int = 0
    var detailsArr =  [String]()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.customizeNavigationItem(title: "Brand Medicine Details", isDetail: true)
        tbleView.dataSource = self
        tbleView.delegate = self
        collectioView_medicineDetails.delegate = self
        collectioView_medicineDetails.dataSource = self
        for (key, value) in getFindMedicine?.details ?? Dictionary()  {
            print("\(key) -> \(value)")
            detailsArr.append(key)
        }
        DispatchQueue.main.async {
            self.tbleView.reloadData()
            self.collectioView_medicineDetails.reloadData()
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.lbl_brandName.text = getFindMedicine?.brandName
         self.lbl_companyName.text = getFindMedicine?.companyName
        self.lbl_mrp.text = getFindMedicine?.mrp
        self.lbl_type.text = getFindMedicine?.type
        self.lbl_use.text = getFindMedicine?.use
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SCFindMedicineDetailsViewController : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return pickUpLines.count
       
        return getFindMedicine?.drugs?.count ?? 0
       
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "AllergyCell", for: indexPath) as! allergyListTableViewCell
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "SCBrandSearchDrugsCell", for: indexPath) as! SCBrandSearchDrugsCell
//            else {
//                return UITableViewCell()}
        let index = Int(indexPath.row)
        let drugsDict = getFindMedicine?.drugs?[index]
        cell.lbl_drugsName.text = drugsDict?.drugName
        cell.lbl_quantity.text = drugsDict?.quantity
        cell.lbl_units.text = drugsDict?.unitName
        
        return cell
       
}
}

extension SCFindMedicineDetailsViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
        
        func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
             return 1     //return number of sections in collection view
         }

         func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return detailsArr.count //return number of rows in section
         }

         func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FindBrandSearchCollectionCell", for: indexPath as IndexPath) as! FindBrandSearchCollectionCell
            
             //cell.img_icon.image = UIImage(named: dataImage[indexPath.row])
            //cell.view_subView.layer.cornerRadius = 5
           // cell.view_subView.backgroundColor = UIColor.clear
            cell.lbl_selectLine.backgroundColor = UIColor.clear
            //cell.lbl_downLine.textColor = UIColor.clear
            cell.lbl_titleName.textColor = UIColor.darkGray
            if selectIndex == indexPath.item{
                cell.lbl_selectLine.backgroundColor = UIColor.black
               // cell.view_subView.backgroundColor = UIColor.white
                // cell.lbl_downLine.backgroundColor = UIColor.red
                 cell.lbl_titleName.text = detailsArr[indexPath.item]
                cell.lbl_titleName.textColor = UIColor.black
            }else{
                
                cell.lbl_titleName.text = detailsArr[indexPath.item]
                // cell.lbl_title.text = dataTitle[indexPath.row]
            }
            
             //configureCell(cell: cell, forItemAtIndexPath: indexPath as IndexPath as NSIndexPath) as! EventMoreCollectionCell
             return cell
         }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            selectIndex = indexPath.item
             //selectedTile = dataTitle[indexPath.item]
            let keyStr = detailsArr[indexPath.item]
            //let getValue = getFindMedicine?.details?[keyStr] as AnyObject
//            if getValue is Kind(of: NSBool){
//
//            }
            if getFindMedicine?.details?[keyStr] as? Bool  == true{
                 txtView_details.text = "Yes"
            }else if getFindMedicine?.details?[keyStr] as? Bool  == false{
               txtView_details.text = "No"
            }else{
                txtView_details.text = getFindMedicine?.details?[keyStr] as? String ?? ""
            }
           
                
            DispatchQueue.main.async {
                self.collectioView_medicineDetails.reloadData()
            }
           
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
               let width = (collectionView.frame.width - 20) / 3.0
               return CGSize(width: width, height: 40)
           }

           func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
               return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
           }

           func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
               return 5
           }

           func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
               return 5
           }

}
