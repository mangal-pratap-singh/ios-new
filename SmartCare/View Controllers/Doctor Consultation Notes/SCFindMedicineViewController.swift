//
//  SCFindMedicineViewController.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 13/04/20.
//  Copyright © 2020 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit


//MARK: step 1 Add Protocol here.
protocol MedicineFindBrandDelegate: class {
    func sendBackMedicineBrandNameMethod(txt: String,unitId: String)
}
class SCFindMedicineViewController: UIViewController {
    
    weak var medicineFindBrandDelegate: MedicineFindBrandDelegate?
    @IBOutlet weak var txtField_select: UITextField!
    @IBOutlet weak var txtField_brandName: UITextField!
    
    @IBOutlet weak var viewConstHeight_outlete: NSLayoutConstraint!
    var doctorFindDrugs: DoctorFindDrugsClass?
    var doctorFindMedicine: DoctorFindMedicineClass?
    var appointMentIdStr: String? = "0"
    var selectTypeCell: String  = ""
    var selectDrugsBtn: String  = ""
    var checkArr = [String]()
    var selectCheckBtnIndex:Int = -1
    
    @IBOutlet weak var tbleView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tbleView.delegate = self
        tbleView.dataSource = self
        
        viewConstHeight_outlete.constant = 0
       // Brand Medicine Search
        self.customizeNavigationItem(title: "Brand Medicine Search", isDetail: true)
        // Do any additional setup after loading the view.
     }
    
    @IBAction func selectBtnClick(_ sender: UIButton) {
        
        print("get user id====\(UserProfile.getUserId())")
        selectTypeCell = ""
        selectDrugsBtn = "selectBtn"
        viewConstHeight_outlete.constant = 0
           
      let urlString = String(format: SCAppConstants.apiRequestPath.doctorDrugsApi)
        SCConsultationNoteService.getHitAPI(withParameters: urlString, onComplete: { (json, bool) in
            if bool != true{
                self.selectDrugsBtn = ""
                return
              }
            self.checkArr.removeAll()
            self.doctorFindDrugs = DoctorFindDrugsClass(data: json)
            if self.doctorFindDrugs?.findDrugs?.count != 0{
                for data in self.doctorFindDrugs!.findDrugs!{
                    self.checkArr.append("unCheck")
                }
            }
            
            DispatchQueue.main.async {
                self.tbleView.reloadData()
            }
            
        }) { (error) in
            self.selectDrugsBtn = ""
        }
       }
    
    
    
    
    
    
    @IBAction func showDetailsBtnClick(_ sender: UIButton) {
        if selectCheckBtnIndex == -1{
            return
        }
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "SCFindMedicineDetailsViewController")  as! SCFindMedicineDetailsViewController
               //vc.appointMentIdStr = appointMentId
            
        vc.getFindMedicine = self.doctorFindMedicine?.findMedicine?[selectCheckBtnIndex]
         self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    
    @IBAction func addMedicineBtnClick(_ sender: UIButton) {
        if selectCheckBtnIndex == -1{
                   return
             }
        
       // vc.getFindMedicine = self.doctorFindMedicine?.findMedicine?[selectCheckBtnIndex]
        
        var  unitIdStr: String = "0"
        
               let medicineDict = self.doctorFindMedicine?.findMedicine?[selectCheckBtnIndex]
        
               let drugsArr = medicineDict?.drugs as? [FindBrandDrugs]
               if drugsArr?.count != 0{
                   unitIdStr = drugsArr?[0].unitId ?? "0"
                 }
                      //cell.lbl_medicineName.text = medicineDict?.brandName
               medicineFindBrandDelegate?.sendBackMedicineBrandNameMethod(txt: medicineDict?.brandName ?? "",unitId: unitIdStr)
               self.navigationController?.popViewController(animated: false)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension SCFindMedicineViewController : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return pickUpLines.count
        if selectTypeCell != "showCellBrandType"{
        return self.doctorFindDrugs?.findDrugs?.count ?? 0
        }
        return self.doctorFindMedicine?.findMedicine?.count ?? 0
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "AllergyCell", for: indexPath) as! allergyListTableViewCell
        if selectTypeCell != "showCellBrandType"{
        let cell = tableView.dequeueReusableCell(withIdentifier: "SCFindMedicineCell", for: indexPath) as! SCFindMedicineCell
//            else {
//                return UITableViewCell()}
        let index = Int(indexPath.row)
        let drugsDict = self.doctorFindDrugs?.findDrugs?[index]
        cell.lbl_medicineTitle.text = drugsDict?.drugName
        
        return cell
        }else{
            
             let cell = tableView.dequeueReusableCell(withIdentifier: "SCBrandSearchTableCell", for: indexPath) as! SCBrandSearchTableCell
            //            else {
            //                return UITableViewCell()}
                    let index = Int(indexPath.row)
            cell.btn_checckUncheck.tag = indexPath.row
            cell.btn_checckUncheck.addTarget(self, action: #selector(checkBtnClick), for: .touchUpInside)
                    let drugsDict = self.doctorFindMedicine?.findMedicine?[index]
                    cell.lbl_nameTitle.text = drugsDict?.brandName
            let checkCell = checkArr[indexPath.row]
             cell.btn_checckUncheck.isSelected = false
            if checkCell == "Check"{
                cell.btn_checckUncheck.isSelected = true
            }
                    return cell
            }
        
        return UITableViewCell()
}
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
       
        if selectDrugsBtn == "selectBtn"{
           selectTypeCell = "showCellBrandType"
             viewConstHeight_outlete.constant = 40
            let drugsDict = self.doctorFindDrugs?.findDrugs?[indexPath.row]
                   txtField_select.text = drugsDict?.drugName
                   if txtField_brandName.text == nil || txtField_brandName.text == ""{
                       
                       return
                   }
                   let userId = UserProfile.getUserId()
                   
                   let urlString = String(format: SCAppConstants.apiRequestPath.doctorFindMedicineSearch,userId,txtField_brandName.text!,appointMentIdStr!,drugsDict!.id)
                          SCConsultationNoteService.getHitAPI(withParameters: urlString, onComplete: { (json, bool) in
                              if bool != true{
                               
                                  return
                              }
                            self.selectDrugsBtn = ""
                             self.doctorFindMedicine = DoctorFindMedicineClass(data: json)
                              DispatchQueue.main.async {
                                  self.tbleView.reloadData()
                              }
                              
                          }) { (error) in
                              
                          }
        }else{
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "SCFindMedicineDetailsViewController")  as! SCFindMedicineDetailsViewController
                   //vc.appointMentIdStr = appointMentId
                
            vc.getFindMedicine = self.doctorFindMedicine?.findMedicine?[indexPath.row]
             self.navigationController?.pushViewController(vc, animated: true)
            
        }
        }
    
    @objc func checkBtnClick(sender: UIButton){
         selectCheckBtnIndex = -1
        let indexTag = sender.tag
        //let checkStr = checkArr[indexTag]
        
        for i in 0..<checkArr.count{
            if i == indexTag{
                
                selectCheckBtnIndex = sender.tag
                checkArr[indexTag] = "Check"
            }else{
                
                checkArr[i] = "unCheck"
            }
        }
        
        DispatchQueue.main.async {
            self.tbleView.reloadData()
        }
        
    }
    
}
