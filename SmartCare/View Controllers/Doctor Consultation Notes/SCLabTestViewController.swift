//
//  SCLabTestViewController.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 28/03/20.
//  Copyright © 2020 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCLabTestViewController: UIViewController {

    @IBOutlet weak var lbl_labTestTitle: UILabel!
    
    @IBOutlet weak var tbleView: UITableView!
    
    @IBOutlet weak var txtField_labtest: UITextField!
    
    @IBOutlet weak var view_hieghtConst: NSLayoutConstraint!
    var doctorLabTestSearch: DoctorLabTestSearchClass?
    var currentItem:LabTestReaml?
    var labTestSearch:LabtestSearch?
    var appointMentIdStr: String = "0"
    var doctorAppointment: DoctorAppointment?
    var labTestSearchSatatus: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtField_labtest.delegate = self
        self.customizeNavigationItem(title: "Lab Test", isDetail: true)
               self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
        
        tbleView.dataSource = self
        tbleView.delegate = self
        
        lbl_labTestTitle.text = ""
        view_hieghtConst.constant = 50
        tbleView.isHidden = true

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        labTestSearchSatatus = ""
        if DBManager.sharedInstance.getDataLabTestFromDB().count != 0{
            lbl_labTestTitle.text = "Lab Test List"
            view_hieghtConst.constant = 0
             tbleView.isHidden = false
            
            DispatchQueue.main.async {
                self.tbleView.reloadData()
            }
        }
        
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
              textField.resignFirstResponder()
              return true
          }
    @IBAction func txtFieldEditingChanged(_ sender: UITextField) {
        if sender.text != ""{
             labTestSearchSatatus = "Yes"
            getLabTestSearchMethod(searchStr: sender.text!)
          }
      }
    
   
    
  @objc func addTapped(){
            labTestSearchSatatus  = ""
            lbl_labTestTitle.text = ""
            view_hieghtConst.constant = 50
                 
          tbleView.isHidden = true
             self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveTapped))
        }

     @objc func saveTapped(){
            labTestSearchSatatus = ""
            lbl_labTestTitle.text = "Lab Test List"
            view_hieghtConst.constant = 0
            tbleView.isHidden = false
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
            
                 lbl_labTestTitle.text = "Lab Test list"
                 view_hieghtConst.constant = 0
                let item = LabTestReaml()
                                   
                if(currentItem == nil) {
                           //            item.ID = lab.sharedInstance.getDataFromDB().count
                                //item.id = (DBManager.sharedInstance.getDataLabTestFromDB()).count
                    item.id = 1
                        }
                            
        item.appointmentId =  Int(appointMentIdStr)!
        item.type = labTestSearch?.type ?? "0"
        item.id = Int(labTestSearch?.id ?? "0") ?? 0
        
                    item.TestDescription = txtField_labtest.text!
                    DBManager.sharedInstance.addLabTestData(object: item)
        
          DispatchQueue.main.async {
            self.tbleView.reloadData()
            }
            
             self.dismiss(animated: true) { }
        }
    
    
    func getLabTestSearchMethod(searchStr: String){
           
           lbl_labTestTitle.text = ""
           view_hieghtConst.constant = 50
           tbleView.isHidden = false
           
      let userId = UserProfile.getUserId()
                            
        let urlString = String(format: SCAppConstants.apiRequestPath.doctorLabtestSearch,userId,appointMentIdStr,searchStr,String(doctorAppointment?.hospital?.id ?? 0),String(doctorAppointment?.clinic?.id ?? 0))
                                   SCConsultationNoteService.getHitAPI(withParameters: urlString, onComplete: { (json, bool) in
                                       if bool != true{
                                        self.labTestSearchSatatus = ""
                                           return
                                       }
                                    self.labTestSearchSatatus = "Yes"
                                    print("lab tes response=\(json)")
                                    // self.selectDrugsBtn = ""
                                      self.doctorLabTestSearch = DoctorLabTestSearchClass(data: json)
                                       DispatchQueue.main.async {
                                                             self.tbleView.reloadData()
                                                         }
                                       
                                   }) { (error) in
                                    self.labTestSearchSatatus = ""
                                   }
       }
           
    //        let dataRecieved = labTest.init(name: allergyDetailsText.text!, scores: 1 , email: addText.text!)
    //        RealmService.shared.create(dataRecieved)

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SCLabTestViewController : UITableViewDataSource , UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return pickUpLines.count
        if  labTestSearchSatatus != "Yes"{
             return DBManager.sharedInstance.getDataLabTestFromDB().count
        }
        return doctorLabTestSearch?.labtestSearch?.count ?? 0
        }
    
 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if labTestSearchSatatus != "Yes"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "SCLabTestCell", for: indexPath) as! SCLabTestCell
                       let index = Int(indexPath.row)
                       
                       let item = DBManager.sharedInstance.getDataLabTestFromDB()[index] as LabTestReaml
                       
                            cell.lbl_labTestNo.text = "Lab Test \(index + 1)"
                            cell.lbl_labTestName.text = item.TestDescription
          return cell
      }else{
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SCLabTestSearchCell", for: indexPath) as! SCLabTestSearchCell
               let index = Int(indexPath.row)
               
        let item = doctorLabTestSearch?.labtestSearch?[index]
               
                   // cell.lbl_labTestNo.text = "Lab Test \(index + 1)"
                    cell.lbl_labTestName.text = item?.name
           return cell
           }
        
          return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if labTestSearchSatatus != "Yes"{
        return UITableView.automaticDimension
        }else{
            return 44
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete
            else
        {return}
/*
        if( indexPath.row > -1) {
            

            let index = Int(indexPath.row)
           let items = DBManager.sharedInstance.getDataFromDB()[index] as labTest
            if let item = currentItem {
                DBManager.sharedInstance.deleteFromDb(object: item)
               
            }
           */
//            vc.currentItem = item
//            self.present(vc, animated: true, completion: nil)
        //}

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if labTestSearchSatatus == "Yes"{
            labTestSearch = doctorLabTestSearch?.labtestSearch?[indexPath.row]
            txtField_labtest.text = doctorLabTestSearch?.labtestSearch?[indexPath.row].name
         }
    }
    
}
