//
//  SCMedicineSearchOnlyViewController.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 15/04/20.
//  Copyright © 2020 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

//MARK: step 1 Add Protocol here.
protocol MedicineSearchOnlyDelegate: class {
    func sendBackMessageValue(txt: String,unitId: String)
}

class SCMedicineSearchOnlyViewController: UIViewController {
    
    
    @IBOutlet weak var txtField_medicineSearch: UITextField!
    @IBOutlet weak var tbleViw: UITableView!
    weak var medicineSearchOnlyDelegate:MedicineSearchOnlyDelegate?
    var doctorMedicineSearchOnly: DoctorMedicineSearchOnlyClass?
    var appointMentIdStr: String? = "0"
    override func viewDidLoad() {
        super.viewDidLoad()
        txtField_medicineSearch.delegate  = self
        tbleViw.delegate = self
        tbleViw.dataSource = self
        self.customizeNavigationItem(title: "Medicine Search", isDetail: true)

        // Do any additional setup after loading the view.
      }
    
    @IBAction func txtFieldEditingChangeCharater(_ sender: UITextField) {
        
        //print("character ==\(sender.text)")
       
        if sender.text != ""{
             getMedicineSearchMethod(searchStr: sender.text ?? "")
//                   if sender.text!.count == 3{
//                    print("character ==\(sender.text)")
//
//
//               }
               }
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
             textField.resignFirstResponder()
             return true
         }
    
    func getMedicineSearchMethod(searchStr: String){
        
        
        
                        let userId = UserProfile.getUserId()
                         
                         let urlString = String(format: SCAppConstants.apiRequestPath.doctorMedicineSearch,userId,searchStr,appointMentIdStr!)
                                SCConsultationNoteService.getHitAPI(withParameters: urlString, onComplete: { (json, bool) in
                                    if bool != true{
                                     
                                        return
                                    }
                                    
                                    print("json Response of medice=\(json)")
                                 // self.selectDrugsBtn = ""
                                   self.doctorMedicineSearchOnly = DoctorMedicineSearchOnlyClass(data: json)
                                    DispatchQueue.main.async {
                                                          self.tbleViw.reloadData()
                                                      }
                                    
                                }) { (error) in
                                    
                                }
    }
//    @IBAction func txtFieldChangeCharacters(_ sender: UITextField) {
//        medicineSearchOnlyDelegate?.sendBackMessageValue(txt: sender.text ?? "")
//        if sender.text != ""{
//            if sender.text!.count  > 2{
//            
//            DispatchQueue.main.async {
//                self.tbleViw.reloadData()
//            }
//        }
//        }
//    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SCMedicineSearchOnlyViewController : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return pickUpLines.count
       
        return doctorMedicineSearchOnly?.findMedicine?.count ?? 0
       
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "AllergyCell", for: indexPath) as! allergyListTableViewCell
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "SCMedicineSearchOnlyCell", for: indexPath) as! SCMedicineSearchOnlyCell
//            else {
//                return UITableViewCell()}
        let index = Int(indexPath.row)
       let medicineDict = doctorMedicineSearchOnly?.findMedicine?[index]
        cell.lbl_medicineName.text = medicineDict?.brandName
       // cell.lbl_units.text = drugsDict?.unitName
        
        return cell
       
}
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var  unitIdStr: String = "0"
        let medicineDict = self.doctorMedicineSearchOnly?.findMedicine?[indexPath.row]
        let drugsArr = medicineDict?.drugs as? [FindBrandDrugs]
        if drugsArr?.count != 0{
            unitIdStr = drugsArr?[0].unitId ?? "0"
        }
        
               //cell.lbl_medicineName.text = medicineDict?.brandName
        medicineSearchOnlyDelegate?.sendBackMessageValue(txt: medicineDict?.brandName ?? "",unitId: unitIdStr)
        self.navigationController?.popViewController(animated: false)
    }
}
