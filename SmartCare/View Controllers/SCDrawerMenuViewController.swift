//
//  SCDrawerMenuViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 01/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.

import UIKit

class SCDrawerMenuViewController: UIViewController {

    var navigationOptions = [SCAppConstants.pageTitles.dashboard, SCAppConstants.pageTitles.bookAppointment, SCAppConstants.pageTitles.manageDependant, SCAppConstants.pageTitles.ratingHistory, SCAppConstants.pageTitles.insurancePolicy,SCAppConstants.pageTitles.paymentHistoryStatus,SCAppConstants.pageTitles.consultationNotes, SCAppConstants.pageTitles.editProfile, SCAppConstants.pageTitles.logout]
    var navigationOptionsImages = [SCAppConstants.Image.dashboard, SCAppConstants.Image.bookAppointment, SCAppConstants.Image.manageDependant, SCAppConstants.Image.ratingHistory, SCAppConstants.Image.insurancePolicy,SCAppConstants.Image.consultationNotes,SCAppConstants.Image.consultationNotes,  SCAppConstants.Image.editProfile, SCAppConstants.Image.logout]
    
    @IBOutlet weak var navigationOptionTable: UITableView!
    @IBOutlet weak var userFullNameLabel: UILabel!
    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var userProfileImageView: UIImageView!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        registerNibsForCells()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         updateProfileInformation()
    }
    
    
    private func configureView() {
        navigationOptionTable.tableFooterView = UIView()
        navigationOptionTable.delegate = self
        userProfileImageView.layer.cornerRadius = userProfileImageView.frame.width/2
        userProfileImageView.layer.masksToBounds = true
    }
    
    private func updateProfileInformation() {
        self.userFullNameLabel.text =  "\((UserProfile.getUserProfile()?.firstName) ?? "") \((UserProfile.getUserProfile()?.lastName) ?? "")"
        self.userEmailLabel.text = UserProfile.getUserProfile()?.email
        let imageBaseUrl = AppInfo.getAppInfo()?.s3Url ?? ""
        let imageUrl = imageBaseUrl + (UserProfile.getUserProfile()?.profilePhotoURL ?? "")
        if let url = URL(string: imageUrl) {
            userProfileImageView.sd_setImage(with: url, placeholderImage: SCAppConstants.Image.userProfile.image())
        }
    }
    
    private func registerNibsForCells() {
        let nibName = UINib(nibName: SCAppConstants.nibNames.drawerMenuItemTableCell, bundle:nil)
        navigationOptionTable.register(nibName, forCellReuseIdentifier: SCAppConstants.cellReuseIdentifiers.drawerMenuItem)
    }
}

extension SCDrawerMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return navigationOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SCAppConstants.cellReuseIdentifiers.drawerMenuItem, for: indexPath as IndexPath) as! DrawerMenuItemCellTableViewCell
        cell.configureCell(WithMenu: navigationOptions[indexPath.row], imageName: navigationOptionsImages[indexPath.row].imageName())
        cell.backgroundColor = SCRoutingManager.sharedInstance.selectedDrawerItemIndex == indexPath ? UIColor.SCTextFilePlaceHolderTextColor() : UIColor.clear
        
        if indexPath.row == 6 {
            let seperatorView = UIView(frame: CGRect(x: 0, y: 0, width: cell.frame.width, height: 0.5))
            seperatorView.backgroundColor = UIColor.lightGray
            cell.contentView.addSubview(seperatorView)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SCRoutingManager.sharedInstance.selectedDrawerItemIndex = indexPath
        switch indexPath.row {
            case 0: SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .dashboard)
            case 1: SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .bookAppointment)
            case 2: SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .manageDependent)
            case 3: SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .ratingHistory)
            case 4: SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .insurancePolicy)
            case 5: SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .paymentHistory)
            case 6: SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .consultationNotes)
//            case 7: SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .authorisedHistory)
            case 7: SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .profile)
           // case 7: SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .logout)
            case 8:
            let storyboard = UIStoryboard(name: SCAppConstants.storyBoardName.login, bundle: nil)
            let menuOptionVC = storyboard.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.logoutNavigation) as! UINavigationController
            self.present(menuOptionVC, animated: true, completion: nil)
        default:
            break
        }
        tableView.reloadData()
    }
}
