//
//  SCAuthorisedHistoryViewController.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 29/11/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCAuthorisedHistoryViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var fullScreenMessageView:FullScreenMessageView?
    @IBOutlet weak var tbleView_authorise: UITableView!
    var authorisedMedicineData: AuthorisedMedicineModel?
    
    var appointMentId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tbleView_authorise.delegate = self
        self.tbleView_authorise.dataSource = self
        customizeNavigationItem(title: SCAppConstants.pageTitles.authoriseHistory, isDetail: true)
        DispatchQueue.main.async {
            self.tbleView_authorise.reloadData()
        }


        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       self.tbleView_authorise.isHidden = true
        self.fullScreenMessageView?.removeFromSuperview()
               self.fullScreenMessageView = self.showFullScreenMessageOnView(parentView: self.tbleView_authorise, withMessage: "No Authorised List")
        fullScreenMessageView?.center = self.view.center
    
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getAuthoriseHistoryMethod()
    }
    
    private func getAuthoriseHistoryMethod(){
         
                 SCProgressView.show()
        
             print("get Authorise History user id====\(UserProfile.getUserId())")
        SCAuthoriseHistoryService.getAuthoriseHistoryAPI(withParameters:appointMentId!, onComplete: { (response, result) in
                   SCProgressView.hide()
                  self.fullScreenMessageView?.removeFromSuperview()
                  print("patien Authorise History list==\(response)")
                self.authorisedMedicineData = AuthorisedMedicineModel(json: response)
                  DispatchQueue.main.async {
                        self.tbleView_authorise.isHidden = false
                        self.tbleView_authorise.reloadData()
                      
                  }
                  
              }) { (errorMessage) in
                   SCProgressView.hide()
                   self.view.addSubview(self.fullScreenMessageView!)
                  
              }
          }
    
    private func authoriseCancelWebserviceMethod(pharmacyId: String){
     
             SCProgressView.show()
    
         print("get Authorise History user id====\(UserProfile.getUserId())")
       // let pharmacyId = authorisedMedicineData?.a.authorisedPharmacy.id
        SCAuthoriseHistoryService.postAuthoriseCancelAPI(withParameters: appointMentId!,pharmacyId: pharmacyId, onComplete: { (response, result) in
               SCProgressView.hide()
             
              print("authorisePharmacy Cancel==\(response)")
               self.getAuthoriseHistoryMethod()
              
          }) { (errorMessage) in
               SCProgressView.hide()
          }
      }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SCAuthorisedHistoryViewController{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return authorisedMedicineData?.authorisedMedicine?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tbleView_authorise.dequeueReusableCell(withIdentifier: "AuthoriseHistoryCell", for: indexPath) as! AuthoriseHistoryCell
        
        cell.btn_deny.tag = indexPath.row
        cell.btn_deny.addTarget(self, action: #selector(denyBtnClick), for: .touchUpInside)
        let authorisedMedicine = authorisedMedicineData?.authorisedMedicine?[indexPath.row]
        cell.lbl_pharmacyName.text = authorisedMedicine?.authorisedPharmacy?.name
        cell.lbl_authorisedOn.text = Date().toStringDate(dateString: (authorisedMedicine?.createdAt ?? ""), formatter: SCAppConstants.DateFormatType.slotAPI, outPutFormatter: SCAppConstants.DateFormatType.date)
        cell.lbl_boughtAt.text = Date().toStringDate(dateString: (authorisedMedicine?.cancelAt ?? ""), formatter: SCAppConstants.DateFormatType.slotAPI, outPutFormatter: SCAppConstants.DateFormatType.date)
        if authorisedMedicine?.cancelAt == ""{
            cell.lbl_status.text = ""
            cell.btn_deny.isHidden = false
        }else{
            
            cell.lbl_status.text = "Cancelled"
            cell.btn_deny.isHidden = true
            
        }
        
//        cell.paymentDetailsDelegate = self
//        cell.btn_reciept.tag = indexPath.row
//        cell.btn_viewDetails.tag = indexPath.row
//        let paymentDict = paymentHistoryData?.paymentHistory?[indexPath.row]
//        cell.lbl_patientName.text = paymentDict?.paymentPatient?.name
//        cell.lbl_consultingFees.text = paymentDict?.paymentDetailsStatus?.amount
//        cell.lbl_doctorDetails.text = paymentDict?.paymentDoctor?.degree
//        cell.lbl_clinicName.text = (paymentDict?.paymentHospital?.name ?? "") + " / " + (paymentDict?.paymentClinic?.name ?? "")
        
        return cell
    }
    
    @objc func denyBtnClick(sender: UIButton){
        
        let authorisedMedicine = authorisedMedicineData?.authorisedMedicine?[sender.tag]
        let pharmacyId = authorisedMedicine?.authorisedPharmacy?.id ?? "0"
        authoriseCancelWebserviceMethod(pharmacyId: pharmacyId)
    }
    
}
