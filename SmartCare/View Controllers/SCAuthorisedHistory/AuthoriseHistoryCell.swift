//
//  AuthoriseHistoryCell.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 29/11/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class AuthoriseHistoryCell: UITableViewCell {
    

    @IBOutlet weak var lbl_pharmacyName: UILabel!
    
    @IBOutlet weak var lbl_authorisedOn: UILabel!
    
    @IBOutlet weak var lbl_boughtAt: UILabel!
    
    @IBOutlet weak var btn_deny: UIButton!
    
    @IBOutlet weak var lbl_status: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
