//
//  ChatCell.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 08/02/20.
//  Copyright © 2020 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class ChatCell: UITableViewCell {
    
    @IBOutlet weak var imgProfile_send: UIImageView!
    @IBOutlet weak var sendProfileWidthConst: NSLayoutConstraint!
    
    @IBOutlet weak var sendNotchWidthConst: NSLayoutConstraint!
    
    @IBOutlet weak var imgProfile_reciever: UIImageView!
    
    @IBOutlet weak var recieveProfileWdthConst: NSLayoutConstraint!
    
    @IBOutlet weak var recieveNotchWidthConst: NSLayoutConstraint!
    
    
    @IBOutlet weak var view_message: UIView!
    
    @IBOutlet weak var lbl_chatMessage: UILabel!
    
    
    
    @IBOutlet weak var lbl_senderName: UILabel!
    
    @IBOutlet weak var lbl_recieverName: UILabel!
    
    @IBOutlet weak var lbl_dateTime: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = UIColor.clear
        imgProfile_send.layer.cornerRadius = imgProfile_send.frame.width/2
        imgProfile_send.layer.masksToBounds = true
        imgProfile_reciever.layer.cornerRadius = imgProfile_reciever.frame.width/2
        imgProfile_reciever.layer.masksToBounds = true
        view_message.layer.cornerRadius = 5
        view_message.layer.masksToBounds = true
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
