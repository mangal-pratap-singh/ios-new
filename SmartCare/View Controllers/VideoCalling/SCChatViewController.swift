//
//  SCChatViewController.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 07/02/20.
//  Copyright © 2020 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCChatViewController: UIViewController,VCConnectorIRegisterMessageEventListener,VCConnectorIConnect,UITableViewDelegate,UITableViewDataSource {
    
    
   
    private enum Constants {
       static let incomingMessageCell = "incomingMessageCell"
       static let outgoingMessageCell = "outgoingMessageCell"
       static let contentInset: CGFloat = 24
       static let placeholderMessage = "Type something"
     }
    
    
    
    @IBOutlet weak var lbl_headerTitle: UILabel!
    
    @IBOutlet weak var btn_back: UIButton!
    @IBOutlet weak var tbleView: UITableView!
    @IBOutlet weak var txtView: UITextView!
    
    @IBOutlet weak var txtAreaBottom: NSLayoutConstraint!
    
    @IBOutlet weak var txtViewSuperBottomConst: NSLayoutConstraint!
    
    var chatAndVideoStr: String? = ""
     var videoCallModel: VideoTokenModel? = nil
     var connector1: VCConnector?
    private let HOST = "prod.vidyo.io"
    private let HOST1 = "Vidyo.io service"
    var displayName: String? = ""//"Demo User"
    var resourceID:String?// "demoRoom"
    var VIDYO_TOKEN: String?
    var chatArr:[AnyObject]?
    var senderChatName: String? = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
         chatArr = [AnyObject]()
        //doctorName = videoCallModel?.doctor?.name
        lbl_headerTitle.text = displayName
        // Do any additional setup after loading the view.
       
        setUpTableView()
        addTextViewPlaceholer()
         
        connector1 = VCConnector(nil,
                                                     viewStyle: .default,
                                                     remoteParticipants: 15,
                                                     logFileFilter: UnsafePointer(""),
                                                     logFileName: UnsafePointer(""),
                               
                                                     userData: 0)
         connector1?.registerMessageEventListener(self)
        let notificationCenter = NotificationCenter.default
              notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        
              notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
     }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //addTextViewPlaceholer()
       // VCConnectorPkg.vcInitialize()
        
              
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         self.navigationController?.navigationBar.isHidden = true
       // addTextViewPlaceholer()
        connector1?.connect(HOST,
        token: VIDYO_TOKEN,
        displayName: displayName,
        resourceId: resourceID,
        connectorIConnect: self)
      
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        /*
         connector?.disconnect()
         connector?.reportLocalParticipant(onJoined: false)
         connector?.setPool("")
         connector?.unregisterMessageEventListener()
         connector?.disable()
         connector  = nil
 */
//         VCConnectorPkg.uninitialize()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
         self.navigationController?.navigationBar.isHidden = true
    
        if isInCallingState() {
            connector1?.disconnect()
                             
          } else {
            if chatAndVideoStr == "fromChat"{
               // connector1?.unregisterMessageEventListener()
                // closeConference()
                connector1?.disconnect()
                connector1?.disable()
                
            }else{
               closeConference()
           }
        }
    
        /*
          connector1?.unregisterMessageEventListener()
          connector1?.disconnect()
          connector1?.disable()
          connector1  = nil
         NotificationCenter.default.removeObserver(self)
        */
        
    }
    func onChatMessageReceived(_ participant: VCParticipant!, chatMessage: VCChatMessage!) {
        
        print("chat send==\(chatMessage.body ?? "")")
        let dateTimeTmp = Date().currentMontAndTime
        let dName = displayName?.getFistCharactorFromString()
        var recDict = [String: AnyObject]()
        recDict["message"] = chatMessage?.body
        recDict["inputOut"] = "recieve" as AnyObject
        recDict["name"] = dName as AnyObject?
        recDict["name"] = dName as AnyObject?
        
        recDict["dateTime"] = dateTimeTmp as AnyObject?
        
        chatArr?.append(recDict as AnyObject)
       // ChatService.shared.send(message: message)
        DispatchQueue.main.async {
            self.tbleView.reloadData()
            self.scrollToLastCell()
          }
        
        
     }
    
    
    
    @IBAction func sendMessageBtnClick(_ sender: UIButton) {
    
        //Int8(message.utf8CString
        let message: String = txtView.text
              guard !message.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
                return
              }
        
        let pName = senderChatName?.getFistCharactorFromString()
        let dateTimeTmp = Date().currentMontAndTime
        connector1?.sendChatMessage(message)
            var recDict = [String: AnyObject]()
        recDict["message"] = message as AnyObject
        recDict["inputOut"] = "send" as AnyObject
        recDict["name"] = pName as AnyObject?
         recDict["dateTime"] = dateTimeTmp as AnyObject?
        chatArr?.append(recDict as AnyObject)
          DispatchQueue.main.async {
            self.tbleView.reloadData()
            self.scrollToLastCell()
            self.txtView.endEditing(true)
            self.addTextViewPlaceholer()
            
               }
             
        
    }
    
  //MARK: VIConnector delegate method
    func onSuccess() {
           
       }
       
       func onFailure(_ reason: VCConnectorFailReason) {
           
       }
       
       func onDisconnected(_ reason: VCConnectorDisconnectReason) {
       
       
           
       }
    
    
    @IBAction func VideoCallinBtnClick(_ sender: UIButton) {
       // connector?.setMode(.default)
        /*
      NotificationCenter.default.removeObserver(self)
      self.connector1?.unregisterMessageEventListener()
        self.connector1?.disconnect()
        self.connector1?.disable()
        self.connector1  = nil
         sleep(2)
      */
        //MARK: BELLOW CODE IS WORKING....................
       /* if chatAndVideoStr == "fromVideo"{
             self.connector1?.unregisterMessageEventListener()
             self.connector1?.disconnect()
             self.connector1?.disable()
            self.connector1  = nil
            
            
             self.navigationController?.popViewController(animated: true)
        }else{
             self.connector1?.disconnect()
            
            let storyBoard = UIStoryboard(name: "Home", bundle: nil)
            
                 let vc = storyBoard.instantiateViewController(withIdentifier: "SCVideoCompositViewController")  as! SCVideoCompositViewController
                     vc.fromVideo = "fromChatVideo"
            
                      vc.displayName = displayName ?? ""

                      vc.resourceID = resourceID ?? ""
            
                      vc.VIDYO_TOKEN = VIDYO_TOKEN ?? ""
            
                self.navigationController?.pushViewController(vc, animated: true)
            
            
        }
       
            
    
        */
       
         //VCConnectorPkg.uninitialize()
        
       
        
        
    }
    
    // MARK: Private functions
    
    private func isInCallingState() -> Bool {
        if let connector = connector1 {
            let state = connector.getState()
            return state != .idle && state != .ready
        }
        
        return false
    }
    
    private func closeConference() {
        DispatchQueue.main.async {
            [weak self] in
            
            guard let this = self else {
                fatalError("Can't maintain self reference.")
            }
            
            this.dismiss(animated: true, completion: nil)
        }
    }
    
    
    //MARK:- Keyboard resturn
    // MARK: - Keyboard
    
    
    @objc func adjustForKeyboard(notification: Notification) {
     
     guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }

     let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let safeAreaBottom = view.safeAreaLayoutGuide.layoutFrame.maxY
             let viewHeight = view.bounds.height
             let safeAreaOffset = viewHeight - safeAreaBottom
    
        // let lastVisibleCell = tbleView.indexPathsForVisibleRows?.last
     if notification.name == UIResponder.keyboardWillHideNotification {
         txtViewSuperBottomConst.constant = 0
        DispatchQueue.main.async {
             self.scrollToLastCell()
           }
        
        } else {
       
         UIView.animate(
           withDuration: 0.3,
           delay: 0,
           options: [.curveEaseInOut],
           animations: {
             self.txtViewSuperBottomConst.constant = -keyboardScreenEndFrame.height + safeAreaOffset
            DispatchQueue.main.async {
                        self.scrollToLastCell()
                      }
         })
         
          }
     
    }
      
      func setUpTableView() {
         self.tbleView.dataSource = self
         self.tbleView.delegate = self
         self.tbleView.estimatedRowHeight = 70
       // tbleView.tableFooterView = UIView()
         self.tbleView.separatorStyle = .none
       // tbleView.contentInset = UIEdgeInsets(top: Constants.contentInset, left: 0, bottom: 0, right: 0)
       
      }
    
    //MARK: Data source and delegate method of TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatArr?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       var cell:ChatCell? = tbleView.dequeueReusableCell(withIdentifier:"ChatCell") as? ChatCell
        
          if cell == nil{
            tbleView.register(UINib.init(nibName: "ChatCell", bundle: nil), forCellReuseIdentifier: "ChatCell")
            let arrNib:Array = Bundle.main.loadNibNamed("ChatCell",owner: self, options: nil)!
            cell = arrNib.first as? ChatCell
        }
        
        cell?.lbl_chatMessage.layer.cornerRadius = 5
        cell?.lbl_chatMessage.clipsToBounds = true
        let dict = chatArr?[indexPath.row] as! [String: AnyObject]
        let statust = dict["inputOut"] as? String
        
        cell?.recieveNotchWidthConst.constant = 0
        cell?.sendNotchWidthConst.constant = 0
        cell?.imgProfile_send.isHidden = true
        cell?.imgProfile_reciever.isHidden = true
        cell?.lbl_senderName.text = ""
         cell?.lbl_recieverName.text = ""
        cell?.lbl_dateTime.textColor = UIColor.white
       // dateTime
         cell?.lbl_dateTime.text = dict["dateTime"] as? String
        if statust == "send"{
            cell?.lbl_senderName.text = dict["name"] as? String
            cell?.sendNotchWidthConst.constant = 25
            cell?.imgProfile_send.isHidden = false
            cell?.view_message.backgroundColor = UIColor.SCChatCellColor()
            cell?.lbl_chatMessage.textColor = UIColor.white
            cell?.lbl_chatMessage.text = dict["message"] as? String
            cell?.lbl_dateTime.textAlignment = .right
        }else{
            cell?.lbl_recieverName.text = dict["name"] as? String
            cell?.recieveNotchWidthConst.constant = 25
            cell?.imgProfile_reciever.isHidden = false
            cell?.view_message.backgroundColor = UIColor.white
            
            cell?.lbl_chatMessage.textColor = UIColor.SCChatCellColor()
            cell?.lbl_dateTime.textColor = UIColor.SCChatCellColor()
            cell?.lbl_chatMessage.text = dict["message"] as? String
            cell?.lbl_dateTime.textAlignment = .left
            
        }
        return cell!
      }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
    private func scrollToLastCell() {
      let lastRow = tbleView.numberOfRows(inSection: 0) - 1
      guard lastRow > 0 else {
        return
      }
      
      let lastIndexPath = IndexPath(row: lastRow, section: 0)
      tbleView.scrollToRow(at: lastIndexPath, at: .bottom, animated: true)
    }
    
    @IBAction func backBtnClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        // VCConnectorPkg.uninitialize()
            //self.connector?.setTCPTransport(false)
           
              // sleep(5)
        /*
        DispatchQueue.main.async {
                    //self.connector1?.unregisterMessageEventListener()
                       //self.connector1?.disableDebug()
                       //self.connector1?.disconnect()
                       //self.connector1?.disable()
                      // self.connector1 = nil
             self.navigationController?.popViewController(animated: true)
        }
       */
            
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK: - UITextViewDelegate
extension SCChatViewController: UITextViewDelegate {
  private func addTextViewPlaceholer() {
    txtView.text = Constants.placeholderMessage
    txtView.textColor = UIColor.lightGray
  }
  
  private func removeTextViewPlaceholder() {
    txtView.text = ""
    txtView.textColor = UIColor.darkGray
  }
  
  func textViewDidBeginEditing(_ textView: UITextView) {
    removeTextViewPlaceholder()
  }
  
  func textViewDidEndEditing(_ textView: UITextView) {
    if textView.text.isEmpty {
      addTextViewPlaceholer()
    }
  }
}

