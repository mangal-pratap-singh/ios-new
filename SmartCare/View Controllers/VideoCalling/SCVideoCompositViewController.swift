//
//  SCVideoCompositViewController.swift
//  SmartCare
//
//  Created by Rediflex PvtLtd on 19/07/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCVideoCompositViewController: UIViewController , VCConnectorIConnect{

    // MARK: - Properties and variables
    
    @IBOutlet var vidyoView: UIView!
    
    @IBOutlet weak var view_controllsBtn: UIView!
    
    @IBOutlet weak var micButton: UIButton!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    
    @IBOutlet weak var btn_fontRear: UIButton!
    
    private var connector: VCConnector?
    
    private let HOST = "prod.vidyo.io"
     private let HOST1 = "Vidyo.io service"
    /* Get a valid token. It is recommended that you create short lived tokens on your applications server and then pass it down here.
     * For details on how to get a token check out - https://static.vidyo.io/latest/docs/VidyoConnectorDeveloperGuide.html#tokens */
   // private let VIDYO_TOKEN = "cHJvdmlzaW9uADE5OTQwNDAyQTAwMDFAODFmZDEyLnZpZHlvLmlvADYzNzIzMzIxMDAxAAA3MTUwZjA1NGZjYzdkZjU0YjI2MTllZjJkMTM5YmE0YzI0MDQwNDRiYWQxOTI1ODI3YTk5NGM1ZGRlNzczYjkwZjQ1MzZlYTM4YmY5MTA4YWE3YWZiYjhlZDQzOWNkNDY="
    
    var displayName: String?//"Demo User"
    var resourceID:String?// "demoRoom"
    var VIDYO_TOKEN: String?
    private let VIDYO_TOKEN1 = "cHJvdmlzaW9uAHVzZXIxQGY1YTM1Ny52aWR5by5pbwA2Mzc0Mjk1MTE2OAAANGI3ZDM0YmI2ODE2NDFkZDEwZGI3ZjQ1Mzk1MWU4OGQyZjFjOTk1NDI4MjY5OTUyNDViMjNhOTUxMmI0NzY4NzI3NTQyNjQ1YTQzNGNiYjlkNjBmOTJlMWRkOThmZTlk"
    var fromVideo: String? = ""
    let  displayName1 = "user1"
    var resourceID1 = "demoRoom"
    
   private var micMuted    = false
   private var cameraMuted = false
    var cameraFontRear = false
   
   private var hasDevicesSelected = false
    var disconnectStatusStr: String? = ""
    var chatBtnClickStatus: String? = ""
    var videoCallModel: VideoTokenModel? = nil
    

    @IBOutlet weak var lbl_headerTitle: UILabel!
    required init?(coder aDecoder: NSCoder) {
       super.init(coder :aDecoder)
   }
    
    // MARK: - ViewController lifecycle events
   // info@VidyoClient info@VidyoConnector warning
    override func viewDidLoad() {
        super.viewDidLoad()
        print("tocken print==\(VIDYO_TOKEN  ?? "")")
        print("displayName print==\(displayName ?? "")")
        print("resourcId print==\(resourceID ?? "")")
        self.navigationController?.navigationBar.isHidden =  true
        lbl_headerTitle.text = videoCallModel?.doctor?.name
       
       VCConnectorPkg.vcInitialize()
        
       //  VCConnectorPkg.vcInitialize()
        connector = VCConnector(UnsafeMutableRawPointer(&vidyoView),
                                              viewStyle: .default,
                                              remoteParticipants: 15,
                                              logFileFilter: UnsafePointer(""),
                                              logFileName: UnsafePointer(""),
                        
                                              userData: 0)
                      
                      /*
                       
                      connector?.showView(at: &vidyoView,
                      x: 0,
                      y: 0,
                      width: UInt32(vidyoView.frame.size.width),
                      height: UInt32(vidyoView.frame.size.height))
                      */
                      // Orientation change observer
                      NotificationCenter.default.addObserver(self, selector: #selector(onOrientationChanged),
                                                             name: UIDevice.orientationDidChangeNotification, object: nil)
                      
                      // Foreground mode observer
                      NotificationCenter.default.addObserver(self, selector: #selector(onForeground),
                                                             name: UIApplication.didBecomeActiveNotification, object: nil)
                      
                      // Background mode observer
                      NotificationCenter.default.addObserver(self, selector: #selector(onBackground),
                                                             name: UIApplication.willResignActiveNotification, object: nil)
        
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // connector?.showWindowSharePreview(false)
        self.view.bringSubviewToFront(view_controllsBtn)
        
       
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        chatBtnClickStatus = "NotChat"
         self.refreshUI()
        connector?.connect(HOST,
                           token: VIDYO_TOKEN,
                           displayName: displayName,
                           resourceId: resourceID,
                           connectorIConnect: self)
       // connector?.setCameraPrivacy(cameraMuted)
           DispatchQueue.main.async {
            self.callButton.isEnabled = false
           }
        
    
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        if isInCallingState() {
            connector?.disconnect()
            connector?.disable()
            connector?.disableDebug()
           // disconnectStatusStr = "disconnect"
        }
        /*
        if isInCallingState() {
            connector?.disconnect()
            disconnectStatusStr = "disconnect"
        } else {
            // disconnectStatusStr = "ActiveCall"
            closeConference()
        }

       */
       
        /*
        self.connector?.select(nil as VCLocalCamera?)
                   self.connector?.select(nil as VCLocalMicrophone?)
                   self.connector?.select(nil as VCLocalSpeaker?)
                   self.connector?.hideView(UnsafeMutableRawPointer(&self.vidyoView))
                   self.connector?.disable()
                   
                   self.connector = nil
                   
                   NotificationCenter.default.removeObserver(self)
        */
     }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //  VCConnectorPkg.uninitialize()
         /*
                if isInCallingState() {
                    connector?.disconnect()
                    disconnectStatusStr = "disconnect"
                } else {
                     disconnectStatusStr = "ActiveCall"
                   // closeConference()
                }
               */
                           self.connector?.select(nil as VCLocalCamera?)
                           self.connector?.select(nil as VCLocalMicrophone?)
                           self.connector?.select(nil as VCLocalSpeaker?)
                           self.connector?.hideView(UnsafeMutableRawPointer(&self.vidyoView))
                           self.connector?.disable()
                           
                           self.connector = nil
                            VCConnectorPkg.uninitialize()
                        

        
        /*
        if chatBtnClickStatus != "ChatClick"{
        self.connector?.select(nil as VCLocalCamera?)
        self.connector?.select(nil as VCLocalMicrophone?)
        self.connector?.select(nil as VCLocalSpeaker?)
        self.connector?.hideView(UnsafeMutableRawPointer(&self.vidyoView))
        self.connector?.disable()
                    
        self.connector = nil

        NotificationCenter.default.removeObserver(self)
        }else{
            
            if isInCallingState() {
                       connector?.disconnect()
                      
                   } else {
                       
                       closeConference()
                   }
        }
      */
        
    }
    
    // MARK: - NotificationCenter observers: UI application lifecycle events
    
    @objc func onForeground() {
       guard let connector = connector else {
           return
         }
       
       connector.setMode(.foreground)
       
        if !hasDevicesSelected {
            hasDevicesSelected = true

           connector.selectDefaultCamera()
           connector.selectDefaultMicrophone()
           connector.selectDefaultSpeaker()
          }
       
         connector.setCameraPrivacy(cameraMuted)
      }
    
    @objc func onBackground() {
        guard let connector = connector else {
                   return
               }
               
               if isInCallingState() {
                   connector.setCameraPrivacy(true)
               } else {
                   hasDevicesSelected = false

                   connector.select(nil as VCLocalCamera?)
                   connector.select(nil as VCLocalMicrophone?)
                   connector.select(nil as VCLocalSpeaker?)
                  }
               
               connector.setMode(.background)
        }
    
    @objc func onOrientationChanged() {
        self.refreshUI();
    }
    
    // MARK: - IConnect delegate methods
    
    func onSuccess() {
        print("Connection Successful.")
         disconnectStatusStr = "ActiveCall"
     // connector?.showWindowSharePreview(false)
        // After successfully connecting, enable the button which allows user to disconnect.
        DispatchQueue.main.async {
            self.callButton.isEnabled = true;
           // callEnd
            self.callButton.setImage(UIImage(named: "callEnd"), for: .normal)
        }
    }
    
    func onFailure(_ reason: VCConnectorFailReason) {
       print("Connection failed \(reason)")
              
              closeConference()
    }
    
    func onDisconnected(_ reason: VCConnectorDisconnectReason) {
        print("Call Disconnected")
        
         closeConference()
    }
    
    
   // MARK: - UI Actions
    
    @IBAction func chatMessageBtnClick(_ sender: UIButton) {
        /*
          chatBtnClickStatus = "ChatClick"
          connector?.disconnect()
          //connector?.disable()
           sleep(2)
        if fromVideo == "fromVideo"{
            
            
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
               let vc = storyBoard.instantiateViewController(withIdentifier: "SCChatViewController")  as! SCChatViewController
                    // vc.connector = connector
                      vc.chatAndVideoStr = fromVideo
                      vc.displayName = videoCallModel?.patient?.name
               
                       vc.resourceID = videoCallModel?.resourceId ?? ""
                       vc.VIDYO_TOKEN = videoCallModel?.joinToken ?? ""
                       vc.videoCallModel = videoCallModel
                     self.navigationController?.pushViewController(vc, animated: true)
        }else{
            
          self.navigationController?.popViewController(animated: true)
        }
       */
    
     }
    
    
    @IBAction func backBtnClick(_ sender: UIButton) {
       
        if disconnectStatusStr == "ActiveCall"{
            showAlert(title:"Alert", msg:SCAppConstants.alertMessages.callDicConnectMessage, style: .alert)
              return
        }
        sleep(2)
        NotificationCenter.default.removeObserver(self)
         connector?.disconnect()
         connector?.disable()
     //   DispatchQueue.main.async {
           // self.connector?.select(nil as VCLocalCamera?)
                    //self.connector?.select(nil as VCLocalMicrophone?)
                   //self.connector?.select(nil as VCLocalSpeaker?)
                //self.connector?.hideView(UnsafeMutableRawPointer(&self.vidyoView))

                 //  self.connector = nil
                  //  VCConnectorPkg.uninitialize()
         //  }
        
       
        
    self.navigationController?.popViewController(animated: true)
  
      
        
    }
    
    @IBAction func fontRearCameraClick(_ sender: UIButton) {
       // cameraFontRear = !cameraFontRear
        if cameraFontRear == false{
        self.btn_fontRear.setImage(UIImage(named: "cameraSwitch.png"), for: .normal)
            cameraFontRear = true
        }else{
             self.btn_fontRear.setImage(UIImage(named: "cameraSwitch.png"), for: .normal)
            cameraFontRear = false
         
        }
       // connector?.select(cameraFontRear)
           connector?.cycleCamera()
    }
    
    
    @IBAction func cameraClicked(_ sender: Any) {
        cameraMuted = !cameraMuted
        self.cameraButton.setImage(UIImage(named: cameraMuted ? "cameraOff.png" : "cameraOn.png"), for: .normal)
        connector?.setCameraPrivacy(cameraMuted)
    }
    
    @IBAction func micClicked(_ sender: Any) {
        micMuted = !micMuted
        self.micButton.setImage(UIImage(named: micMuted ? "microphoneOff.png" : "microphoneOn.png"), for: .normal)
        connector?.setMicrophonePrivacy(micMuted)
    }
    
    @IBAction func callClicked(_ sender: Any) {
        if isInCallingState() {
            connector?.disconnect()
            disconnectStatusStr = "disconnect"
        } else {
            closeConference()
        }
    }
    
    // MARK: - Refresh renderer
    
    private func refreshUI() {
        DispatchQueue.main.async {
            [weak self] in
            
            guard let this = self else {
                fatalError("Can't maintain self reference.")
            }
            
            this.connector?.showView(at: UnsafeMutableRawPointer(&this.vidyoView),
                                     x: 0,
                                     y: 0,
                                     width: UInt32(this.vidyoView.frame.size.width),
                                     height: UInt32(this.vidyoView.frame.size.height))
        }
    }
    
    // MARK: Private functions
    
    private func isInCallingState() -> Bool {
        if let connector = connector {
            let state = connector.getState()
            return state != .idle && state != .ready
         }
        
        return false
    }
    
    private func closeConference() {
        DispatchQueue.main.async {
            [weak self] in
            
            guard let this = self else {
                fatalError("Can't maintain self reference.")
            }
            
            this.dismiss(animated: true, completion: nil)
        }
    }
}
