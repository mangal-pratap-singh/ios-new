//
//  SCVideoCallingHomeViewController.swift
//  SmartCare
//
//  Created by Rediflex PvtLtd on 19/07/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCVideoCallingHomeViewController: UIViewController {
    
    var videoCallModel: VideoTokenModel? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        //  VCConnectorPkg.vcInitialize()
//         let email = UserDefaults.standard.object(forKey: (model.data?.joinToken!))!
    }
    @IBAction func compositeTapped(_ sender: UIButton) {
        
        //performSegue(withIdentifier: "compositedSegue", sender: nil)
        /*
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "SCVideoCompositViewController")  as! SCVideoCompositViewController
               vc.displayName = videoCallModel?.patient?.name
        
                vc.resourceID = videoCallModel?.resourceId ?? ""
                 vc.VIDYO_TOKEN = videoCallModel?.joinToken ?? ""
              self.navigationController?.pushViewController(vc, animated: true)
        */

        
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "SCChatViewController")  as! SCChatViewController
                vc.displayName = videoCallModel?.patient?.name
                vc.resourceID = videoCallModel?.resourceId ?? ""
                 vc.VIDYO_TOKEN = videoCallModel?.joinToken ?? ""
              self.navigationController?.pushViewController(vc, animated: true)
                   

    }
    
    @IBAction func customTapped(_ sender: UIButton) {
       // performSegue(withIdentifier: "customSegue", sender: nil)
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "SCVideoCustomViewController")  as! SCVideoCustomViewController
               vc.displayName = "saba"
        
                vc.resourceID = videoCallModel?.resourceId ?? ""
                 vc.VIDYO_TOKEN = videoCallModel?.joinToken ?? ""
              self.navigationController?.pushViewController(vc, animated: true)

      }
    
//    compositedSegue
//    customSegue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("resourceId==\(videoCallModel?.resourceId ?? "")")
        print("joinTockenI==\(videoCallModel?.joinToken ?? "")")
        
        if segue.identifier == "compositedSegue"{
            let vc = segue.destination as! SCVideoCompositViewController
            
            vc.displayName = "saba"
            vc.resourceID = videoCallModel?.resourceId ?? ""
            vc.VIDYO_TOKEN = videoCallModel?.joinToken ?? ""
            //vc.resourceID = "25_19940402A0001_19980402A0001"
            
        } else if segue.identifier == "customSegue"{
            let vc = segue.destination as! SCVideoCustomViewController
//            vc.displayName = "sabbu"
//            vc.resourceID = "25_19940402A0001_19980402A0001"
            vc.displayName = "saba"
            vc.resourceID = videoCallModel?.resourceId ?? ""
            vc.VIDYO_TOKEN = videoCallModel?.joinToken ?? ""
        }
    }
}
