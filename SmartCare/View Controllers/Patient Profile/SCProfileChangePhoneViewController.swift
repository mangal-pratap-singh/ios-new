//
//  SCProfileChangePhoneViewController.swift
//  SmartCare
//
//  Created by synerzip on 21/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SwiftyJSON

class SCProfileChangePhoneViewController: UIViewController {

    private let coungtryCodePickerTitle = "Select Country Code"
    
    @IBOutlet weak var countryCodeTextField: UITextField!
    @IBOutlet weak var containerViewImage: UIView!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var otpTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    
    var countryCodeArray = [String]()
    var countryCodeIDArray = [String]()
    var selectedIdCountryIndex = 0
    var phone:String?
    var countryCodePickerDataSource = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.updatePhoneNumber, isDetail: true)
        configureView()
        getCountryCode()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Disable Drawer for Detailed Pages
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = true
    }
    
    func configureView() {
        self.countryCodeTextField.delegate = self
        otpTextField.isHidden = true
        submitButton.isHidden = true
        phoneTextField.text = phone
        countryCodeTextField.configureTextFieldLeftView(width: SCAppConstants.textFieldPadding.textFieldPaddingLeftSmall.rawValue, borderColor: UIColor.white, borderWidth: SCAppConstants.borderWidths.zeroBorderWidth.rawValue, cornerRadius:  SCAppConstants.cornerRadiusValues.zeroCornerRadus.rawValue)
            countryCodeTextField.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
    }
    
    private func updateContact() {
        SCProgressView.show(withMessage: SCAppConstants.progressMessages.changingPhoneNumber)
        let parameters = ["userId": UserProfile.getUserId(),
                          "phoneNo": phoneTextField.text ?? "",
                          "countryCode": countryCodeIDArray[selectedIdCountryIndex]
        ]
        SCProfileService.contactChangeAPI(withParameters: parameters , onComplete: { [weak self] in
            SCProgressView.hide()
            self?.otpTextField.isHidden = false
            self?.submitButton.isHidden = false
        }) { (errorMessage) in
            SCProgressView.hide()
            self.showAlert(title: SCAppConstants.pageTitles.updatePhoneNumber, msg: errorMessage)
        }
    }
    
    private func doPhoneVerification() {
        SCProgressView.show(withMessage: SCAppConstants.progressMessages.verifyingPhone)
        let parameters = ["userId":UserProfile.getUserId(),
                          "verificationCode":otpTextField.text ?? "",
                          "type":"phone_verification_code"
        ]
        
        SCUserService.verifyContactAPI(withParameters: parameters, onComplete: {
            SCProgressView.hide()
            self.navigationController?.popToRootViewController(animated: true)
        }) { (errorMessage) in
            SCProgressView.hide()
            self.showAlert(title: SCAppConstants.pageTitles.updatePhoneNumber, msg: errorMessage)
        }
    }
    
    func  getCountryCode() {
        SCUserService.getCountryCodeAPI(onComplete: { (countryList ,success) in
            if success {
                self.processCountryCodeResponse(responseJSON: countryList["data"])
            }
        }) { (errorMessage) in
            print("Failed -- \(errorMessage)")
        }
    }
    
    private func processCountryCodeResponse(responseJSON: JSON)  -> Void {
        for (_, countryCodeJSON): (String, JSON) in responseJSON {
            let countryCodeName = "+\(countryCodeJSON["label"].string!) \(countryCodeJSON["value"].string!)"
            self.countryCodeArray.append(countryCodeName)
            self.countryCodeIDArray.append(countryCodeJSON["label"].string!)
           
        }
        self.createCountryCodePickerDataSource()
        countryCodeTextField.text = self.countryCodeArray[0]
    }
    
    @IBAction func submitButtonAction(_ sender: Any) {
       doPhoneVerification()
    }
    
    @IBAction func sendOTPButtonAction(_ sender: Any) {
        updateContact()
    }
    
    private func createCountryCodePickerDataSource() {
        countryCodePickerDataSource.append(coungtryCodePickerTitle)
        for countryCode in self.countryCodeArray {
            countryCodePickerDataSource.append(countryCode)
        }
    }
    
    func showCountryCodePicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle:coungtryCodePickerTitle, rows: countryCodePickerDataSource, initialSelection:  self.selectedIdCountryIndex, doneBlock: { picker, selectedIndex, selectedValue in
            self.selectedIdCountryIndex = selectedIndex-1
            if let aValue = selectedValue {
                textField.text = aValue as? String
            }
        }, cancel:nil, origin: textField)
    }
}

extension SCProfileChangePhoneViewController{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if (textField == countryCodeTextField) {
            self.showCountryCodePicker(textField: textField)
            return false
        }
        return true
    }
}
