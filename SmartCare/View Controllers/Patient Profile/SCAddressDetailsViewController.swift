//
//  SCAddressDetailsViewController.swift
//  SmartCare
//
//  Created by synerzip on 12/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCAddressDetailsViewController: UIViewController {
    
    @IBOutlet weak var address2TextField: UITextField!
    @IBOutlet weak var address1TextField: UITextField!
    @IBOutlet weak var postalCodeTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
         customizeNavigationItem(title: SCAppConstants.pageTitles.addressDetails, isDetail: true)
         addRightNavBarItem()
         updateProfileInformation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Disable Drawer for Detailed Pages
       
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = false
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 400)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
       
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = true
    }
    
    private func addRightNavBarItem() {
        let saveBarButtonItem = UIBarButtonItem(title: "SAVE", style: .plain, target: self, action: #selector(saveButtonAction))
        self.navigationItem.rightBarButtonItem  = saveBarButtonItem
    }
    
    private func updateProfileInformation() {
        self.address1TextField.text = UserProfile.getUserProfile()?.address?.address1
        self.address2TextField.text = UserProfile.getUserProfile()?.address?.address2
        self.postalCodeTextField.text = UserProfile.getUserProfile()?.address?.pincode
        self.countryTextField.text = UserProfile.getUserProfile()?.address?.country
        self.cityTextField.text = UserProfile.getUserProfile()?.address?.city
        self.stateTextField.text = UserProfile.getUserProfile()?.address?.state
    }

    @objc private func saveButtonAction() {
        saveAddress()
    }
    
     func saveAddress() {
        let messageDictionary : [String: Any] =
            [ "address":[
                "address1": address1TextField.text ?? "",
                "address2":address2TextField.text ?? "",
                "city": cityTextField.text ?? "",
                "state":stateTextField.text ?? "",
                "country":countryTextField.text ?? "",
                "pincode":postalCodeTextField.text ?? "",
                "id": UserProfile.getUserId()
                ]
           ]
        SCProgressView.show()
        SCProfileService.addAddressAPI(withParameters: messageDictionary, onComplete: { (jsonString, success) in
            SCProgressView.hide()
            self.navigationController?.popViewController(animated: true)
        }, onError: { (errorMessage) in
            SCProgressView.hide()
            self.showAlert(title:SCAppConstants.pageTitles.addressDetails, msg:errorMessage, style: .alert)
        })
    }
    
}
