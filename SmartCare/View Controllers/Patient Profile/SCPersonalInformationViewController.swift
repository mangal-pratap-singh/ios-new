//
//  SCPersonalInformationViewController.swift
//  SmartCare
//
//  Created by synerzip on 11/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCPersonalInformationViewController: UIViewController {

    @IBOutlet weak var profilePicImage: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var updateFirstButton: UIButton!
    @IBOutlet weak var phoneTextFiled: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var dateOfBirthTextField: UITextField!
    @IBOutlet weak var identificationTypeTextField: UITextField!
    @IBOutlet weak var identificationNumberTextField: UITextField!
    @IBOutlet weak var updateSecondButton: UIButton!
    
    var attachmentLink:String?
    let picker = UIImagePickerController()
    var userInfo:UserInfo?
    var isEditImage:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.personalInformation, isDetail: true)
        configureView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Disable Drawer for Detailed Pages
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = false
        updateProfileInformation()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = true
    }
    
    @IBAction func profilePicChangeButtonAction(_ sender: Any) {
        self.isEditImage = true
         present(picker, animated: true, completion: nil)
    }
    
    private func configureView() {
        profileImage.layer.cornerRadius = profileImage.frame.width/2
        profilePicImage.layer.cornerRadius = profilePicImage.frame.width/2
        profileImage.clipsToBounds = true
        profilePicImage.clipsToBounds = true
        picker.allowsEditing = false
        picker.delegate = self
    }
    
    private func updateProfileInformation() {
        let imageBaseUrl = AppInfo.getAppInfo()?.s3Url ?? ""
        let imageUrl = imageBaseUrl + (UserProfile.getUserProfile()?.profilePhotoURL ?? "")
        if let url = URL(string: imageUrl) {
            profileImage.sd_setImage(with: url, placeholderImage: SCAppConstants.Image.userProfile.image())
        }
        
        nameTextField.text = UserProfile.getUserProfile()?.firstName
        lastNameTextField.text = UserProfile.getUserProfile()?.lastName
        emailTextField.text = UserProfile.getUserProfile()?.email
        phoneTextFiled.text = UserProfile.getUserProfile()?.phone
        genderTextField.text = UserProfile.getUserProfile()?.gender
        dateOfBirthTextField.text = UserProfile.getUserProfile()?.dob
        identificationTypeTextField.text = UserProfile.getUserProfile()?.government?.name
        identificationNumberTextField.text = UserProfile.getUserProfile()?.government?.govtIdValue
    }
    
    @IBAction func updateEmailIdButtonAction(_ sender: Any) {
        if let emailChnageVC = UIStoryboard(name: SCAppConstants.storyBoardName.profile, bundle: nil).instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.emailChangeViewController) as? SCProfileEmailChangeViewController {
            emailChnageVC.email = emailTextField.text
            self.navigationController?.pushViewController(emailChnageVC, animated: true)
        }
    }
    
    @IBAction func updatePhoneNumberButtonAction(_ sender: Any) {
        if let phoneChnageVC = UIStoryboard(name: SCAppConstants.storyBoardName.profile, bundle: nil).instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.phoneChangeViewController) as? SCProfileChangePhoneViewController {
            phoneChnageVC.phone = phoneTextFiled.text
            self.navigationController?.pushViewController(phoneChnageVC, animated: true)
        }
    }
    
    @IBAction func saveAttachmentButtonAction(_ sender: Any) {
         present(picker, animated: true, completion: nil)
    }
    
    func saveAttachment() {
        var govtValue = 0
        if let govtIdTypeId = UserProfile.getUserProfile()?.government?.govtIdType {
            govtValue = govtIdTypeId
        }
        let messageDictionary : [String: Any] =
            [ "govtId":[
                "attachmentLink": attachmentLink ?? "",
                "govtIdType":govtValue,
                ]
        ]
        SCProgressView.show()
        SCProfileService.setAttachmentLinkAPI(withParameters: messageDictionary, onComplete: { (jsonString, success) in
            SCProgressView.hide()
            self.navigationController?.popViewController(animated: true)
        }, onError: { (errorMessage) in
            SCProgressView.hide()
            self.showAlert(title:SCAppConstants.pageTitles.addressDetails, msg:errorMessage, style: .alert)
        })
    }
}

extension SCPersonalInformationViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        
        guard let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage else {
            return
        }
        
        profileImage.image = image
        let compressImage = image.compressImage()
        SCProgressView.show()
        SCUserService.uploadAvatar(withImage:compressImage , onComplete: { ( data ,success) in
            SCProgressView.hide()
            if success {
                let tempPath = data["tempPath"].string ?? ""
                self.userInfo?.setAvtar(path:tempPath)
                
                if(self.isEditImage) {
                    self.uploadPicAPI(picUrl:tempPath)
                }else{
                    self.attachmentLink = tempPath
                    self.saveAttachment()
                }
            }
        }) { (errorMessage) in
            SCProgressView.hide()
            self.showAlert(title:SCAppConstants.pageTitles.personalInformation, msg:errorMessage, style: .alert)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func uploadPicAPI(picUrl:String) {
        let messageDictionary : [String: Any] = [ "pic_url": picUrl]
        SCProgressView.show()
        SCProfileService.uploadPicAPI(withParameters: messageDictionary, onComplete: { (jsonString, success) in
            SCProgressView.hide()
            self.navigationController?.popViewController(animated: true)
        }, onError: { (errorMessage) in
            SCProgressView.hide()
            self.showAlert(title:SCAppConstants.pageTitles.personalInformation, msg:errorMessage, style: .alert)
        })
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
