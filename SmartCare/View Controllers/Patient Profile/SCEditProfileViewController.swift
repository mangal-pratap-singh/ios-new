//
//  SCEditProfileViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 05/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCEditProfileViewController: UIViewController {
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var namelabel: UILabel!
    @IBOutlet weak var profilePicImageView: UIImageView!
    var docterProfilelabelArray = [SCAppConstants.editProfileValues.personalInformation.rawValue,SCAppConstants.editProfileValues.generalDetails.rawValue,    SCAppConstants.editProfileValues.educationDetails.rawValue,SCAppConstants.editProfileValues.speciality.rawValue,SCAppConstants.editProfileValues.address.rawValue]
    var docterProfileImageArray = ["profile_info_icon","more","educationdetail","speciality","address_details_icon"]
    var patianetProfilelabelArray = [SCAppConstants.editProfileValues.personalInformation.rawValue,SCAppConstants.editProfileValues.address.rawValue]
    var patianetProfileImageArray = ["profile_info_icon","address_details_icon"]
  
    @IBOutlet weak var profiletblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.editProfile, isDetail: false)
        configureView()
        registerNibsForCells()
        updateProfileInformation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getProfileData()
    }
    
    private func registerNibsForCells() {
        let nibName = UINib(nibName: SCAppConstants.nibNames.editProfielListItemTableCell, bundle:nil)
        profiletblView.register(nibName, forCellReuseIdentifier: SCAppConstants.cellReuseIdentifiers.editProfileListItem)
    }
    
    private func configureView() {
        profilePicImageView.layer.cornerRadius = profilePicImageView.frame.width/2
        profilePicImageView.layer.masksToBounds = true
        profiletblView.tableFooterView = UIView()
    }
    
    private func gotoProfileInformationController(){
    if let personalInformationVC = self.storyboard?.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.profileInformationViewController) as? SCPersonalInformationViewController {
       self.navigationController?.pushViewController(personalInformationVC, animated: true)
     }
    }
    
    private func gotoAddressInformationController(){
        if let addressDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.addressDetailsViewController) as? SCAddressDetailsViewController {
            self.navigationController?.pushViewController(addressDetailsVC, animated: true)
        }
    }
    
    private func updateProfileInformation() {
        self.namelabel.text =  "\((UserProfile.getUserProfile()?.firstName) ?? "") \((UserProfile.getUserProfile()?.lastName) ?? "")"
        self.emailLabel.text = UserProfile.getUserProfile()?.email
        let imageBaseUrl = AppInfo.getAppInfo()?.s3Url ?? ""
        let imageUrl = imageBaseUrl + (UserProfile.getUserProfile()?.profilePhotoURL ?? "")
        if let url = URL(string: imageUrl) {
            profilePicImageView.sd_setImage(with: url, placeholderImage: SCAppConstants.Image.userProfile.image())
        }
    }
    
    private func getProfileData() {
        SCProgressView.show()
        if UserProfile.isDoctor(){
            
            SCDoctorProfileService.getDoctorProfileData(onComplete: {(success) in
                SCProgressView.hide()
                self.updateProfileInformation()
            }) { (errorMessage) in
            }
        }
        else{
            SCProfileService.getProfileAPI(onComplete: {(success) in
                SCProgressView.hide()
                self.updateProfileInformation()
            }) { (errorMessage) in
            }
        }
    }
    
    private func gotoEducationDetailViewController() {
        if let educationDetailVC = UIStoryboard(name: SCAppConstants.storyBoardName.DoctorProfile, bundle: nil).instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.EducationDetails) as? SCEducationDetailListViewController {
            self.navigationController?.pushViewController(educationDetailVC, animated: true)
        }
    }
    private func gotoSpecialityDetailViewController() {
        
        if let speacilityDetailVC = UIStoryboard(name: SCAppConstants.storyBoardName.DoctorProfile, bundle: nil).instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.SpecalityDetails) as? SCSpecialityListViewController {
            self.navigationController?.pushViewController(speacilityDetailVC, animated: true)
        }
    }
    
    private func gotoGeneralDetailViewController() {
        if let generalDetailVC = UIStoryboard(name: SCAppConstants.storyBoardName.DoctorProfile, bundle: nil).instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.generalDetailsViewController) as? SCGeneralDetailViewController {
            self.navigationController?.pushViewController(generalDetailVC, animated: true)
        }
    }
}

extension SCEditProfileViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return UserProfile.isDoctor() ? docterProfilelabelArray.count : patianetProfilelabelArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SCAppConstants.cellReuseIdentifiers.editProfileListItem, for: indexPath as IndexPath) as! EditProfileTableViewCell
        cell.selectionStyle = .none
        cell.accessoryType = .disclosureIndicator
        cell.tintColor = UIColor.black
        var profileLbl = ""
        var profileImage = ""
         if UserProfile.isDoctor() {
            profileLbl = self.docterProfilelabelArray[indexPath.row]
            profileImage = self.docterProfileImageArray[indexPath.row]
         }else{
             profileLbl = self.patianetProfilelabelArray[indexPath.row]
             profileImage = self.patianetProfileImageArray[indexPath.row]
        }
        cell.configureCell(image: profileImage, name: profileLbl)
        return cell
    }
    
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
           if UserProfile.isDoctor(){
            let selctedValue = self.docterProfilelabelArray[indexPath.row]
            switch selctedValue {
            case SCAppConstants.editProfileValues.personalInformation.rawValue:
                gotoProfileInformationController()
            case SCAppConstants.editProfileValues.generalDetails.rawValue:
                gotoGeneralDetailViewController()
                break
            case SCAppConstants.editProfileValues.educationDetails.rawValue:
                gotoEducationDetailViewController()
            case SCAppConstants.editProfileValues.speciality.rawValue:
                gotoSpecialityDetailViewController()
                break
            case SCAppConstants.editProfileValues.address.rawValue:
                 gotoAddressInformationController()
            default: break
            }
           }else{
            let selctedValue = self.patianetProfilelabelArray[indexPath.row]
            switch selctedValue {
            case SCAppConstants.editProfileValues.personalInformation.rawValue:
                gotoProfileInformationController()
            case SCAppConstants.editProfileValues.address.rawValue:
                gotoAddressInformationController()
            default: break
        }
       }
    }
}
