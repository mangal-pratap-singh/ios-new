//
//  SCProfileEmailChangeViewController.swift
//  SmartCare
//
//  Created by synerzip on 21/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCProfileEmailChangeViewController: UIViewController {
    
    @IBOutlet weak var otpTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    
    var email:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.updateEmail, isDetail: true)
        configureView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Disable Drawer for Detailed Pages
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = true
    }
    
    func configureView()  {
        otpTextField.isHidden = true
        submitButton.isHidden = true
        emailTextField.text = email
    }
    
    @IBAction func otpButtonAction(_ sender: Any) {
        updateEmail()
    }
    
    @IBAction func submitButtonAction(_ sender: Any) {
        doEmailVerification()
    }
    
    private func updateEmail() {
        SCProgressView.show(withMessage: SCAppConstants.progressMessages.changingEmail)
        let parameters = ["userId":UserProfile.getUserId(),
                          "emailId":emailTextField.text ?? ""
        ]
        SCProfileService.contactChangeAPI(withParameters: parameters , onComplete: { [weak self] in
            SCProgressView.hide()
            self?.otpTextField.isHidden = false
            self?.submitButton.isHidden = false
        }) { (errorMessage) in
            SCProgressView.hide()
            self.showAlert(title: SCAppConstants.pageTitles.updateEmail, msg: errorMessage)
        }
    }
    
    private func doEmailVerification() {
        SCProgressView.show(withMessage: SCAppConstants.progressMessages.verifyingEmail)
        let parameters = ["userId":UserProfile.getUserId(),
                          "verificationCode":otpTextField.text ?? "",
                          "type":"email_verification_code"
        ]
        
        SCUserService.verifyContactAPI(withParameters: parameters, onComplete: { [weak self] in
            SCProgressView.hide()
            self?.navigationController?.popToRootViewController(animated: true)
        }) { (errorMessage) in
            SCProgressView.hide()
            self.showAlert(title: SCAppConstants.pageTitles.updateEmail, msg: errorMessage)
        }
    }
}
