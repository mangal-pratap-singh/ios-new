//
//  SCManageDependabaleDetailViewController.swift
//  SmartCare
//
//  Created by synerzip on 07/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import DLRadioButton
import ActionSheetPicker_3_0

class SCManageDepentDetailViewController: UIViewController {
    
    private let relationPickerTitle = "Select Relation"
    private let disasbleDependantButtonTitle = "DISABLE DEPENDENT"
    private let enableDependantButtonTitle = "ENABLE DEPENDENT"
    private let addDependantButtonTitle = "ADD DEPENDENT"
    private let paymentPickerTitle = "SELECT PAYMENT MODE"
    
    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfDateOfBirth: UITextField!
    @IBOutlet weak var tfSlibing: UITextField!
    @IBOutlet weak var disableDependentBtn: UIButton!
    @IBOutlet weak var radioBtnMale: DLRadioButton!
    @IBOutlet weak var radioBtnFemale: DLRadioButton!
    @IBOutlet weak var paymentMathod: UITextField!
    
    var isEdit:Bool?
    var selectedDependentObject:Dependent?
    var relationShipArray = ["Select Relation","Spouse","Child","Parent","Sibling","Other"]
    var paymentMethodArray = ["Pay Online" , "Pay at Clinic" ]
    var selectedIdentityIndex = 0
    var gneder:String = ""
    var relationType:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.manageDependant, isDetail: true)
        self.configureView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
      SCRoutingManager.sharedInstance.drawerController?.screenEdgePanGestureEnabled = true
    }
    
    private func configureView() {
        tfDateOfBirth.SetRightSideImage(with: SCAppConstants.Image.calenderBlack.image())
         tfSlibing.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        paymentMathod.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
        if isEdit == false {
            self.disableDependentBtn.setTitle(addDependantButtonTitle, for: .normal)
        } else {
            let buttonTitle = (self.selectedDependentObject?.status == false) ? enableDependantButtonTitle : disasbleDependantButtonTitle
            self.disableDependentBtn.setTitle(buttonTitle, for: .normal)
            self.setObject()
        }
    }
    
    private func setObject() {
        self.tfFirstName.text = self.selectedDependentObject?.firstName
        self.tfLastName.text = self.selectedDependentObject?.lastNmae
        self.tfSlibing.text = self.selectedDependentObject?.relationType?.statusText()
        self.tfDateOfBirth.text = self.selectedDependentObject?.dob
        self.tfFirstName.isUserInteractionEnabled = false
        self.tfLastName.isUserInteractionEnabled = false
        self.tfSlibing.isUserInteractionEnabled = false
        self.tfDateOfBirth.isUserInteractionEnabled = false
        self.radioBtnMale.isSelected = self.selectedDependentObject?.gender == "M" ? true : false
        self.radioBtnFemale.isSelected = self.selectedDependentObject?.gender == "F" ? true : false
        self.radioBtnFemale.isUserInteractionEnabled = false
        self.radioBtnMale.isUserInteractionEnabled = false
        self.paymentMathod.isUserInteractionEnabled = false
    }
    
    private func setRadioButton(name:String) {
        
        switch name {
        case SCAppConstants.radioButtonValues.male.rawValue:
            if(self.radioBtnMale.isSelected == true) {
                self.gneder = "M"
            }
        case SCAppConstants.radioButtonValues.female.rawValue:
            if(self.radioBtnFemale.isSelected == true) {
                self.gneder = "F"
            }
        default: break
        }
    }
    
    private func setRelationType(name:String) {

        switch name {
        case SCAppConstants.relationTypeValues.spouse.rawValue:
                self.relationType = "S"
        case SCAppConstants.relationTypeValues.child.rawValue:
                self.relationType = "C"
        case SCAppConstants.relationTypeValues.parent.rawValue:
            self.relationType = "P"
        case SCAppConstants.relationTypeValues.sibling.rawValue:
            self.relationType = "B"
        case SCAppConstants.relationTypeValues.other.rawValue:
            self.relationType = "O"
        default: break
        }
    }
    
    @IBAction func dependentButtonAction(_ sender: Any) {
        self.setRadioButton(name: SCAppConstants.radioButtonValues.male.rawValue)
        self.setRadioButton(name: SCAppConstants.radioButtonValues.female.rawValue)
        if(tfSlibing.text == "Select Relation"){
            self.showAlert(title:SCAppConstants.pageTitles.manageDependant, msg:"Please Select Relation", style: .alert)
            return
        }
        self.setRelationType(name: tfSlibing.text ?? "")
        if((sender as! UIButton).titleLabel?.text ?? "" == disasbleDependantButtonTitle){
            disableDependant(false)
        }
        else  if((sender as! UIButton).titleLabel?.text ?? "" == enableDependantButtonTitle){
            disableDependant(true)
        }
        else {
            if(self.checkTextFiledValidations())
            {
                 addDependant()
            }
        }
    }
   
    private func checkTextFiledValidations() -> Bool{
        
        if (tfFirstName.text?.isEmpty)! {
            self.showAlert(title:SCAppConstants.pageTitles.manageDependant, msg:SCAppConstants.validationMessages.dependentEmptyNameMessage, style: .alert)
            return false
        }
        if (tfLastName.text?.isEmpty)! {
            self.showAlert(title:SCAppConstants.pageTitles.manageDependant, msg:SCAppConstants.validationMessages.dependentEmptyLastNameMessage, style: .alert)
            return false
        }
        if (self.gneder.isEmpty) {
            self.showAlert(title:SCAppConstants.pageTitles.manageDependant, msg:SCAppConstants.validationMessages.dependentEmptyGnederMessage, style: .alert)
            return false
        }
        if (tfDateOfBirth.text?.isEmpty)! {
            self.showAlert(title:SCAppConstants.pageTitles.manageDependant, msg:SCAppConstants.validationMessages.dependentEmptyDateMessage, style: .alert)
            return false
        }
        return true
    }
    
    
    private func addDependant() {
        
        SCProgressView.show()
        let parameters = ["fName": tfFirstName.text?.capitalized ?? "",
                          "lName":tfLastName.text?.capitalized ?? "",
                          "sex":self.gneder,
                          "dob": tfDateOfBirth.text ?? "",
                          "relationType":self.relationType ?? "",
                          ]
        SCManageDependentServices.addDependentAPI(withParameters:parameters , onComplete: { (jsonString, success) in
            if success{
                SCProgressView.hide()
                self.navigationController?.popViewController(animated: true)
            }
        }, onError: { (errorMessage) in
            SCProgressView.hide()
            self.showAlert(title:SCAppConstants.pageTitles.manageDependant, msg:errorMessage, style: .alert)
        })
    }
    
    private func disableDependant(_ status: Bool) {
        SCProgressView.show()
        SCManageDependentServices.disableDependentAPI(withUserInfo: self.selectedDependentObject!,withStatus:status, onComplete: { (message,success) in
            if success{
                SCProgressView.hide()
                self.navigationController?.popViewController(animated: true)
            }
            
        }, onError: { (errorMessage) in
            SCProgressView.hide()
            self.showAlert(title:SCAppConstants.pageTitles.manageDependant, msg:errorMessage, style: .alert)
        })
    }
    
    func showDatePicker(textField:UITextField) {
        let datePicker = ActionSheetDatePicker(title: "Select Date", datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let dateString = (value as! Date).toDateString(withFormatter: SCAppConstants.DateFormatType.date)
            textField.text = dateString
            return
        }, cancel: {
            ActionStringCancelBlock in return
        },
           origin: textField.superview)
        datePicker?.show()
    }
    
    func showRelationPicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: relationPickerTitle, rows: self.relationShipArray, initialSelection: 0, doneBlock: { picker, selectedIndex, selectedValue in
            self.selectedIdentityIndex = selectedIndex
            if let aValue = selectedValue {
                textField.text = aValue as? String
            }
        }, cancel: { picker in
        }, origin: textField)
    }
    
//  hardcoded list of actionsheetPicker
    
    func showPaymentPicker(textField:UITextField) {
        ActionSheetStringPicker.show(withTitle: paymentPickerTitle, rows: self.paymentMethodArray, initialSelection: 0, doneBlock: { picker, selectedIndex, selectedValue in
            self.selectedIdentityIndex = selectedIndex
            if let aValue = selectedValue {
                textField.text = aValue as? String
            }
        }, cancel: { picker in
        }, origin: textField)
    }
}

extension SCManageDepentDetailViewController{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if (textField == tfDateOfBirth) {
            self.showDatePicker(textField: textField)
            return false
        }else if (textField == tfSlibing) {
            self.showRelationPicker(textField: textField)
            return false
        }else if (textField == paymentMathod) {
            // paymentMathod.resignFirstResponder()
             view.endEditing(true)
            self.showPaymentPicker(textField: textField)
             return false
          }
        return true
    }
}
