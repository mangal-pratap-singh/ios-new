//
//  SCManageDependantViewController.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 05/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON

class SCManageDependantViewController: UIViewController {

    @IBOutlet weak var dependableTable: UITableView!
    
    var dependentListArray = [Dependent]()
    var selectedDependentObject:Dependent?
    var fullScreenMesageView: FullScreenMessageView?

    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationItem(title: SCAppConstants.pageTitles.manageDependant, isDetail: false)
        configureView()
        addRightNavBarItem()
        registerNibsForCells()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.getManageDependentList()
    }
   
    private func configureView() {
        dependableTable.tableFooterView = UIView()
    }
    
    private func registerNibsForCells() {
        let nibName = UINib(nibName: SCAppConstants.nibNames.dependableListItemTableCell, bundle:nil)
        dependableTable.register(nibName, forCellReuseIdentifier: SCAppConstants.cellReuseIdentifiers.dependableListItem)
    }
    
    private func addRightNavBarItem(){
      let plusButton:UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
      plusButton.setImage(SCAppConstants.Image.plusButton.image(), for: UIControl.State.normal)
      plusButton.addTarget(self, action: #selector(plusButtonTapped), for: UIControl.Event.touchUpInside)
      let plusBarButton : UIBarButtonItem = UIBarButtonItem(customView: plusButton)
      self.navigationItem.rightBarButtonItem  = plusBarButton
    }
    
    @objc private func plusButtonTapped() {
        self.addoManageDependabaleDetailViewController()
    }
    
    private func addoManageDependabaleDetailViewController() {
        if let manageDependableVC = UIStoryboard(name: SCAppConstants.storyBoardName.manageDependant, bundle: nil).instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.managmanetDetailViewController) as? SCManageDepentDetailViewController {
            manageDependableVC.isEdit = false
            self.navigationController?.pushViewController(manageDependableVC, animated: true)
        }
    }

    private func getManageDependentList(){
//        SCProgressView.show()
        SCManageDependentServices.manageDependentListAPI(onComplete: {[weak self](dependnetList) in
            SCProgressView.hide()
            self?.fullScreenMesageView?.removeFromSuperview()
            if dependnetList.count == 0 {
                self?.fullScreenMesageView = self?.showFullScreenMessageOnView(parentView: self!.dependableTable, withMessage: "No Dependants Added")
                self?.view.addSubview(self!.fullScreenMesageView!)
                 SCProgressView.hide()
            }
            self?.dependentListArray = dependnetList
            self?.dependableTable.reloadData()
        }) { (errorMessage) in
        }
    }
}

extension SCManageDependantViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dependentListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SCAppConstants.cellReuseIdentifiers.dependableListItem, for: indexPath as IndexPath) as! ManageDependentTableViewCell
        cell.selectionStyle = .none
        let currentDependent = self.dependentListArray[indexPath.row]
        cell.configureCell(WithDependnet: currentDependent)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           self.selectedDependentObject=self.dependentListArray[indexPath.row]
           self.goToManageDependabaleDetailViewController()
    }
    
    private func goToManageDependabaleDetailViewController() {
        if let manageDependableVC = UIStoryboard(name: SCAppConstants.storyBoardName.manageDependant, bundle: nil).instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.managmanetDetailViewController) as? SCManageDepentDetailViewController {
            manageDependableVC.isEdit = true
            manageDependableVC.selectedDependentObject=selectedDependentObject
            self.navigationController?.pushViewController(manageDependableVC, animated: true)
        }
    }

    
}
