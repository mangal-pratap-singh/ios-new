//
//  SCRoutingManager.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 01/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

enum SCRoutingOption {
    case login
    case dashboard
    case bookAppointment
    case manageDependent
    case ratingHistory
    case insurancePolicy
    case paymentHistory
    case consultationNotes
    case authorisedHistory
    case profile
    case logout
    case addDependent
    case hospitalRequests
    case doctorDashboard
    case procedureFees
    case doctorAppointments
    case doctorProcedureAppointments
    case filterSlot
    case createSlot
    case createShift
    case startEndShift
    case shiftSummary
}

class SCRoutingManager {
    
    static let sharedInstance = SCRoutingManager ()
    var drawerController: KYDrawerController?
    var selectedDrawerItemIndex = IndexPath(row: 0, section: 0)
    
    func showHomeDashboard() {
        UserProfile.isDoctor() ? showDoctorDashboard() : showPatientDashboard()
    }
 
    private func showPatientDashboard() {
        let storyboard = UIStoryboard(name: SCAppConstants.storyBoardName.home, bundle: nil)
        if let dashboardNavigationVC = storyboard.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.dashboardNavigation) as? UINavigationController {
            updateDrawerWithMainController(mainVC: dashboardNavigationVC)
        }
    }
    
    private func showDoctorDashboard() {
        let storyboard = UIStoryboard(name: SCAppConstants.storyBoardName.doctorHome, bundle: nil)
        if let dashboardNavigationVC = storyboard.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.doctorDashboardNavigation) as? UINavigationController {
            updateDrawerWithMainController(mainVC: dashboardNavigationVC)
        }
    }
    
    func updateDrawerState(state: KYDrawerController.DrawerState) {
        drawerController?.setDrawerState(state, animated: true)
    }
    
    private func updateDrawerWithMainController(mainVC: UINavigationController) {
        drawerController = KYDrawerController(drawerDirection: .left, drawerWidth: (0.75 * UIScreen.main.bounds.width))
        drawerController?.mainViewController = mainVC
        let drawerVC = UserProfile.isDoctor() ? doctorDrawerMenuController() : patientDrawerMenuController()
        drawerController?.drawerViewController = drawerVC
        makeRootViewController(drawerController!)
    }

    private func patientDrawerMenuController() -> UIViewController? {
        let storyboard = UIStoryboard(name: SCAppConstants.storyBoardName.drawerMenu, bundle: nil)
        let drawerVC = storyboard.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.patientDrawer)
        return drawerVC
    }
    
    private func doctorDrawerMenuController() -> UIViewController? {
        let storyboard = UIStoryboard(name: SCAppConstants.storyBoardName.drawerMenu, bundle: nil)
        let drawerVC = storyboard.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.doctorDrawer)
        return drawerVC
    }
    
    private func makeRootViewController(_ viewController: UIViewController) {
        guard let window = UIApplication.shared.delegate?.window! else { return }
        window.backgroundColor = UIColor.white
        window.makeKeyAndVisible()
        window.rootViewController = viewController
    }
    
    func navigateToMenuOption(routingOption: SCRoutingOption) {
        drawerController?.screenEdgePanGestureEnabled = true
        switch routingOption {
        case .login:
            drawerController?.mainViewController = initialControllerForStoryBoard(storyBoardName: SCAppConstants.storyBoardName.login)
            drawerController?.screenEdgePanGestureEnabled = false
        case .dashboard:
            drawerController?.mainViewController = initialControllerForStoryBoard(storyBoardName: SCAppConstants.storyBoardName.home)
        case .bookAppointment:
            let storyboard = UIStoryboard(name: SCAppConstants.storyBoardName.home, bundle: nil)
            let menuOptionVC = storyboard.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.bookAppointmentNavigation) as! UINavigationController
            drawerController?.mainViewController = menuOptionVC
        case .doctorProcedureAppointments:
             drawerController?.mainViewController = initialControllerForStoryBoard(storyBoardName: SCAppConstants.storyBoardName.procedureAppointments)
        case .manageDependent:
            drawerController?.mainViewController = initialControllerForStoryBoard(storyBoardName: SCAppConstants.storyBoardName.manageDependant)
        case .ratingHistory:
            drawerController?.mainViewController = initialControllerForStoryBoard(storyBoardName: SCAppConstants.storyBoardName.ratingHistory)
        case .insurancePolicy:
            drawerController?.mainViewController = initialControllerForStoryBoard(storyBoardName: SCAppConstants.storyBoardName.insurancePolicy)
        case .paymentHistory:
            drawerController?.mainViewController = initialControllerForStoryBoard(storyBoardName: SCAppConstants.storyBoardName.paymentHistory)
        case .authorisedHistory:
            drawerController?.mainViewController = initialControllerForStoryBoard(storyBoardName: SCAppConstants.storyBoardName.authorisedHistory)
        case .consultationNotes:
            let storyboard = UIStoryboard(name: SCAppConstants.storyBoardName.home, bundle: nil)
            let menuOptionVC = storyboard.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.consultationNotesPatient)
            let uinavigation = UINavigationController(rootViewController: menuOptionVC) 
            
           drawerController?.mainViewController = uinavigation
        //   drawerController?.mainViewController =   initialControllerForStoryBoard(storyBoardName: SCAppConstants.viewControllerIdentifiers.consultationNotesPatient)
        
        case .profile:
            drawerController?.mainViewController = initialControllerForStoryBoard(storyBoardName: SCAppConstants.storyBoardName.profile)
        case .logout:
            let storyboard = UIStoryboard(name: SCAppConstants.storyBoardName.login, bundle: nil)
            let menuOptionVC = storyboard.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.logoutNavigation) as! UINavigationController
            drawerController?.mainViewController = menuOptionVC
            drawerController?.screenEdgePanGestureEnabled = false
        case .addDependent:
            drawerController?.mainViewController = initialControllerForStoryBoard(storyBoardName: SCAppConstants.storyBoardName.manageDependant)
        case .hospitalRequests:
            drawerController?.mainViewController = initialControllerForStoryBoard(storyBoardName: SCAppConstants.storyBoardName.hospitalRequest)
        case .doctorDashboard:
                drawerController?.mainViewController = initialControllerForStoryBoard(storyBoardName: SCAppConstants.storyBoardName.doctorHome)
        case .procedureFees:
            drawerController?.mainViewController = initialControllerForStoryBoard(storyBoardName: SCAppConstants.storyBoardName.procedureFees)
        case .doctorAppointments:
            drawerController?.mainViewController = initialControllerForStoryBoard(storyBoardName: SCAppConstants.storyBoardName.doctorAppointments)
        case .createSlot:
            let storyboard = UIStoryboard(name: SCAppConstants.storyBoardName.manageSlots, bundle: nil)
            let createSlotVC = storyboard.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.createSlotNavigation) as! UINavigationController
            drawerController?.mainViewController = createSlotVC
        case .filterSlot:
            let storyboard = UIStoryboard(name: SCAppConstants.storyBoardName.manageSlots, bundle: nil)
            let createSlotVC = storyboard.instantiateViewController(withIdentifier: SCAppConstants.viewControllerIdentifiers.filterSlotNavigation) as! UINavigationController
            drawerController?.mainViewController = createSlotVC
        case .createShift:
            let shiftListViewController = SCShiftsListViewController.initialize(with: SCShiftsLiftViewModel())
            drawerController?.mainViewController = shiftListViewController
        case .startEndShift:
            drawerController?.mainViewController = SCShiftsByDateListViewController.initialize(with: SCShiftsByDateListViewModel(with: .startEnd))
        case .shiftSummary:
            drawerController?.mainViewController = SCShiftsByDateListViewController.initialize(with: SCShiftsByDateListViewModel(with: .summary))
    }
        drawerController?.setDrawerState(.closed, animated: true)
    }
    
    private func initialControllerForStoryBoard(storyBoardName: String) -> UINavigationController {
        let storyboard = UIStoryboard(name: storyBoardName, bundle: nil)
        if let controller = storyboard.instantiateInitialViewController() as? UINavigationController {
            return controller
        }
        return UINavigationController()
    }
}
