//
//  SCSessionManager.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 28/08/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation

class SCSessionManager {
    
    static let sharedInstance = SCSessionManager ()
    
    func login(withUsername userName: String, password: String ,onSuccess: @escaping () -> Void, onFailure: @escaping (String) -> Void) {
        SCUserService.loginAPI(withUsername: userName, Password: password, onComplete: { (success, authToken) in
            if success {
                self.saveAuthenticationToken(token: authToken)
                onSuccess()
                
            }
        }) { (errorMessage) in
            onFailure(errorMessage)
        }
    }
    
    func getAuthenticationToken() -> String? {
        let defaults = UserDefaults.standard
        return defaults.value(forKey: "authenticationToken") as? String
    }
    
    func saveAuthenticationToken(token: String) {
        let defaults = UserDefaults.standard
        defaults.set(token, forKey: "authenticationToken")
    }
    
    func isUserLoggedIn() -> Bool? {
        let defaults = UserDefaults.standard
        return defaults.value(forKey: "userloggedin") as? Bool
    }
    
    func setUserLoggedInFlag(flag: Bool) {
        let defaults = UserDefaults.standard
        defaults.set(flag, forKey: "userloggedin")
        defaults.synchronize()
    }
    
    func loginComplete() {
        SCSessionManager.sharedInstance.setUserLoggedInFlag(flag: true)
    }
    
    func logout() {
        SCSessionManager.sharedInstance.setUserLoggedInFlag(flag: false)
        UserProfile.deleteUserProfile()
    }
}

