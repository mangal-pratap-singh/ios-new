import Foundation
import UIKit
import SwiftyJSON
class VideoToken {
    var data: VideoTokenModel?

     init(videojson: JSON) {
        self.data = VideoTokenModel(VideoTokenJson : videojson["data"])
    }

}

class VideoTokenModel {
    var appointmentId : Int?
    var startDateTime : String?
    var endDateTime : String?
    var patient : Patient?
    var clinic : Clinic?
    var hospital : Hospital?
    var doctor : Doctor?
    var joinToken : String?
    var resourceId : String?
    init(VideoTokenJson: JSON) {
        self.appointmentId = VideoTokenJson["appointmentId"].int
        self.startDateTime = VideoTokenJson["startDateTime"].string
        self.endDateTime = VideoTokenJson["endDateTime"].string
        self.patient = Patient(patientJson : VideoTokenJson["patient"])
        self.clinic = Clinic(appointmentJson : VideoTokenJson["clinic"])
        self.hospital = try? Hospital(VideoTokenJson["hospital"].stringValue)
        self.doctor =  Doctor(doctorJson: VideoTokenJson["doctor"])
        self.joinToken = VideoTokenJson["joinToken"].string
        self.resourceId = VideoTokenJson["resourceId"].string
        
    }
    
}

