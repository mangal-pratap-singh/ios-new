//
//  Rating.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 18/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON

class Rating {
    var count: Int?
    var review: String?
    
    init(ratingsJSON: JSON) {
        self.count = ratingsJSON["count"].int
        self.review = ratingsJSON["review"].string
    }
}


