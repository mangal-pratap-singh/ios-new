//
//  PayNow.swift
//  SmartCare
//
//  Created by Rediflex PvtLtd on 18/06/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class PayNowModel {
    var tid : Int?
    var accessCode : String?
    var merchantId : String?
    var currency : String?
    var redirectUrl : String?
    var cancelUrl : String?
    var language : String?
    var billingName : String?
    var billingAddress : String?
    var billingCity : String?
    var billingState : String?
    var billingZip : String?
    var billingCountry : String?
    var billingEmail : String?
    var billingTel : String?
    var orderId : String?
    var amount : String?
    
    init(paynowJson: JSON) {
        
        print("print payment json response=\(paynowJson)")
        self.tid = paynowJson["tid"].int
        self.accessCode = paynowJson["accessCode"].string
        self.merchantId = paynowJson["merchantId"].string
        self.currency = paynowJson["currency"].string
        self.redirectUrl = paynowJson["redirectUrl"].string
        self.cancelUrl = paynowJson["cancelUrl"].string
        self.language = paynowJson["language"].string
        self.billingName = paynowJson["billingName"].string
        self.billingAddress = paynowJson["billingAddress"].string
        self.billingCity = paynowJson["billingCity"].string
        self.billingState = paynowJson["billingState"].string
        self.billingZip = paynowJson["billingZip"].string
        self.billingCountry = paynowJson["billingCountry"].string
        self.billingEmail = paynowJson["billingEmail"].string
        self.billingTel = paynowJson["billingTel"].string
        self.orderId = paynowJson["orderId"].string
        self.amount = paynowJson["amount"].string
    }
}

