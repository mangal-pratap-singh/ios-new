//
//  SlotItem.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 13/12/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON

class SlotItem {
    var id: Int?
    var status: Bool = false
    var startTime: Date?
    var endTime: Date?
    var hospital: Hospital?
    var clinic: Clinic?
    var slotType: SlotType?
    var consultationType: ConsultationType?
    var shift: Shift?
    var appointmentStatus: AppointmentStatus?
    
    init(slotJson: JSON) {
        self.id = slotJson["id"].int
        self.status = slotJson["slotStatus"].bool ?? false
        self.startTime = (slotJson["startTime"].string ?? "").toDate()
        self.endTime = (slotJson["endTime"].string ?? "").toDate()
        self.hospital = try? Hospital(slotJson["hospital"].stringValue)
        self.clinic = try? Clinic(slotJson["clinic"].stringValue)
        self.slotType = SlotType(rawValue: slotJson["slotType"].string ?? "")
        self.consultationType = ConsultationType(rawValue: slotJson["slotType"].string ?? "")
        self.shift = try? Shift(data: slotJson["shift"].rawData())
        self.appointmentStatus = AppointmentStatus(rawValue: slotJson["appointment"]["status"].string ?? "")
    }
}
