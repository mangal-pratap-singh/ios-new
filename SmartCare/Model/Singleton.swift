//
//  Singleton.swift
//  SmartCare
//
//  Created by Rediflex PvtLtd on 22/07/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation

//arm64 arm64e armv7 armv7s
class Singleton {
    
    static let sharedInstance = Singleton()
    var singleUserDefaults:UserDefaults?
    
    
    func userDefaultsObj()->UserDefaults
    {
        
        if (singleUserDefaults==nil)
        {
            singleUserDefaults = UserDefaults.standard
        }
        
        return singleUserDefaults!;
    }
    
    
    
    
}
