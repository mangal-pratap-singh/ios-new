//
//  SCSearchModel.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 03/10/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import AVFoundation
class SCSearchModel {
    var returnSearch = [PatientSearchPharmacy]()
    func searchMethod(search: String,listData: [PatientSearchPharmacy])->[PatientSearchPharmacy]{
        
        for i in 0..<listData.count{
            
            let searchDict = listData[i] as PatientSearchPharmacy
            let mainStr = searchDict.name
            let pinCodeStr = searchDict.addressSearchPharmacy?.pincode
            let stringMatch = ((mainStr?.lowercased().range(of: search.lowercased())) != nil) as Bool
            let pinCodeMatch = ((pinCodeStr?.lowercased().range(of: search.lowercased())) != nil) as Bool
            if stringMatch == true || pinCodeMatch == true{
                
                returnSearch.append(searchDict)
            }
            }
         return returnSearch
    

    }
    
}
