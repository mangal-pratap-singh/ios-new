//
//  UserInfo.swift
//  SmartCare
//
//  Created by synerzip on 26/08/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserInfo {
    
    var userType :String?
    var firstName : String?
    var lastName : String?
    var gender : String?
    var dateOfBirth : String?
    var emailId : String?
    var countryCode : String?
    var phoneNumber : String?
    var password : String?
    var confirmPasword : String?
    var selectIdentityText : String?
    var selectIdentityId : String?
    var countryCodeId : String?
    var identificationNumber : String?
    var avtarUrl : String?
    
    init(userType: String?,firstName: String?,lastName: String?,gender: String?,dateOfBirth: String?,emailId: String?,countryCode: String?,phoneNumber: String?,password: String?,confirmPasword: String? ,selectIdentityText: String? ,selectIdentityId: String? ,countryCodeId: String?,identificationNumber: String? ,avtarUrl:String?){
        self.userType = userType
        self.firstName = firstName
        self.lastName = lastName
        self.gender = gender
        self.dateOfBirth = dateOfBirth
        self.emailId = emailId
        self.countryCode = countryCode
        self.phoneNumber = phoneNumber
        self.password = password
        self.confirmPasword = confirmPasword
        self.selectIdentityText = selectIdentityText
        self.selectIdentityId = selectIdentityId
        self.countryCodeId = countryCodeId
        self.identificationNumber = identificationNumber
        self.avtarUrl = avtarUrl
    }
    
    init(json : JSON){
    
    }
    func getFirstName() -> String {
        return self.firstName!
    }
    
    func setFirstName(fristName:String) {
         self.firstName = fristName
    }
    
    func getLastName() -> String {
        return self.lastName!
    }
    
    func setLastName(lastName:String) {
        self.lastName = lastName
    }
    
    func getGender() -> String {
        return self.gender!
    }
    
    func setGender(gender:String) {
        self.gender = gender
    }
    
    func getUserType() -> String {
        return self.userType!
    }

    func setUserType(userType:String) {
        self.userType = userType
    }
    
    func getDateOfBirth() -> String {
        return self.dateOfBirth!
    }
    
    func setDateOfBirth(dateOfBirth:String) {
        self.dateOfBirth = dateOfBirth
    }
    
    func getCountryCode() -> String {
        return self.countryCode!
    }
    
    func setEmailId(emailId:String) {
        self.emailId = emailId
    }
    
    func getEmailID() -> String {
        return self.emailId!
    }
    
    func setCountryCode(countryCode:String) {
        self.countryCode = countryCode
    }
    
    func getPhoneNumber() -> String {
        return self.phoneNumber!
    }
    
    func setPhoneNumber(phoneNumber:String) {
        self.phoneNumber = phoneNumber
    }
    
    func getPassword() -> String {
        return self.password!
    }
    
    func setPassword(password:String) {
        self.password = password
    }
    
    func getConfirmPasword() -> String {
        return self.confirmPasword!
    }
    
    func setConfirmPasword(confirmPasword:String) {
        self.confirmPasword = confirmPasword
    }
    
    func getIdentityText() -> String {
        return self.selectIdentityText!
    }
    
    func setIdentityText(selectIdentityText:String) {
        self.selectIdentityText = selectIdentityText
    }
    
    func getIdentityId() -> String {
        return self.selectIdentityId!
    }
    
    func setIdentityId(selectIdentityId:String) {
        self.selectIdentityId = selectIdentityId
    }
    
    func getCountryCodeId() -> String {
        return self.countryCodeId!
    }
    
    func setCountryCodeId(countryCodeId:String) {
        self.countryCodeId = countryCodeId
    }
    
    func getIdentificationNumber() -> String {
        return self.identificationNumber!
    }
    
    func setIdentificationNumber(identificationNumber:String) {
        self.identificationNumber = identificationNumber
    }
    func getAvtar() -> String {
        return self.avtarUrl!
    }
    
    func setAvtar(path:String) {
        self.avtarUrl = path
    }
}
