//
//  ProcedureFeeItem.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 15/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON

class ProcedureFeeItem {
    var orgNum: Int?
    var hospital: Hospital1?
    var clinic: Clinic1?
    var procedureType: ProcedureType?
    var procedureFee: ProcedureFee?
    
    init(procedureFeesJson: JSON) {
        self.orgNum = procedureFeesJson["orgNum"].int
        self.hospital = Hospital1(hospital: procedureFeesJson["hospital"])
        self.clinic = Clinic1(appointmentJson: procedureFeesJson["clinic"])
        self.procedureType = ProcedureType(ProcedureTypeJson: procedureFeesJson["procedureType"])
        self.procedureFee = ProcedureFee(ProcedureFeeJson: procedureFeesJson["fee"])
    }
}

class ProcedureType {
    var id: Int?
    var name: String = ""
    var details: String = ""
    
    init(ProcedureTypeJson: JSON) {
        self.id = ProcedureTypeJson["id"].int
        self.name = ProcedureTypeJson["name"].string ?? ""
        self.details = ProcedureTypeJson["details"].string ?? ""
    }
}

class ProcedureFee {
    var charges: String = ""
    var gstInclusive: Bool = false
    
    init(ProcedureFeeJson: JSON) {
        self.charges = ProcedureFeeJson["charges"].string ?? ""
        self.gstInclusive = ProcedureFeeJson["inclusiveGst"].bool ?? false
    }
}

class Clinic1 {
    var id: Int?
    var name: String?
    var clinicAddress: ClinicAddress?
    
//    clinicJson is written as appointmentJson
 
    init(appointmentJson: JSON) {
        self.id = appointmentJson["id"].int
        self.name = appointmentJson["name"].string
        
        print(" name print procedure ==\(String(describing: self.name))")
        self.clinicAddress = try? ClinicAddress.init(appointmentJson["clinicAddress"].stringValue)
    }
}

class Hospital1 {
    let id: Int
    let name: String?
   // let clinics: [Clinic]?
    init(hospital: JSON) {
        self.id = hospital["id"].int ?? 0
        self.name = hospital["name"].string
    }
    
}
