//
//  AppointmentDetailType.swift
//  SmartCare
//
//  Created by Rediflex PvtLtd on 11/06/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON

class appointmentArray {
    var type: String?
    var status: String?
    
    init(appointmentJSON: JSON) {
        self.type = appointmentJSON["type"].string
        self.status = appointmentJSON["status"].string
    }
}
