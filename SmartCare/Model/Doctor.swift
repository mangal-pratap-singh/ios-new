//
//  Doctor.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 04/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON

 class Doctor {
    var id: Int?
    var name: String?
    var speciality: String?
    var degree: String?
    var avtarImageURL: String?
    var emailId : String?
    var phoneNo : String?
    var countryCode : String?
    
    init(doctorJson: JSON) {
        self.id = doctorJson["id"].int
        self.name = doctorJson["name"].string ?? ""
        self.speciality = doctorJson["speciality"].string ?? ""
        self.degree = doctorJson["degree"].string ?? ""
        self.avtarImageURL = doctorJson["avatar"].string ?? ""
        self.emailId = doctorJson["emailId"].string ?? ""
        self.phoneNo = doctorJson["phoneNo"].string ?? ""
        self.countryCode = doctorJson["countryCode"].string ?? ""
    }
}
