//
//  Patient.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 04/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON

class Patient {
    var id: String?
    var name: String?
    var age: String?
    var dependant: String?
    var phoneNumber: String?
    var email: String?
    var countryCode: String?
    var gender: String?
    
    
    init(patientJson: JSON) {
        
        print("patientAppointDetails=\(patientJson)")
        self.id = patientJson["id"].string
        self.name = patientJson["name"].string ?? ""
        self.dependant = patientJson["dependant"].string ?? ""
        self.age = patientJson["age"].string ?? ""
        self.phoneNumber = patientJson["phoneNo"].string ?? ""
        self.email = patientJson["emailId"].string ?? ""
        self.countryCode = patientJson["countryCode"].string ?? ""
        self.gender = patientJson["gender"].string ?? ""
    }
}
