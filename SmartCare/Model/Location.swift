//
//  Location.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 11/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON

class Location {
    var id: Int?
    var city: String?
    
    init(locationJSON: JSON) {
        self.id = locationJSON["id"].int
        self.city = locationJSON["city"].string ?? ""
    }
}
