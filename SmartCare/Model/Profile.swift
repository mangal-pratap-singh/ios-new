//
//  Profile.swift
//  SmartCare
//
//  Created by synerzip on 18/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//
import UIKit
import Foundation
import SwiftyJSON

class Profile {
    
    var id: String?
    var policyNum: String?
    var company: String?
    var startDate:String?
    var endDate:String?
    var attachmentLink:String?
    var comment:String?
    
    
    init(insuranceJson: JSON) {
        self.id = insuranceJson["id"].string ?? ""
        self.policyNum = insuranceJson["policyNum"].string ?? ""
        self.company = insuranceJson["companyName"].string ?? ""
        self.startDate = insuranceJson["startDate"].string ?? ""
        self.endDate = insuranceJson["endDate"].string ?? ""
        self.comment = insuranceJson["comment"].string ?? ""
        self.attachmentLink = insuranceJson["attachmentLink"].string ?? ""
    }
}

