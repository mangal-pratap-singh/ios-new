//
//  labTest.swift
//  SmartCare
//
//  Created by Rediflex PvtLtd on 26/07/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

//@objcMembers class labTest: Object {
//    dynamic var id = 0
//    dynamic var appointmentId = 0
//    dynamic var Testdescription = ""
//    override static func primaryKey() -> String? {
//        return "id"
//    }
//
//    convenience required init(labTestJson: JSON){
//        self.init()
//        self.appointmentId = labTestJson["appointmentId"].int ?? 0
//        self.Testdescription = labTestJson["Testdescription"].string ?? ""
//
//    }
//
//
//}

 @objcMembers class LabTestReaml: Object {
     dynamic var id = 0
    dynamic var appointmentId = 0
    dynamic var TestDescription = ""
    dynamic var type = "0"
    override static func primaryKey() -> String? {
           return "id"
       }
 }

@objcMembers class AllergiesReaml: Object{
    dynamic var id = 0
    dynamic var appointmentId = 0
    dynamic var TestDecription = ""
    override static func primaryKey() -> String? {
        return "id"
    }
    

}

@objcMembers class DiagnosisReaml: Object{
    dynamic var id = 0
    dynamic var appointmentId = 0
    dynamic var TestDecription = ""
    override static func primaryKey() -> String? {
        return "id"
     }
  }
    
    @objcMembers class ProcedureReaml: Object{
    dynamic var id = 0
    dynamic var appointmentId = 0
    dynamic var TestDecription = ""
    override static func primaryKey() -> String? {
        return "id"
      }
  }
        
   @objcMembers class AdviceReaml: Object{
        dynamic var id = 0
        dynamic var appointmentId = 0
        dynamic var TestDecription = ""
        override static func primaryKey() -> String? {
            return "id"
        }
    

}

 @objcMembers class VitalsReaml: Object{
        dynamic var id = 0
        dynamic var appointmentId = 0
        dynamic var hieght = ""
        dynamic var weight = ""
        dynamic var bloodPresureSystolic = ""
        dynamic var bloodPresureDiastolic = ""
        dynamic var heartRate = ""
        dynamic var temperature = ""
        dynamic var respiratory = ""
        override static func primaryKey() -> String? {
            return "id"
        }
    

}


@objcMembers class MedicineReaml: Object{
        dynamic var id = 0
        dynamic var appointmentId = 0
        dynamic var medicineName = ""
        dynamic var morning = ""
        dynamic var afternoon = ""
        dynamic var evening = ""
        dynamic var night = ""
        dynamic var comments = ""
        dynamic var days = ""
        dynamic var uomId = 1
        override static func primaryKey() -> String? {
            return "id"
        }
    

}

