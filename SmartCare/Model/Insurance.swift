//
//  Insurance.swift
//  SmartCare
//
//  Created by synerzip on 17/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON

class Insurance {
    
    var id: Int?
    var policyNum: String?
    var company: String?
    var startDate:String?
    var endDate:String?
    var attachmentLink:String?
    var comment:String?

    
    init(insuranceJson: JSON) {
        self.id = insuranceJson["id"].int ?? 0
        self.policyNum = insuranceJson["policyNum"].string ?? ""
        self.company = insuranceJson["companyName"].string ?? ""
        self.startDate = insuranceJson["startDate"].string ?? ""
        self.endDate = insuranceJson["endDate"].string ?? ""
        self.comment = insuranceJson["comment"].string ?? ""
        self.attachmentLink = insuranceJson["attachmentLink"].string ?? ""
    }
}
