//
//  Rsa.swift
//  SmartCare
//
//  Created by Rediflex PvtLtd on 24/06/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class RSA {
    var  message : String?
    var rsaKey : String?
    
    init(RSAJson: JSON){
        self.message = RSAJson["message"].string ?? ""
        self.rsaKey = RSAJson["rsaKey"].string ?? ""
        
    }
    
}

