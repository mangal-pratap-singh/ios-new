//
//  DB.swift
//  RealmSwift
//
//  Created by Riccardo Rizzo on 12/07/17.
//  Copyright © 2017 Riccardo Rizzo. All rights reserved.
//

import UIKit
import RealmSwift

class DBManager {
    
    private var database:Realm
    static let sharedInstance = DBManager()
    
    private init() {
        
        database = try! Realm()
        
    }
    
    func getDataLabTestFromDB() -> Results<LabTestReaml> {
        
        let results: Results<LabTestReaml> = database.objects(LabTestReaml.self)
        return results
    }
    
    func addLabTestData(object: LabTestReaml) {
        
        try! database.write {
            database.add(object, update: .all)
            print("Added new object")
        }
    }
    
    func getDatAllergiesFromDB() -> Results<AllergiesReaml> {
           
           let results: Results<AllergiesReaml> = database.objects(AllergiesReaml.self)
           return results
       }
    func getDataVitalsFromDB() -> Results<VitalsReaml> {
              
              let results: Results<VitalsReaml> = database.objects(VitalsReaml.self)
              return results
          }
    func getDataMedicineListFromDB() -> Results<MedicineReaml> {
                 
                 let results: Results<MedicineReaml> = database.objects(MedicineReaml.self)
                 return results
             }
    
    func addVitalsData(object: VitalsReaml) {
              
              try! database.write {
                if DBManager.sharedInstance.getDataVitalsFromDB().count != 0{
                     database.delete(object)
                 }
               
               database.add(object, update: .all)
                  print("Added new object")
              }
          }
    
    func addMedicineData(object: MedicineReaml) {
                 
                 try! database.write {
                  database.add(object, update: .all)
                     print("Added new object")
                 }
             }
       
  func addAlergiesData(object: AllergiesReaml) {
           
           try! database.write {
            database.add(object, update: .all)
               print("Added new object")
           }
       }
    
    func getDiagnosisFromDB() -> Results<DiagnosisReaml> {
              
              let results: Results<DiagnosisReaml> = database.objects(DiagnosisReaml.self)
              return results
          }
          
     func addDiagnosisData(object: DiagnosisReaml) {
              
              try! database.write {
                database.add(object, update: .all)
                  print("Added new object")
              }
          }
    
    func getProcedureFromDB() -> Results<ProcedureReaml> {
             
             let results: Results<ProcedureReaml> = database.objects(ProcedureReaml.self)
             return results
         }
         
    func addProcedureData(object: ProcedureReaml) {
             
             try! database.write {
                database.add(object, update: .all)
                 print("Added new object")
             }
         }
    
    func getAdviceFromDB() -> Results<AdviceReaml> {
             
             let results: Results<AdviceReaml> = database.objects(AdviceReaml.self)
             return results
         }
         
    func addAdviceData(object: AdviceReaml) {
             
             try! database.write {
                database.add(object, update: .all)
                 print("Added new object")
             }
         }
    
    func deleteAllDatabase()  {
        try! database.write {
            database.deleteAll()
        }
    }
    
    func deleteFromDb(object: LabTestReaml) {
        
        try! database.write {
            
            database.delete(object)
        }
    }
    
    func deleteaNYFromDb(object: Any) {
           
           try! database.write {
               
            database.delete(object as! Object)
           }
       }
    
}
