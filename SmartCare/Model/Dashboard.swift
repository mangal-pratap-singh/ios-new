import Foundation
import SwiftyJSON

struct DailyStatus {
    var todayAppointment = 0
    var upcomingAppointment = 0
    var overallAppointment = 0
    var satisfiedPatient = 0
    
    init() {}
    
    init(dailyStatus: JSON) {
        self.todayAppointment = dailyStatus["todayAppointment"].int ?? 0
        self.upcomingAppointment = dailyStatus["upcomingAppointment"].int ?? 0
        self.overallAppointment = dailyStatus["overallAppointment"].int ?? 0
        self.satisfiedPatient = dailyStatus["satisfiedPatient"].int ?? 0
    }
}

struct DailyChart {
    var done = 0
    var cancelled = 0
    var pending = 0
    
    init() {}

    init(dailyChart: JSON) {
        self.done = dailyChart["done"].intValue
        self.cancelled = dailyChart["cancelled"].intValue
        self.pending = dailyChart["pending"].intValue
    }
}

struct YearwiseStats {
    var months = [UnitwiseStats]()
    
    init() {}
    
    init(json: JSON) {
        self.months = json.arrayValue.map({ UnitwiseStats(json: $0) })
    }
}


struct MonthwiseStats {
    var days = [UnitwiseStats]()
    
    init() {}

    init(json: JSON) {
        self.days = json.arrayValue.map({ UnitwiseStats(json: $0) })
    }
}

struct UnitwiseStats {
    var unit = ""
    var totalPatient = 0
    var reviewedPatient = 0
    var satisfiedPatient = 0
    
    init(json: JSON) {
        if json["day"].exists() {
            self.unit = json["day"].stringValue
        }
        if json["month"].exists() {
            self.unit = json["month"].stringValue
        }
        self.totalPatient = json["totalPatient"].intValue
        self.reviewedPatient = json["reviewedPatient"].intValue
        self.satisfiedPatient = json["satisfiedPatient"].intValue
    }
}
