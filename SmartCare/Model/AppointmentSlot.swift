//
//  AppointmentSlot.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 11/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON


enum SlotType: String {
    case procedure = "P"
    case consultation = "C"
    
    func text() -> String {
        switch self {
        case .procedure: return "Procedure"
        case .consultation: return "Consultation"
        }
    }
    
    static func toArray() -> [SlotType] {
        var valuesArray = [SlotType]()
        switch SlotType.procedure {
        case .procedure: valuesArray.append(.procedure); fallthrough
        case .consultation: valuesArray.append(.consultation)
        }
        return valuesArray
    }
}

enum ConsultationType: String {
    case walkin = "W"
    case remote = "R"
    case both = "B"
    
    func text() -> String {
        switch self {
        case .walkin: return "Walk-in"
        case .remote: return "Remote"
        case .both: return "Both"
        }
    }
    
    static func toArray() -> [ConsultationType] {
        var valuesArray = [ConsultationType]()
        switch ConsultationType.walkin {
        case .walkin: valuesArray.append(.walkin); fallthrough
        case .remote: valuesArray.append(.remote); fallthrough
        case .both: valuesArray.append(.both)
        }
        return valuesArray
    }
}

class Slot {
    var id: Int?
    var time: Date?
    
    init(slotJson: JSON) {
        self.id = slotJson["id"].int
        self.time = (slotJson["time"].string ?? "").toDate()
    }
}

class AppointmentSlot {
    
    var duration: Int?
    var clinicName: String?
    var hospitalName: String?
    var doctorName: String?
    var doctorPhone: String?
    var location: String?
    var doctorDegree: String?
    var specialities: String?
    var doctorAvatarURL: String?
    var address: ClinicAddress?
    var practiceStartedAt: Date?
    var availableSlots = [Slot]()
    
    init(AppointmentSlotJSON: JSON) {
        self.duration = AppointmentSlotJSON["id"].int
        self.practiceStartedAt = (AppointmentSlotJSON["practiceStartedAt"].string ?? "").toDate()
        self.location = AppointmentSlotJSON["location"].string ?? ""
        self.clinicName = AppointmentSlotJSON["clinicName"].string ?? ""
        self.hospitalName = AppointmentSlotJSON["hospitalName"].string ?? ""
        self.doctorName = AppointmentSlotJSON["doctorName"].string ?? ""
        self.doctorPhone = AppointmentSlotJSON["doctorPhoneNumbe"].string ?? ""
        self.doctorDegree = AppointmentSlotJSON["doctorDegree"].string ?? ""
        self.specialities = AppointmentSlotJSON["specialities"].string ?? ""
        self.doctorAvatarURL = AppointmentSlotJSON["doctorAvatar"].string ?? ""
        self.availableSlots = getAvailableSlots(slotsJSON: AppointmentSlotJSON["slot"])
        self.address = try? ClinicAddress(AppointmentSlotJSON["address"].stringValue)
    }
    
    func getAvailableSlots(slotsJSON: JSON) -> [Slot] {
        var availableSlots = [Slot]()
        for json in slotsJSON.arrayValue {
            let slot = Slot(slotJson: json)
            availableSlots.append(slot)
        }
        return availableSlots
    }
}
