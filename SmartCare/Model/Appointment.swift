//
//  Appointment.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 04/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON


enum AppointmentTypeStatus: String {
case walkin = "W"
case remote = "R"
case procedure = "P"


func statusTypeText() -> String {
    switch self {
        case .walkin: return "Walk-in Consultation"
        case .remote: return "Remote Consultation"
        case .procedure: return "Procedure Consultation"
       
    }
}
}


enum PatientTypeSwitchDetails: String {
case allergies = "Allergies"
case labTest = "Lab Test"
case medicines = "Medicines"
case vitals = "Vitals"
case diagnosis = "Diagnosis"
case procedures = "Procedures"
case advice = "Advice"

func statusText() -> String {
    switch self {
        case .allergies: return "Allergies type"
        case .labTest: return "Lab Test"
        case .medicines: return "Medicines"
        case .vitals: return "Vitals"
        case .diagnosis: return "diagnosis"
        case .procedures: return "Procedures"
        case .advice: return "Advice"
    }
}
}

enum AppointmentStatus: String {
    case completed = "C"
    case cancelled = "R"
    case confirmed = "A"
    case missed = "M"
    case booked = "S"
    
    func statusText() -> String {
        switch self {
            case .completed: return "Completed"
            case .cancelled: return "Cancelled"
            case .confirmed: return "Confirmed"
            case .missed: return "Missed"
            case .booked: return "Booked"
        }
    }

    

    func statusColor() -> UIColor {
        switch self {
            case .completed: return UIColor.SCBlueColor()
            case .cancelled: return UIColor.SCYellowColor()
            case .confirmed: return UIColor.SCGreenColor()
            case .missed: return UIColor.SCRedColor()
            case .booked: return UIColor.SCBrownColor()
        }
    }
    
    static func toArray() -> [AppointmentStatus] {
        var valuesArray = [AppointmentStatus]()
        switch AppointmentStatus.completed {
            case .completed: valuesArray.append(.completed); fallthrough
            case .cancelled: valuesArray.append(.cancelled); fallthrough
            case .confirmed: valuesArray.append(.confirmed); fallthrough
            case .missed: valuesArray.append(.missed); fallthrough
            case .booked: valuesArray.append(.booked);
        }
        return valuesArray
    }
}



class Appointment {
    var id: Int?
    var status: AppointmentStatus?
    var appointmentTypeStatus: AppointmentTypeStatus?
    var startTime: Date?
    var endTime: Date?
    var location: String?
    var patient: Patient?
    var doctor: Doctor?
    var hospital: Hospital?
    var clinic: Clinic?
    
    var hospital1: AHospital?
    var clinic1: AClinic?
    var cancel: PCancel?
    
    var ratings: Rating?
    var payment :Payment?
    
    init(appointmentJson: JSON) {
        self.id = appointmentJson["id"].int
        let appStatusDict = appointmentJson["appointment"].dictionaryObject
        self.status = AppointmentStatus(rawValue: appStatusDict?["status"] as? String ?? "")
        self.appointmentTypeStatus = AppointmentTypeStatus(rawValue: appStatusDict?["type"] as? String ?? "")
        self.cancel = PCancel(cancelData: appointmentJson["cancel"].dictionaryObject as [String : AnyObject]?)
        self.startTime = (appointmentJson["startDateTime"].string ?? "").toDate()
        self.endTime = (appointmentJson["endDateTime"].string ?? "").toDate()
        self.location = appointmentJson["location"].string ?? ""
        self.patient = Patient(patientJson: appointmentJson["patient"])
        self.doctor = Doctor(doctorJson: appointmentJson["doctor"])
        self.hospital = try? Hospital(appointmentJson["hospital"].stringValue)
       self.clinic = try? Clinic.init(appointmentJson["clinic"].stringValue)
        self.hospital1 =  AHospital(hospitalDict: appointmentJson["hospital"].dictionaryObject as [String : AnyObject]?)
        self.clinic1 = AClinic(clinicDict: appointmentJson["clinic"].dictionaryObject as [String : AnyObject]?)
        self.ratings = Rating(ratingsJSON: appointmentJson["rating"])
        self.payment = Payment(paymentJson: appointmentJson["payment"])
       
    }
    
}

class AHospital {
    var name: String?
    var id: Int?

    init(hospitalDict: [String: AnyObject]?) {
        self.name = hospitalDict?["name"] as? String ?? ""
        self.id = hospitalDict?["id"] as? Int ?? 0
    }
    
}

class AClinic {
    var name: String?
    var id : Int?
    var address: AAddress?
    init(clinicDict: [String: AnyObject]?) {
        self.name = clinicDict?["name"] as? String ?? ""
        self.id = clinicDict?["id"] as? Int ?? 0
        self.address = AAddress(clinicDict: clinicDict?["address"] as? [String : AnyObject])
    }
    
}

class PCancel {
var cancelBy: String?
var comments: String?
init(cancelData: [String: AnyObject]?) {
    self.cancelBy = cancelData?["by"] as? String ?? ""
    self.comments = cancelData?["comments"] as? String ?? ""
}
}
class AAddress {
    var address2: String?
    var city: String?
    var pincode: String?
    var phoneNumber: String?
    var country: String?
    var address1: String?
    var state: String?
    var id: Int?
    init(clinicDict: [String: AnyObject]?) {
        self.address2 = clinicDict?["address2"] as? String ?? ""
        self.city = clinicDict?["city"] as? String ?? ""
        self.pincode = clinicDict?["pincode"] as? String ?? ""
        self.phoneNumber = clinicDict?["phoneNumber"] as? String ??  ""
        self.country = clinicDict?["country"] as? String ?? ""
        self.address1 = clinicDict?["address1"] as? String ?? ""
        self.state = clinicDict?["state"] as? String ?? ""
        self.id = clinicDict?["id"] as? Int ?? 0
        
    }
    
    func fullAddress()->String{
        
        let address = "\(address1 ?? ""), \(address2 ?? ""), \(city ?? ""), \(state ?? ""), \(country ?? ""), \(pincode ?? "")"
       return address
    }
    
}
