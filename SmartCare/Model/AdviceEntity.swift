//
//  AdviceEntity.swift
//  SmartCare
//
//  Created by Rediflex PvtLtd on 29/07/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

@objcMembers class AdviceEntity: Object {
    dynamic var id = 0
    dynamic var appointmentId = 0
    dynamic var Advicedescription = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience required init(AdviceEntityJson: JSON){
        self.init()
        self.appointmentId = AdviceEntityJson["appointmentId"].int ?? 0
        self.Advicedescription = AdviceEntityJson["Advicedescription"].string ?? ""
        
    }
    
    
}
