import Foundation
import SwiftyJSON

struct ShiftSummary: CodableExtensions, Equatable {
    var totalSlots: Int = 0
    var totalAppointments: Int = 0
    var bookedAppointments: Int = 0
    var confirmedAppointments: Int = 0
    var cancelledAppointments: Int = 0
    var missedAppointments: Int = 0
    var completedAppointments: Int = 0
    
    init(shifSummarytJson: JSON) {
        self.totalSlots = shifSummarytJson["totalSlots"].int ?? 0
        self.totalAppointments = shifSummarytJson["totalAppointments"].int ?? 0
        self.bookedAppointments = shifSummarytJson["bookedAppointments"].int ?? 0
        self.confirmedAppointments = shifSummarytJson["confirmedAppointments"].int ?? 0
        self.cancelledAppointments = shifSummarytJson["cancelledAppointments"].int ?? 0
        self.missedAppointments = shifSummarytJson["missedAppointments"].int ?? 0
        self.completedAppointments = shifSummarytJson["completedAppointments"].int ?? 0
    }
    
    static func == (lhs: ShiftSummary, rhs: ShiftSummary) -> Bool {
        return (
            lhs.totalSlots == rhs.totalSlots &&
            lhs.totalAppointments == rhs.totalAppointments &&
            lhs.bookedAppointments == rhs.bookedAppointments &&
            lhs.confirmedAppointments == rhs.confirmedAppointments &&
            lhs.cancelledAppointments == rhs.cancelledAppointments &&
            lhs.missedAppointments == rhs.missedAppointments &&
            lhs.completedAppointments == rhs.completedAppointments
        )
    }
}
