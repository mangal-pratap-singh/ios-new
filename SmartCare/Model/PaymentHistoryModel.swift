//
//  PaymentHistoryModel.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 20/11/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class PaymentHistoryModel{
    
    
    var paymentHistory: [PaymentHistory]?
    
    init(jsonData: JSON) {
        paymentHistory = [PaymentHistory]()
        let dataArr = jsonData["data"].arrayObject
        for data1 in dataArr! as [AnyObject]{
                print("consultData==\(data1)")
            let paymentData = PaymentHistory(data: (data1 as? [String: AnyObject])!)
                self.paymentHistory?.append(paymentData)
            }
        }
    
}

class PaymentHistory{
    var startDate:String?
    var endDate:String?
    var startDate1:Date?
    var endDate1:Date?
    var id:String?
    var location:String?
    var paymentHospital: PaymentHospital?
    var paymentClinic: PaymentClinic?
    var paymentPatient: PaymentPatient?
    var paymentDoctor: PaymentDoctor?
    var paymentAppointment: PaymentAppointment?
    var paymentDetailsStatus:PaymentDetailsStatus?

    init(data: [String: AnyObject]) {
        
         self.startDate1 = (data["startDateTime"] as? String ?? "").toDate()
          self.endDate1 = (data["endDateTime"] as? String ?? "").toDate()
        
        self.startDate = data["startDateTime"] as? String ?? ""
        self.endDate = data["endDateTime"] as? String ?? ""
        self.id = String(data["id"] as? Int ?? 0)
        self.location = data["location"] as? String
        self.paymentHospital = PaymentHospital(hospitalData: data["hospital"] as! [String : AnyObject])
        self.paymentClinic = PaymentClinic(clinicData: (data["clinic"] as AnyObject) as! [String : AnyObject])
        self.paymentPatient = PaymentPatient(patientData: (data["patient"] as AnyObject) as! [String : AnyObject])
        self.paymentDoctor =  PaymentDoctor(doctorData: (data["doctor"] as AnyObject) as! [String : AnyObject])
        self.paymentAppointment = PaymentAppointment(appointmentData: (data["appointment"] as AnyObject) as! [String : AnyObject])
        self.paymentDetailsStatus = PaymentDetailsStatus(paymentData: data["payment"] as! [String: AnyObject])
        
        
    }
}


class PaymentHospital{
    var id: String?
    var name:String?
    init(hospitalData: [String: AnyObject]) {
        self.id = String(hospitalData["id"] as? Int ?? 0)
        self.name = hospitalData["name"] as? String ?? ""
    }
    
}

class PaymentClinic{
    var id: String?
    var name:String?
    init(clinicData: [String: AnyObject]) {
        self.id = String(clinicData["id"] as? Int ?? 0)
        self.name = clinicData["name"] as? String ?? ""
    }
    
}

class PaymentAddressClinic{
    var state:String?
    var address2: String?
    var address1: String?
    var pincode: String?
    var phoneNumber: String?
    var city: String?
    var id: String?
    var country: String?
    init(addressData: [String: AnyObject]) {
        
        self.state = addressData["state"] as? String
        self.address2 = addressData["address2"] as? String
        self.address1 = addressData["address1"] as? String
        self.pincode = String(addressData["pincode"] as? Int ?? 0)
        self.phoneNumber = String(addressData["phoneNumber"] as? Int ?? 0)
        self.city = addressData["city"] as? String
        self.id = String(addressData["id"] as? Int ?? 0)
        self.country = addressData["country"] as? String
    }
    
    
}

class PaymentPatient{
    var name: String?
    var emailId:String?
    var id:String?
    var age:String?
    var phoneNo:String
    init(patientData: [String: AnyObject]) {
        self.name = patientData["name"] as? String
        self.emailId = patientData["emailId"] as? String ?? ""
         self.id = patientData["id"] as? String ?? "0"
         self.age = patientData["age"] as? String ?? ""
        self.phoneNo = patientData["phoneNo"] as? String ?? ""
    }
    
}

class PaymentDetailsStatus{
    var transactionMode: String?
    var transactionStatus: String?
    var paymentType: PaymentType?
    var gstPercent: String?
    var status: PaymentStatus?
    var netAmount: String?
    var discountNumeric: String?
    var transactionId: String?
    var gstNumeric: String?
    var amount: String?
    var discountReason: String?
    var receiptUrl: String?
    var discountPercent: String?
    var discountType: String?

    init(paymentData: [String: AnyObject]) {
        
    let modeStr    =  paymentData["transactionMode"] as? String ?? ""
        print("transationMode==\(modeStr)")
      self.paymentType =   PaymentType(rawValue: paymentData["transactionMode"] as? String ?? "")
       // self.transactionMode = PaymentType(rawValue: paymentData["transactionMode"] as? String ?? "")
        self.transactionStatus = paymentData["transactionStatus"] as? String
        self.gstPercent = String(paymentData["gstPercent"] as? Double ?? 0.0)
        //self.status = paymentData["status"] as? String
         self.status = PaymentStatus(rawValue: paymentData["status"] as? String ?? "")
            
        self.netAmount = paymentData["netAmount"] as? String ?? "0.0"
        self.discountNumeric = paymentData["discountNumeric"] as? String
        self.transactionId = paymentData["transactionId"] as? String ?? "0.0"
        self.gstNumeric = String(paymentData["gstNumeric"] as? Double ?? 0.0)
        self.amount = paymentData["amount"] as? String ?? "0.0"
        self.discountReason = paymentData["discountReason"] as? String
        self.receiptUrl = paymentData["receiptUrl"] as? String
        self.discountPercent = paymentData["discountPercent"] as? String
        self.discountType = paymentData["discountType"] as? String
        
    }
    
}
class PaymentDoctor{
    
    var phoneNo:String?
    var speciality:String?
    var id: String?
    var emailId:String?
    var name:String?
    var avatar: String?
    var degree:String?
    var slotDuration:PaymentSlotDuration?
    init(doctorData: [String: AnyObject]) {
        
        self.phoneNo = String(doctorData["phoneNo"] as? Int ?? 0)
        self.speciality = doctorData["speciality"] as? String ?? ""
        self.id = String(doctorData["id"] as? Int ?? 0)
        self.emailId = doctorData["emailId"] as? String ?? ""
        self.name = doctorData["name"] as? String ?? ""
        self.avatar = doctorData["avatar"] as? String ?? ""
        self.degree = doctorData["degree"] as? String ?? ""
        self.slotDuration = PaymentSlotDuration(slotDurationData: (doctorData["slotDuration"] as? [String: AnyObject])!)
        
    }
    
}

class PaymentSlotDuration{
    var procedure:String?
    var consultation:String?
    init(slotDurationData: [String: AnyObject]) {
        self.procedure = String(slotDurationData["procedure"] as? Int ?? 0)
        self.consultation = String(slotDurationData["consultation"] as? Int ?? 0)
    }
}


class PaymentAppointment{
    var type:String?
    var appointmentType: AppointmentTypeStatus?

    var status: String?
    init(appointmentData: [String: AnyObject]) {
        self.type = appointmentData["type"] as? String ?? ""
        self.appointmentType =  AppointmentTypeStatus(rawValue: appointmentData["type"] as? String ?? "")
        self.status = appointmentData["status"] as? String ?? ""
    }
    
}
