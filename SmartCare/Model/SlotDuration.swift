//
//  SlotDuration.swift
//  SmartCare
//
//  Created by synerzip on 02/12/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class SlotDuration: Object {
    
    @objc dynamic var consultation = 0
    @objc dynamic var procedure = 0

    convenience required init(slotDuration: JSON) {
        self.init()
        self.consultation = slotDuration["consultation"].int ?? 0
        self.procedure = slotDuration["procedure"].int ?? 0
    }
}
