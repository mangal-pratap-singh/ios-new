//
//  HospitalRequest.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 04/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

enum HospitalRequestStatus: String {
    case requested = "S"
    case new = "N"
    case confirmed = "A"
    case denied = "D"
    
    func statusText() -> String {
        switch self {
        case .requested: return "Requested"
        case .new: return "New"
        case .confirmed: return "Confirmed"
        case .denied: return "Denied"
        }
    }
    
    func statusColor() -> UIColor {
        switch self {
        case .requested: return UIColor.SCBlueColor()
        case .new: return UIColor.gray
        case .confirmed: return UIColor.SCGreenColor()
        case .denied: return UIColor.SCRedColor()
        }
    }
}

class HospitalRequest {
    var orgNum: Int?
    var status: HospitalRequestStatus?
    var hospitalName: String = ""
    var location: String = ""
    var requestedBy: String = ""
    var updatedBy: String = ""
    var address: ClinicAddress?
    var address1: AAddress?
    var hospitalAdmin: HospitalAdmin?
    
    init(hospitalRequestJson: JSON) {
        self.orgNum = hospitalRequestJson["orgNum"].int
        self.status = HospitalRequestStatus(rawValue: hospitalRequestJson["status"].string ?? "")
        self.hospitalName = hospitalRequestJson["name"].string ?? ""
        self.location = hospitalRequestJson["location"].string ?? ""
        self.requestedBy = hospitalRequestJson["requestedBy"].string ?? ""
        self.updatedBy = hospitalRequestJson["updatedBy"].string ?? ""
        self.address = try? ClinicAddress(hospitalRequestJson["address"].stringValue)
        let adddict = hospitalRequestJson["address"].dictionaryObject
        self.address1 = AAddress(clinicDict: adddict as [String : AnyObject]?)
        self.hospitalAdmin = HospitalAdmin(adminJson: hospitalRequestJson["adminDetail"])
    }
}
