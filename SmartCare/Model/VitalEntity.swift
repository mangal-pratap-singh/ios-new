//
//  VitalEntity.swift
//  SmartCare
//
//  Created by Rediflex PvtLtd on 29/07/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

@objcMembers class VitalEntity: Object {
    dynamic var id = 0
    dynamic var appointmentId = 0
    dynamic var weight = ""
    dynamic var height = ""
    dynamic var bpSystolic = ""
    dynamic var bpDiastolic = ""
    dynamic var heartRate = ""
    dynamic var temprature = ""
    
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience required init(VitalEntityJson: JSON){
        self.init()
        self.appointmentId = VitalEntityJson["appointmentId"].int ?? 0
        self.weight = VitalEntityJson["weight"].string ?? ""
        self.height = VitalEntityJson["height"].string ?? ""
        self.bpSystolic = VitalEntityJson["bpSystolic"].string ?? ""
        self.bpDiastolic = VitalEntityJson["bpDiastolic"].string ?? ""
        self.heartRate = VitalEntityJson["heartRate"].string ?? ""
        self.temprature = VitalEntityJson["temprature"].string ?? ""
        
    }
    
    
}
