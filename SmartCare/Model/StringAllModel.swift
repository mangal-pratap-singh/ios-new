//
//  StringAllModel.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 12/02/20.
//  Copyright © 2020 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation

extension String {
    
    func getFistCharactorFromString()->String{
        
        let first2Chars  = String(self[self.index(self.startIndex, offsetBy: 0)])
        let capitalLetterStr = first2Chars.capitalized
        return capitalLetterStr
     }
    
}
