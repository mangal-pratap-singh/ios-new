//
//  InsuranceCompany.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 14/10/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON

class InsuranceCompany {
    var id: Int?
    var name: String?
    
    init(insuranceCompanyJSON: JSON) {
        self.id = insuranceCompanyJSON["id"].int
        self.name = insuranceCompanyJSON["name"].string ?? ""
    }
}
