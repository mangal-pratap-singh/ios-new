//
//  DoctorAppointment.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 18/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

enum AppointmentType: String {
    case walkin = "W"
    case remote = "R"
    case procedure = "P"
    
    
    func text() -> String {
        switch self {
        case .walkin: return "Walk-in Consultation"
        case .remote: return "Remote Consultation"
        case .procedure: return "Procedure Consultation"
        }
    }
    
    func Desc() -> String {
        switch self {
        case .walkin: return "W"
        case .remote: return "R"
        case .procedure: return "P"
        }
    }
    
    static let arr = [walkin, remote, procedure]
    
    func image() -> UIImage? {
        switch self {
        case .walkin: return UIImage(named: "walkin_consultation_icon")
        case .remote: return UIImage(named: "remote_consultation_icon")
        case .procedure: return UIImage(named: "procedure_consultation_icon")
        }
    }
}


class DoctorAppointment {
    var id: Int?
    var status: AppointmentStatus?
    var type: AppointmentType?
    var procedure:Procedure?
    var startTime: Date?
    var endTime: Date?
    var location: String?
    var patient: Patient?
    var hospital: Hospital?
    var doctorHospital: DoctorHospital?
    var clinic: Clinic?
    var payment: Payment?
    var cancelBy = "--"
    var cancelComments: String?
    var consultationNoteUrl: String?
    
    init(appointmentJson: JSON) {
        
         print("doctorAppointDetails=\(appointmentJson)")
        self.id = appointmentJson["id"].int
        self.status = AppointmentStatus(rawValue: appointmentJson["appointment"]["status"].string ?? "")
        self.type = AppointmentType(rawValue: appointmentJson["appointment"]["type"].string ?? "")
        self.procedure = Procedure(procedureJson: appointmentJson["appointment"]["procedure"])
        self.startTime = (appointmentJson["startDateTime"].string ?? "").toDate()
        self.endTime = (appointmentJson["endDateTime"].string ?? "").toDate()
        self.location = appointmentJson["location"].string ?? ""
        self.patient = Patient(patientJson: appointmentJson["patient"])
        self.hospital = try? Hospital(appointmentJson["hospital"].stringValue)
        self.doctorHospital = DoctorHospital(hospitalJson: appointmentJson["hospital"])
        self.clinic = try? Clinic(appointmentJson["clinic"].stringValue)
        self.payment = Payment(paymentJson: appointmentJson["payment"])
        if appointmentJson["cancel"]["by"].string == "P" {
            self.cancelBy = "Patient"
        } else if appointmentJson["cancel"]["by"].string == "D" {
            self.cancelBy = "Doctor"
        }
        self.cancelComments = appointmentJson["cancel"]["comments"].string ?? "--"
        self.consultationNoteUrl = appointmentJson["consultationNoteUrl"].string ?? ""
    }
}
