//
//  Payment.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 18/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON

enum PaymentType: String {
    case cash = "C"
    case online = "O"
    
    func statusText() -> String {
        switch self {
        case .cash: return "Cash"
        case .online: return "Online"
        }
    }
}

enum PaymentStatus: String {
    case pending = "P"
    case done = "C"
    
    func statusText() -> String {
        switch self {
        case .pending: return "Pending"
        case .done: return "Done"
        }
    }
}


class Payment {
    var id: Int?
    var transactionId: Int?
    var status: PaymentStatus?
    var type: PaymentType?
    var amount: String = "--"
    var gstPercent: String = ""
    var gstNumeric: String = ""
    var netAmount: String = ""
    var discountType: String = ""
    var discountPercent: String = ""
    var discountNumeric: String = ""
    var discountReason: String = ""
    var receiptUrl: String = ""
    
    init(paymentJson: JSON) {
        self.id = paymentJson["id"].int
        self.status = PaymentStatus(rawValue: paymentJson["status"].string ?? "")
        self.type = PaymentType(rawValue: paymentJson["type"].string ?? "")
        self.amount = paymentJson["amount"].string ?? "--"
        self.gstPercent = paymentJson["gstPercent"].string ?? ""
        self.gstNumeric = paymentJson["gstNumeric"].string ?? ""
        self.netAmount = paymentJson["netAmount"].string ?? ""
        self.discountType = paymentJson["discountType"].string ?? ""
        self.discountPercent = paymentJson["discountPercent"].string ?? ""
        self.discountNumeric = paymentJson["discountNumeric"].string ?? ""
        self.discountReason = paymentJson["discountReason"].string ?? ""
         self.receiptUrl = paymentJson["receiptUrl"].string ?? ""
        
    }
}

class DoctorHospital {
    var id: Int?
   
    var name: String?
    
    init(hospitalJson: JSON) {
        self.id = hospitalJson["id"].int
        self.name = hospitalJson["name"].string ?? ""
       
        
    }
}
