//
//  Speciality.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 11/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class Speciality : Object{
    
    @objc dynamic var id = 0
    @objc dynamic var name: String?
    @objc dynamic var certificateLink:String?
    

    convenience required   init(specialityJSON: JSON) {
        self.init()
        self.id = specialityJSON["id"].int ?? 0
        self.name = specialityJSON["name"].string ?? ""
        self.certificateLink = specialityJSON["certificateLink"].string ?? ""
    }

}

