//
//  UserProfile.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 28/08/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class UserProfile: Object {
    @objc dynamic var id = 1 // This is Primary Key always 1 so that there is only one user logged in at a time
    @objc dynamic var userId = ""
    @objc dynamic var userType = ""
    @objc dynamic var firstName = ""
    @objc dynamic var lastName = ""
    @objc dynamic var email = ""
    @objc dynamic var phone = ""
    @objc dynamic var countryCode = ""
    @objc dynamic var gender = ""
    @objc dynamic var dob = ""
    @objc dynamic var profilePhotoURL = ""
    @objc dynamic var practiceStartedAt = ""
    @objc dynamic var governmentRegistrationId = ""
   
    @objc dynamic var address: Address?
    @objc dynamic var government : Government?
    @objc dynamic var slotDurationGeneralDetails: SlotDuration?
    
    @objc dynamic var isEmailVerified = false
    @objc dynamic var isPhoneVerified = false
    @objc dynamic var isCertificateVerified = false
    
    @objc dynamic var registerAt: Date?
    @objc dynamic var passwordUpdatedAt: Date?
    
    @objc dynamic var experience = 0
    @objc dynamic var slotDuration = 0
    
    var specialities = RealmSwift.List<Speciality>()
    var degrees = RealmSwift.List<Degree>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience required init(profileJson: JSON) {
        self.init()
        self.userId = profileJson["userId"].string ?? ""
        self.userType = profileJson["userType"].string ?? ""
        self.firstName = profileJson["fName"].string ?? ""
        self.lastName = profileJson["lName"].string ?? ""
        self.email = profileJson["email"].string ?? ""
        self.phone = profileJson["phone"].string ?? ""
        self.governmentRegistrationId = profileJson["governmentRegistrationId"].string ?? ""
        self.profilePhotoURL = profileJson["picUrl"].string ?? ""
        self.gender = profileJson["gender"].string ?? ""
        self.dob = profileJson["dob"].string ?? ""
        self.practiceStartedAt = profileJson["practiceStartedAt"].string ?? ""
        self.countryCode = profileJson["countryCode"].string ?? ""
        self.isEmailVerified = profileJson["isEmailVerified"].bool ?? false
        self.isPhoneVerified = profileJson["isPhoneVerified"].bool ?? false
        self.isCertificateVerified = profileJson["isCertificateVerified"].bool ?? false
        self.address = Address(profileAddressJson: profileJson["address"])
        self.government = Government(governmentJson: profileJson["govtId"])
        self.specialities = getSpecialitsForUser(specialitsJSON: profileJson["speciality"])
        self.degrees = getDegreesForUser(degreeJSON: profileJson["degree"])
        self.experience = profileJson["experience"].int ?? 0
        self.slotDuration = profileJson["slotDuration"]["procedure"].int ?? 0
        self.slotDurationGeneralDetails = SlotDuration(slotDuration: profileJson["slotDuration"])
        if let dateStr = profileJson["registerAt"].string {
            self.registerAt = dateStr.toDateWithFormat(format: "yyyy-MM-dd'T'HH:mm:SSZ")
        }
        if let dateStr = profileJson["passwordUpdatedAt"].string {
            self.passwordUpdatedAt = dateStr.toDateWithFormat(format: "yyyy-MM-dd'T'HH:mm:SSZ")
        }
    }

    func getSpecialitsForUser(specialitsJSON: JSON) -> RealmSwift.List<Speciality> {
        let specialitys = RealmSwift.List<Speciality>()
        for json in specialitsJSON.arrayValue {
            let speciality = Speciality(specialityJSON: json)
            specialitys.append(speciality)
        }
        return specialitys
    }
   
    func getDegreesForUser(degreeJSON: JSON) -> RealmSwift.List<Degree> {
        let degrees = RealmSwift.List<Degree>()
        for json in degreeJSON.arrayValue {
            let degree = Degree(degreeJSON: json)
            degrees.append(degree)
        }
        return degrees
    }
    
    
    static func isDoctor() -> Bool {
        do {
            let realm = try Realm()
            if let userProfile = realm.objects(UserProfile.self).first {
                return userProfile.userType == "D"
            }
        } catch let error as NSError {
            print("Realm Error :: Retriving User Id -- \(error.localizedDescription)")
        }
        return false
    }
    
    static func getUserId() -> String {
        do {
            let realm = try Realm()
            if let userProfile = realm.objects(UserProfile.self).first {
                return userProfile.userId
            }
        } catch let error as NSError {
            print("Realm Error :: Retriving User Id -- \(error.localizedDescription)")
        }
        return ""
    }
    
    static func getUserProfile() -> UserProfile? {
        do {
            let realm = try Realm()
            if let userProfile = realm.objects(UserProfile.self).first {
                return userProfile
            }
        } catch let error as NSError {
            print("Realm Error :: Retriving User Id -- \(error.localizedDescription)")
        }
        return nil
    }
    
    static func deleteUserProfile() {
        do {
            let realm = try Realm()
            let result = realm.objects(UserProfile.self)
            do {
                try realm.write {
                    realm.delete(result)
                }
            }
        } catch let error as NSError {
            print("Realm Error :: Deleting User Profile -- \(error.localizedDescription)")
        }
    }
    
    
}

