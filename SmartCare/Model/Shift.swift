import Foundation

struct Shifts: CodableExtensions, Equatable {
    let data: [Shift]?
}

struct ShiftObjectParser: CodableExtensions {
    let data: Shift
}

struct Shift: CodableExtensions, Equatable {
    let id: Int
    let name: String?
    let startTime: Date?
    let endTime: Date?
    let actualStartTime: Date?
    let actualEndTime: Date?
    let hospital: Hospital?
    let clinic: Clinic?
    let shiftSummary: ShiftSummary?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case startTime = "startTime"
        case endTime = "endTime"
        case actualStartTime = "actualStartTime"
        case actualEndTime = "actualEndTime"
        case hospital = "hospital"
        case clinic = "clinic"
        case shiftSummary = "summary"
    }
    
    static func == (lhs: Shift, rhs: Shift) -> Bool {
        return lhs.id == rhs.id
    }
}

