//
//  RealmService.swift
//  SmartCare
//
//  Created by Rediflex PvtLtd on 28/07/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import RealmSwift

class RealmOperations {
    private init() {}
    static let shared = RealmOperations()
    var realm = try! Realm()
    
    func create<T: Object>(_ Object : T){
        do {
            try realm.write {
                realm.add(Object)
            }
        }
        catch {
            post(error)
        }
        
    }
    
    func update<T:Object>(_ object : T , with dictionary : [String : Any?]) {
        do {
            try realm.write {
                for (key , value) in dictionary{
                    object.setValue(value, forKey: key)
                }
            }
            
            
        }
        catch {
            post(error)
        }
        
    }
    
    
    
    
    func delete<T : Object>(_ Object : T){
        
        do{
            if let user = realm.objects(T.self).first {
                try realm.write {
                    realm.delete(user)
                }
            }
        }
        catch {
            post(error)
        }
        
        
    }
    
    
    func post (_ error : Error){
        NotificationCenter.default.post(name: NSNotification.Name("RealmError"), object: error)
    }
    
    func observeRealmError(in vc : UIViewController , completion : @escaping (Error?) -> Void){
        NotificationCenter.default.addObserver(forName: NSNotification.Name("RealmError"), object: nil, queue: nil) { (notification) in
            completion(notification.object as? Error)
        }
    }
    
    func stopObservingError(in vc : UIViewController){
        NotificationCenter.default.removeObserver(vc, name: NSNotification.Name("RealmError"), object: nil)
    }
}

