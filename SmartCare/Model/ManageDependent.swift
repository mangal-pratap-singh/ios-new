//
//  ManageDependent.swift
//  SmartCare
//
//  Created by synerzip on 08/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON

enum relationShipStatus: String {
    case spouse = "S"
    case child = "C"
    case parent = "P"
    case sibling = "B"
    case other = "O"
    
    func statusText() -> String {
        switch self {
        case .spouse: return "Spouse"
        case .child: return "Child"
        case .parent: return "Parent"
        case .sibling: return "Sibling"
        case .other: return "Other"
        }
    }
}

class Dependent {

    var dob: String?
    var lastNmae: String?
    var firstName: String?
    var gender:String?
    var dependentID:String?
    var relationType:relationShipStatus?
    var status:Bool
   
    init(dependentJson: JSON) {

        self.gender = dependentJson["sex"].string ?? ""
        self.dob = dependentJson["dob"].string ?? ""
        self.lastNmae = dependentJson["lName"].string ?? ""
         self.firstName = dependentJson["fName"].string ?? ""
         self.relationType = relationShipStatus(rawValue: dependentJson["relationType"].string ?? "")
         self.dependentID = dependentJson["dependentId"].string ?? ""
         self.status = dependentJson["status"].bool ?? true
       
    }
}

