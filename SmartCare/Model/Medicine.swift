//
//  Medicine.swift
//  SmartCare
//
//  Created by Rediflex PvtLtd on 29/07/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

@objcMembers class Medicine: Object {
    dynamic var id = 0
    dynamic var appointmentId = 0
    dynamic var name = ""
    dynamic var morning = ""
    dynamic var afternoon = ""
    dynamic var evening = ""
    dynamic var night = ""
    dynamic var comments = ""
    
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience required init(MedicineJson: JSON){
        self.init()
        self.appointmentId = MedicineJson["appointmentId"].int ?? 0
        self.name = MedicineJson["name"].string ?? ""
        self.morning = MedicineJson["morning"].string ?? ""
        self.afternoon = MedicineJson["afternoon"].string ?? ""
        self.evening = MedicineJson["evening"].string ?? ""
        self.night = MedicineJson["night"].string ?? ""
        self.comments = MedicineJson["commentsinsurancePolicy"].string ?? ""
        
      }
    
    
    
}
