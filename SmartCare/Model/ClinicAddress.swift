import Foundation

struct ClinicAddress: CodableExtensions {
    let id: Int
    let address1: String?
    let address2: String?
    let city: String?
    let state: String?
    let country: String?
    let pincode: String?
    let phoneNumber: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case address1 = "address1"
        case address2 = "address2"
        case city = "city"
        case state = "state"
        case country = "country"
        case pincode = "pincode"
        case phoneNumber = "phoneNumber"
    }
    func fullAddress() -> String {
        
        struct FullAddressBuilder {
            var address: String? = ""
            
            func with(addressComponent: String?, delimiter: String = ", ") -> FullAddressBuilder {
                if let addressComponent = addressComponent, !addressComponent.isEmpty {
                    return FullAddressBuilder(address: ((address ?? "") + delimiter + addressComponent))
                }
                return self
            }
        }
        
        return FullAddressBuilder(address: "")
                .with(addressComponent: address1)
                .with(addressComponent: address2)
                .with(addressComponent: city)
                .with(addressComponent: state)
                .with(addressComponent: country)
                .with(addressComponent: pincode, delimiter: " - ")
            .address ?? ""

    }
    
    func fullAddress1() -> String {
        
        print("doctor to call phonenumber==\(phoneNumber)")

        let address = "\(address1 ?? ""), \(address2 ?? ""), \(city ?? ""), \(state ?? ""), \(country ?? ""), \(pincode ?? "")"
           return address

    }
    
}
