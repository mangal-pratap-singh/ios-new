//
//  AppInfo.swift
//  SmartCare
//
//  Created by synerzip on 04/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//
import Foundation
import SwiftyJSON
import RealmSwift

class AppInfo: Object {
    @objc dynamic var version = 0
    @objc dynamic var appVersion = 0
    @objc dynamic var appName = ""
    @objc dynamic var adminEmail = ""
    @objc dynamic var adminPhone = ""
    @objc dynamic var resetPasswordTimeLimit = 0
    @objc dynamic var dashboardRefreshDuration = 0
    @objc dynamic var s3Url = ""
   
    
    convenience required init(appJson: JSON) {
        self.init()
        self.version = appJson["version"].int ?? 0
        self.appVersion = appJson["appVersion"].int ?? 0
        self.appName = appJson["appName"].string ?? ""
        self.adminEmail = appJson["adminEmail"].string ?? ""
        self.adminPhone = appJson["adminPhone"].string ?? ""
        self.resetPasswordTimeLimit = appJson["resetPasswordTimeLimit"].int ?? 0
        self.dashboardRefreshDuration = appJson["dashboardRefreshDuration"].int ?? 0
        self.s3Url = appJson["s3Url"].string ?? ""
    }
    
    static func getAppInfo() -> AppInfo? {
        do {
            let realm = try Realm()
            if let appInfo = realm.objects(AppInfo.self).first {
                return appInfo
            }
        } catch let error as NSError {
            print("Realm Error :: Retriving User Id -- \(error.localizedDescription)")
        }
        return nil
    }
}
