//
//  AllergyEntity.swift
//  SmartCare
//
//  Created by Rediflex PvtLtd on 29/07/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

@objcMembers class AllergyEntity: Object {
    dynamic var id = 0
    dynamic var appointmentId = 0
    dynamic var Allergysdescription = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience required init(AllergyEntityJson: JSON){
        self.init()
        self.appointmentId = AllergyEntityJson["appointmentId"].int ?? 0
        self.Allergysdescription = AllergyEntityJson["Allergysdescription"].string ?? ""
        
    }
    
    
}
