//
//  Procedure.swift
//  SmartCare
//
//  Created by synerzip on 17/12/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//


import Foundation
import SwiftyJSON

class Procedure {
    var id: Int?
    var name: String?
    var detail: String?
    var comment: String?

    init(procedureJson: JSON) {
        self.id = procedureJson["id"].int
        self.name = procedureJson["name"].string ?? ""
        self.detail = procedureJson["detail"].string ?? ""
        self.comment = procedureJson["comment"].string ?? ""
    }
}
