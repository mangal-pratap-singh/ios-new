import Foundation
import SwiftyJSON

struct Clinic: CodableExtensions {
    var id: Int?
    var name: String?
    var clinicAddress: ClinicAddress?
    
//    clinicJson is written as appointmentJson
 
    init(appointmentJson: JSON) {
        self.id = appointmentJson["id"].int
        self.name = appointmentJson["name"].string
        
        print(" name print procedure ==\(String(describing: self.name))")
        self.clinicAddress = try? ClinicAddress.init( (appointmentJson["address"].stringValue))
        /*
        let addDict = appointmentJson["address"].dictionaryObject
            
        let id = addDict?["id"] as? Int
        let address2 = addDict?["address2"] as? String
        let address1 = addDict?["address1"] as? String
        let city = addDict?["city"] as? String
        let state = addDict?["state"] as? String
        let country = addDict?["country"] as? String
        let phoneNumber = addDict?["phoneNumber"] as? String
        let pincode = addDict?["pincode"] as? String
        
        self.clinicAddress = ClinicAddress.init(id: id ?? 0, address1: address1, address2: address2, city: city, state: state, country: country, pincode: pincode, phoneNumber: phoneNumber)
      */

        
//        if let value = appointmentJson["id"].int {
//            self.id = value
//        } else if let value = appointmentJson["id"].string {
//            self.id = Int(value) ?? 0
//        } else {
//            self.id = appointmentJson["id"].intValue
//        }
//        self.name = appointmentJson["name"].string
//        self.clinicAddress = try? ClinicAddress(appointmentJson["address"].stringValue)
    }
}

