//
//  Address.swift
//  SmartCare
//
//  Created by synerzip on 18/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.

import Foundation
import SwiftyJSON
import RealmSwift

class Address: Object {
  // @objc dynamic var id: Int?
   @objc dynamic var address1: String?
   @objc dynamic var address2: String?
   @objc dynamic var city: String?
   @objc dynamic var state: String?
   @objc dynamic var country: String?
   @objc dynamic var pincode: String?
   @objc dynamic var phoneNumber: String?
    
    convenience required init(profileAddressJson: JSON) {
        self.init()
       // self.id = profileAddressJson["id"].int
        self.address1 = profileAddressJson["address1"].string ?? ""
        self.address2 = profileAddressJson["address2"].string ?? ""
        self.city = profileAddressJson["city"].string ?? ""
        self.state = profileAddressJson["state"].string ?? ""
        self.country = profileAddressJson["country"].string ?? ""
        self.pincode = profileAddressJson["pincode"].string ?? ""
        self.phoneNumber = profileAddressJson["phoneNumber"].string ?? ""
    }
}
