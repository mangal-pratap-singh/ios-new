//
//  Goverment.swift
//  SmartCare
//
//  Created by synerzip on 18/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class Government: Object {
    
   @objc dynamic var name: String?
   @objc dynamic var govtIdValue: String?
   @objc dynamic var attachmentLink: String?
   @objc dynamic var govtIdType = 0

    
    convenience required init(governmentJson: JSON) {
        self.init()
        self.name = governmentJson["name"].string ?? ""
        self.govtIdValue = governmentJson["govtIdValue"].string ?? ""
        self.attachmentLink = governmentJson["attachmentLink"].string ?? ""
        self.govtIdType = governmentJson["govtIdType"].int ?? 0
    }
}

