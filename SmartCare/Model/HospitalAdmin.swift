//
//  HospitalAdmin.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 04/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON

class HospitalAdmin {
    
    var emailId: String = ""
    var phoneNo: String = ""
    var countryCode:String = ""
    
    init(adminJson: JSON) {
        self.emailId = adminJson["emailId"].string ?? ""
        self.phoneNo = adminJson["phoneNo"].string ?? ""
        self.countryCode = adminJson["countryCode"].string ?? ""
    }
}
