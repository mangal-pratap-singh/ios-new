//
//  Degree.swift
//  SmartCare
//
//  Created by synerzip on 24/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//
//
import Foundation
import SwiftyJSON
import RealmSwift

class Degree : Object {

    @objc dynamic var id = 0
    @objc dynamic var name: String?
    @objc dynamic var certificateLink:String?
    
    
    convenience required   init(degreeJSON: JSON) {
        self.init()
        self.id = degreeJSON["id"].int ?? 0
        self.name = degreeJSON["name"].string ?? ""
        self.certificateLink = degreeJSON["certificateLink"].string ?? ""
    }

}

