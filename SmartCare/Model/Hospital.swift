import Foundation

struct Hospitals: CodableExtensions {
    let data: [Hospital]?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
}

struct Hospital: CodableExtensions, Equatable {
    let id: Int
    let name: String?
    let clinics: [Clinic]?
    
    static func == (lhs: Hospital, rhs: Hospital) -> Bool {
        return lhs.id == rhs.id
    }
}

