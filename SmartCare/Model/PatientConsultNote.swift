//
//  PatientConsultNote.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 20/09/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class PatientConsultNote{
    
   var patientConsultNoteList: [PatientConsultNoteList]?
    
    init(jsonData: JSON) {
        patientConsultNoteList = [PatientConsultNoteList]()
        var dataArr = jsonData["data"].arrayObject
        for data1 in dataArr! as [AnyObject]{
                print("consultData==\(data1)")
                let patientConsultNote = PatientConsultNoteList(data: (data1 as? [String: AnyObject])!)
                self.patientConsultNoteList?.append(patientConsultNote)
            }
        }
    }

class PatientConsultNoteList{
    
    var patientNote: PatientNote?
    var hospitalNote: HospitalNote?
    var appointmentNote: AppointmentNote?
    var doctorNote: DoctorNote?
    init(data: [String: AnyObject]) {
        self.patientNote = PatientNote(patientData: data["patient"] as! [String : AnyObject])
        self.hospitalNote = HospitalNote(hospitalAdd: data["hospital"] as! [String: AnyObject])
        self.appointmentNote = AppointmentNote(dataAppoint: data["appointment"] as! [String: AnyObject])
        self.doctorNote = DoctorNote(dataDoctor: data["doctor"] as! [String: AnyObject])
    }
}

class PatientNote {
    var id : String?
    var age : String?
    var address : String?
    var phoneNumber : String?
    var countryCode: String?
    var emailId : String?
    var name: String?
    var gender : String?
    var patientNoteAddress: PatientNoteCosultAddress?
    init(patientData: [String: AnyObject]) {
         self.id = patientData["id"] as? String ?? ""
         self.age = patientData["age"] as? String ?? ""
         self.address = patientData["address"] as? String ?? ""
        self.patientNoteAddress = PatientNoteCosultAddress(address: patientData["address"] as? [String:AnyObject])
         self.phoneNumber = patientData["phoneNumber"] as? String ?? ""
         self.countryCode = patientData["countryCode"] as? String ?? ""
         self.emailId = patientData["emailId"] as? String ?? ""
         self.name = patientData["name"] as? String ?? ""
         self.gender = patientData["gender"] as? String ?? ""
    }
}


class PatientNoteCosultAddress{
    
   
    var city:String?
    var phoneNumber: String?
    var id: String
    var country: String?
    var address1:String?
    var state: String?
    var pincode: String?
    var address2: String?
          
    init(address: [String: AnyObject]?) {
        self.city = address?["city"] as? String
        self.id = String(address?["id"] as? Int ?? 0)
         self.phoneNumber = address?["phoneNumber"] as? String
         self.country = address?["country"] as? String
         self.address1 = address?["address1"] as? String
         self.address2 = address?["address2"] as? String
         self.state = address?["state"] as? String
         self.pincode = address?["pincode"] as? String
       
        
        
    }
}
class HospitalNote {
    var id :Int?
    var name : String?
    var address: PatientConsultHAdd?
    
    init(hospitalAdd: [String: AnyObject]) {
        self.id = hospitalAdd["id"] as? Int ?? 0
        self.name = hospitalAdd["name"] as? String ?? ""
        self.address = PatientConsultHAdd(dataAdd: (hospitalAdd["address"] as? [String: AnyObject])!)
    }
    
}
class AppointmentNote {
    var appointmentType: AppointmentTypeStatus?
    var prescriptionId: String?
    var startTime : String?
    var paymentType: String?
    var endTime: String?
    var id : Int?
    var appointmentStatus: AppointmentStatus?
    var paymentStatus: PaymentStatus?
    init(dataAppoint: [String: AnyObject]) {
        self.appointmentType = AppointmentTypeStatus(rawValue: dataAppoint["appointmentType"] as? String ?? "")
         self.prescriptionId = dataAppoint["prescriptionId"] as? String ?? ""
         self.startTime = dataAppoint["startTime"] as? String ?? ""
         self.paymentType = dataAppoint["paymentType"] as? String ?? ""
         self.endTime = dataAppoint["endTime"] as? String ?? ""
         self.id = dataAppoint["id"] as? Int ?? 0
        self.appointmentStatus = AppointmentStatus(rawValue: dataAppoint["appointmentStatus"] as? String ?? "")
        self.paymentStatus = PaymentStatus(rawValue: dataAppoint["paymentStatus"] as? String ?? "")
    }
}

class DoctorNote {
    var picUrl: String?
    var phoneNumber : String?
    var id : String?
    var regNo : String?
    var name : String?
    init(dataDoctor: [String: AnyObject]) {
        self.picUrl = dataDoctor["picUrl"] as? String ?? ""
        self.phoneNumber = dataDoctor["phoneNumber"] as? String ?? ""
        self.id = dataDoctor["id"] as? String ?? ""
        self.regNo = dataDoctor["regNo"] as? String ?? ""
        self.name = dataDoctor["name"] as? String ?? ""
    }
}

class PatientConsultHAdd {
    var country: String?
    var phoneNumber : String?
    var pincode: String?
    var id : Int?
    var state : String?
    var address2 : String?
    var address1 : String?
    var city: String?
    init(dataAdd: [String: AnyObject]) {
        self.country = dataAdd["country"] as? String ?? ""
        self.phoneNumber = dataAdd["phoneNumber"] as? String ?? ""
        self.pincode = dataAdd["pincode"] as? String ?? ""
        self.id = dataAdd["id"] as? Int ?? 0
        self.state = dataAdd["state"] as? String ?? ""
        self.address2 = dataAdd["address2"] as? String ?? ""
        self.address1 = dataAdd["address1"] as? String ?? ""
        self.city = dataAdd["city"] as? String ?? ""
      }
 }

class PatientConsultationNoteDetail{
    
    var primaryComplain:PatientConsPrimaryComplain?
    var advice: [PatientConsAdvice]?
    var diagnosis: [PatientConsDiagnosis]?
    var labTest: [PatientConsLabTest]?
    var allergies: [PatientConsAllergies]?
    var medicines: [PatientConsMedicines]?
    var vitals: PatientConsVitals?
    var procedure: [PatientConsProcedure]?
    init(data: JSON) {
        self.medicines = [PatientConsMedicines]()
        self.advice = [PatientConsAdvice]()
        self.diagnosis = [PatientConsDiagnosis]()
        self.allergies = [PatientConsAllergies]()
        self.procedure = [PatientConsProcedure]()
        self.labTest = [PatientConsLabTest]()
        if let primaryData = data["data"].dictionaryObject{
            self.primaryComplain = PatientConsPrimaryComplain(primaryCompData:primaryData["consultationDetail"] as! [String : AnyObject] )
            self.vitals = PatientConsVitals(vitalsData: primaryData["vitals"] as! [String: AnyObject])
            
            var dataArr = primaryData["medicines"] as? [AnyObject]
            if let dataMedicinesArr = dataArr{
                if dataMedicinesArr.count>0{
                    for medicines in dataMedicinesArr{
                        
                        let medicinesDict = PatientConsMedicines(medicinesData: medicines as! [String : AnyObject])
                        self.medicines?.append(medicinesDict)
                        
                    }
                }
                
            }
            
            
            var adviceArr = primaryData["advice"] as? [AnyObject]
            if let dataAdviceArr = adviceArr{
                if dataAdviceArr.count>0{
                    for advice in dataAdviceArr{
                        
                        let adviceDict = PatientConsAdvice(adviceData: advice as! [String : AnyObject])
                        self.advice?.append(adviceDict)
                        
                    }
                }
                
            }
            
            var diagnosisArr = primaryData["diagnosis"] as? [AnyObject]
                     if let dataDiagnosisArr = diagnosisArr{
                         if dataDiagnosisArr.count>0{
                             for diagnosis in dataDiagnosisArr{
                                 
                                 let diaganosicDict = PatientConsDiagnosis(diagnosisData: diagnosis as! [String : AnyObject])
                                 self.diagnosis?.append(diaganosicDict)
                                 
                             }
                         }
                         
                    }
            
        var allergiesArr = primaryData["allergies"] as? [AnyObject]
         if let dataAllergiesArr = allergiesArr{
             if dataAllergiesArr.count>0{
                 for allergies in dataAllergiesArr{
                     
                     let allergiesDict = PatientConsAllergies(allergiesData: allergies as! [String : AnyObject])
                     self.allergies?.append(allergiesDict)
                     
                 }
             }
             
        }
            
            var procedureArr = primaryData["procedure"] as? [AnyObject]
                    if let dataProcedureArr = procedureArr{
                        if dataProcedureArr.count>0{
                            for procedure in dataProcedureArr{
                                
                                let procedureDict = PatientConsProcedure(procedureData: procedure as! [String : AnyObject])
                                self.procedure?.append(procedureDict)
                                
                            }
                        }
                        
                    }
            
    var labTestArr = primaryData["labTest"] as? [AnyObject]
            if let datalabTestArr = labTestArr{
                if datalabTestArr.count>0{
                    for labTest in datalabTestArr{
                        
                        let labTestDict = PatientConsLabTest(labTestData: labTest as! [String : AnyObject])
                        self.labTest?.append(labTestDict)
                        
                    }
                }
                
            }
            
        }
    }
    
}

class PatientConsPrimaryComplain{
    
   var primarycomplaint : String?
   var followUp: String?
   var presentHistory:String?
    var pastHistory:String?
    var referTo:String?
    init(primaryCompData: [String: AnyObject]) {
       self.primarycomplaint = primaryCompData["primarycomplaint"] as? String ?? ""
       self.followUp = primaryCompData["followUp"] as? String ?? ""
       self.presentHistory = primaryCompData["presentHistory"] as? String ?? ""
       self.pastHistory = primaryCompData["pastHistory"] as? String ?? ""
       self.referTo = primaryCompData["referTo"] as? String ?? ""
    }
}

class PatientConsAdvice{
    var advice: String?
    init(adviceData: [String: AnyObject]) {
         self.advice = adviceData["advice"] as? String ?? ""
     }
 }
class PatientConsDiagnosis{
     var description: String?
    init(diagnosisData: [String: AnyObject]) {
        self.description = diagnosisData["description"] as? String ?? ""
     }
}

class PatientConsLabTest{
    //"testHeaderId" : null,
    //"uploadLink" : "",
    var description: String?
    var testGroupName: String?
    //"testGroupId" : null,
    //"testId" : null,
    var testHeaderName: String?
    var testName: String?
    init(labTestData: [String: AnyObject]) {
         self.testName = labTestData["testName"] as? String ?? ""
         self.testGroupName = labTestData["testGroupName"] as? String ?? ""
         self.testHeaderName = labTestData["testHeaderName"] as? String ?? ""
         self.description = labTestData["description"] as? String ?? ""
    }
}

class PatientConsAllergies{
    var description: String?
    init(allergiesData: [String: AnyObject]) {
        self.description = allergiesData["description"] as? String ?? ""
    }
}

class PatientConsMedicines{
    
    var afternoon: String?
    var night: String?
    var days: String?
    var id : Int?
    var med_description: String?
    var name: String?
    var comments: String?
    var evening: String?
    var morning: String?
    
    init(medicinesData: [String: AnyObject]) {
        self.afternoon = medicinesData["afternoon"] as? String ?? ""
        self.night = medicinesData["night"] as? String ?? ""
        self.days = medicinesData["days"] as? String ?? ""
        self.id = medicinesData["id"] as? Int ?? 0
        self.med_description = medicinesData["med_description"] as? String ?? ""
        self.name = medicinesData["name"] as? String ?? ""
        self.comments = medicinesData["comments"] as? String ?? ""
        self.evening = medicinesData["evening"] as? String ?? ""
        self.morning = medicinesData["morning"] as? String ?? ""
        
    }
}

class PatientConsVitals{
    
    var bpDiastolic: String?
    var weight: String?
    var height : String?
    var bpSystolic: String?
    var respiratoryRate: Int?
    var heartRate: String?
    var temperature: String?
    init(vitalsData: [String: AnyObject]) {
        self.bpDiastolic = vitalsData["bpDiastolic"] as? String ?? ""
        self.weight = vitalsData["weight"] as? String ?? ""
        self.height = vitalsData["height"] as? String ?? ""
        self.bpSystolic = vitalsData["bpSystolic"] as? String ?? ""
        self.respiratoryRate = vitalsData["respiratoryRate"] as? Int ?? 0
        self.heartRate = vitalsData["heartRate"] as? String ?? ""
        self.temperature = vitalsData["heartRate"] as? String ?? ""
    }
}

class PatientConsProcedure{
    var name: String?
    init(procedureData: [String: AnyObject]) {
        self.name = procedureData["name"] as? String ?? ""
    }
}




// MARK: bellow code are Patient Search List
class PatientConsultSearchPharmacy{
    
    var searchPharmacy :[PatientSearchPharmacy]?
    init(data: JSON) {
        searchPharmacy?.removeAll()
        if let dataArr = data["data"].arrayObject {
            self.searchPharmacy = [PatientSearchPharmacy]()
            for dataDict in dataArr{
                let patientSearchPharmacy = PatientSearchPharmacy(pharmacy: dataDict as! [String : AnyObject])
                self.searchPharmacy?.append(patientSearchPharmacy)
                
            }
        }
    }
}

class PatientSearchPharmacy{
    
    var name: String?
    var  id: Int?
    var addressSearchPharmacy:PatientAddressSearchPharmacy?
    init(pharmacy: [String: AnyObject]) {
        
        self.name = pharmacy["name"] as? String ?? ""
        self.id  = pharmacy["id"] as? Int ?? 0
        self.addressSearchPharmacy  = PatientAddressSearchPharmacy(pharmacy: pharmacy["address"] as! [String: AnyObject] )
        //pharmacy[address]
    }
    
}

class PatientAddressSearchPharmacy{
    var address1: String?
    var address2: String?
    var pincode: String?
    var city: String?
    var state: String?
    var country: String?
    var phoneNumber: String?
    init(pharmacy: [String: AnyObject]) {
        print("======= address of pharmacy=== \(pharmacy)")
         self.address1 = pharmacy["address1"] as? String ?? ""
         self.address2 = pharmacy["address2"] as? String ?? ""
         self.pincode = pharmacy["pincode"] as? String ?? ""
         self.city = pharmacy["city"] as? String ?? ""
         self.state = pharmacy["state"] as? String ?? ""
         self.country = pharmacy["country"] as? String ?? ""
         self.phoneNumber = pharmacy["phoneNumber"] as? String ?? ""
    }
    
}

class DoctorFindDrugsClass{
    
    var findDrugs :[FindDrugs]?
    init(data: JSON) {
        findDrugs?.removeAll()
        if let dataArr = data["data"].arrayObject {
            print("Find Medicine==\(dataArr)")
            self.findDrugs = [FindDrugs]()
            for dataDict in dataArr{
                
                self.findDrugs?.append(FindDrugs(dataDrugs: dataDict as! [String : AnyObject]))
                
            }
        }
    }
}

struct FindDrugs {
    var details: String?
    var maxDosage: String
    var id: String
    var drugName: String?
    var iupac: String?
    init(dataDrugs: [String: AnyObject]) {
        
        self.details = dataDrugs["details"] as? String ?? ""
        self.maxDosage = dataDrugs["maxDosage"] as? String ?? ""
        self.id = String(dataDrugs["id"] as? Int ?? 0)
        self.drugName  = dataDrugs["drugName"] as? String ?? ""
        self.iupac  = dataDrugs["iupac"] as? String ?? ""
        
    }
}


class DoctorFindMedicineClass{
    var findMedicine :[FindMedicine]?
    init(data: JSON) {
        findMedicine?.removeAll()
        if let dataArr = data["data"].arrayObject {
            print("Find Medicine==\(data)")
            self.findMedicine = [FindMedicine]()
            for dataDict in dataArr{
                self.findMedicine?.append(FindMedicine(dataDrugs: dataDict as! [String : AnyObject]))
            }
        }
    }
}


class DoctorReferToSearchClass{
    var referToDoctorSearch :[ReferToDoctorSearch]?
    init(data: JSON) {
        referToDoctorSearch?.removeAll()
        if let dataArr = data["data"].arrayObject {
           // print("Find Medicine==\(data)")
        self.referToDoctorSearch = [ReferToDoctorSearch]()
            for dataDict in dataArr{
                self.referToDoctorSearch?.append(ReferToDoctorSearch(data: dataDict as! [String : AnyObject]))
            }
        }
    }
}

struct ReferToDoctorSearch {
    
    var specialities: String?
//      "hospitals" : [
//        {
//          "name" : "Medical Plus",
//          "phoneNumber" : "9087954275"
//        }
//      ],
    var phoneNumber: String?
    var id: String?
    var name: String?
    var email_id: String?
    init(data: [String: AnyObject]) {
        self.specialities = data["specialities"] as? String ?? ""
        self.phoneNumber = data["phoneNumber"] as? String ?? ""
        self.name = data["name"] as? String ?? ""
        self.email_id = data["email_id"] as? String ?? ""
        self.id = data["id"] as? String ?? ""
    }
}
class DoctorLabTestSearchClass{
    var labtestSearch :[LabtestSearch]?
    init(data: JSON) {
        labtestSearch?.removeAll()
        if let dataArr = data["data"].arrayObject {
           // print("Find Medicine==\(data)")
        self.labtestSearch = [LabtestSearch]()
            for dataDict in dataArr{
                self.labtestSearch?.append(LabtestSearch(data: dataDict as! [String : AnyObject]))
            }
        }
    }
}

struct LabtestSearch {

    var name: String?
    var type: String?
    var id: String?
    init(data: [String: AnyObject]) {
        
            self.name = data["name"] as? String ?? ""
            self.type = String(data["type"] as? Int ?? 0)
            self.id = String(data["id"] as? Int ?? 0)
        
      }
}
class DoctorMedicineSearchOnlyClass{
    var findMedicine :[FindMedicine]?
    init(data: JSON) {
        findMedicine?.removeAll()
        if let dataArr = data["data"].arrayObject {
           // print("Find Medicine==\(data)")
        self.findMedicine = [FindMedicine]()
            for dataDict in dataArr{
                self.findMedicine?.append(FindMedicine(dataDrugs: dataDict as! [String : AnyObject]))
            }
        }
    }
}
struct FindMedicine {
   
    var companyName: String?
//        "drugs" : [
//          {
//            "drugName" : "Paracetamol ",
//            "quantity" : 1000,
//            "unitName" : "mg",
//            "drugId" : 132,
//            "unitId" : 3
//          }
//        ],
    var packingOfMedicine: String?
    var quantityInPack: String?
    var type: String?
    var packingOfMedicineStr: String?
    var storageType: String?
    var mrp: String?
    var subCategory: String?
    var brandName: String?
    var discountPercent: String
    //var details:FindMedicineDetails?
    var details:[String: AnyObject]?
    var drugs: [FindBrandDrugs]?
       
    var productDiscountPrice: String?
    var use: String?
    var id: String?
    
    init(dataDrugs: [String: AnyObject]) {
        
        self.companyName = dataDrugs["companyName"] as? String ?? ""
        self.packingOfMedicine = dataDrugs["packingOfMedicine"] as? String ?? ""
        self.id = String(dataDrugs["id"] as? Int ?? 0)
        self.quantityInPack  = dataDrugs["quantityInPack"] as? String ?? ""
        self.type  = dataDrugs["type"] as? String ?? ""
        self.packingOfMedicineStr  = dataDrugs["packingOfMedicineStr"] as? String ?? ""
        self.storageType  = dataDrugs["storageType"] as? String ?? ""
        self.mrp  = dataDrugs["mrp"] as? String ?? ""
        self.subCategory  = dataDrugs["subCategory"] as? String ?? ""
        self.brandName  = dataDrugs["brandName"] as? String ?? ""
        self.discountPercent  = dataDrugs["discountPercent"] as? String ?? ""
        self.productDiscountPrice  = dataDrugs["productDiscountPrice"] as? String ?? ""
        self.use  = dataDrugs["use"] as? String ?? ""
        let detailsMedicine = dataDrugs["details"] as AnyObject
        if detailsMedicine is NSNull || detailsMedicine as? String == ""{
            
        }else{
           
            details = [String: AnyObject]()
            self.details  =  (dataDrugs["details"] as! [String : AnyObject])
           // self.details  = FindMedicineDetails(details: dataDrugs["details"] as! [String : AnyObject])
        }
       
        
        let detailsDrugsList = dataDrugs["drugs"] as AnyObject
        if detailsDrugsList is NSNull || detailsDrugsList as? String == ""{
            
        }else{
            
            if let drugsArr = dataDrugs["drugs"] as? [AnyObject] {
                       print("Find Medicine==\(drugsArr)")
                       self.drugs = [FindBrandDrugs]()
                       for dataDict in drugsArr{
                           self.drugs?.append(FindBrandDrugs(dataDrugs: dataDict as! [String : AnyObject]))
                        }
                    }
           // self.details  = FindBrandDrugs(details: dataDrugs["drugs"] as! [String : AnyObject])
        }
        
    }
}


struct FindBrandDrugs {
    var unitId: String?
    var unitName: String?
    var quantity: String?
    var drugName: String?
    var drugId: String?
    init(dataDrugs: [String: AnyObject]) {
        
        self.unitId = String(dataDrugs["unitId"] as? Int ?? 0)
        self.unitName = dataDrugs["unitName"] as? String ?? ""
        self.quantity = String(dataDrugs["quantity"] as? Int ?? 0)
        self.drugName  = dataDrugs["drugName"] as? String ?? ""
        self.drugId  = String(dataDrugs["drugId"] as? Int ?? 0)
        
    }
}
struct FindMedicineDetails {
    var expertAdvice: String?
    var drivingInteraction: String?
    var modeOfAction: String?
    var pregnancyInteraction: String?
    var kidneyInteraction: String?
    var medicineInteraction: String?
    var action: String?
    var sideEffect: String?
    var liverInteraction: String?
    var usage: String?
    var prescription: Bool
    var lactationInteraction: String?
    var faq: String?
    var alcoholInteraction: String?
    var primaryUse: String?
        
    init(details: [String: AnyObject]) {
        self.expertAdvice = details["expertAdvice"] as? String ?? ""
        self.drivingInteraction = details["expertAdvice"] as? String ?? ""
        self.modeOfAction = details["modeOfAction"] as? String ?? ""
        self.pregnancyInteraction = details["pregnancyInteraction"] as? String ?? ""
        self.kidneyInteraction = details["kidneyInteraction"] as? String ?? ""
        self.medicineInteraction = details["medicineInteraction"] as? String ?? ""
            self.action = details["action"] as? String ?? ""
        self.sideEffect = details["sideEffect"] as? String ?? ""
        self.liverInteraction = details["liverInteraction"] as? String ?? ""
        self.usage = details["usage"] as? String ?? ""
        self.prescription = details["prescription"] as? Bool ?? false
        self.lactationInteraction = details["lactationInteraction"] as? String ?? ""
        self.faq = details["faq"] as? String ?? ""
        self.alcoholInteraction = details["alcoholInteraction"] as? String ?? ""
        self.primaryUse = details["primaryUse"] as? String ?? ""
    }
}

