//
//  AuthorisedMedicine.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 01/12/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
class AuthorisedMedicineModel{
    
    var authorisedMedicine:[AuthorisedMedicine]?
    init(json: JSON) {
         var dataArr = json["data"].arrayObject
        if let dataArr = dataArr {
            self.authorisedMedicine = [AuthorisedMedicine]()
            for obect in dataArr{
                
                self.authorisedMedicine?.append(AuthorisedMedicine(data: obect as! [String : AnyObject]))
            }
        }
        
    }
}

class AuthorisedMedicine{
    
   //"createdBy" : "17641012A0001",
    var authorisedPharmacy: AuthorisedPharmacy?
    var boughtAt: String?
    var cancelAt: String?
    var appointmentId: String?
    var createdAt: String?
    init(data: [String: AnyObject]) {
        self.boughtAt = data["boughtAt"] as? String ?? ""
        self.cancelAt = data["cancelAt"] as? String ?? ""
        self.createdAt = data["createdAt"] as? String ?? ""
        self.authorisedPharmacy = AuthorisedPharmacy(pharmacy: data["pharmacy"] as! [String : AnyObject])
    }
}

class AuthorisedPharmacy{
    var id: String?
    var name: String?
    init(pharmacy: [String: AnyObject]) {
        self.id = String(pharmacy["id"] as? Int ?? 0)
        self.name = pharmacy["name"] as? String ?? ""
    }
}
