//
//  Diagnosis.swift
//  SmartCare
//
//  Created by Rediflex PvtLtd on 29/07/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

@objcMembers class Diagnosis: Object {
    dynamic var id = 0
    dynamic var appointmentId = 0
    dynamic var Diagnosisdescription = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience required init(DiagnosisJson: JSON){
        self.init()
        self.appointmentId = DiagnosisJson["appointmentId"].int ?? 0
        self.Diagnosisdescription = DiagnosisJson["Diagnosisdescription"].string ?? ""
        
    }
    
    
}
