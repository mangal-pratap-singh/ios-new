//
//  SCAppConstants.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 22/08/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//


//health.smartcare.app.dev
import UIKit

typealias SuccessCallback = (Data?) -> Void
typealias ErrorCallback = (String) -> Void
typealias DateFormat = SCAppConstants.DateFormatType

enum Configuration {
    case Development
    case Staging
    case Production
    
    static func apiEnvironment() -> Configuration {
        if let configuration = Bundle.main.object(forInfoDictionaryKey: "Configuration") as? String {
            if configuration.range(of: "Staging") != nil {
                return .Staging
            }
            if configuration.range(of: "Production") != nil {
                return .Production
            }
        }
        return .Development
    }
}

class SCAppConstants {
    
    static func apiBaseURL() -> String {
        switch Configuration.apiEnvironment() {
        case .Development:
            return "https://test-api.smartcare.health/api/v1/"
        case .Staging:
            return "https://stage-api.smartcare.health/api/v1/"
        case .Production:
            return "https://api.smartcare.health/api/v1/"
        }
    }
    
    static func apiBaseURLVersion2() -> String {
        switch Configuration.apiEnvironment() {
        case .Development:
            return "https://test-api.smartcare.health/api/v2/"
        case .Staging:
            return "https://stage-api.smartcare.health/api/v2/"
        case .Production:
            return "https://api.smartcare.health/api/v2/"
        }
    }
    
    static let applicationFontFamily = "OpenSans-Regular"
    
    enum cornerRadiusValues: CGFloat {
        case zeroCornerRadus = 0.0
        case verySmall = 4.0
        case small = 5.0
        case medium = 6.0
        case large = 8.0
    }
    
    enum storyBoardName {
        static let login = "Login"
        static let home = "Home"
        static let manageDependant = "ManageDependant"
        static let ratingHistory = "RatingHistory"
        static let insurancePolicy = "InsurancePolicy"
        static let paymentHistory = "paymentHistory"
        static let authorisedHistory = "AuthorisedHostory"
        static let profile = "Profile"
        static let logout = "Logout"
        static let drawerMenu = "DrawerMenu"
        static let doctorHome = "DoctorHome"
        static let hospitalRequest = "HospitalRequest"
        static let DoctorProfile = "DoctorProfile"
        static let procedureFees = "ProcedureFees"
        static let doctorAppointments = "DoctorAppointments"
        static let procedureAppointments = "ProcedureAppoinment"
        static let manageSlots = "ManageSlots"
        static let manageShift = "ManageShift"
    }
    
    enum nibNames {
        static let drawerMenuItemTableCell = "DrawerMenuItemCellTableViewCell"
        static let appointmentListItemTableCell = "AppointmentListItemTableViewCell"
        static let dependableListItemTableCell = "DependableListItemTableViewCell"
        static let availableDoctorTableCell = "AvailableDoctorTableViewCell"
        static let insuranceTableViewCell = "InsuranceTableViewCell"
        static let ratingTableViewCell = "RatingListItemTableViewCell"
        static let fullScreenMessageView = "FullScreenMessageView"
        static let hospitalRequestTableViewCell = "HospitalRequestTableViewCell"
        static let editProfielListItemTableCell = "EditProfileTableViewCell"
        static let educationDetailslListItemTableCell = "EducationDetailTableViewCell"
        static let procedureFeesTableViewCell = "ProcedureFeesTableViewCell"
        static let doctorAppointmentTableViewCell = "DoctorAppointmentTableViewCell"
        static let specialityListItemTableCell = "SpecialityTableViewCell"
        static let genericDetailCustomView = "GenericDetailCustomView"
        static let procedureAppoinmentTableViewCell = "ProcedureAppoinmentTableViewCell"
        static let slotItemTableViewCell = "SlotItemTableViewCell"
        static let shiftItemTableViewCell = "ShiftItemTableViewCell"
        static let shiftStartEndItemTableViewCell = "ShiftStartEndItemTableViewCell"
    }
    
    enum viewControllerIdentifiers {
        static let registrationStepOne = "RegistrationStepOne"
        static let registrationStepTwo = "RegistrationStepTwo"
        static let emailVerification = "EmailVerification"
        static let phoneVerification = "PhoneVerification"
        static let dashboardNavigation = "DashboardNavigation"
        
        static let patientDrawer = "PatientDrawer"
        static let doctorDrawer = "DoctorDrawer"
        static let forgotPasswordEmailScreen  = "ForgotPasswordEmailScreen"
        static let forgotPasswordVerificationScreen = "SCForgotPasswordVerificationViewController"
        static let confirmPasswordViewController = "SCConfirmPasswordViewController"
        static let resetConfirmPasswordViewController = "SCResetConfirmationViewController"
        static let appointmentList = "AppointmentList"
        static let bookAppointment = "BookAppointment"
        static let bookAppointmentNavigation = "BookAppointmentNavigation"
        static let procedureAppointmentNavigation = "ProcedureAppoinmentNavigation"
         static let consultantNoteDetailsNavigation = "ConsultantNoteDetailVC"
        static let appointmentDetails = "AppointmentDetails"
        static let logoutNavigation = "LogoutNavigation"
        static let managmanetDetailViewController = "SCManageDependabaleDetailViewController"
        static let profileInformationViewController = "SCPersonalInformationViewController"
        static let insurancePolicyEditViewController = "SCInsurancePolicyEditViewController"
        static let consultationNotesPatient = "ConsultationNotes"
        static let addressDetailsViewController = "SCAddressDetailsViewController"
        static let availableDoctorsList = "SCAvailableDoctorsList"
        static let requestAppointment = "RequestAppointment"
        static let appointmentSchedule = "AppointmentSchedule"
        static let ratingDetails = "RatingDetails"
        static let phoneChangeViewController = "SCProfileChangePhoneViewController"
        static let emailChangeViewController = "SCProfileEmailChangeViewController"
        static let fullScreenImageView = "FullScreenImageView"
        
        static let doctorDashboardNavigation = "DoctorDashboardNavigation"
        static let doctorDashboardAppointments = "DoctorDashboardAppointments"
        static let doctorDashboard = "DoctorDashboard"
        static let hospitalRequestDetails = "HospitalRequestDetails"
        static let doctorAppointmentDetails = "DoctorAppointmentDetails"
        static let bookedAppointmentDetails = "BookedAppointmentDetails"
        static let confirmedAppointmentDetails = "ConfirmedAppointmentDetails"
        static let missedAppointmentDetails = "MissedAppointmentDetails"
        static let EducationDetails = "EducationDetails"
        static let SpecalityDetails = "SpecalityDetails"
        static let educationDetailsViewController = "SCEducationDetailViewController"
        static let speaclityDetailsViewController = "SCSpecialityDetailViewController"
        static let generalDetailsViewController = "SCGeneralDetailViewController"
        static let createSlotNavigation = "CreateSlotNavigation"
        static let selectAppointmentDateVC = "SelectAppointmentDateVC"
        static let procedureAppoinmentDetailVC = "ProcedureAppoinmentDetailVC"
        static let procedureApproveAppoinmentVC = "ApproveProcedureVC"
        
        
        static let filterSlotNavigation = "FilterSlotNavigation"
        static let changeSlot = "ChangeSlot"
        
        static let shiftsByDateListNavigationn = "ShiftsByDateListNavigation"
        static let shiftListNavigation = "ShiftListNavigation"
        static let createAndEditShift = "CreateAndEditShift"
        static let shiftStartEndDetails = "ShiftStartEndDetails"
        static let shiftSummaryDetail = "ShiftSummaryDetail"
    }
    
    enum cellReuseIdentifiers {
        static let drawerMenuItem = "DrawerMenuItem"
        static let appointmentListItem = "AppointmentListItem"
        static let dependableListItem = "DependableListItem"
        static let availableDoctorListItem = "AvailableDoctorListItem"
        static let insuranceTableListItem = "InsuranceTableViewCell"
        static let ratingListItem = "RatingListItem"
        static let hospitalRequestListItem = "HospitalRequestListItem"
        static let editProfileListItem = "EditProfileTableViewCell"
        static let educationDetailListItem = "EducationDetailCell"
        static let procedureFeesListItem = "ProcedureFeesListItem"
        static let doctorAppointmentListItem = "DoctorAppointmentListItem"
        static let specialityListItem = "SpecialityDetailCell"
        static let procedureAppointmentListItem = "ProcedureAppoinemtTableViewCell"
        
        static let slotListItem = "SlotListItemTableViewCell"
        static let shiftListItem = "ShiftListItemTableViewCell"
        static let ShiftsByDateItem = "ShiftsByDateTableViewCell"
        static let shiftDetailTableCell = "SCShiftDetailTableCell"
        static let shiftAppointmentDetailTableViewCell = "SCShiftAppointmentDetailTableViewCell"
        static let shiftAppointmentListItem = "ShiftAppointmentListItem"
        static let shiftAppointmentListItemHeader = "SCShiftAppointmentDetailHeaderCell"
    }
    
    enum borderWidths: CGFloat {
        case textFiledBorderWidth = 1.0
        case zeroBorderWidth = 0.0
        case medium = 2.0
    }
    
    enum textFieldPadding: CGFloat {
        case textFieldPaddingLeft = 15.0
        case textFieldPaddingLeftSmall = 10.0
        case textFieldPaddingLeftExtraLarge = 30.0
    }
    
    enum radioButtonValues: String {
        case patient = "Patient"
        case doctor = "Doctor"
        case male = "Male"
        case female = "Female"
    }
    
    enum relationTypeValues: String {
        case spouse = "Spouse"
        case child = "Child"
        case parent = "Parent"
        case sibling = "Sibling"
        case other = "Other"
    }
    
    enum generalDetails: String {
        case selectDuration = "Select duration"
        case titleConsulation = "Change Duration - Consulation"
        case titleProcedure = "Change Duration - Procedure"
        case appoinmentNotes = "Note: If you change the slot duration then all the future slots will be deleted permentaly."
        case appoinmentAvailableNotes = "Note: You can not change the slot duration, since you got appoinments. Please contact the support team."
    }
    
    enum editProfile: String {
        case personalInformation = "Personal Information"
        case generalDetails = "General details"
        case educationDetails = "Education details"
        case speciality = "Speciality"
        case address = "Address"
    }
    enum editProfileValues: String {
        case personalInformation = "Personal Information"
        case generalDetails = "General details"
        case educationDetails = "Education details"
        case speciality = "Speciality"
        case address = "Address"
        
        func statusText() -> String {
            switch self {
            case .personalInformation: return "Personal Information"
            case .generalDetails: return "General details"
            case .educationDetails: return "Education details"
            case .speciality: return "Speciality"
            case .address: return "Address"
            }
        }
        
        func statusImage() -> String {
            switch self {
            case .personalInformation: return "profile_info_icon"
            case .generalDetails: return "more"
            case .educationDetails: return "educationdetail"
            case .speciality: return "speciality"
            case .address: return "address_details_icon"
            }
        }
        
    }
    enum apiRequestPath {
        static let login = "login"
        static let countryCode = "country-code"
        static let identity = "govt-ids"
        static let sendVerificvationCode = "change-contact"
        static let profileAvtar = "upload-avatar"
        static let registration = "registration"
        static let verifyContact = "code-verification-change-contact"
        static let verifyAccount = "forgot-password"
        static let codeVerification = "code-verification"
        static let passwordChange = "change-password"
        static let patientDashboardOverallStatus = "patients/dashboard/overall-status"
        static let nextAppointments = "patients/dashboard/next-appointments?date=" //2018-06-30T10%3A30%3A16%2B05%3A30
        static let configuration = "configuration"
        static let manageDependent = "patients/dependents"
        static let cityList = "cities"
        static let specialitiesList = "specialities"
        static let searchAppointments = "patients/search-slots?"
        static let bbokAppointmentRequest = "patients/appointments"
        static let insurancePolicyList = "patients/insurance-policies"
        static let ratingsList = "patients/ratings"
        // 19990909A0002
        static let consultationNoteList = "patients/%@/consultation-notes?date="
        static let consultationNotesDetails = "patients/%@/appointments/%@/consultation-notes"
         static let consultationNotesSearchPharmacy = "appointments/%@/search-pharmacies?"
        static let consultationNotesSearchPharmacyAuthorise = "patients/%@/authorize?"
        
        static let profile = "profiles"
        static let contactChange = "change-contact"
        static let insurancePolicySave = "patients/insurance-policies"
        static let insurancePolicyEdit = "patients/insurance-policies/%@"
        static let insurancePolicyDelete = "patients/insurance-policies"
        static let insuranceCompaniesList = "patients/insurance-companies"
        static let doctorUpcomingAppointments = "doctors/%@/dashboard/up-coming-appointment?date="
        static let doctorDashboardStatus = "doctors/%@/dashboard/status?date="
        static let doctorDashboardDailyChart = "doctors/%@/dashboard/daily-chart?date="
        static let doctorDashboardDayStats = "doctors/%@/dashboard/day-stats?date="
        static let doctorDashboardMonthStats = "doctors/%@/dashboard/month-stats?date="
        static let doctorHospitalRequestsList = "doctor/requests"
        static let sendHospitalRequest = "doctor/requests/"
        static let resendHospitalRequest = "doctor/re-requests/"
        static let procedureFees = "doctors/%@/procedures-fee"
        static let asociateHospitals = "doctors/%@/associtate-hospitals"
        static let procedureTypes = "hospitals/%@/procedures"
        static let createProcedureFee = "doctors/%@/procedures-fee"
        static let doctorAppointments = "doctors/%@/appointments?date=%@"
        static let doctorProcedureAppointments = "doctors/%@/procedure-amount?date=%@"

        static let doctorToReferSearch = "doctor-search?&name=%@"
        static let doctorLabtestSearch = "doctors/%@/appointments/%@/lab-test-group?&name=%@&hospitalId=%@&clinicId=%@"
        static let doctorFindMedicineSearch = "doctors/%@/medicine-search?&brandName=%@&appointmentId=%@&drugs=%@"
        static let doctorMedicineSearch = "doctors/%@/medicine-search?&brandName=%@&appointmentId=%@"
         static let doctorDrugsApi = "drugs?"
        static let doctorSaveConsultationNote = "doctors/%@/appointments/%@/consultation-notes"
        static let clinicSlots = "change-slots?date=%@&doctorId=%@&clinicId=%@"
        static let doctorAppointmentStatusUpodate = "doctors/%@/appointments/%@"
        static let doctorChangeAppointment = "doctors/%@/appointments/%@/change"
        static let degree = "degrees"
        static let specialities = "specialities"
        static let generalDetailConsulation = "doctors/%@/change-slot-duration?slotType=%@"
        static let changeDuration = "doctors/%@/change-duration"
        static let shiftList = "doctors/%@/shifts"
        static let createShift = "doctors/%@/shifts"
        static let deleteShift = "doctors/%@/shifts/%@"
        static let createSlot = "doctors/%@/slots"
        static let filterSlots = "doctors/%@/slots?startDate=%@&endDate=%@"
        static let editSlot = "doctors/%@/slots/%@"
        static let changeProcedureType = "hospitals/%@/clinics/%@/doctors/%@/procedure-fee"
        static let denyProcedureAppoinment = "doctors/%@/appointments/%@"
        static let approveProcedureAppoinment = "doctors/%@/appointments/%@/procedure-amount"
        static let uploadGeneralDetails = "profiles/%@"
        static let shiftsByDate = "doctors/%@/shifts-by-date?date=%@"
        static let shiftDetails = "doctors/%@/shifts-by-date/%@"
        static let shiftSummary = "doctors/%@/shifts-by-date/%@/summary"
        static let startShift = "doctors/%@/shifts/%@/start"
        static let endShift = "doctors/%@/shifts/%@/end"
    }
    
    enum Image: String {
        case whiteEmail = "email_icon_white"
        case whitePassword = "password_icon_white"
        case downArrow = "down_arrow_icon"
        case eye = "eye"
        case drawerIcon = "drawer_icon"
        case backIcon = "back"
        case closeIcon = "close_icon"
        case userProfile = "user_profile"
        case dashboard = "dashboard_icon"
        case bookAppointment = "book_appointment_icon"
        case procedureAppointment = "procedure_appoinment_icon"
        case manageDependant = "manage_dependant_icon"
        case ratingHistory = "rating_history_icon"
        case insurancePolicy = "insurance_policy_icon"
        case editProfile = "edit_profile_icon"
        case consultationNotes = "ConsultationNotes_icon"
        case logout = "logout_icon"
        case calenderBlack = "calender_icon_black"
        case plusImage = "plus"
        case plusButton = "plus-button"
        case image_placeholder = "image_placeholder"
        case uploadButton = "upload_icon"
        case hospitalRequest = "hospital_request_icon"
        case createSlot = "create_slot_icon"
        case filterSlot = "filter_slot_icon"
        case gstInclusive = "gst_inclusive_true"
        case gstNonInclusive = "gst_inclusive_false"
        case procedureFees = "procedure_fees_icon"
        case createShift = "create_shift_icon"
        case shiftStartEnd = "start_end_shift_icon"
        case shiftSummary = "shift_summary_icon"
        
        func imageName() -> String {
            return self.rawValue
        }
        
        func image() -> UIImage {
            return UIImage(named: self.rawValue)!.withRenderingMode(.alwaysTemplate)
        }
    }
    
    enum pageTitles {
        static let registration = "Registration"
        static let emailVerification = "Email Verification"
        static let allergyTitle = "Allergies"
        static let labTestTitle = "Lab Test"
        static let medicinesTitle = "Medicines"
        static let vitalsTitle = "Medicines"
        static let diagnosisTitle = "Diagnosis"
        static let procedureTitle = "Medicines"
        static let adviceTitle = "Medicines"
        
        static let phoneVerification = "Phone Verification"
        static let dashboard = "Dashboard"
        static let procedureAppoinemts = "Procedure Appoinments"
        static let bookAppointment = "Book Appointment"
        static let manageDependant = "Manage Dependant"
        static let ratingHistory = "Rating & History"
        static let insurancePolicy = "Insurance Policy"
        static let editProfile = "Edit Profile"
        static let personalInformation = "Personal Information"
        static let addressDetails = "Address Details"
        static let logout = "Logout"
        static let forgotPassword = "Forgot Password"
        static let login = "Login"
        static let emailVerify = "Email Verification"
       
        static let appointmentDetails = "Appointment Details"
        static let availableDoctors = "Available Doctors"
        static let requestAppointment = "Request Appointment"
        static let paymentDetails = "Payment Page"
        static let appointmentSchedule = "Appointments Schedule"
        static let paymentHistoryStatus = "Payment History"
        static let paymentHistoryDetails = "Payment History Details"
        static let consultationNotes = "Consultation Notes"
        static let consultationNotesDetail = "Consultation Notes Details"
        static let authoriseHistory = "Authorise History"
        static let updatePhoneNumber = "Update Phone Number"
        static let updateEmail = "Update Email"
        static let appointments = "Appointments"
        static let procedureAppointments = "Procedure Appointments"
        static let hospitalRequest = "Hospital Requests"
        static let createSlots = "Create Slots"
        static let filterSlots = "Filter Slots"
        static let editSlot = "Edit Slot"
        static let hospitalDetails = "Hospital Details"
        static let EducationDetails = "Education Details"
        static let speaciltiyDetails = "Speciality Details"
        static let GeneralDetails = "General Details"
        static let procedureFees = "Procedure Fees"
        static let addProcedureFees = "Add Procedure Fees"
        static let selectAppointmentDates = "Select Appointment Dates"
        static let procedureDetails = "Procedure Details"
        static let shiftList = "Shift List"
        static let createShift = "Create Shift"
        static let shiftStartEnd = "Shift Start/End"
        static let shiftSummary = "Summary"
        static let shiftSummaryDetails = "Summary Details"
        static let editShift = "Edit Shift"
        static let approveProcedure = "Approve Procedure"
        static let shiftStart = "Shift Start"
        static let shiftEnd = "Shift End"
    }
    
    enum validationMessages {
        static let loginUserNameValidationMessage = "Please enter username"
        static let loginPassswordValidationMessage = "Please enter password"
        static let registrationFirstNameValidationMessage = "Please enter firstname"
        static let registrationLastNameValidationMessage = "Please enter lastname"
        static let registrationDateOfBirthValidationMessage = "Please enter date of birth"
        static let registrationUserTypeValidationMessage = "Please select usertype"
        static let registrationEmailValidationMessage = "Please enter email"
        static let registrationValidEmailValidationMessage = "Please enter valid email"
        static let registrationPhoneValidationMessage = "Please enter phonenumber"
        static let registrationValidPhoneMessage = "Please enter valid phonenumber"
        static let registrationPasswordValidationMessage = "Please enter password"
        static let registrationConfirmPasswordValidationMessage = "Please enter confirm password"
        static let registrationIdentificationValidationMessage = "Please enter identification number"
        static let registrationCountryCodeValidationMessage = "Please enter country code"
        static let registrationSelectIdentityValidationMessage = "Please select identity"
        static let loginVerificationCodeValidationMessage = "Please enter verification code"
        static let forgotPasswordNewPasswordValidationMessage = "Please enter new password"
        static let forgotPasswordConfirmPasswordValidationMessage = "Please enter confrim Password"
        static let searchAppointmentValidationMessage = "Please Select date to search appointments"
        static let searchAppointmentTypeMessage = "Please Select Appointment-Type to search appointments"
        static let allFieldsValidationMessage = "All fields are mandatory"
        static let dependentEmptyNameMessage = "Please enter name"
        static let dependentEmptyLastNameMessage = "Please enter lastname"
        static let dependentEmptyDateMessage = "Please enter dateofbirth"
        static let dependentEmptyGnederMessage = "Please enter gender"
        static let procedureTypeRequired = "Procedure Type is required."
        static let successfulUploadImage = "Successfullly Uploaded."
        static let failedUploadImage = "Failed to upload image."
        static let actualStartTimeRequired = "Actual shift start is required. Please review the fields again."
        static let actualEndTimeRequired = "Actual shift end is required. Please review the fields again."
    }
    
    enum progressMessages {
        static let verifyingEmail = "Verifying Email Address"
        static let verifyingAccountEmail = "Sending Verification Code to email"
        static let resendCodeVarification = "Resend varification code"
        static let verifyingPhone = "Verifying Phone Number"
        static let sendingVerificationCode = "Verifying"
        static let changingPasswordCode = "Changing Password"
        static let changingPhoneNumber = "Sending OTP to Phone Number"
        static let changingEmail = "Sending OTP to Email"
    }
    
    /// Date Format type
    enum DateFormatType {
        static let time = "HH:mm:ss"
        static let  dateWithTime = "yyyy-MM-dd HH:mm:ss"
        static let  date = "yyyy-MM-dd"
        static let  appointment = "MMM d, yyyy"
        static let  appointmentAPI = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        static let  slotAPI = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        static let  slotTime = "hh:mm a"
        static let  slotDateTime = "EEEE, MMM d, yyyy - hh:mm a"
        static let  slotDate = "EEEE, MMM d, yyyy"
        static let  shiftAPI = "yyyy-MM-dd'T'00:00:00ZZZZZ"
        static let appointmentListItemStart = "EEE d MMM 'at' hh:mm a"
        static let appointmentListItemEnd = "' to' hh:mm a"
        static let currentMonthSymbol = "MMM"
        static let currentMonth = "MM"
        static let currentYear = "yyyy"
    }

    enum GeneralType: String {
        case procedureType = "P"
        case consultationType = "C"
    }
    
    enum alertMessages {
        static let slotDeleteConfirmation = "Do you want to disable this slot?"
        static let shiftDeleteConfirmation = "Do you want to delete the shift?"
        static let callDicConnectMessage = "Call is active so please disconnect your call"
    }
    
    enum alertButtonTitles {
        static let agree = "AGREE"
        static let disagree = "DISAGREE"
    }
}

