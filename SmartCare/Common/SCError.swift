struct SCError: Error {
    enum ErrorType {
        case inCompleteForm
        case serverError
    }
    let error: ErrorType
    let localizedDescription: String
}
