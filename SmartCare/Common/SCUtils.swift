//
//  SCUtils.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 08/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

func initiateCall(forNumber number:String) {
    
    
    if let phoneURL = URL(string: "tel://" + number) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(phoneURL)
        } else {
            // Fallback on earlier versions
            UIApplication.shared.openURL(phoneURL)
        }
    } else {
        let errorAlert = UIAlertController(title: "Sorry!", message: "Invalid Phone number. Can not make Call", preferredStyle: .alert)
        errorAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
    }
  
}
