//
//  SCProgressView.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 29/08/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import SVProgressHUD

class SCProgressView {
    
    static func show(withMessage statusMessage: String = "") {
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
            SVProgressHUD.setRingThickness(3.0)
            SVProgressHUD.setRingRadius(10)
            SVProgressHUD.setCornerRadius(6)
            SVProgressHUD.setFont(UIFont.preferredFont(forTextStyle: UIFont.TextStyle.caption1))
            SVProgressHUD.setBackgroundColor(UIColor.darkGray)
            SVProgressHUD.setForegroundColor(UIColor.white)
            statusMessage.isEmpty ? SVProgressHUD.show() : SVProgressHUD.show(withStatus: statusMessage)
        }
    }
    
    static func hide() {
        //Hide on main thread
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
}

