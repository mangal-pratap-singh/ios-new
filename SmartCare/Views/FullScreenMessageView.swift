//
//  FullScreenMessageView.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 25/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class FullScreenMessageView: UIView {

    @IBOutlet weak var messageLabel: UILabel!
    
    func setupPopup(_ frame: CGRect, message: String) {
        self.frame = frame
        self.messageLabel.text = message
    }
}
