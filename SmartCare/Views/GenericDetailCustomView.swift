//
//  GenericDetailCustomView.swift
//  SmartCare
//
//  Created by synerzip on 29/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SwiftyJSON

protocol genericPopUpDelegate {
    func proceedButtonActionDelegate(slotDurationObject:[String : Any])
    func cancelButtonActionDelegate(sender:UIButton)
}


class GenericDetailCustomView: UIView {
    
    @IBOutlet weak var slotTextField: UITextField!
    @IBOutlet weak var changeDurationTitelLable: UILabel!
    @IBOutlet weak var appoinmentLabel: UILabel!
    @IBOutlet weak var featureSlotLabel: UILabel!
    @IBOutlet weak var notesLabel: UILabel!
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var popViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cancelbuttonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var changeSlotLbl: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var sepraterView2: UIView!
    @IBOutlet weak var sepraterView1: UIView!
    
    var consulationDurationArray = ["3","5","10","15"]
    var procedureDurationArray = ["10","20","30","45","60"]
    var selectedIdentityIndex = 0
    var type = ""
    var delegate : genericPopUpDelegate?
    

    class func instanceFromNib(frame:CGRect) -> GenericDetailCustomView{
        let dialogView = UINib(nibName:SCAppConstants.nibNames.genericDetailCustomView, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! GenericDetailCustomView
        dialogView.frame = CGRect(x: 0, y:frame.origin.y-64, width: frame.width, height: frame.height)
        return dialogView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.slotTextField.delegate = self
         slotTextField.SetRightSideImage(with: SCAppConstants.Image.downArrow.image())
    }
    
    func  getConsultationDurationDetails(type:String) {
        SCProgressView.show()
        self.type = type
        SCDoctorProfileService.getGeneralDetailsData(withType:type, onComplete: { [weak self] (generalDetails, success) in
            SCProgressView.hide()
            if success {
                self?.updateView(statusObject: generalDetails)
            }
        }) { (errorMessage) in
            SCProgressView.hide()
            print("Error Degree API - \(errorMessage)")
        }
    }
    
    func updateView(statusObject:JSON) {
        var appointments = 0
        var featureSlots = 0
        if(self.type == SCAppConstants.GeneralType.consultationType.rawValue){
            self.changeDurationTitelLable.text = SCAppConstants.generalDetails.titleConsulation.rawValue
        }else if(self.type == SCAppConstants.GeneralType.procedureType.rawValue){
            self.changeDurationTitelLable.text = SCAppConstants.generalDetails.titleProcedure.rawValue
        }
        appointments = statusObject["appointments"].intValue
        featureSlots = statusObject["futureSlots"].intValue
        self.appoinmentLabel.text = "You have \(appointments) appointments"
        self.featureSlotLabel.text = "You have \(featureSlots) future slots"
        
        if(appointments == 5){
            self.notesLabel.text = SCAppConstants.generalDetails.appoinmentNotes.rawValue
            self.slotTextField.isHidden = false
            self.changeSlotLbl.isHidden = false
            self.sepraterView2.isHidden = false
            self.proceedButton.isEnabled = true
            //Set Value From Here
            popViewHeightConstraint.constant = 339;
            cancelbuttonTopConstraint.constant = 113
        }
        else{
            self.notesLabel.text = SCAppConstants.generalDetails.appoinmentAvailableNotes.rawValue
            self.slotTextField.isHidden = true
            self.proceedButton.isEnabled = false
            self.changeSlotLbl.isHidden = true
            self.sepraterView2.isHidden = true
            //Set Value From Here
            popViewHeightConstraint.constant = 240;
            cancelbuttonTopConstraint.constant = 5
        }
    }
    
    @IBAction func proccedButtonAction(_ sender: Any) {
        let slotObject = [
            "slotDuration":self.slotTextField.text ?? "",
            "slotType":self.type
            ] as [String : Any]
        delegate?.proceedButtonActionDelegate(slotDurationObject:slotObject)
    }
    
    @IBAction func cancelButtonAction(sender: AnyObject)
    {
        delegate?.cancelButtonActionDelegate(sender: sender as! UIButton)
    }
    
    func showConsulationDataPicker(textField:UITextField , pickerData:[String]) {
        ActionSheetStringPicker.show(withTitle: SCAppConstants.generalDetails.selectDuration.rawValue, rows: pickerData, initialSelection: 0, doneBlock: { picker, selectedIndex, selectedValue in
            self.selectedIdentityIndex = selectedIndex
            if let aValue = selectedValue {
                textField.text = aValue as? String
            }
        }, cancel: { picker in
        }, origin: textField)
    }
}
extension GenericDetailCustomView: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        if (textField == slotTextField) {
            if(self.type == SCAppConstants.GeneralType.consultationType.rawValue){
               self.showConsulationDataPicker(textField: textField, pickerData:self.consulationDurationArray)
            }else if(self.type == SCAppConstants.GeneralType.procedureType.rawValue){
                 self.showConsulationDataPicker(textField: textField, pickerData:self.procedureDurationArray)
            }
            return false
        }
        return true
    }
}
