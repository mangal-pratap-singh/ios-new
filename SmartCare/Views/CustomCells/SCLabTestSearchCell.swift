//
//  SCLabTestSearchCell.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 16/04/20.
//  Copyright © 2020 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCLabTestSearchCell: UITableViewCell {
    
    
    @IBOutlet weak var lbl_labTestName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
