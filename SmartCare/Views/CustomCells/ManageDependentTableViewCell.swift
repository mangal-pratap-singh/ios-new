//
//  ManageDependentTableViewCell.swift
//  SmartCare
//
//  Created by synerzip on 07/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class ManageDependentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var picUrl: UIImageView!
    @IBOutlet weak var lblRelationship: UILabel!
    @IBOutlet weak var dependentName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configureCell(WithDependnet dependent: Dependent) {
        dependentName.text =  "\((dependent.firstName) ?? "") \((dependent.lastNmae) ?? "")"
        lblRelationship.text = dependent.relationType?.statusText()
        containerView.backgroundColor = dependent.status == false ? UIColor.SCDisableDependnetColor() : UIColor.white
        picUrl.layer.cornerRadius = picUrl.frame.height / 2
        picUrl.clipsToBounds = true
        picUrl.image = dependent.gender == "M" ? UIImage(named:"male_placeholder") : UIImage(named:"female_placeholder")
    }
}
