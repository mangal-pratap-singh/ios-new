//
//  AppointmentListItemTableViewCell.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 04/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class AppointmentListItemTableViewCell: UITableViewCell {

    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var hospitalNameLabel: UILabel!
    @IBOutlet weak var appointmentTimeLabel: UILabel!
    @IBOutlet weak var patientNameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var doctorAvtarImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        doctorAvtarImageView.layer.cornerRadius = doctorAvtarImageView.frame.width/2
        doctorAvtarImageView.layer.masksToBounds = true
    }
    
    func configureCell(WithAppointment appointment: Appointment) {
        
        if UserProfile.isDoctor() {
            let patientName = appointment.patient?.name ?? ""
            let patientAge = (appointment.patient?.age ?? "") + " Yrs."
            doctorNameLabel.text = "\(patientName) (\(patientAge))"
            patientNameLabel.text = "+\(appointment.patient?.countryCode ?? "") \(appointment.patient?.phoneNumber ?? "")"
        } else {
            doctorNameLabel.text = appointment.doctor?.name
            patientNameLabel.text = appointment.patient?.name
        }
        
        if let hospitalName = appointment.hospital?.name, let clicnicName = appointment.clinic?.name {
            hospitalNameLabel.text = hospitalName + ", " + clicnicName
        }
        
        
        print("-------- Hello ------------ status Label===\(appointment.status?.statusText())")
        statusLabel.text = appointment.status?.statusText()
        statusLabel.textColor = appointment.status?.statusColor()
        if let startDate = appointment.startTime, let endDate = appointment.endTime {
           appointmentTimeLabel.text = startDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointment) + " at " + startDate.toTimeString() + " - " + endDate.toTimeString()
        }
        
        if let imageBaseURL = AppInfo.getAppInfo()?.s3Url, let avtarImageURL = appointment.doctor?.avtarImageURL {
            // Load Image From URLwith Caching
            let doctorAvtarImageURLString = imageBaseURL + avtarImageURL
            if let url = URL(string: doctorAvtarImageURLString) {
                doctorAvtarImageView.sd_setImage(with: url, placeholderImage: SCAppConstants.Image.userProfile.image())
            }
        }
    }
}
