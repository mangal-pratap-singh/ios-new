//
//  EditProfileTableViewCell.swift
//  SmartCare
//
//  Created by synerzip on 05/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class EditProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var namelabel: UILabel!
   
    func configureCell(image:String,name:String) {
        iconImageView.clipsToBounds = true
        iconImageView.image = UIImage(named: image)
        namelabel.text = name
    }
    
}
