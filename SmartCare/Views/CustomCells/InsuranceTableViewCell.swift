//
//  InsuranceTableViewCell.swift
//  SmartCare
//
//  Created by synerzip on 15/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class InsuranceTableViewCell: UITableViewCell {

    @IBOutlet weak var policyCompanyNameLabel: UILabel!
    @IBOutlet weak var policyNumberLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(WithInsurance insurance: Insurance) {
        self.policyCompanyNameLabel.text = insurance.company ?? ""
        self.policyNumberLabel.text = insurance.policyNum ?? ""
        self.endDateLabel.text = insurance.endDate ?? ""
        self.startDateLabel.text = insurance.startDate ?? ""
    }
}
