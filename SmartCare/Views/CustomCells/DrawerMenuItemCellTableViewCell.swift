//
//  DrawerMenuItemCellTableViewCell.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 01/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class DrawerMenuItemCellTableViewCell: UITableViewCell {

    @IBOutlet weak var optionLabel: UILabel!
    @IBOutlet weak var optionImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configureCell(WithMenu title:String, imageName: String) {
        self.selectionStyle = .none
        optionLabel.text = title
        optionImageView.image = UIImage(named: imageName)
    }
}
