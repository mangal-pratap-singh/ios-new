//
//  FindBrandSearchCollectionCell.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 15/04/20.
//  Copyright © 2020 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class FindBrandSearchCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl_titleName: UILabel!
    @IBOutlet weak var lbl_selectLine: UILabel!
}
