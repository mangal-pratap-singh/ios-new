//
//  DoctorAppointmentTableViewCell.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 18/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class DoctorAppointmentTableViewCell: UITableViewCell {

    @IBOutlet weak var patientNameLabel: UILabel!
    @IBOutlet weak var appointmentTimeLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var apoointmentTypeButton: UIButton!
    @IBOutlet weak var paymentStatusLabel: UILabel!
    @IBOutlet weak var consultationFeeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        statusLabel.layer.masksToBounds = true
    }

    func configureCell(WithAppointment appointment: DoctorAppointment) {
        self.selectionStyle = .none
        patientNameLabel.text = appointment.patient?.name
        statusLabel.text = appointment.status?.statusText()
        statusLabel.backgroundColor = appointment.status?.statusColor()
        paymentStatusLabel.text = appointment.payment?.status?.statusText()
        consultationFeeLabel.text = appointment.payment?.amount
        apoointmentTypeButton.setTitle(appointment.type?.text(), for: .normal)
        apoointmentTypeButton.setImage(appointment.type?.image(), for: .normal)
        if let startDate = appointment.startTime, let endDate = appointment.endTime {
            appointmentTimeLabel.text = startDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentListItemStart) + endDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentListItemEnd)
        }
    }
}
