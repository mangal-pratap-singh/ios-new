//
//  ProcedureFeesTableViewCell.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 15/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class ProcedureFeesTableViewCell: UITableViewCell {

    @IBOutlet weak var hospitalNameLabel: UILabel!
    @IBOutlet weak var procedureTypeLabel: UILabel!
    @IBOutlet weak var procedureFeeLabel: UILabel!
    @IBOutlet weak var gstInclusiveImageview: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(WithProcedureFees feesItem: ProcedureFeeItem) {
        hospitalNameLabel.text = "\(feesItem.hospital?.name ?? "") / \(feesItem.clinic?.name ?? "")"
        procedureTypeLabel.text = feesItem.procedureType?.name ?? ""
        procedureFeeLabel.text = feesItem.procedureFee?.charges ?? ""
        
        if let gstInclusive = feesItem.procedureFee?.gstInclusive {
            gstInclusiveImageview.image = gstInclusive ? SCAppConstants.Image.gstInclusive.image() : SCAppConstants.Image.gstNonInclusive.image()
        }
    }
}
