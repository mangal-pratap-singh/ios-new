//
//  EducationDetailTableViewCell.swift
//  SmartCare
//
//  Created by synerzip on 06/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
class EducationDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var degreeName: UILabel!
    func configureCell(WithDegreeItem degreeItem: Degree)  {
        degreeName.text = degreeItem.name
    }
}
