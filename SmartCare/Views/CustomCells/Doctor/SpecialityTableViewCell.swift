//
//  SpecialityTableViewCell.swift
//  SmartCare
//
//  Created by synerzip on 25/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SpecialityTableViewCell: UITableViewCell {

    @IBOutlet weak var specialityName: UILabel!

    func configureCell(WithSpecialityItem specialityItem: Speciality)  {
        specialityName.text = specialityItem.name
    }
}
