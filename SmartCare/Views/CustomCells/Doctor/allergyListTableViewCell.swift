//
//  allergyListTableViewCell.swift
//  SmartCare
//
//  Created by Rediflex PvtLtd on 01/07/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class allergyListTableViewCell: UITableViewCell {

    @IBOutlet weak var allergyType: UILabel!
    @IBOutlet weak var allergyDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
