import RxCocoa
import RxSwift

class ShiftItemTableViewCell: UITableViewCell {

    @IBOutlet weak var hospitalNameLabel: UILabel!
    @IBOutlet weak var fromTimeLabel: UILabel!
    @IBOutlet weak var toTimeLabel: UILabel!
    @IBOutlet weak var shiftNameLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    private let disposeBag = DisposeBag()
    private let currentShift: Variable<Shift?> = Variable<Shift?>(nil)
    var edit: ((Shift) -> ())?
    var delete: ((Shift) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let currentShiftObservable = currentShift.asObservable()
        currentShiftObservable.map({ $0?.name }).bind(to: shiftNameLabel.rx.text).disposed(by: disposeBag)
        currentShiftObservable.map({ "\($0?.hospital?.name ?? "") / \($0?.clinic?.name ?? "")" }).bind(to: hospitalNameLabel.rx.text).disposed(by: disposeBag)
        currentShiftObservable.map({ $0?.startTime?.toDateString(withFormatter: SCAppConstants.DateFormatType.slotTime) }).bind(to: fromTimeLabel.rx.text).disposed(by: disposeBag)
        currentShiftObservable.map({ $0?.endTime?.toDateString(withFormatter: SCAppConstants.DateFormatType.slotTime) }).bind(to: toTimeLabel.rx.text).disposed(by: disposeBag)
        
        editButton.rx.tap.share()
            .bind { [weak self] in
                if let `self` = self, let shift = self.currentShift.value  {
                    self.edit?(shift)
                }
            }
            .disposed(by: disposeBag)
        
        deleteButton.rx.tap
            .bind { [weak self] in
                if let `self` = self, let shift = self.currentShift.value {
                    self.delete?(shift)
                }
            }
            .disposed(by: disposeBag)
    }
    
    func configureCell(withShift shift: Shift) {
        self.selectionStyle = .none
        currentShift.value = shift
    }
}
