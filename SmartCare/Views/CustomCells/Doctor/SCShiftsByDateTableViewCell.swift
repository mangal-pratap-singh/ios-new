import RxCocoa
import RxSwift

class SCShiftsByDateTableViewCell: UITableViewCell {
    
    @IBOutlet weak var hospitalNameLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var actualStartTimeLabel: UILabel!
    @IBOutlet weak var actualEndTimeLabel: UILabel!
    @IBOutlet weak var shiftNameLabel: UILabel!
    
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var endButton: UIButton!
    @IBOutlet weak var summaryButton: UIButton!
    
    var start: ((Shift) -> ())?
    var end: ((Shift) -> ())?
    var summary: ((Shift) -> ())?
    
    private let currentShift: Variable<Shift?> = Variable<Shift?>(nil)
    private let disposeBag = DisposeBag()
    private var cellType = BehaviorRelay<ShiftListState>(value: .startEnd)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        startButton.rx.tap.bind(onNext: { [weak self] in
            if let `self` = self, let shift = self.currentShift.value {
                self.start?(shift)
            }
        }).disposed(by: disposeBag)
        
        endButton.rx.tap.bind(onNext: { [weak self] in
            if let `self` = self, let shift = self.currentShift.value {
                self.end?(shift)
            }
        }).disposed(by: disposeBag)
        
        summaryButton.rx.tap.bind(onNext: { [weak self] in
            if let `self` = self, let shift = self.currentShift.value {
                self.summary?(shift)
            }
        }).disposed(by: disposeBag)
        
        let currentShiftObservable = currentShift.asObservable()
        currentShiftObservable.map({ $0?.name }).bind(to: shiftNameLabel.rx.text).disposed(by: disposeBag)
        currentShiftObservable.map({ "\($0?.hospital?.name ?? "") / \($0?.clinic?.name ?? "")" }).bind(to: hospitalNameLabel.rx.text).disposed(by: disposeBag)
        currentShiftObservable.map({ $0?.startTime?.toDateString(withFormatter: SCAppConstants.DateFormatType.slotTime) }).bind(to: startTimeLabel.rx.text).disposed(by: disposeBag)
        currentShiftObservable.map({ $0?.endTime?.toDateString(withFormatter: SCAppConstants.DateFormatType.slotTime) }).bind(to: endTimeLabel.rx.text).disposed(by: disposeBag)
        currentShiftObservable.map({ $0?.actualStartTime?.toDateString(withFormatter: SCAppConstants.DateFormatType.slotTime) }).bind(to: actualStartTimeLabel.rx.text).disposed(by: disposeBag)
        currentShiftObservable.map({ $0?.actualEndTime?.toDateString(withFormatter: SCAppConstants.DateFormatType.slotTime) }).bind(to: actualEndTimeLabel.rx.text).disposed(by: disposeBag)
        currentShiftObservable.map({ $0?.actualStartTime == nil }).bind(to: startButton.rx.isEnabled).disposed(by: disposeBag)
        currentShiftObservable.map({ ($0?.actualStartTime == nil) ? 1.0 : 0.5 }).bind(to: startButton.rx.alpha).disposed(by: disposeBag)
        currentShiftObservable.map({ $0?.actualStartTime != nil && $0?.actualEndTime == nil }).bind(to: endButton.rx.isEnabled).disposed(by: disposeBag)
        currentShiftObservable.map({ ($0?.actualStartTime != nil && $0?.actualEndTime == nil) ? 1.0 : 0.5 }).bind(to: endButton.rx.alpha).disposed(by: disposeBag)
        
        cellType.map({ $0 == .summary }).bind(to: startButton.rx.isHidden).disposed(by: disposeBag)
        cellType.map({ $0 == .summary }).bind(to: endButton.rx.isHidden).disposed(by: disposeBag)
        cellType.map({ $0 == .startEnd }).bind(to: summaryButton.rx.isHidden).disposed(by: disposeBag)
    }
    
    func configureCell(with shift: Shift, for type: ShiftListState) {
        cellType.accept(type)
        selectionStyle = .none
        startTimeLabel.text = "--"
        endTimeLabel.text = "--"
        actualStartTimeLabel.text = "--"
        actualEndTimeLabel.text = "--"
        currentShift.value = shift
    }
}

