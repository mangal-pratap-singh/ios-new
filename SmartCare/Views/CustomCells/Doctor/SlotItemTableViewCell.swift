//
//  SlotItemTableViewCell.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 13/12/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

protocol SlotActionsDelegate {
    func enableButtonClickedForSlot(slot:SlotItem)
    func disableButtonClickedForSlot(slot:SlotItem)
    func changeButtonClickedForSlot(slot:SlotItem)
}

class SlotItemTableViewCell: UITableViewCell {

    @IBOutlet weak var hospitalNameLabel: UILabel!
    @IBOutlet weak var slotTimeLabel: UILabel!
    @IBOutlet weak var slotTypeLabel: UILabel!
    @IBOutlet weak var slotStatusLabel: UILabel!
    @IBOutlet weak var appointmemntStausLabel: UILabel!
    
    @IBOutlet weak var slotActionButton: UIButton!
    @IBOutlet weak var changeSlotButton: UIButton!
    
    var slotActionDelegate: SlotActionsDelegate?
    var currentSlotItem: SlotItem?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func slotActionButtonClicked(_ sender: UIButton) {
        if let slotItem = currentSlotItem {
            slotItem.status ? slotActionDelegate?.disableButtonClickedForSlot(slot: slotItem) : slotActionDelegate?.enableButtonClickedForSlot(slot: slotItem)
        }
    }
    
    @IBAction func changeSlotButtonClicked(_ sender: UIButton) {
        if let slotItem = currentSlotItem {
            slotActionDelegate?.changeButtonClickedForSlot(slot: slotItem)
        }
    }
    
    func configureCell(withSlotItem slotItem: SlotItem) {
        currentSlotItem = slotItem
        hospitalNameLabel.text = slotItem.hospital?.name
        if let startTime = slotItem.startTime?.toDateString(withFormatter: SCAppConstants.DateFormatType.slotTime), let endTime = slotItem.endTime?.toDateString(withFormatter: SCAppConstants.DateFormatType.slotTime) {
            slotTimeLabel.text = startTime + " - " + endTime
        }
        
        slotStatusLabel.text = slotItem.status ? "ENABLED" : "DISABLED"
        slotActionButton.setTitle(slotItem.status ? "DISABLE SLOT" : "ENABLE SLOT", for: .normal)
        
        if let slotType = slotItem.slotType {
            slotTypeLabel.text = slotType.text()
            changeSlotButton.isHidden = true
        } else if let consultationType = slotItem.consultationType {
            slotTypeLabel.text = consultationType.text()
            changeSlotButton.isHidden = !slotItem.status
        }
        
        appointmemntStausLabel.text = slotItem.status ? slotItem.appointmentStatus?.statusText() ?? "Free" : "--"
        appointmemntStausLabel.textColor = slotItem.status ? UIColor.white : UIColor.black
        appointmemntStausLabel.textAlignment = slotItem.status ? .center : .left
        appointmemntStausLabel.backgroundColor = slotItem.status ? (slotItem.appointmentStatus != nil ? slotItem.appointmentStatus?.statusColor() : UIColor.lightGray) : UIColor.clear
    }
    
}
