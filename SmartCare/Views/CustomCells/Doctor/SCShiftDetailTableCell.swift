import RxCocoa
import RxSwift

class SCShiftDetailTableCell: UITableViewCell {
    @IBOutlet weak var shiftActualTimingLabel: UILabel!
    @IBOutlet weak var shiftDateLabel: UILabel!
    @IBOutlet weak var shiftNameLabel: UILabel!
    @IBOutlet weak var shiftTimingLabel: UILabel!
    @IBOutlet weak var shiftHospitalLabel: UILabel!
    
    private let currentShift = BehaviorRelay<Shift?>(value: nil)
    private let date = BehaviorRelay<Date?>(value: nil)
    
    private let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        currentShift.asDriver()
            .map({ $0?.name})
            .drive(shiftNameLabel.rx.text).disposed(by: disposeBag)
        currentShift.asDriver()
            .map({ "\(($0?.hospital?.name ?? "")) / \(($0?.clinic?.name ?? ""))" })
            .drive(shiftHospitalLabel.rx.text).disposed(by: disposeBag)
        currentShift.asDriver().filterNil()
            .map({
                "\($0.startTime?.toDateString(withFormatter: SCAppConstants.DateFormatType.slotTime) ?? "") - \($0.endTime?.toDateString(withFormatter: SCAppConstants.DateFormatType.slotTime) ?? "")"
            })
            .drive(shiftTimingLabel.rx.text).disposed(by: disposeBag)
        currentShift.asDriver().filterNil()
            .map({
                "\($0.actualStartTime?.toDateString(withFormatter: SCAppConstants.DateFormatType.slotTime) ?? "") - \($0.actualEndTime?.toDateString(withFormatter: SCAppConstants.DateFormatType.slotTime) ?? "")"
            })
            .drive(shiftActualTimingLabel.rx.text).disposed(by: disposeBag)
        date.asDriver().filterNil()
            .map({ $0.toDateString(withFormatter: SCAppConstants.DateFormatType.appointment) })
            .drive(shiftDateLabel.rx.text)
            .disposed(by: disposeBag)
    }
    
    func configureCell(with shift: Shift?, andDate date: Date) {
        self.selectionStyle = .none
        currentShift.accept(shift)
        self.date.accept(date)
    }
}
