//
//  HospitalRequestTableViewCell.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 04/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class HospitalRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var hospitalNameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var emailAddressLabel: UILabel!
    @IBOutlet weak var requestByLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        statusLabel.layer.masksToBounds = true
    }

    func configureCell(WithHospitalRequest request: HospitalRequest) {
        hospitalNameLabel.text = request.hospitalName
        phoneNumberLabel.text = request.hospitalAdmin!.countryCode + "-" + request.hospitalAdmin!.phoneNo
        emailAddressLabel.text = request.hospitalAdmin?.emailId
        locationLabel.text = request.location
        requestByLabel.text = request.requestedBy
        statusLabel.text = request.status?.statusText()
        statusLabel.backgroundColor = request.status?.statusColor()
    }
}
