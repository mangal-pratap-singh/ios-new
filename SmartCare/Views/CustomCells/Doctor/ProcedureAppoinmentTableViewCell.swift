//
//  ProcedureAppoinmentTableViewCell.swift
//  SmartCare
//
//  Created by synerzip on 14/12/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class ProcedureAppoinmentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var patientNameLabel: UILabel!
    @IBOutlet weak var statuslbl: UILabel!
    @IBOutlet weak var hospitalNameLabel: UILabel!
    @IBOutlet weak var appointmentTimeLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        statuslbl.layer.masksToBounds = true
        self.selectionStyle = UITableViewCell.SelectionStyle.none
    }
    
    func configureCell(WithAppointment appointment: DoctorAppointment) {
        patientNameLabel.text = appointment.patient?.name
        statusLabel.text = appointment.status?.statusText()
        statusLabel.backgroundColor = appointment.status?.statusColor()
        hospitalNameLabel.text = appointment.hospital?.name
        if let startDate = appointment.startTime, let endDate = appointment.endTime {
            appointmentTimeLabel.text = startDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointment) + " at " + startDate.toTimeString() + " - " + endDate.toTimeString()
        }
    }
    
}
