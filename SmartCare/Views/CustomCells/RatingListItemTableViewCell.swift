//
//  RatingListItemTableViewCell.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 17/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class RatingListItemTableViewCell: UITableViewCell {

    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var hospitalNameLabel: UILabel!
    @IBOutlet weak var appointmentTimeLabel: UILabel!
    @IBOutlet weak var specialityLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var doctorAvtarImageView: UIImageView!
    @IBOutlet weak var cosmosRatingsView: CosmosView!
    override func awakeFromNib() {
        super.awakeFromNib()
       // cosmosRatingsView.rating = 0.0
        doctorAvtarImageView.layer.cornerRadius = doctorAvtarImageView.frame.width/2
        doctorAvtarImageView.layer.masksToBounds = true
    }
    
    func configureCell(WithAppointment appointment: Appointment) {
        doctorNameLabel.text = appointment.doctor?.name
        if let hospitalName = appointment.hospital?.name, let clicnicName = appointment.clinic?.name {
            hospitalNameLabel.text = hospitalName + " - " + clicnicName
        }
        specialityLabel.text = (appointment.doctor?.degree ?? "") + " - " + (appointment.doctor?.speciality ?? "")
        statusLabel.text = appointment.status?.statusText()
       statusLabel.textColor = appointment.status?.statusColor()
        if let startDate = appointment.startTime, let endDate = appointment.endTime {
            appointmentTimeLabel.text = startDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointment) + " at " + startDate.toTimeString() + " - " + endDate.toTimeString()
        }
        
        if let imageBaseURL = AppInfo.getAppInfo()?.s3Url, let avtarImageURL = appointment.doctor?.avtarImageURL {
            // Load Image From URLwith Caching
            let doctorAvtarImageURLString = imageBaseURL + avtarImageURL
            if let url = URL(string: doctorAvtarImageURLString) {
                doctorAvtarImageView.sd_setImage(with: url, placeholderImage: SCAppConstants.Image.userProfile.image())
            }
        }
        //MARK: man singh comment here
       // TODO:
        //Show Star Ratings
        //cosmosRatingsView.rating = 9.00
        cosmosRatingsView.rating = 0.0
        if let ratings = appointment.ratings?.count {
            print("Rating Show===\(ratings)")
            cosmosRatingsView.rating = Double(ratings)
        }
    }
}
