//
//  SCBrandSearchDrugsCell.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 14/04/20.
//  Copyright © 2020 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCBrandSearchDrugsCell: UITableViewCell {
    
    @IBOutlet weak var lbl_drugsName: UILabel!
    
    @IBOutlet weak var lbl_quantity: UILabel!
    
    @IBOutlet weak var lbl_units: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
      }

}
