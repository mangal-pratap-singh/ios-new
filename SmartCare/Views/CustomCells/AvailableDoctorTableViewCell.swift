//
//  AvailableDoctorTableViewCell.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 13/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class AvailableDoctorTableViewCell: UITableViewCell {

    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var hospitalNameLabel: UILabel!
    @IBOutlet weak var doctorSpecialityLabel: UILabel!
    @IBOutlet weak var doctorAvtarImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        doctorAvtarImageView.layer.cornerRadius = doctorAvtarImageView.frame.width/2
        doctorAvtarImageView.layer.masksToBounds = true
    }
    
    func configureCell(WithAppointmentSlot appointmentSlot: AppointmentSlot) {
        doctorNameLabel.text = (appointmentSlot.doctorName ?? "") + " - " + (appointmentSlot.doctorDegree ?? "")
        if let hospitalName = appointmentSlot.hospitalName, let clicnicName = appointmentSlot.clinicName {
            hospitalNameLabel.text = hospitalName + " - " + clicnicName
        }
        doctorSpecialityLabel.text = appointmentSlot.specialities
        if let imageBaseURL = AppInfo.getAppInfo()?.s3Url, let avtarImageURL = appointmentSlot.doctorAvatarURL {
            // Load Image From URLwith Caching
            let doctorAvtarImageURLString = imageBaseURL + avtarImageURL
            if let url = URL(string: doctorAvtarImageURLString) {
                doctorAvtarImageView.sd_setImage(with: url, placeholderImage: SCAppConstants.Image.userProfile.image())
            }
        }
    }
}
