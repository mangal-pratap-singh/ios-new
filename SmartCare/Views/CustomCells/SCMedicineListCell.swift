//
//  SCMedicineListCell.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 13/04/20.
//  Copyright © 2020 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

class SCMedicineListCell: UITableViewCell {

    @IBOutlet weak var lbl_medicineName: UILabel!
    
    @IBOutlet weak var lbl_afternoon: UILabel!
    @IBOutlet weak var lbl_morning: UILabel!
    
    @IBOutlet weak var lbl_evening: UILabel!
    
    @IBOutlet weak var lbl_night: UILabel!
    
    @IBOutlet weak var lbl_comments: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
