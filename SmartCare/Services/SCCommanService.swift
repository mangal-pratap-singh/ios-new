//
//  SCCommanService.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 11/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class SCCommanService {
    
    static func getLocationsListAPI( onComplete: @escaping ([Location],Bool) -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.cityList
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                let locations = mapLocatoinListData(locationListJson: json["data"])
                onComplete(locations,true)
                print(urlString)
                print(json as Any)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func getSpecialityListAPI( onComplete: @escaping ([Speciality],Bool) -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.specialitiesList
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                let specialities = mapSpecialityListData(specialityListJSON: json["data"])
                onComplete(specialities,true)
                print(urlString)
                print(json as Any)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func getAsociateHospitalListAPI(onComplete: @escaping ([Hospital]) -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.asociateHospitals, UserProfile.getUserId())
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                do {
                    let hospitals = try Hospitals(data: data)
                    onComplete(hospitals.data ?? [Hospital]())
                } catch {
                    onComplete([Hospital]())
                }
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func geDegreeListAPI( onComplete: @escaping ([Degree],Bool) -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.degree
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                let degree = mapDegreeListData(degreeListJSON: json["data"])
                onComplete(degree,true)
                print(urlString)
                print(json as Any)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func geSpecialityListAPI( onComplete: @escaping ([Speciality],Bool) -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.specialities
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                let specality = mapSpecialityListData(specialityListJSON: json["data"])
                onComplete(specality,true)
                print(urlString)
                print(json as Any)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func mapLocatoinListData(locationListJson: JSON) -> [Location] {
        var locations = [Location]()
        for locationJSON in locationListJson.arrayValue {
           locations.append(Location(locationJSON: locationJSON))
        }
        return locations
    }
    
    static func mapSpecialityListData(specialityListJSON: JSON) -> [Speciality] {
        var specialities = [Speciality]()
        for specialityJSON in specialityListJSON.arrayValue {
            specialities.append(Speciality(specialityJSON: specialityJSON))
        }
        return specialities
    }
    
    static func mapDegreeListData(degreeListJSON: JSON) -> [Degree] {
        var degrees = [Degree]()
        for degreeJSON in degreeListJSON.arrayValue {
            degrees.append(Degree(degreeJSON: degreeJSON))
        }
        return degrees
    }
    
    static func getInsuranceCompanyListAPI( onComplete: @escaping ([InsuranceCompany],Bool) -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.insuranceCompaniesList
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                let insuranceCompanies = mapInsuranceCompanyListData(insuranceCompanyListJson: json["data"])
                onComplete(insuranceCompanies,true)
                print(urlString)
                print(json as Any)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func mapInsuranceCompanyListData(insuranceCompanyListJson: JSON) -> [InsuranceCompany] {
        var insuranceCompanies = [InsuranceCompany]()
        for insuranceCompanyJSON in insuranceCompanyListJson.arrayValue {
            insuranceCompanies.append(InsuranceCompany(insuranceCompanyJSON: insuranceCompanyJSON))
        }
        return insuranceCompanies
    }
}
