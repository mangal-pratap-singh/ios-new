//
//  SCAppointmentService.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 11/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift
import Alamofire

class SCAppointmentService {
    
    static func appointmentCountAPI(onComplete: @escaping (JSON) -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.patientDashboardOverallStatus
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                let outputJSON = json["data"]
                onComplete(outputJSON)
                print(urlString)
                print(json as Any)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func appointmentsListAPI(onComplete: @escaping ([Appointment]) -> Void, onError: @escaping (String) -> Void ) {
        
        let currentDateString = Date().toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentAPI)
        let encodedString = currentDateString.percentEncoded
        
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.nextAppointments + encodedString
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                var appointmentList = [Appointment]()
                let json = JSON(data)
                for appointMentJSON  in json["data"].arrayValue {
                    let appointment = Appointment(appointmentJson: appointMentJSON)
                    appointmentList.append(appointment)
                }
                onComplete(appointmentList)
                print(urlString)
                print(json as Any)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func cancelAppointmentAPI(appointmentId: Int, onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {
        let patientId = UserProfile.getUserId()
        let urlString = SCAppConstants.apiBaseURL() + "patients/\(patientId)/appointments/\(appointmentId)/cancel"
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let mydata = resposnseData {
               
                 let json = JSON(mydata)
                print(urlString)
                print(json as Any)
                 onComplete()
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func searchAppointmentAPI(locationId: Int?, specialityId: Int?, apoointmentDate: Date, appointmentType: String? , onComplete: @escaping ([AppointmentSlot]) -> Void, onError: @escaping (String) -> Void ) {
        
        let currentDateString = apoointmentDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentAPI)
        let encodedString = currentDateString.percentEncoded
        
        var urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.searchAppointments + "date=\(encodedString)"
        
        if let appointmentType = appointmentType {
            urlString = urlString + "&slotType=\(appointmentType)"
        }
        
        if let locationID = locationId {
            urlString = urlString + "&location=\(locationID)"
        }
        
        if let specialityID = specialityId {
            urlString = urlString + "&specitality=\(specialityID)"
        }
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                let appointmentSlots = mapAppointmenmtSlotsData(appointmentSlotsJSON: json["data"])
                onComplete(appointmentSlots)
                print(urlString)
                print(json as Any)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func mapAppointmenmtSlotsData(appointmentSlotsJSON: JSON) -> [AppointmentSlot] {
        var appointmentSlot = [AppointmentSlot]()
        for slotJSON in appointmentSlotsJSON.arrayValue {
            appointmentSlot.append(AppointmentSlot(AppointmentSlotJSON: slotJSON))
        }
        return appointmentSlot
    }
    
    static func bookAppointmentRequestAPI(slotId: Int, patientId: String,paymentType : String,appointmentType :String, onComplete: @escaping (JSON) -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.bbokAppointmentRequest
        let parameters = ["slotId":slotId, "patientId":patientId,"paymentType":paymentType , "appointmentType":appointmentType] as [String : Any]
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters:parameters, successCallback: { (resposnseData) in
            if let mydata = resposnseData {
                let json = JSON(mydata)
                onComplete(json)
                print(urlString)
                print("book appointment=\(json)")
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func appointmentScheduleForWeakAPI(weakStartDate: Date, onComplete: @escaping ([Appointment]) -> Void, onError: @escaping (String) -> Void ) {
        
        let currentDateString = weakStartDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentAPI)
        let encodedString = currentDateString.percentEncoded
        let patientId = UserProfile.getUserId()
        
        let urlString = SCAppConstants.apiBaseURL() + "patients/" + patientId + "/dashboard/appointments-schedule?date=" + encodedString
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                var appointmentList = [Appointment]()
                let json = JSON(data)
                for appointMentJSON  in json["data"].arrayValue {
                    let appointment = Appointment(appointmentJson: appointMentJSON)
                    appointmentList.append(appointment)
                }
                onComplete(appointmentList)
                print(urlString)
                print(json as Any)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
//DOCTOR VIDEO TOKEN
    
//    static func videoTokenDoctorAPI(UsersAppointmentID: Int, onComplete: @escaping ([VideoTokenModel]) -> Void, onError: @escaping (String) -> Void ) {
//
////        let currentDateString = weakStartDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentAPI)
////        let encodedString = currentDateString.percentEncoded
//        let doctorId = UserProfile.getUserId()
//
//        let urlString = SCAppConstants.apiBaseURL() + "doctors/" + doctorId + "/appointments/" + "\(UsersAppointmentID)" + "/join-token"
//
//        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
//            if let data = resposnseData {
//                var videoToken = [VideoTokenModel]()
//                let json = JSON(data)
//                for videoTokenJSON  in json["data"].arrayValue {
//                    let requiredvideoToken = VideoTokenModel(VideoTokenJson: videoTokenJSON)
//                    videoToken.append(requiredvideoToken)
//                }
//                onComplete(videoToken)
//                print(urlString)
//                print(json as Any)
//            }
//        }) { (errorMessage) in
//            onError(errorMessage)
//        }
//    }
//
    

    static func payNowAPI(UsersAppointmentID: Int, completionHandler: @escaping (PayNowModel) ->Void, onError: @escaping (String) -> Void ) {
        let patientId = UserProfile.getUserId()
        //https://stage-api.smartcare.health/api/v1/
       // let urlString = SCAppConstants.apiBaseURL() + "patients/" + patientId + "/appointments/" + "\(UsersAppointmentID)" + "/mobile-pay-now"
        let urlString = "https://stage-api.smartcare.health/api/v1/" + "patients/" + patientId + "/appointments/" + "\(UsersAppointmentID)" + "/mobile-pay-now"
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            print("for payment man ==\(resposnseData)")
            if let data = resposnseData {
                  completionHandler(
                    PayNowModel.init(paynowJson: JSON(data))
                )
                
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
        
    }
    
    static func RSAstringAPI(UsersAppointmentID: Int, UsersaccessCode: String , UsersOrderID: String , completionHandler: @escaping (RSA) -> Void, onError: @escaping (String) -> Void) {

//        {{url}}/api/v1/patients/19980402A0001/appointments/24/get-rsa-key?accessCode=AVCS81FK65BA79SCAB&orderId=1
        
        let patientId = UserProfile.getUserId()
        let urlString = SCAppConstants.apiBaseURL() + "patients/" + patientId + "/appointments/" + "\(UsersAppointmentID)" + "/get-rsa-key?accessCode=" + "\(UsersaccessCode)" + "&orderId=" + "\(UsersOrderID)"
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                 RSA.init(RSAJson: JSON(data))
            print(resposnseData)
                completionHandler(
                    RSA.init(RSAJson: JSON(data))
                    
//                    print(resposnseData)
                )
                
            }
        }) { (errorMessage) in
                       onError(errorMessage)
        }
        
    }
    
////    Token Generation Doctor
    static func DoctorstokenGenerationAPI(UsersAppointmentID: Int, completionHandler: @escaping (VideoToken) ->Void, onError: @escaping (String) -> Void ) {
        let doctorId = UserProfile.getUserId()
        let urlString = SCAppConstants.apiBaseURL() + "doctors/" + doctorId + "/appointments/" + "\(UsersAppointmentID)" + "/join-token"
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            
            if let data = resposnseData {
                let json = JSON(data)
                VideoToken.init(videojson: JSON(data))
                print(resposnseData)
                completionHandler(
                    VideoToken.init(videojson: JSON(data))
                    
                )
                print(urlString)
                print(json as Any)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }

    }
    
    
    //    Token Generation Patient
    static func PatienttokenGenerationAPI(UsersAppointmentID: Int, completionHandler: @escaping (VideoToken) ->Void, onError: @escaping (String) -> Void ) {
        let PatientID = UserProfile.getUserId()
        let urlString = SCAppConstants.apiBaseURL() + "patients/" + PatientID + "/appointments/" + "\(UsersAppointmentID)" + "/join-token"
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            print(resposnseData)
            if let data = resposnseData {
                VideoToken.init(videojson: JSON(data))
                print(resposnseData)
                completionHandler(
                    VideoToken.init(videojson: JSON(data))
                )
                
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
        
    }
    
    
    //MARK: Doctor APIs
        
    static func doctorUpcomingappointmentsListAPI(onComplete: @escaping ([DoctorAppointment]) -> Void, onError: @escaping (String) -> Void ) {
        
        let currentDateString = Date().toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentAPI)
        let encodedString = currentDateString.percentEncoded
        
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.doctorUpcomingAppointments, UserProfile.getUserId()) + encodedString
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                var appointmentList = [DoctorAppointment]()
                let json = JSON(data)
                for appointMentJSON  in json["data"].arrayValue {
                    let appointment = DoctorAppointment(appointmentJson: appointMentJSON)
                    appointmentList.append(appointment)
                }
                onComplete(appointmentList)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
}
