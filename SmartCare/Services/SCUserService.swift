//
//  SCLoginService.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 24/08/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import RealmSwift

class SCUserService {
//    andDeviceToken deviceToken: String *add this parameter to the call below
    
    static func loginAPI(withUsername username: String, Password password: String, onComplete: @escaping (Bool, String) -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.login
        let parameters = ["login": username,
                          "password":password]
//     *add "deviceToken": deviceToken]
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters: parameters, successCallback: { (resposnseData) in
            if let data = resposnseData {
                
                let json = JSON(data)
                
                
                let profileJSON = json["userProfile"]
                self.mapAndSaveUserProfileData(userProfileJson: profileJSON)
            
                let successJSON = json["success"]
                let authenticationToken =  successJSON["token"].string ?? ""
                onComplete(true, authenticationToken)
                print(urlString)
                print(json as Any)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func mapAndSaveUserProfileData(userProfileJson: JSON) {
        let loggedInuserProfile = UserProfile(profileJson: userProfileJson)
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(loggedInuserProfile, update: true)            }
        } catch let error as NSError {
            print("Error Saving User Profile to Realm -- \(error.localizedDescription)")
        }
    }
    
    static func getCountryCodeAPI( onComplete: @escaping (JSON,Bool) -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.countryCode
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                onComplete(json,true)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func getIdentityAPI( onComplete: @escaping (JSON,Bool) -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.identity
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                onComplete(json,true)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func sendVerificationCodeAPI(withParameters parameters: [String: String], onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.sendVerificvationCode
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters:parameters, successCallback: { (resposnseData) in
            if let mydata = resposnseData {
                let json = JSON(mydata)
                print(json as Any)
                print(urlString)
                onComplete()
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func verifyContactAPI(withParameters parameters: [String: String], onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.verifyContact
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters:parameters, successCallback: { (resposnseData) in
            if let mydata = resposnseData {
                let json = JSON(mydata)
                onComplete()
                print(urlString)
                print(json as Any)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func uploadAvatar(withImage image:UIImage ,onComplete: @escaping (JSON,Bool) -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.profileAvtar
        SCAPIClient.sendRequestForUploadAvtar(.post, resourceURLString: urlString, image:image, successCallback: { (resposnseData) in
                  if let data = resposnseData {
                       let json = JSON(data)
                         onComplete(json,true)
                    print(urlString)
                    print(json as Any)
                          }
                       }) { (errorMessage) in
                                 onError(errorMessage)
                        }
     }
    
    
    static func registrationParameter(userInfo:UserInfo) ->  [String: Any] {
        let parameters = ["fName": userInfo.getFirstName(),
                          "lName":userInfo.getLastName(),
                          "emailId": userInfo.getEmailID(),
                          "sex":userInfo.getGender(),
                          "dob": userInfo.getDateOfBirth(),
                          "userType":userInfo.getUserType(),
                          "password": userInfo.getPassword(),
                          "phoneNo":userInfo.getPhoneNumber(),
                          "countryCode": userInfo.getCountryCodeId(),
                          "govtIdType":userInfo.getIdentityId(),
                          "govtIdValue": userInfo.getIdentificationNumber(),
                          "picUrl":userInfo.getAvtar(),
                          ]
        return parameters
    }
    
    static func registrationAPI(withUserInfo userInfo:UserInfo, onComplete: @escaping (JSON, Bool) -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.registration
        let parameters = self.registrationParameter(userInfo: userInfo)
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters: parameters, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                onComplete(json, true)
                print(urlString)
                print(json as Any)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
}
