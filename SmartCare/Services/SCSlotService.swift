//
//  SCSlotService.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 09/12/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class SCSlotService {
    
    static func createSlotAPI(withParameters parameters:[String : Any], onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {

        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.createSlot, UserProfile.getUserId())
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters:parameters, successCallback: { (resposnseData) in
            if let _ = resposnseData {
                onComplete()
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func filteredSlotListAPI(startDate: String, endDate:String, onComplete: @escaping ([SlotItem]) -> Void, onError: @escaping (String) -> Void ) {
        
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.filterSlots, UserProfile.getUserId(), startDate.percentEncoded, endDate.percentEncoded)
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                var slotItemList = [SlotItem]()
                let json = JSON(data)
                for slotJSON  in json["data"].arrayValue {
                    let slotItem = SlotItem(slotJson: slotJSON)
                    slotItemList.append(slotItem)
                }
                onComplete(slotItemList)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func editSlotAPI(slotId:String, parameters:[String : Any]? = nil, onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {
        
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.editSlot, UserProfile.getUserId(), slotId)
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters:parameters, successCallback: { (resposnseData) in
            if let _ = resposnseData {
                onComplete()
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
}
