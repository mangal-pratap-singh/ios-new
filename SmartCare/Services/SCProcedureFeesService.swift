//
//  SCProcedureFees.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 16/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class SCProcedureFeesService {
    
    static func procedureFeesListAPI(onComplete: @escaping ([ProcedureFeeItem]) -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.procedureFees, UserProfile.getUserId())
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                var procedureFeesList = [ProcedureFeeItem]()
                let json = JSON(data)
                for procedureFeesJSON  in json["data"].arrayValue {
                    let procedureFeesItem = ProcedureFeeItem(procedureFeesJson: procedureFeesJSON)
                    procedureFeesList.append(procedureFeesItem)
                }
                onComplete(procedureFeesList)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func procedureTypesListForHospitalAPI(hospitalId: String, onComplete: @escaping ([ProcedureType]) -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.procedureTypes, hospitalId)
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                var procedureFeesList = [ProcedureType]()
                let json = JSON(data)
                for procedureFeesJSON  in json["data"].arrayValue {
                    let procedureFeesItem = ProcedureType(ProcedureTypeJson: procedureFeesJSON)
                    procedureFeesList.append(procedureFeesItem)
                }
                onComplete(procedureFeesList)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func createProcedureFeeAPI(parameters: [String: Any], onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {

        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.createProcedureFee, UserProfile.getUserId())
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters:parameters, successCallback: { (resposnseData) in
            if let _ = resposnseData {
                onComplete()
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
}

