//
//  SCHospitalService.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 04/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class SCHospitalServices {
    
    static func hospitalRequestListAPI(onComplete: @escaping ([HospitalRequest]) -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.doctorHospitalRequestsList
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                var hospitalRequestList = [HospitalRequest]()
                let json = JSON(data)
                print(" hospital request response==\(json)")
                for hospitalRequestJSON  in json["data"].arrayValue {
                    let hospitalRequest = HospitalRequest(hospitalRequestJson: hospitalRequestJSON)
                    hospitalRequestList.append(hospitalRequest)
                }
                onComplete(hospitalRequestList)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func sendRequestForHospital(hospitalId: String, withStatus status: String, onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {
        
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.sendHospitalRequest + hospitalId
        let parameters = ["status": status]
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters:parameters, successCallback: { (resposnseData) in
            if let _ = resposnseData {
                onComplete()
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func resendRequestForHospital(hospitalId: String, withStatus status: String, onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {
        
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.resendHospitalRequest + hospitalId
        let parameters = ["status": status]
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters:parameters, successCallback: { (resposnseData) in
            if let _ = resposnseData {
                onComplete()
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
}

