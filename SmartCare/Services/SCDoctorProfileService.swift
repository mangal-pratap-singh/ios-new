//
//  SCDoctorProfileService.swift
//  SmartCare
//
//  Created by synerzip on 24/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class SCDoctorProfileService {
    
    static func uploadDegreeAPI(withParameters parameters:[String : Any], onComplete: @escaping (JSON, Bool) -> Void, onError: @escaping (String) -> Void ) {
        let profileID = UserProfile.getUserProfile()?.userId ?? ""
        let urlString =  "\((SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.profile + "/") )\((profileID) )"
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters: parameters, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                onComplete(json, true)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func uploadSpecialityAPI(withParameters parameters:[String : Any], onComplete: @escaping (JSON, Bool) -> Void, onError: @escaping (String) -> Void ) {
        let profileID = UserProfile.getUserProfile()?.userId ?? ""
        let urlString =  "\((SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.profile + "/") )\((profileID) )"
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters: parameters, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                onComplete(json, true)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func getDoctorProfileData(onComplete: @escaping (Bool) -> Void,onError: @escaping (String) -> Void ) {
        let profileID = UserProfile.getUserProfile()?.userId ?? ""
        let urlString =  "\((SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.profile + "/") )\((profileID) )"
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                let profileJSON = json["data"]
                self.mapAndSaveUserProfileData(userProfileJson: profileJSON)
                onComplete(true)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func mapAndSaveUserProfileData(userProfileJson: JSON) {
        let loggedInuserProfile = UserProfile(profileJson: userProfileJson)
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(loggedInuserProfile, update: true)
                
            }
        } catch let error as NSError {
            print("Error Saving User Doctor Profile to Realm -- \(error.localizedDescription)")
        }
    }
    
    static func getGeneralDetailsData(withType type: String, onComplete: @escaping (JSON,Bool) -> Void,onError: @escaping (String) -> Void ) {
         let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.generalDetailConsulation, UserProfile.getUserId(),type)
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                let detailJSON = json["status"]
                 onComplete(detailJSON, true)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func changeDurationAPI(withParameters parameters:[String : Any], onComplete: @escaping (JSON, Bool) -> Void, onError: @escaping (String) -> Void ) {
        
         let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.changeDuration, UserProfile.getUserId())
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters: parameters, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                onComplete(json, true)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func uploadGeneralDetailAPI(withParameters parameters:[String : Any], onComplete: @escaping (JSON, Bool) -> Void, onError: @escaping (String) -> Void ) {
        let profileID = UserProfile.getUserProfile()?.userId ?? ""
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.uploadGeneralDetails, profileID)
        print("upload url ==\(urlString)")
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters: parameters, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                onComplete(json, true)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
}
