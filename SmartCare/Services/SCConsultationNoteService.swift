//
//  SCConsultationNoteService.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 18/09/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//
import Foundation
import SwiftyJSON
import Alamofire
class SCConsultationNoteService {
    static func getConsultationNoteListAPI(withParameters parameters:[String: Date], onComplete: @escaping (JSON, Bool) -> Void, onError: @escaping (String) -> Void ) {
        // let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.ratingsList)
        let endDate = parameters["date"]!
        let currentDateString = Date().toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentAPI)
        let endDateString = endDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentAPI)
        
        let encodedString = currentDateString.percentEncoded
         let endDateEncodedString = endDateString.percentEncoded
        let patientId = UserProfile.getUserId()
              // let urlString = SCAppConstants.apiBaseURL() + "patients/\(patientId)/appointments/\(appointmentId)/cancel"
        
      // let urlString = "https://test-api.smartcare.health/api/v2/patients/19990909A0002/consultation-notes?startDate=2019-01-01T00:00:00%2B05:30&endDate=2019-07-31T00:00:00%2B05:30
      let urlString =  SCAppConstants.apiBaseURLVersion2() + "patients/\(patientId)/consultation-notes?date=" + "startDate=\(encodedString)" + "&" + "endDate=\(endDateEncodedString)"
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters: nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                onComplete(json, true)
            }
        }) { (errorMessage) in
             onError(errorMessage)
        }
    }
    
    static func getConsultationNoteDetailsAPI(withParameters parameters:[String: String], onComplete: @escaping (JSON, Bool) -> Void, onError: @escaping (String) -> Void ) {
           // let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.ratingsList)
//           let endDate = parameters["date"] as! Date
//           let currentDateString = Date().toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentAPI)
//           let endDateString = endDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentAPI)
//
//           let encodedString = currentDateString.percentEncoded
//            let endDateEncodedString = endDateString.percentEncoded
           let patientId = UserProfile.getUserId()
        let appointmentId = parameters["appointMentId"]!
                 // let urlString = SCAppConstants.apiBaseURL() + "patients/\(patientId)/appointments/\(appointmentId)/cancel"
           
         // let urlString = "https://test-api.smartcare.health/api/v2/patients/19990909A0002/consultation-notes?startDate=2019-01-01T00:00:00%2B05:30&endDate=2019-07-31T00:00:00%2B05:30
         let urlString =  SCAppConstants.apiBaseURLVersion2() + "patients/\(patientId)/appointments/\(appointmentId)/consultation-notes"
           SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters: nil, successCallback: { (resposnseData) in
               if let data = resposnseData {
                   let json = JSON(data)
                   onComplete(json, true)
               }
           }) { (errorMessage) in
                onError(errorMessage)
           }
       }
    
    static func getConsultationNoteSearchPharmacyAPI(withParameters parameters:[String: String], onComplete: @escaping (JSON, Bool) -> Void, onError: @escaping (String) -> Void ) {
        let app = parameters["appointMentId"]!
               let patientId = UserProfile.getUserId()
         
        let urlString =  SCAppConstants.apiBaseURLVersion2() + String(format: SCAppConstants.apiRequestPath.consultationNotesSearchPharmacy,app)
        
               SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters: nil, successCallback: { (resposnseData) in
                   if let data = resposnseData {
                       let json = JSON(data)
                       onComplete(json, true)
                   }
               }) { (errorMessage) in
                    onError(errorMessage)
               }
           }
    
    static func getGraphAPI(withParameters parameters:[String: String], onComplete: @escaping (JSON, Bool) -> Void, onError: @escaping (String) -> Void ) {
           let app = parameters["appointMentId"]!
                  let patientId = UserProfile.getUserId()
            
           let urlString =  "http://34.93.30.214/graphql?query=%7Bbanners(name:%22Mobile%20Home%20Slider%22)%7Bcover%7D%7D"
        //SCAppConstants.apiBaseURLVersion2() + String(format: SCAppConstants.apiRequestPath.consultationNotesSearchPharmacy,app)
           
                  SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters: nil, successCallback: { (resposnseData) in
                      if let data = resposnseData {
                          let json = JSON(data)
                          onComplete(json, true)
                      }
                  }) { (errorMessage) in
                       onError(errorMessage)
                  }
              }
    
    static func postHitAPI(urlStr: String ,withParameters parameters:[String: String], onComplete: @escaping (JSON, Bool) -> Void, onError: @escaping (String) -> Void ) {
       // let app = parameters["appointMentId"]!
              // let patientId = UserProfile.getUserId()
        
         print(parameters)
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        let urlString =  SCAppConstants.apiBaseURLVersion2() + urlStr
    
/*
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
                let authorizationToken = SCSessionManager.sharedInstance.getAuthenticationToken()
                   
        request.setValue("Bearer \(String(describing: authorizationToken))", forHTTPHeaderField: "Authorization")
        request.httpBody = httpBody

        Alamofire.request(request).responseJSON {
            (response) in
            
            print(JSON(response.data))
        }
        */
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters: parameters, successCallback: { (resposnseData) in
                   if let data = resposnseData {
                       let json = JSON(data)
                       onComplete(json, true)
                   }
               }) { (errorMessage) in
                    onError(errorMessage)
               }
           }
    
    
    
   static func jsonToDictionary(from text: String) -> [String: Any]? {
        guard let data = text.data(using: .utf8) else { return nil }
        let anyResult = try? JSONSerialization.jsonObject(with: data, options: [])
        return anyResult as? [String: Any]
    
    }
     static func postHitAPI2(urlStr: String ,withParameters parameters:[String: AnyObject], onComplete: @escaping (JSON, Bool) -> Void, onError: @escaping (String) -> Void ) {
           // let app = parameters["appointMentId"]!
                  // let patientId = UserProfile.getUserId()
            
             //print(parameters)
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                return
            }
            let urlString =  SCAppConstants.apiBaseURL() + urlStr
        
//            if let jsonString = String(data: httpBody, encoding: .utf8) {
//                print(jsonString)
//            }
        
        //var params = jsonToDictionary(from: parameters) ?? [String : Any]()
        
        
        
        let jsonString = String(data: httpBody, encoding: String.Encoding.ascii)!
       // print ("json Str=\(jsonString)")
       var params = jsonToDictionary(from: jsonString) ?? [String : Any]()
        
    /*
            let url = URL(string: urlString)!
            var request = URLRequest(url: url)
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
            let authorizationToken = SCSessionManager.sharedInstance.getAuthenticationToken()
                       
            request.setValue("Bearer \(String(describing: authorizationToken))", forHTTPHeaderField: "Authorization")
            request.httpBody = httpBody

            Alamofire.request(request).responseJSON {
                (response) in
                
                print(JSON(response.data))
            }
       
            */
            SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters: params, successCallback: { (resposnseData) in
                       if let data = resposnseData {
                           let json = JSON(data)
                           onComplete(json, true)
                       }
                   }) { (errorMessage) in
                        onError(errorMessage)
                   }
        
               }
    
    static func getHitAPI(withParameters parameters: String, onComplete: @escaping (JSON, Bool) -> Void, onError: @escaping (String) -> Void ) {
          // let app = parameters["appointMentId"]!
            let patientId = UserProfile.getUserId()
            
           let urlString =  SCAppConstants.apiBaseURLVersion2() + parameters
           
                  SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters: nil, successCallback: { (resposnseData) in
                      if let data = resposnseData {
                          let json = JSON(data)
                          onComplete(json, true)
                      }
                  }) { (errorMessage) in
                       onError(errorMessage)
                  }
              }

}

