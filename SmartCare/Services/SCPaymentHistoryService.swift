//
//  SCPaymentHistoryService.swift
//  SmartCare
//
//  Created by Vivek Gaurav Singh on 19/11/19.
//  Copyright © 2019 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
class SCPaymentHistoryService{
    
    static func getPaymentHistoryAPI(withParameters parameters:[String: Date], onComplete: @escaping (JSON, Bool) -> Void, onError: @escaping (String) -> Void ) {
        // let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.ratingsList)
        var urlString1: String = ""
        let endDate = parameters["date"]!
        
        let currentDateString = Date().toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentAPI)
        
        let endDateString = endDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentAPI)
        
         let encodedCurrentString = currentDateString.percentEncoded
        
         let endDateEncodedString = endDateString.percentEncoded
        
        let patientId = UserProfile.getUserId()
              // let urlString = SCAppConstants.apiBaseURL() + "patients/\(patientId)/appointments/\ (appointmentId)/cancel"
        
      // let urlString = "https://test-api.smartcare.health/api/v2/patients/19990909A0002/consultation-notes?startDate=2019-01-01T00:00:00%2B05:30&endDate=2019-07-31T00:00:00%2B05:30
        
        //https://test-api.smartcare.health/api/v1/patients/19990909A0002/payment-history?transactionStatus=S&per_page=10&page=1&startDate=2019-11-14T00:00:00%2B05:30&endDate=2019-11-14T23:59:59%2B05:30
     // let urlString =  SCAppConstants.apiBaseURL() + "patients/\(patientId)/payment-history?transactionStatus=\("S")"
        
        
       // let urlString =  SCAppConstants.apiBaseURL() + "patients/\(patientId)/payment-history?transactionStatus=" + "S" + "&" + "per_page=\(0)" + "&" + "page=\(0)" + "&" + "startDate=\("")" + "&" + "endDate=\("")"
       // let urlString  = "https://test-api.smartcare.health/api/v1/patients/\(patientId)/payment-history?transactionStatus=S&per_page=10&page=1&startDate=2018-11-14T00:00:00%2B05:30&endDate=2019-11-14T23:59:59%2B05:30"
        
      // 2018-11-14T00:00:00%2B05:30
        //https://test-api.smartcare.health/api/v1/patients/19990909A0002/payment-history?transactionStatus=S&per_page=10&page=1&startDate=2019-11-22T10%3A33%3A39%2B05%3A30&endDate=2019-11-21T10%3A33%3A11%2B05%3A30
       // let urlString1 =  SCAppConstants.apiBaseURL() + "patients/\(patientId)/payment-history?transactionStatus=S&per_page=10&page=1&startDate=\(endDateEncodedString)&endDate=\(currentDateString)"
        if encodedCurrentString == endDateEncodedString {
           urlString1 =  SCAppConstants.apiBaseURL() + "patients/\(patientId)/payment-history?transactionStatus=S&per_page=10&page=1&startDate=\(endDateEncodedString)"
        }else{
             urlString1 =  SCAppConstants.apiBaseURL() + "patients/\(patientId)/payment-history?transactionStatus=S&per_page=10&page=1&startDate=\(endDateEncodedString)&endDate=\(encodedCurrentString)"
        }
        
       // var urlString = urlString1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let urlString :String = urlString1.replacingOccurrences(of: "+", with: "%2B")
       // components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters: nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                onComplete(json, true)
            }
        }) { (errorMessage) in
             onError(errorMessage)
        }
    }

     static func postHitAPI(urlStr: String ,withParameters parameters:[String: String], onComplete: @escaping (JSON, Bool) -> Void, onError: @escaping (String) -> Void ) {
           // let app = parameters["appointMentId"]!
                  // let patientId = UserProfile.getUserId()
            
             print(parameters)
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                return
            }
            let urlString =  SCAppConstants.apiBaseURLVersion2() + urlStr
        
    /*
            let url = URL(string: urlString)!
            var request = URLRequest(url: url)
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
                    let authorizationToken = SCSessionManager.sharedInstance.getAuthenticationToken()
                       
            request.setValue("Bearer \(String(describing: authorizationToken))", forHTTPHeaderField: "Authorization")
            request.httpBody = httpBody

            Alamofire.request(request).responseJSON {
                (response) in
                
                print(JSON(response.data))
            }
            */
            
            SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters: parameters, successCallback: { (resposnseData) in
                       if let data = resposnseData {
                           let json = JSON(data)
                           onComplete(json, true)
                       }
                   }) { (errorMessage) in
                        onError(errorMessage)
                   }
               }
    
    
}



class SCAuthoriseHistoryService{

    static func getAuthoriseHistoryAPI(withParameters parameters: String, onComplete: @escaping (JSON, Bool) -> Void, onError: @escaping (String) -> Void ) {
        // let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.ratingsList)
       
        let patientId = UserProfile.getUserId()
              
        let urlString =  SCAppConstants.apiBaseURLVersion2() + "patients/\(patientId)/appointments/\(parameters)/authorized-pharmacies"
      
      
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters: nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                onComplete(json, true)
            }
        }) { (errorMessage) in
             onError(errorMessage)
        }
    }
    
    static func postAuthoriseCancelAPI(withParameters parameters: String,pharmacyId: String, onComplete: @escaping (JSON, Bool) -> Void, onError: @escaping (String) -> Void ) {
        // let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.ratingsList)
       
        let patientId = UserProfile.getUserId()
              
        let urlString =  SCAppConstants.apiBaseURLVersion2() + "patients/\(patientId)/appointments/\(parameters)/cancel-authorized-pharmacies/\(pharmacyId)"
      
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters: nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                onComplete(json, true)
            }
        }) { (errorMessage) in
             onError(errorMessage)
        }
    }
    
}
