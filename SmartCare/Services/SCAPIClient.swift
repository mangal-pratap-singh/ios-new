//
//  APIClient.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 24/08/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class SCAPIClient {
    
    static func sendRequest(_ methodType: Alamofire.HTTPMethod,
                            resourceURLString: String,
                            parameters: [String: Any]? = nil,
                            headers: [String: String]? = nil,
                            successCallback: SuccessCallback?,
                            errorCallback: ErrorCallback?) {

        var headerParams: [String: String]? = nil
        if let authorizationToken = SCSessionManager.sharedInstance.getAuthenticationToken() {
            headerParams = ["Authorization":"Bearer \(authorizationToken)"]
          }
        
        Alamofire.request(resourceURLString, method: methodType, parameters: parameters, encoding: JSONEncoding.default, headers: headerParams).response { (response) in
            
            // Check for internet Connection error
            if let error =  response.error {
                errorCallback?(error.localizedDescription)
            }
            print( "response status code==\(response.response?.statusCode)")
            if let statusCode = response.response?.statusCode, statusCode >= 200 && statusCode < 300 {
                successCallback?(response.data)
            } else {
                // Handled Error Message coming from Server
                let errorJSON = JSON(response.data!)
                print("JSON===\(errorJSON)")
                let errorMessage = errorJSON["message"].string ?? ""
                errorCallback?(errorMessage)
            }
        }
    }
    
    
    static func sendBodyRequest(_ methodType: Alamofire.HTTPMethod,
                            resourceURLString: String,
                            parameters: [String: Any]? = nil,
                            headers: [String: String]? = nil,
                            successCallback: SuccessCallback?,
                            errorCallback: ErrorCallback?) {

        var headerParams: [String: String]? = nil
        if let authorizationToken = SCSessionManager.sharedInstance.getAuthenticationToken() {
            headerParams = ["Authorization":"Bearer \(authorizationToken)"]
          }
        
        Alamofire.request(resourceURLString, method: methodType, parameters: parameters, encoding: JSONEncoding.default, headers: headerParams).response { (response) in
            
            // Check for internet Connection error
            if let error =  response.error {
                errorCallback?(error.localizedDescription)
            }
            
            print( "response status code==\(response.response?.statusCode)")
            if let statusCode = response.response?.statusCode, statusCode >= 200 && statusCode < 300 {
                successCallback?(response.data)
            } else {
                // Handled Error Message coming from Server
                let errorJSON = JSON(response.data!)
                let errorMessage = errorJSON["message"].string ?? ""
                errorCallback?(errorMessage)
            }
        }
        
    }
    
    
    
    static func sendRequestForUploadAvtar(_ methodType: Alamofire.HTTPMethod,
                            resourceURLString: String,
                            image : UIImage,
                            parameters: [String: Any]? = nil,
                            headers: [String: String]? = nil,
                            successCallback: SuccessCallback?,
                            errorCallback: ErrorCallback?)
       {
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(image.jpegData(compressionQuality: 0.8)!, withName: "avatar", fileName: "swift_file.jpeg", mimeType: "image/jpeg")
        }, to:resourceURLString)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if let statusCode = response.response?.statusCode, statusCode >= 200 && statusCode < 300 {
                        successCallback?(response.data)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                errorCallback?(encodingError as! String)
            }
            
        }
    }
}
