//
//  SCInsuranceServices.swift
//  SmartCare
//
//  Created by synerzip on 17/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class SCInsuranceServices {
    
    static func insurancePolicyListAPI(onComplete: @escaping ([Insurance]) -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.insurancePolicyList
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                var insuranceList = [Insurance]()
                let json = JSON(data)
                
                
                print("json ")
                for insuranceJSON  in json["data"].arrayValue {
                    
                    
                    let insurance = Insurance(insuranceJson:insuranceJSON)
                    insuranceList.append(insurance)
                }
                onComplete(insuranceList)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func createInsurancePolicyAPI(parameters: [String: String], onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.insurancePolicySave
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters:parameters, successCallback: { (resposnseData) in
            if let _ = resposnseData {
                onComplete()
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func editeInsurancePolicyAPI(parameters: [String: String],policyId: String, onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {
        
        
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.insurancePolicyEdit , policyId)
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters:parameters, successCallback: { (resposnseData) in
            if let _ = resposnseData {
                onComplete()
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    static func deleteInsurancePolicyAPI(policyId:String, onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.insurancePolicyDelete + "/" + policyId
        
        SCAPIClient.sendRequest(.delete, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let _ = resposnseData {
                onComplete()
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
}
