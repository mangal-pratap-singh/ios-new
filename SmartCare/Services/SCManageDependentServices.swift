//
//  SCManageDependentServices.swift
//  SmartCare
//
//  Created by synerzip on 08/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class SCManageDependentServices {
    
    static func addDependentAPI(withParameters parameters:[String: String], onComplete: @escaping (JSON, Bool) -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.manageDependent
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters: parameters, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                onComplete(json, true)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func disableDependentAPI(withUserInfo dependnetInfo:Dependent,withStatus:Bool,onComplete: @escaping (JSON, Bool) -> Void, onError: @escaping (String) -> Void ) {

        let urlString =  "\((SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.manageDependent + "/") )\((dependnetInfo.dependentID) ?? "")\(("/status"))"
        let parameters = ["status": withStatus]
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters: parameters, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                onComplete(json, true)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func manageDependentListAPI(onComplete: @escaping ([Dependent]) -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.manageDependent
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                var dependentList = [Dependent]()
                let json = JSON(data)
                print(json as Any)
                print(urlString)
                for dependentJSON  in json["data"].arrayValue {
                    let appointment = Dependent(dependentJson: dependentJSON)
                    dependentList.append(appointment)
                }
                onComplete(dependentList)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }

}
