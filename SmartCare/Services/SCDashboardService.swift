import Foundation
import SwiftyJSON

class SCDashboardService {
    static func doctorDashboardStatus(onComplete: @escaping (DailyStatus) -> Void, onError: @escaping (String) -> Void ) {
        let currentDateString = Date().toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentAPI)
        let encodedString = currentDateString.percentEncoded
        
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.doctorDashboardStatus, UserProfile.getUserId()) + encodedString
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                let outputJSON = json["data"]
                onComplete(DailyStatus(dailyStatus: outputJSON))
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func doctorDashboardDailyChart(onComplete: @escaping (DailyChart) -> Void, onError: @escaping (String) -> Void ) {
        let currentDateString = Date().toDateString(withFormatter: SCAppConstants.DateFormatType.shiftAPI)
        let encodedString = currentDateString.percentEncoded
        
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.doctorDashboardDailyChart, UserProfile.getUserId()) + encodedString
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                let outputJSON = json["data"]
                onComplete(DailyChart(dailyChart: outputJSON))
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func doctorDashboardDayStats(for date: Date, onComplete: @escaping (MonthwiseStats) -> Void, onError: @escaping (String) -> Void ) {
        let currentDateString = date.toDateString(withFormatter: SCAppConstants.DateFormatType.shiftAPI)
        let encodedString = currentDateString.percentEncoded
        
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.doctorDashboardDayStats, UserProfile.getUserId()) + encodedString
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                let outputJSON = json["data"]
                onComplete(MonthwiseStats(json: outputJSON))
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func doctorDashboardMonthStats(for date: Date, onComplete: @escaping (YearwiseStats) -> Void, onError: @escaping (String) -> Void ) {
        let currentDateString = date.toDateString(withFormatter: SCAppConstants.DateFormatType.shiftAPI)
        let encodedString = currentDateString.percentEncoded
        
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.doctorDashboardMonthStats, UserProfile.getUserId()) + encodedString
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                let outputJSON = json["data"]
                onComplete(YearwiseStats(json: outputJSON))
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
}
