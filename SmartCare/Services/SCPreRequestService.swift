//
//  SCPreRequestService.swift
//  SmartCare
//
//  Created by synerzip on 04/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class SCPreRequestService {
    
    static func configurationAPI( onComplete: @escaping (Bool) -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.configuration
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                    let appJSON = json["data"]
                    self.mapAndSaveAppInfoData(userAppJson: appJSON)
                onComplete(true)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func mapAndSaveAppInfoData(userAppJson: JSON) {
        let appInfo =  AppInfo(appJson: userAppJson)
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(appInfo, update: false)            }
        } catch let error as NSError {
            print("Error Saving User Profile to Realm -- \(error.localizedDescription)")
        }
    }
}


