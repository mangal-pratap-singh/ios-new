//
//  SCPasswordService.swift
//  SmartCare
//
//  Created by synerzip on 04/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//
import Foundation
import SwiftyJSON
import RealmSwift

class SCPasswordService {
    
    static func verifyAccountAPI(withParameters parameters: [String: String], onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.verifyAccount
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters:parameters, successCallback: { (resposnseData) in
            if let _ = resposnseData {
                onComplete()
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func passwordChangeAPI(withParameters parameters: [String: String], onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.passwordChange
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters:parameters, successCallback: { (resposnseData) in
            if let _ = resposnseData {
                onComplete()
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    
    static func codeVerificationAPI(withParameters parameters: [String: String], onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.codeVerification
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters:parameters, successCallback: { (resposnseData) in
            if let _ = resposnseData {
                onComplete()
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
}

