//
//  SCProfileService.swift
//  SmartCare
//
//  Created by synerzip on 18/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//
import Foundation
import SwiftyJSON
import RealmSwift

class SCProfileService {
    
    static func getProfileAPI(onComplete: @escaping (Bool) -> Void,onError: @escaping (String) -> Void ) {
         let profileID = UserProfile.getUserProfile()?.userId ?? ""
         let urlString =  "\((SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.profile + "/") )\((profileID) )"
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
             let json = JSON(data)
                let profileJSON = json["data"]
                self.mapAndSaveUserProfileData(userProfileJson: profileJSON)
                onComplete(true)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func mapAndSaveUserProfileData(userProfileJson: JSON) {
        let loggedInuserProfile = UserProfile(profileJson: userProfileJson)
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(loggedInuserProfile, update: true)
                
            }
        } catch let error as NSError {
            print("Error Saving User Profile to Realm -- \(error.localizedDescription)")
        }
    }
    
    
    static func addAddressAPI(withParameters parameters:[String : Any], onComplete: @escaping (JSON, Bool) -> Void, onError: @escaping (String) -> Void ) {
        let profileID = UserProfile.getUserProfile()?.userId ?? ""
        let urlString =  "\((SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.profile + "/") )\((profileID) )"
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters: parameters, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                onComplete(json, true)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func uploadPicAPI(withParameters parameters:[String : Any], onComplete: @escaping (JSON, Bool) -> Void, onError: @escaping (String) -> Void ) {
        let profileID = UserProfile.getUserProfile()?.userId ?? ""
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.profile + "/" + profileID
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters: parameters, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                onComplete(json, true)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    
    
    static func contactChangeAPI(withParameters parameters: [String: String], onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.contactChange
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters:parameters, successCallback: { (resposnseData) in
            if let _ = resposnseData {
                onComplete()
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func setAttachmentLinkAPI(withParameters parameters:[String : Any], onComplete: @escaping (JSON, Bool) -> Void, onError: @escaping (String) -> Void ) {
        let profileID = UserProfile.getUserProfile()?.userId ?? ""
        let urlString =  "\((SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.profile + "/") )\((profileID) )"
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters: parameters, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                onComplete(json, true)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }   
}
