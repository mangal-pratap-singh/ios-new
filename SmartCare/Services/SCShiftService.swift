//
//  SCShiftService.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 08/12/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class SCShiftService {
    
    static func shiftsListAPI(onComplete: @escaping ([Shift]) -> Void, onError: @escaping (String) -> Void ) {
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.shiftList, UserProfile.getUserId())
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                do {
                    let shifts = try Shifts(data: data)
                    onComplete(shifts.data ?? [Shift]())
                } catch {
                    onComplete([Shift]())
                }
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func shiftsByDateAPI(date: Date, onComplete: @escaping ([Shift]) -> Void, onError: @escaping (String) -> Void ) {
        
        let currentDateString = date.toDateString(withFormatter: SCAppConstants.DateFormatType.shiftAPI)
        let encodedString = currentDateString.percentEncoded
        
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.shiftsByDate, UserProfile.getUserId(), encodedString)
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                do {
                    let shifts = try Shifts(data: data)
                    onComplete(shifts.data ?? [Shift]())
                } catch {
                    onError("Invalid JSON Object")
                }
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func shiftsDetailsAPI(shiftId: String, onComplete: @escaping (Shift) -> Void, onError: @escaping (String) -> Void ) {
        
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.shiftDetails, UserProfile.getUserId(), shiftId)
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                do {
                    let shift = try ShiftObjectParser(data: data)
                    onComplete(shift.data)
                } catch {
                    onError("Incorrect JSON Object")
                }
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func createOrEditShiftAPI(forShiftId shiftId:String?, andParameters parameters:[String : Any], onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {
        
        var urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.createShift, UserProfile.getUserId())
        
        // Append Shift ID if its editShift Request
        if let id = shiftId {
            urlString = urlString + "/" + id
        }
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters:parameters, successCallback: { (resposnseData) in
            if let _ = resposnseData {
                onComplete()
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func deleteShiftAPI(forShiftId shiftId:String, onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {
        
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.deleteShift, UserProfile.getUserId(), shiftId)
        
        SCAPIClient.sendRequest(.delete, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let _ = resposnseData {
                onComplete()
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func updateShiftWithActualTimeAPI(forShiftId shiftId:String, isShiftStarted: Bool, parameters:[String : Any], onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {
        
        let urlString = SCAppConstants.apiBaseURL() + String(format: (!isShiftStarted ? SCAppConstants.apiRequestPath.startShift : SCAppConstants.apiRequestPath.endShift), UserProfile.getUserId(), shiftId)
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters:parameters, successCallback: { (resposnseData) in
            if let _ = resposnseData {
                onComplete()
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func shiftSummaryDetailsAPI(forShiftId shiftId: String, onComplete: @escaping ([DoctorAppointment]) -> Void, onError: @escaping (String) -> Void ) {
        
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.shiftSummary, UserProfile.getUserId(), shiftId)
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                let json = JSON(data)
                let jsonData = json["data"].arrayValue
                var doctorAppointments = [DoctorAppointment]()
                for item in jsonData {
                    let appointment = DoctorAppointment(appointmentJson: item)
                    doctorAppointments.append(appointment)
                }
                onComplete(doctorAppointments)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
}
