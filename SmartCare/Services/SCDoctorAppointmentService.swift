//
//  SCDoctorAppointmentService.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 19/11/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON

class SCDoctorAppointmentService {
 
    static func doctorAppointmentsListAPI(withStatus status: AppointmentStatus?, apoointmentDate: Date, onComplete: @escaping ([DoctorAppointment]) -> Void, onError: @escaping (String) -> Void ) {
        
        let currentDateString = apoointmentDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentAPI)
        
        let encodedString = currentDateString.percentEncoded
        
        var urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.doctorAppointments, UserProfile.getUserId(),encodedString)
       
        if let appointmentStatus = status {
            urlString = urlString + "&status=\(appointmentStatus.rawValue)"
        }
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                var appointmentList = [DoctorAppointment]()
                let json = JSON(data)
                print("json print doctor appointmentList=\(json)")
                for appointMentJSON  in json["data"].arrayValue {
                    let appointment = DoctorAppointment(appointmentJson: appointMentJSON)
                    appointmentList.append(appointment)
                }
                onComplete(appointmentList)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func availableSlotsAPI(forClinic clinicID: String, onDate date: Date, onComplete: @escaping ([Slot]) -> Void, onError: @escaping (String) -> Void ) {
        let currentDateString = date.toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentAPI)
        let encodedString = currentDateString.percentEncoded
        
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.clinicSlots, encodedString, UserProfile.getUserId(),clinicID)
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                var slots = [Slot]()
                let json = JSON(data)
                for slotJSON  in json["data"].arrayValue {
                    let slot = Slot(slotJson: slotJSON)
                    slots.append(slot)
                }
                onComplete(slots)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func statusUpdateForAppointmentAPI(parameters: [String:String], appointmentId: String, onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {
        
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.doctorAppointmentStatusUpodate, UserProfile.getUserId(), appointmentId)
    
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters:parameters, successCallback: { (resposnseData) in
            if let _ = resposnseData {
                onComplete()
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func changeAppointmentAPI(parameters: [String:Any], appointmentId: String, onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {
        
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.doctorChangeAppointment, UserProfile.getUserId(), appointmentId)
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters:parameters, successCallback: { (resposnseData) in
            if let _ = resposnseData {
                onComplete()
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
}
