//
//  SCRatingService.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 18/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class SCRatingService {
    
    static func ratingsListAPI(startDate: Date, endDate: Date, status: AppointmentStatus?, onComplete: @escaping ([Appointment]) -> Void, onError: @escaping (String) -> Void ) {

        let startDateString = startDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentAPI)
        let encodedStartDateString = startDateString.percentEncoded
        
        let endDateString = endDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentAPI)
        let encodedEndDateString = endDateString.percentEncoded
        
        var urlString = SCAppConstants.apiBaseURL() + SCAppConstants.apiRequestPath.ratingsList + "?perPage=200&startDate=\(encodedStartDateString)&endDate=\(encodedEndDateString)"
        
        if let appointmentStatus = status {
            urlString = urlString + "&status=\(appointmentStatus.rawValue)"
        }
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                var appointmentList = [Appointment]()
                let json = JSON(data)
                print("Rating JSON Parse==%@",json)
                for appointMentJSON  in json["data"].arrayValue {
                    let appointment = Appointment(appointmentJson: appointMentJSON)
                    appointmentList.append(appointment)
                }
                onComplete(appointmentList)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func saveRatingsForAppointmentAPI(appointmentId: String, count: Int, review: String, onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {

        let urlString = SCAppConstants.apiBaseURL() + "patients/appointments/" + appointmentId + "/ratings"
        let parameters = ["ratingCount": "\(count)", "review": review]

        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters:parameters, successCallback: { (resposnseData) in
            if let _ = resposnseData {
                onComplete()
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
}
