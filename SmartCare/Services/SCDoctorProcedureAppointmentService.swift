//
//  SCDoctorProcedureAppointmentService.swift
//  SmartCare
//
//  Created by synerzip on 15/12/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//


import Foundation
import SwiftyJSON

class SCDoctorProcedureAppointmentService {
    
    static func doctorProcedureAppointmentsListAPI(withDate apoointmentDate: Date, onComplete: @escaping ([DoctorAppointment]) -> Void, onError: @escaping (String) -> Void ) {
        
        let currentDateString = apoointmentDate.toDateString(withFormatter: SCAppConstants.DateFormatType.appointmentAPI)
        let encodedString = currentDateString.percentEncoded
        
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.doctorProcedureAppointments, UserProfile.getUserId(),encodedString)
        print("URL String ::::::::::::",urlString)
        
        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                var appointmentList = [DoctorAppointment]()
                let json = JSON(data)
                for appointMentJSON  in json["data"].arrayValue {
                    let appointment = DoctorAppointment(appointmentJson: appointMentJSON)
                    appointmentList.append(appointment)
                }
                onComplete(appointmentList)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func changeProcedureTypeAPI(withAppoinment appoinment:DoctorAppointment ,onComplete: @escaping ([ProcedureFeeItem]) -> Void, onError: @escaping (String) -> Void ) {
        var hosptialID = ""
        if let hospitalId = appoinment.hospital?.id{
            hosptialID = String(hospitalId)
        }
        var clinicID = ""
        if let clinicId = appoinment.clinic?.id{
            clinicID = String(clinicId)
        }
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.changeProcedureType, hosptialID,clinicID, UserProfile.getUserId())

        SCAPIClient.sendRequest(.get, resourceURLString: urlString, parameters:nil, successCallback: { (resposnseData) in
            if let data = resposnseData {
                var procedureFeesList = [ProcedureFeeItem]()
                let json = JSON(data)
                for procedureFeesJSON  in json["data"].arrayValue {
                    let procedureFeesItem = ProcedureFeeItem(procedureFeesJson: procedureFeesJSON)
                    procedureFeesList.append(procedureFeesItem)
                }
                onComplete(procedureFeesList)
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    
    static func denyProcedureAppointmentAPI(parameters: [String:String], appointmentId: String, onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {
        
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.denyProcedureAppoinment, UserProfile.getUserId(), appointmentId)
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters:parameters, successCallback: { (resposnseData) in
            if let _ = resposnseData {
                onComplete()
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
    static func saveProcedureAppointmentAPI(parameters: [String:Any], appointmentId: String, onComplete: @escaping () -> Void, onError: @escaping (String) -> Void ) {
        
        let urlString = SCAppConstants.apiBaseURL() + String(format: SCAppConstants.apiRequestPath.approveProcedureAppoinment, UserProfile.getUserId(), appointmentId)
        
        SCAPIClient.sendRequest(.post, resourceURLString: urlString, parameters:parameters, successCallback: { (resposnseData) in
            if let _ = resposnseData {
                onComplete()
            }
        }) { (errorMessage) in
            onError(errorMessage)
        }
    }
    
}

