//
//  Sequence+helper.swift
//  Pastrty Corner
//
//  Created by synerzip on 12/07/17.
//  Copyright © 2017 Pastry Corner. All rights reserved.
//

import Foundation

extension Sequence {
    
    func categorise<U : Hashable>(_ key: (Iterator.Element) -> U) -> [U:[Iterator.Element]] {
        var dict: [U:[Iterator.Element]] = [:]
        for el in self {
            let key = key(el)
            if case nil = dict[key]?.append(el) { dict[key] = [el] }
        }
        return dict
    }
}
