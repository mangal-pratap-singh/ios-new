//
//  UITextFiled+Helper.swift
//  SmartCare
//
//  Created by Prashant Pawar on 23/08/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
extension UITextField {
    
    func SetRightSideImage(with image: UIImage, tintColor: UIColor = UIColor.gray ,leftPadding: Int = 4) {
        let rightImageView = UIImageView()
        rightImageView.contentMode = .scaleAspectFit
        let rightView = UIView()
        rightView.frame = CGRect(x: 10, y: 0, width: 30, height: 20)
        rightImageView.frame = CGRect(x: leftPadding, y: 0, width: 15, height: 20)
        self.rightViewMode = .always
        self.rightView = rightView
        rightImageView.image = image
        rightImageView.tintColor = tintColor
        rightImageView.tintColorDidChange()
        rightView.addSubview(rightImageView)
    }
    
    func configureTextFieldLeftView( width: CGFloat, borderColor:UIColor , borderWidth:CGFloat , cornerRadius:CGFloat) {
        self.leftView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: self.frame.height))
        self.leftViewMode = .always
        self.layer.masksToBounds = true
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.cornerRadius=cornerRadius
    }
    func setPaddingWithImage(image: UIImage){
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        let view = UIView(frame: CGRect(x: 0, y: 2, width: 50, height: 50))
        view.backgroundColor = UIColor.clear
        imageView.frame = CGRect(x: 15.0, y: 16.0, width: 18.0, height: 18.0)
        self.leftViewMode = .always
        view.addSubview(imageView)
        self.leftViewMode = UITextField.ViewMode.always
        self.leftView = view
    }
}

