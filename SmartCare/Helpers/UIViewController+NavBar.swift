import Foundation
import RxCocoa
import RxSwift


extension UIViewController {
    
    
    func setStatusBarStyCustom() {
             
    }
    
    
    func customizeNavigationItem(title: String = "", isDetail: Bool) {
        self.navigationItem.backBarButtonItem?.title = nil
        self.navigationItem.title = title
        self.navigationController?.navigationBar.isTranslucent = false
        if isDetail == false {
            let menuButton:UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            menuButton.setImage(SCAppConstants.Image.drawerIcon.image(), for: UIControl.State.normal)
            menuButton.addTarget(self, action: #selector(menuButtonTapped), for: UIControl.Event.touchUpInside)
            let menuBarButton : UIBarButtonItem = UIBarButtonItem(customView: menuButton)
            self.navigationItem.leftBarButtonItem = menuBarButton
        } else {
            self.navigationItem.leftBarButtonItem = nil
        }
       
        let leftItem: UIBarButtonItem = UIBarButtonItem()
        leftItem.title = ""
        self.navigationItem.backBarButtonItem = leftItem
    
    }
    
    
    func customizeNavigationRootBackItem(title: String = "", isDetail: Bool) {
        self.navigationItem.backBarButtonItem?.title = nil
        self.navigationItem.title = title
        self.navigationController?.navigationBar.isTranslucent = false
        if isDetail == false {
            let menuButton:UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            menuButton.setImage(SCAppConstants.Image.drawerIcon.image(), for: UIControl.State.normal)
            menuButton.addTarget(self, action: #selector(menuButtonTapped), for: UIControl.Event.touchUpInside)
            let menuBarButton : UIBarButtonItem = UIBarButtonItem(customView: menuButton)
            self.navigationItem.leftBarButtonItem = menuBarButton
        } else {
            self.navigationItem.leftBarButtonItem = nil
        }
       
        let menuButton:UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
                   menuButton.setImage(SCAppConstants.Image.backIcon.image(), for: UIControl.State.normal)
                   menuButton.addTarget(self, action: #selector(backRootTapped), for: UIControl.Event.touchUpInside)
                   let menuBarButton : UIBarButtonItem = UIBarButtonItem(customView: menuButton)
                   self.navigationItem.leftBarButtonItem = menuBarButton
    
    }
    
    @objc func menuButtonTapped() {
        SCRoutingManager.sharedInstance.updateDrawerState(state: .opened)
    }
    
    @objc func backRootTapped() {
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    
    func customizePresentNavigationItem(title: String = "", isDetail: Bool) {
        
           self.navigationItem.backBarButtonItem?.title = nil
           self.navigationItem.title = title
           self.navigationController?.navigationBar.isTranslucent = false
           if isDetail == false {
               let menuButton:UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
               menuButton.setImage(SCAppConstants.Image.closeIcon.image(), for: UIControl.State.normal)
               menuButton.addTarget(self, action: #selector(dissmmisButtonTapped), for: UIControl.Event.touchUpInside)
               let menuBarButton : UIBarButtonItem = UIBarButtonItem(customView: menuButton)
               self.navigationItem.leftBarButtonItem = menuBarButton
           } else {
               self.navigationItem.leftBarButtonItem = nil
           }
          
           let leftItem: UIBarButtonItem = UIBarButtonItem()
           leftItem.title = ""
           self.navigationItem.backBarButtonItem = leftItem
       
       }
    @objc func dissmmisButtonTapped() {
        self.dismiss(animated: true, completion: nil)
    }
    func showAlert(title : String?, msg : String, okButtonTitle: String = "OK", style: UIAlertController.Style = .alert) {
        let ac = UIAlertController.init(title: title, message: msg, preferredStyle: style)
        ac.addAction(UIAlertAction.init(title: okButtonTitle, style: .default, handler: nil))
        DispatchQueue.main.async {
            self.present(ac, animated: true, completion: nil)
        }
    }
    
    func showFullScreenMessageOnView(parentView:UIView, withMessage message:String) -> FullScreenMessageView {
        let fullScreenMessageView = UINib(nibName:SCAppConstants.nibNames.fullScreenMessageView, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! FullScreenMessageView
        fullScreenMessageView.setupPopup(parentView.frame, message: message)
        
        return fullScreenMessageView
    }
}

extension Reactive where Base: UIViewController {
    var detailNavigationTitle: Binder<String> {
        return Binder(self.base) { controller, title in
            controller.customizeNavigationItem(title: title, isDetail: true)
        }
    }
    
    var navigationTitle: Binder<String> {
        return Binder(self.base) { controller, title in
            controller.customizeNavigationItem(title: title, isDetail: false)
        }
    }
}



