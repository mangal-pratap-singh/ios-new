//
//  SCRegistrationViewController.swift
//  SmartCare
//
//  Created by Prashant Pawar on 27/08/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    class func rgba(red:CGFloat, _ green:CGFloat, _ blue:CGFloat, _ alpha:CGFloat) -> UIColor {
        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: alpha)
    }
    
    class func SCNavigationBarBackgroundColor() -> UIColor {
        //return UIColor(red: 30/255.0, green: 35/255.0, blue: 60/255.0, alpha:1)
         return UIColor(red: 24/255.0, green: 31/255.0, blue: 54/255.0, alpha:1)
    }
    
    class func SCTextFilePlaceHolderTextColor() -> UIColor {
        return UIColor(red: 240/255.0, green: 240/255.0, blue: 240/255.0, alpha:1)
    }
    
    class func SCDisableDependnetColor() -> UIColor {
        return UIColor(red: 252/255.0, green: 224/255.0, blue: 224/255.0, alpha:1)
    }
    
    class func SCRedColor() -> UIColor {
        return UIColor(red: 229/255.0, green: 83.0/255.0, blue: 88.0/255.0, alpha:1)
    }
    
    class func SCGreenColor() -> UIColor {
        return UIColor(red: 104.0/255.0, green: 186/255.0, blue: 105.0/255.0, alpha:1)
    }
    
    class func SCBlueColor() -> UIColor {
        return UIColor(red: 55.0/255.0, green: 177.0/255.0, blue: 245/255.0, alpha:1)  
    }
    
    class func SCBrownColor() -> UIColor {
        return UIColor(red: 113/255.0, green: 55.0/255.0, blue: 41.0/255.0, alpha:1)
    }
    
//    class func SCGreenColor() -> UIColor {
//        return UIColor(red: 34.0/255.0, green: 139.0/255.0, blue: 34.0/255.0, alpha:1)
//    }
    
    class func SCYellowColor() -> UIColor {
        return UIColor(red: 251/255.0, green: 199.0/255.0, blue: 81.0/255.0, alpha:1)
    }
    
    class func SCButtonBackground() -> UIColor {
        return UIColor(red: 42/255.0, green: 196.0/255.0, blue: 223.0/255.0, alpha:1)
    }
    
    class func SCCollectionSelectTextCellColor()->UIColor{
       // return UIColor(red: 255.0/255.0, green: 59.0/255.0, blue: 48.0/255.0, alpha:1)
       // return UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 139.0/255.0, alpha:1)
         return UIColor(red: 24/255.0, green: 31/255.0, blue: 54/255.0, alpha:1)
     }
    
    class func SCCollectionTextCellColor()->UIColor{
         return UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha:1)
        
        
    }
    
    class func SCChatCellColor()->UIColor{
         return UIColor(red: 64.0/255.0, green: 78.0/255.0, blue: 112.0/255.0, alpha:1)
        
        
    }
    
    

    
}


