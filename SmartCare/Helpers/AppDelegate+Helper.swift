//
//  AppDelegate+Helper.swift
//  SmartCare
//
//  Created by Prashant Pawar on 24/08/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit
extension AppDelegate {
    func customizeNavigationBar() {
        //UIApplication.shared.isStatusBarHidden = false
       UINavigationBar.appearance().barTintColor = UIColor.SCNavigationBarBackgroundColor()
       
       // UINavigationBar.appearance().barTintColor = UIColor.systemGreen
        UINavigationBar.appearance().tintColor = UIColor.white
        
       // UIApplication.shared.isStatusBarHidden = false
        
       // UINavigationBar.appearance().tintColor = UIColor.white
    UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: SCAppConstants.applicationFontFamily, size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    func configureAPIRequest(){
        SCPreRequestService.configurationAPI(onComplete: { (success) in
        }) { (errorMessage) in
            print("Error in Configure API -- \(errorMessage)")
        }
    }
    
    func checkIfUserLoggedInAndShowNextScreen() {
        if let success = SCSessionManager.sharedInstance.isUserLoggedIn(), success == true {
            SCRoutingManager.sharedInstance.showHomeDashboard()
        } else {
            SCRoutingManager.sharedInstance.navigateToMenuOption(routingOption: .login)
        }
    }
}
