//
//  UIImageView+helper.swift
//  SmartCare
//
//  Created by synerzip on 15/10/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {

    //Compress Image
    func compressImage() -> UIImage {
        let imageData = self.jpegData(compressionQuality: 0.025)
        if let data = imageData {
            if let image = UIImage(data: data){
                return image
            }
        }
        return self
    }
}
