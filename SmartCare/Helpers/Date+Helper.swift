//
//  Date+Helper.swift
//  SmartCare
//
//  Created by Saleel Karkhanis on 04/09/18.
//  Copyright © 2018 Rediflex Consulting Pvt Ltd. All rights reserved.
//

import UIKit

extension Date {

    func toDateString(withFormatter formatter: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatter
        return dateFormatter.string(from: self)
    }
    
    func toStringDate(dateString: String,formatter: String,outPutFormatter: String) -> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatter
        //dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
         dateFormatter.dateFormat = formatter
        guard let date = dateFormatter.date(from: dateString) else { return "" }
        dateFormatter.dateFormat = outPutFormatter
        let dateStringTmp = dateFormatter.string(from: date)
        return dateStringTmp
        
        
    }
    
    
    
    
    func toTimeString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: self)
    }
    
    var startOfWeek: Date {
        return Calendar(identifier: .iso8601).date(from: Calendar(identifier: .iso8601).dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))!
    }
    
    var endOfWeek: Date {
        return Calendar.current.date(byAdding: .second, value: 604799, to: self.startOfWeek)!
    }
    
    var startOfMonth: Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    var endOfMonth: Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth)!
    }
    
    var currentMonthAndYear: String {
        let df = DateFormatter()
        df.setLocalizedDateFormatFromTemplate("MMM YYYY")
        return df.string(from: self)
    }
    
    var currentMontAndTime: String{
        let cdf = DateFormatter()
        cdf.dateFormat = "MM-dd-yyyy h:mm a"
        let dateStr = cdf.string(from: self)
        return dateStr
    }
    
    var currentDateTime: String{
        let cdf = DateFormatter()
        cdf.dateFormat = "yyyy-MM-dd'T'HH:mm:SSZ"
        let dateStr = cdf.string(from: self)
        return dateStr
    }
    
}
